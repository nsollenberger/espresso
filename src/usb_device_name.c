#include "usb_names.h"

#define DEVICE_NAME                                                            \
  { 'E', 's', 'p', 'r', 'e', 's', 's', 'o' }
#define DEVICE_NAME_LEN 8

#define MANUFACTURER_NAME                                                      \
  {                                                                            \
    'D', 'I', 'Y'                                                              \
  }
#define MANUFACTURER_NAME_LEN 3

struct usb_string_descriptor_struct usb_string_product_name = {
    2 + DEVICE_NAME_LEN * 2, 3, DEVICE_NAME};

struct usb_string_descriptor_struct usb_string_manufacturer_name =
    {2 + MANUFACTURER_NAME_LEN * 2, 3, MANUFACTURER_NAME};