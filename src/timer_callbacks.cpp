#include "timer_callbacks.h"
#include <events.h>
#include <usb_host_service.h>

extern Events events;

Timers::softTimer_t sleepTimer;
Timers::softTimer_t refreshTimer;
Timers::softTimer_t keyTimer;
Timers::softTimer_t cvTimer;
Timers::softTimer_t adcTimer;
Timers::softTimer_t clockTimer;

bool cvTimer_callback(void *o) {
  Events::event_t e = {kEventDACRefresh, 0};
  return events.postFromISR(e);
}

bool sleepTimer_callback(void *o) {
  Events::event_t e = {kEventScreenSleep, 0};
  return events.postFromISR(e);
}

bool refreshTimer_callback(void *o) {
  Events::event_t e = {kEventScreenRefresh, 0};
  return events.postFromISR(e);
}

bool keyTimer_callback(void *o) {
  Events::event_t e = {kEventKeyTimer, 0};
  return events.postFromISR(e);
}

bool adcTimer_callback(void *o) {
  Events::event_t e = {kEventPollADC, 0};
  return events.postFromISR(e);
}

bool clockTimer_callback(void *o) {
  Events::event_t e = {kEventClock, 0};
  return events.postFromISR(e);
}
