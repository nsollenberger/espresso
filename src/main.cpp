#include <chrono>
#include <thread>

#include <clock.h>
#include <events.h>
#include <iostream>
#include <log.h>
#include <midi_service.h>
#include <models/settings.h>
#include <models/voice_settings.h>
#include <espresso/parser.h>
#include <espresso/command.h>
#include <espresso/validator.h>
#include <teletype.h>
#include <timers.h>
#include <usb_host_service.h>
#include <voice.h>
#include <sequence.h>

#include "timer_callbacks.h"

Models::VoiceSettings voiceSettings;
Voice voices[VOICE_COUNT] = {
  {voiceSettings}, {voiceSettings}, {voiceSettings}, {voiceSettings},
  {voiceSettings}, {voiceSettings}, {voiceSettings}, {voiceSettings}
};

Scene scene_state(voices);
Teletype tt(scene_state);

Events events;
Timers timers;
Models::Settings settings;

UsbHostService usbHost(events);
MidiService midiService(usbHost);
Clock<Teletype, MidiService> voiceClock(tt, midiService, settings);

static void handle_event(Events::event_t e);
static void simulate_livemode();

int main() {
  timers.initAndRun();
  timers.add(clockTimer, 1, clockTimer_callback, nullptr);

  for (int i = 0; i < VOICE_COUNT; i++) {
    auto &voice = voices[i];
    voice.hardReset();
    voiceClock.setVoice(voice, i);
  }

  usbMIDI.initNative();
  scene_state.initializing = false;

  Events::event_t e;
  LOG("// Espresso ///////////////////////////////////////////////\n");

  std::thread t(simulate_livemode);
  t.detach();

  while (true) {
    midiService.read();
    events.getNext(e);
    handle_event(e);
    std::this_thread::sleep_for(std::chrono::microseconds(100));
  }
}

void handle_event(Events::event_t e) {
  switch (e.type) {
  case kEventClock:
    voiceClock.tick(timers.getTicks());
    break;
  default:
    break;
  }
}

void simulate_livemode() {
  while (true) {
    std::string cmd_string;
    getline(std::cin, cmd_string);

    char error_msg[PARSE_ERROR_STR_LENGTH];
    Espresso::Command command;
    command.comment = false;

    auto status = Espresso::parser(cmd_string.c_str(), command, error_msg);
    if (status != Espresso::E_OK) {
      std::cout << "Parse error: " << Espresso::parse_error_str(status) << ":" << error_msg << "\n";
      continue;
    }
    status = Espresso::validate(&command, error_msg);
    if (status != Espresso::E_OK) {
      std::cout << "Validation error: " << Espresso::parse_error_str(status) << ":" << error_msg << "\n";
      continue;
    }

    scene_state.ss_clear_script(TEMP_SCRIPT);
    scene_state.ss_overwrite_script_command(TEMP_SCRIPT, 0, &command);
    ExecState es;
    es.push();

    auto output = tt.run_script(scene_state, es, TEMP_SCRIPT);

    if (output.has_value) {
      char s[36];
      Util::itoa(output.value, s, 10);
      std::cout << "> " << s << "\n";
    } else {
      std::cout << ">\n";
    }
  }
}

/**
 *  TELETYPE_IO
 */

void tele_run_script(Scene *ss, ExecState *es,
                                     script_number_t script_no) {
  tt.run_script(*ss, *es, script_no);
}

process_result_t tele_process_command(Scene *ss, ExecState *es,
                                      const Espresso::Command *cmd) {
  return tt.process_command(*ss, *es, cmd);
}

uint32_t tele_get_ticks() { return timers.getTicks(); }

void tele_metro_updated() {}

void tele_metro_reset() {}

void tele_tr(uint8_t i, int16_t v) {}
void tele_cv(uint8_t i, int16_t v, uint8_t s) {}
void tele_cv_slew(uint8_t i, int16_t v) {}
void tele_cv_off(uint8_t i, int16_t v) {}
void tele_env_config(uint8_t i, uint16_t attack, uint16_t decay, uint16_t sustain, uint16_t release) {}
void tele_env(uint8_t i, bool high) {}
void tele_update_adc(bool force) {}
void tele_scene(uint8_t sceneIndex, bool loadPattern) {}
void tele_kill() {}
bool tele_get_input_state(uint8_t n) { return 0; }
void tele_vars_updated() {}
void tele_save_calibration() {}
void tele_has_stack(bool has_stack) {}
void device_flip() {}

void send_midi_note(uint8_t ch, uint8_t note, uint8_t vel) {
  midiService.sendNoteOn(ch, note, vel);
}

void send_midi_cc(uint8_t ch, uint8_t cc_num, uint8_t cc_val) {
  midiService.sendControlChange(ch, cc_num, cc_val);
}

void clock_reset_voice(int i) { voiceClock.resetVoice(i); }

void clock_refresh_voice(int i) { voiceClock.refreshVoice(i); }

void clock_refresh_seq(Sequence &seq) { voiceClock.refreshSequence(seq); }
