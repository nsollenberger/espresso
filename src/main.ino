#include "avr/pgmspace.h"
#include <Arduino.h>
#include <Bounce2.h>

#include <clock.h>
#include <config_store.h>
#include <controllers/front_button.h>
#include <controllers/hid_keyboard.h>
#include <controllers/midi.h>
#include <cstdint>
#include <events.h>
#include <flash.h>
#include <gpio.h>
#include <indicator_lights.h>
#include <line_editor.h>
#include <log.h>
#include <midi_service.h>
#include <models/adc_inputs.h>
#include <models/cv_outputs.h>
#include <models/edit_mode.h>
#include <models/help_mode.h>
#include <models/keyboard.h>
#include <models/mode_manager.h>
#include <models/preset_r_mode.h>
#include <models/preset_w_mode.h>
#include <models/settings.h>
#include <models/screensaver.h>
#include <models/voice_settings.h>
#include <region.h>
#include <scene.h>
#include <scene_store.h>
#include <screen.h>
#include <sequence.h>
#include <spi_config.h>
#include <teletype.h>
#include <timers.h>
#include <usb_host_service.h>
#include <views/editor.h>
#include <views/help.h>
#include <views/settings_1.h>
#include <views/preset_write.h>
#include <views/preset_read.h>
#include <views/voice_settings_1.h>
#include <voice.h>
#include <ws_leds.h>

#include "timer_callbacks.h"

device_config_t device_config;

Models::VoiceSettings voiceSettings;
Voice voices[VOICE_COUNT] = {
  {voiceSettings, TT_SCRIPT_1}, {voiceSettings, TT_SCRIPT_2}, {voiceSettings, TT_SCRIPT_3}, {voiceSettings, TT_SCRIPT_4},
  {voiceSettings, TT_SCRIPT_5}, {voiceSettings, TT_SCRIPT_6}, {voiceSettings, TT_SCRIPT_7}, {voiceSettings, TT_SCRIPT_8}
};

Scene scene_state(voices);
Teletype tt;

scene_text_t scene_text;
SceneStore sceneStore(scene_state, scene_text);
ConfigStore configStore;
Models::Settings settings;

Events events;
Timers timers;
UsbHostService usbHost(events);
MidiService midiService(usbHost);
Clock<Teletype, MidiService> voiceClock(tt, scene_state, midiService, settings);

Bounce button;
WSLEDStrip ledStrip;
IndicatorLights indicators(ledStrip);
screen_buffer_t screen_buf = {{0, 0},  {0, 8},  {0, 16}, {0, 24},
                              {0, 32}, {0, 40}, {0, 48}, {0, 56}};

Screen screen(screen_buf);
LineEditor le;

Models::EditMode editMode(scene_state, le);
Models::HelpMode helpMode(le);
Models::PresetRMode presetRMode(tt, scene_state, sceneStore, configStore);
Models::PresetWMode presetWMode(presetRMode, sceneStore, configStore, scene_text, le);

Models::ModeManager modeManager(settings, voiceSettings, editMode, helpMode, presetRMode,
                                presetWMode);
Models::CvOutputs<Teletype, IndicatorLights> cvOutputs(tt, device_config, indicators);
Models::Screensaver screensaver;
Models::Keyboard kbd;
Models::AdcInputs adcInputs(scene_state, modeManager, presetRMode,
                            screensaver, timers);
Views::Settings_1 settings_1View(settings);
Views::Editor editorView(editMode);
Views::VoiceSettings_1 voiceSettings_1View(voiceSettings);
Views::Help helpView(helpMode);
Views::PresetWrite presetWriteView(presetWMode);
Views::PresetRead presetReadView(presetRMode);
Controllers::HidKeyboard hidKeyboard(voices, editMode, screensaver,
                                     modeManager, cvOutputs, kbd, settings, voiceSettings);
Controllers::Midi<Teletype, MidiService> midiController(tt, midiService);
Controllers::FrontButton frontButton(adcInputs, modeManager, presetRMode,
                                     screensaver);

FLASHMEM void update_device_config(uint8_t refresh);
static void gate_isr_handler(int);
static void handle_event(Events::event_t e);

void setup() {
  LOG("booting......\n");

  settings.setModeManager(modeManager);
  presetRMode.setModeManager(modeManager);
  presetWMode.setModeManager(modeManager);
  editMode.setModeManager(modeManager);
  voiceSettings.setModeManager(modeManager);

  Gpio::initPins();
  Spi::initPort();
  randomSeed(Gpio::readAdc(UNUSED_AIN));
  // Reset after seeding random
  scene_state.hardReset();
  screen.init();
  timers.initAndRun();
  ledStrip.setBrightness(10);
  button.attach(BUTTON_PIN);
  Gpio::initInterrupts(gate_isr_handler);
  Flash::prepareFresh(sceneStore, configStore, button, timers, screen,
                      indicators);
  usbHost.start();

  // Restore device config
  configStore.flash_get_device_config(&device_config);
  update_device_config(0);

  // Restore calibration
  configStore.flash_get_cal(&scene_state.cal);
  scene_state.ss_update_param_scale();
  scene_state.ss_update_in_scale();

  // Restore last used scene
  presetRMode.preset_select = configStore.flash_last_saved_scene();
  scene_state.ss_set_scene(presetRMode.preset_select);
  sceneStore.load(presetRMode.preset_select);

  // In case the init script uses IN or PARAM
  adcInputs.sampleInputs(true);

  for (int i = 0; i < VOICE_COUNT; i++) {
    auto &voice = voices[i];
    voice.hardReset();
    voiceClock.setVoice(voice, i);
  }

  midiController.registerHandlers();

  modeManager.activateMode(M_SETTINGS_1);
  tt.run_script(scene_state, INIT_SCRIPT);
  scene_state.initializing = false;

  timers.add(sleepTimer, SLEEP_CHECK_MS, sleepTimer_callback, nullptr);
  timers.add(cvTimer, RATE_CV, cvTimer_callback, nullptr);
  timers.add(keyTimer, 71, keyTimer_callback, nullptr);
  timers.add(adcTimer, 61, adcTimer_callback, nullptr);
  timers.add(refreshTimer, 63, refreshTimer_callback, nullptr);
  timers.add(clockTimer, 1, clockTimer_callback, nullptr);
}

void loop() {
  Events::event_t e;
  LOG("// Espresso ///////////////////////////////////////////////\n");
  while (true) {
    usbHost.task();
    midiService.read();
    events.getNext(e);
    handle_event(e);
    button.update();
    if (button.changed()) {
      frontButton.handleButtonPress(button.read());
    }
  }
}

void update_device_config(uint8_t refresh) {
  screen.setDirection(device_config.flip);
  if (refresh)
    modeManager.activateMode(modeManager.mode);
  configStore.flash_update_device_config(&device_config);
}

void gate_isr_handler(int gpio_index) {
  Events::event_t e = {kEventTrigger, gpio_index};
  events.postFromISR(e);
}

static void updateScreen() {
  switch (static_cast<int8_t>(modeManager.mode)) {
  case M_SETTINGS_1:
    settings_1View.render(screen);
    break;
  case M_PRESET_W:
    presetWriteView.render(screen);
    break;
  case M_PRESET_R:
    presetReadView.render(screen);
    break;
  case M_HELP:
    helpView.render(screen);
    break;
  case M_EDIT:
    editorView.render(screen);
    break;
  case M_VOICE_SETTINGS_1:
    voiceSettings_1View.render(screen);
    break;
  }
}

void handle_event(Events::event_t e) {
  switch (e.type) {
  case kEventClock:
    voiceClock.tick(millis());
    break;
  case kEventNone:
    break;
  case kEventDACRefresh:
    cvOutputs.refresh();
    break;
  case kEventTrigger:
    // @nate - Sync In/Reset/other?
    break;
  case kEventScreenSleep:
    if (!screensaver.active() && screensaver.incrementInactive()) {
      screen.clearScreen();
    }
    break;
  case kEventScreenRefresh:
    if (!screensaver.active()) {
      updateScreen();
    }
    break;
  case kEventPollADC:
    adcInputs.pollInputs();
    break;
  case kEventKeyDown: {
    uint8_t keycode = EventQueue::eventKeycode(e.data);
    uint8_t modifiers = EventQueue::eventModifiers(e.data);
    if (kbd.keyDown(keycode, modifiers)) {
      hidKeyboard.processKeypress(keycode, modifiers, false);
    }
    break;
  }
  case kEventKeyUp:
    kbd.keyUp(EventQueue::eventKeycode(e.data), EventQueue::eventModifiers(e.data));
    break;
  case kEventKeyTimer:
    frontButton.checkHeldButton();
    hidKeyboard.checkHeldKeys();
    break;
  }
}
