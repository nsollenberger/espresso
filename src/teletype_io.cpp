#include <clock.h>
#include <controllers/midi.h>
#include <config_store.h>
#include <gpio.h>
#include <indicator_lights.h>
#include <midi_service.h>
#include <models/adc_inputs.h>
#include <models/cv_outputs.h>
#include <models/preset_r_mode.h>
#include <scene.h>
#include <scene_store.h>
#include <sequence.h>
#include <teletype.h>
#include <teletype_io.h>
#include <timers.h>

#include "espresso/exec_state.h"
#include "timer_callbacks.h"

/** Defined in Main **/
extern device_config_t device_config;
extern Scene scene_state;
extern Teletype tt;

extern SceneStore sceneStore;
extern ConfigStore configStore;
extern Timers timers;
extern MidiService midiService;
extern Clock<Teletype, MidiService> voiceClock;
extern IndicatorLights indicators;

extern Models::AdcInputs adcInputs;
extern Models::CvOutputs<Teletype, IndicatorLights> cvOutputs;
extern Models::PresetRMode presetRMode;

using Espresso::Command;
using Espresso::ExecState;

void update_device_config(uint8_t refresh);

/** End Main **/

void tele_run_script(Scene *ss, ExecState *es,
                                     script_number_t script_no) {
  tt.run_script(*ss, *es, script_no);
}

process_result_t tele_process_command(Scene *ss, ExecState *es,
                                      const Command *cmd) {
  return tt.process_command(*ss, *es, *cmd);
}

uint32_t tele_get_ticks() { return timers.getTicks(); }

// @deprecated (use Settings for tempo)
void tele_metro_updated() {}

void tele_metro_reset() {}

void tele_tr(uint8_t i, int16_t v) {
  Gpio::setLevel(triggerOutPins[device_config.flip ? 3 - i : i], v ? 0 : 1);
  indicators.setTrigger(i, !!v);
}

void tele_cv(uint8_t i, int16_t v, uint8_t s) {
  cvOutputs.setValue(i, v, s);
  timers.immediate(cvTimer);
}

void tele_cv_slew(uint8_t i, int16_t v) { cvOutputs.setSlewDuration(i, v); }

void tele_cv_off(uint8_t i, int16_t v) { cvOutputs.setOffset(i, v); }

void tele_env_config(uint8_t i, uint16_t attack, uint16_t decay, uint16_t sustain, uint16_t release) {
  if (attack == 0 && decay == 0 && sustain == 0 && release == 0) {
    cvOutputs.disableEnvelope(i);
  } else {
    cvOutputs.enableEnvelope(i, attack, decay, sustain, release);
  }
}

void tele_env(uint8_t i, bool high) {
  cvOutputs.envelopeGate(i, high);
}

void tele_update_adc(bool force) { adcInputs.sampleInputs(force); }

void tele_scene(uint8_t sceneIndex, bool loadPattern) {
  if (sceneIndex >= SCENE_SLOTS)
    return;
  presetRMode.preset_select = sceneIndex;
  sceneStore.load(sceneIndex, loadPattern);
}

void tele_kill() {
  for (int i = 0; i < 4; i++) {
    cvOutputs.setStep(i, 1);
    tele_tr(i, 0);
  }
}

bool tele_get_input_state(uint8_t n) {
  return Gpio::getLevel(triggerInPins[n]) > 0;
}

void tele_vars_updated() {}

void tele_save_calibration() { configStore.flash_update_cal(&scene_state.cal); }

void tele_has_stack(bool has_stack) {}

void device_flip() {
  device_config.flip = !device_config.flip;
  update_device_config(1);
}

void send_midi_note(uint8_t ch, uint8_t note, uint8_t vel) {
  // MidiService handles vel=0 as noteOff
  midiService.sendNoteOn(ch, note, vel);
}

void send_midi_cc(uint8_t ch, uint8_t cc_num, uint8_t cc_val) {
  midiService.sendControlChange(ch, cc_num, cc_val);
}

void clock_reset_voice(int i) {
  voiceClock.resetVoice(i);
}

void clock_refresh_voice(int i) {
  voiceClock.refreshVoice(i);
}

void clock_refresh_seq(Sequence &seq) {
  voiceClock.refreshSequence(seq);
}
