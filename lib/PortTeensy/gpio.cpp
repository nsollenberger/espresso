#include "gpio.h"

input_pin_t triggerInPins[] = {TRIG_IN1, TRIG_IN2, TRIG_IN3, TRIG_IN4, TRIG_IN5, TRIG_IN6, TRIG_IN7, TRIG_IN8};
output_pin_t triggerOutPins[] = {TRIG_OUT1, TRIG_OUT2, TRIG_OUT3, TRIG_OUT4};

ADC *Gpio::adc = new ADC();

void (*Gpio::gate_cb)(int) = nullptr;

void Gpio::initPins(void) {
  pinMode(TRIG_IN1, INPUT);
  pinMode(TRIG_IN2, INPUT);
  pinMode(TRIG_IN3, INPUT);
  pinMode(TRIG_IN4, INPUT);
  pinMode(TRIG_IN5, INPUT);
  pinMode(TRIG_IN6, INPUT);
  pinMode(TRIG_IN7, INPUT);
  pinMode(TRIG_IN8, INPUT);
  pinMode(CV_IN1, INPUT);
  pinMode(CV_IN2, INPUT);
  pinMode(PARAM, INPUT);
  pinMode(BUTTON_PIN, INPUT_PULLUP);

  pinMode(TRIG_OUT1, OUTPUT);
  pinMode(TRIG_OUT2, OUTPUT);
  pinMode(TRIG_OUT3, OUTPUT);
  pinMode(TRIG_OUT4, OUTPUT);
  // Output is inverted
  setLevel(TRIG_OUT1, HIGH);
  setLevel(TRIG_OUT2, HIGH);
  setLevel(TRIG_OUT3, HIGH);
  setLevel(TRIG_OUT4, HIGH);

  pinMode(SPI_CS_OLED_PIN, OUTPUT);
  pinMode(SPI_CS_DAC1_PIN, OUTPUT);
  pinMode(SPI_CS_DAC2_PIN, OUTPUT);
  pinMode(OLED_DC_PIN, OUTPUT);
  pinMode(OLED_RES_PIN, OUTPUT);

  adc->adc0->setAveraging(32);
  adc->adc0->setResolution(16);
  adc->adc0->setConversionSpeed(ADC_CONVERSION_SPEED::HIGH_SPEED);
  adc->adc0->setSamplingSpeed(ADC_SAMPLING_SPEED::HIGH_SPEED);

  adc->adc1->setAveraging(32);
  adc->adc1->setResolution(16);
  adc->adc1->setConversionSpeed(ADC_CONVERSION_SPEED::HIGH_SPEED);
  adc->adc1->setSamplingSpeed(ADC_SAMPLING_SPEED::HIGH_SPEED);
}

void Gpio::initInterrupts(void _gate_cb(int)) {
  gate_cb = _gate_cb;
  attachInterrupt(TRIG_IN1, trigger1, CHANGE);
  attachInterrupt(TRIG_IN2, trigger2, CHANGE);
  attachInterrupt(TRIG_IN3, trigger3, CHANGE);
  attachInterrupt(TRIG_IN4, trigger4, CHANGE);
  attachInterrupt(TRIG_IN5, trigger5, CHANGE);
  attachInterrupt(TRIG_IN6, trigger6, CHANGE);
  attachInterrupt(TRIG_IN7, trigger7, CHANGE);
  attachInterrupt(TRIG_IN8, trigger8, CHANGE);
}