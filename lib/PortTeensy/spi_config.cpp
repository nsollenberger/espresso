#include "spi_config.h"

// Used for DACs and OLED
SPISettings Spi::settings(20000000, MSBFIRST, SPI_MODE0);

EventResponder Spi::callbackHandler;
volatile std::atomic<bool> Spi::spi_active;
int Spi::cs;
