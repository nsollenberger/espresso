#include "screen.h"

#include <Arduino.h>

#include "gpio.h"
#include "portmacro.h"
#include "spibus.h"

DMAMEM uint8_t Screen::tx_buffer[8192] __attribute__((aligned(32)));

void Screen::init(void) {
  {
    Gpio::setLevel(OLED_RES_PIN, 1);
    delay(1);
    Gpio::setLevel(OLED_RES_PIN, 0);
    delay(1);
    Gpio::setLevel(OLED_RES_PIN, 1);
    delay(10);

    SpiBus spiBus(Spi::oled_dev);
    writeCommand1Byte(spiBus, eSleepModeOn);
    writeCommand2Bytes(spiBus, eClockFreqDiv, 0x91);
    writeCommand2Bytes(spiBus, eSetMuxRatio, 0x3F);
    writeCommand2Bytes(spiBus, eSetDisplayOffset, 0x00);
    writeCommand2Bytes(spiBus, eSelectVddRegulator, 0x01);
    writeCommand3Bytes(spiBus, eSetRemap, 0x16, 0x11);
    writeCommand2Bytes(spiBus, eSetMasterConstrast, 0x0F);
    writeCommand2Bytes(spiBus, eSetContrast, 0x9F);
    writeCommand2Bytes(spiBus, eSetPhaseLength, 0xF2);
    writeCommand2Bytes(spiBus, eSetPrechargeVoltage, 0x1F);
    writeCommand3Bytes(spiBus, eSetVslAndQuality, 0xA0, 0xFD);
    writeCommand2Bytes(spiBus, eSetVcomHVoltage, 0x04);
    writeCommand1Byte(spiBus, eSetModeNormal);
    writeCommand1Byte(spiBus, eSleepModeOff);
    clearScreen(spiBus);
  }
  delay(10);
}

void Screen::draw() {
  int startAt = -1;
  int endAt = -1;

  for (int i = 0; i < kRegionRows; i++) {
    if (regions[i].dirty) {
      startAt = i;
      endAt = i;
      break;
    }
  }
  if (startAt == -1)
    return;
  
  for (int i = kRegionRows - 1; i > startAt; i--) {
    if (regions[i].dirty) {
      endAt = i;
      break;
    }
  }
  int rows = 1 + endAt - startAt;
  // LOG("draw: %d - %d\t(%d)\n", startAt, endAt, rows);

  auto y = regions[startAt].y;
  auto h = regions[startAt].h * rows;
  column_units x_c = regions[startAt].x / 2;
  column_units w_c = regions[startAt].w / 2;

  tx_buffer_count = 0;
  if (is_screen_flipped) {
    for (int i = startAt; i < (startAt + rows); i++) {
      fillTxBuffer(regions[i]);
    }
  } else {
    for (int i = (startAt + rows - 1); i >= startAt; i--) {
      fillTxBuffer(regions[i]);
    }
    y = kBufferRowSize - y - h;
    x_c = kBufferColSize - x_c - w_c;
  }

  writeScreenBufferAsync(x_c, y, w_c, h);

  for (int i = 0; i < kRegionRows; i++) {
    regions[i].dirty = false;
  }
}

void Screen::fillTxBuffer(Region &reg) {
  if (is_screen_flipped) {
    for (int i = 0; i < reg.len; i++) {
      tx_buffer[tx_buffer_count] = *(reg.data + i);
      tx_buffer_count++;
    }
  } else {
    for (int i = reg.len - 1; i >= 0; i--) {
      tx_buffer[tx_buffer_count] = *(reg.data + i);
      tx_buffer_count++;
    }
  }
}

void Screen::clearScreen(void) {
  SpiBus spiBus(Spi::oled_dev);
  clearScreen(spiBus);
}

void Screen::writeScreenBufferAsync(column_units x, row_units y, column_units w,
                               row_units h) {
  SpiBus spiBus(Spi::oled_dev);

  setRect(spiBus, x, y, w, h);
  writeCommand1Byte(spiBus, eWriteRam);
  setDC(1);
  spiBus.sendMessageAsync(tx_buffer, tx_buffer_count);
}

// 0-63 (l-r), 2 pixels per column
constexpr column_units kDisplayColSize = 64;
// 1 pixel per row, 0-63 (t-b)
constexpr row_units kDisplayRowSize = 64;

void Screen::clearScreen(SpiBus &spiBus) {
  setRect(spiBus, 0, 0, kDisplayColSize, kDisplayRowSize);
  writeCommand1Byte(spiBus, eWriteRam);

  spi_message_t data_conf = {
    .bytes = 2,
  };
  setDC(1);
  size_t write_len = kDisplayColSize * kDisplayRowSize;
  for (size_t i = 0; i < write_len; i++) {
    const bool keepActive = i < write_len - 1;
    screenBuf[i] = 0;
    spiBus.sendMessage(data_conf, keepActive);
  }
}

void Screen::setRect(SpiBus &spiBus, column_units x, row_units y,
                     column_units w, row_units h) {
  // col indexes are offset, 28-91 (l-r)
  const column_units &startCol = 28 + x;
  const column_units &endCol = startCol + w - 1;
  const row_units &startRow = y;
  const row_units &endRow = startRow + h - 1;

  spi_message_t command_conf = {
    .bytes = 1,
  };
  spi_message_t data_conf = {
    .bytes = 2,
  };

  // set column address
  setDC(0);
  command_conf.tx_data[0] = 0x15;
  spiBus.sendMessage(command_conf);
  setDC(1);
  data_conf.tx_data[0] = startCol;
  data_conf.tx_data[1] = endCol;
  spiBus.sendMessage(data_conf);

  // set row address
  setDC(0);
  command_conf.tx_data[0] = 0x75;
  spiBus.sendMessage(command_conf);
  setDC(1);
  data_conf.tx_data[0] = startRow;
  data_conf.tx_data[1] = endRow;
  spiBus.sendMessage(data_conf);
}

void Screen::writeCommand1Byte(SpiBus &spiBus, eCommand1Byte cmd) {
  spi_message_t command_conf = {
    .bytes = 1,
  };
  setDC(0);
  command_conf.tx_data[0] = cmd;
  spiBus.sendMessage(command_conf);
}

void Screen::writeCommand2Bytes(SpiBus &spiBus, eCommand2Bytes cmd,
                                uint8_t data1) {
  spi_message_t command_conf = {
    .bytes = 1,
  };
  setDC(0);
  command_conf.tx_data[0] = cmd;
  spiBus.sendMessage(command_conf);

  spi_message_t data_conf = {
    .bytes = 1,
  };
  setDC(1);
  data_conf.tx_data[0] = data1;
  spiBus.sendMessage(data_conf);
}

void Screen::writeCommand3Bytes(SpiBus &spiBus, eCommand3Bytes cmd,
                                uint8_t data1, uint8_t data2) {
  spi_message_t command_conf = {
    .bytes = 1,
  };
  setDC(0);
  command_conf.tx_data[0] = cmd;
  spiBus.sendMessage(command_conf);

  spi_message_t data_conf = {
    .bytes = 2,
  };
  setDC(1);
  data_conf.tx_data[0] = data1;
  data_conf.tx_data[1] = data2;
  spiBus.sendMessage(data_conf);
}
