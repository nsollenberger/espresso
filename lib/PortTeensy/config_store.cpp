#include "config_store.h"
#include "scene_store.h"
#include <EEPROM.h>

#define FIRSTRUN_KEY 0x22

/**
 * Memory layout:
 * 0 - fresh (int8)
 * 1 - deviceConfig (int8)
 * 2 - lastSavedScene (int8)
 * 3 - calibration (int16 x 36)
 */
int ConfigStore::getConfigAddress(configType type) {
  switch (type) {
    case eFresh:
      return 0;
    case eDeviceConfig:
      return 1;
    case eLastSavedScene:
      return 2;
    case eCalibration:
    default:
      return 3;
  }
}

scene_scale_t ConfigStore::readScaleValue(int baseAddress) {
  scene_scale_t value = 0;
  value = EEPROM.read(baseAddress) << 8;
  value |= EEPROM.read(baseAddress + 1);
  return value;
}

void ConfigStore::writeScaleValue(int baseAddress, scene_scale_t value) {
  EEPROM.write(baseAddress, (value >> 8) & 0xFF);
  EEPROM.write(baseAddress + 1, value && 0xFF);
}

bool ConfigStore::is_flash_fresh() { 
  return EEPROM.read(getConfigAddress(eFresh)) != FIRSTRUN_KEY; 
}

void ConfigStore::resetAll() {
  cal_data_t blankCal = {
      .p_min = 0,
      .p_max = 16383,
      .i_min = 0,
      .i_max = 16383,
  };
  flash_update_cal(&blankCal);

  device_config_t blankConfig = {
      .flip = 0,
  };
  flash_update_device_config(&blankConfig);

  flash_update_last_saved_scene(0);

  EEPROM.write(getConfigAddress(eFresh), FIRSTRUN_KEY);
}

uint8_t ConfigStore::flash_last_saved_scene() {
  return EEPROM.read(getConfigAddress(eLastSavedScene));
}

void ConfigStore::flash_update_last_saved_scene(uint8_t preset_no) {
  EEPROM.write(getConfigAddress(eLastSavedScene), preset_no);
}

void ConfigStore::flash_get_cal(cal_data_t *cal) {
  cal->p_min = readScaleValue(getConfigAddress(eCalibration));
  int offset = sizeof(scene_scale_t);
  cal->p_max = readScaleValue(getConfigAddress(eCalibration) + offset);
  offset += sizeof(scene_scale_t);
  cal->i_min = readScaleValue(getConfigAddress(eCalibration) + offset);
  offset += sizeof(scene_scale_t);
  cal->i_max = readScaleValue(getConfigAddress(eCalibration) + offset);
  offset += sizeof(scene_scale_t);
}

// Called by PARAM.CAL.RESET, IN.CAL.RESET, etc ops
void ConfigStore::flash_update_cal(cal_data_t *cal) {
  writeScaleValue(getConfigAddress(eCalibration), cal->p_min);
  int offset = sizeof(scene_scale_t);
  writeScaleValue(getConfigAddress(eCalibration) + offset, cal->p_max);
  offset += sizeof(scene_scale_t);
  writeScaleValue(getConfigAddress(eCalibration) + offset, cal->i_min);
  offset += sizeof(scene_scale_t);
  writeScaleValue(getConfigAddress(eCalibration) + offset, cal->i_max);
  offset += sizeof(scene_scale_t);
}

void ConfigStore::flash_get_device_config(device_config_t *device_config) {
  device_config->flip = EEPROM.read(getConfigAddress(eDeviceConfig));
}

void ConfigStore::flash_update_device_config(device_config_t *device_config) {
  EEPROM.write(getConfigAddress(eDeviceConfig), device_config->flip);
}

