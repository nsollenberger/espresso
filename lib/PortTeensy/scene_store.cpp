#include <stdio.h>

//@nate!
#include <scene.h>

#include "log.h"
#include "scene_store.h"


SceneStore::SceneStore(Scene &_state, scene_text_t &_text)
    : scene_state(_state), scene_text(_text) {
  if (!myfs.begin(diskSizeBytes)) {
    LOG("Error starting LittleFS\n");
  }
}

/**
 * Scene binary file structure:
 * 1. version (scene_state_version_t)
 * 2. scripts (scene_script_t[])
 * 3. pattern data (scene_pattern_t[])
 * 4. scene text (scene_text_t)
 */
void SceneStore::load(uint8_t sceneIndex, bool loadPatterns) {
  if (sceneIndex >= SCENE_SLOTS)
    return;
  char filename[16];
  sprintf(filename, "s_%d.bin", sceneIndex);
  File scenefile = myfs.open(filename, FILE_WRITE_BEGIN);
  if (scenefile && scenefile.size()) {
    // Still on version 1, just skip past it for now
    scenefile.seek(sizeof(scene_state_version_t), SeekCur);
    scenefile.read(scene_state.ss_scripts_ptr(), file_scripts_size());
    scenefile.read(scene_text, sizeof(scene_text_t));
    scenefile.close();

    scene_state.resetTimestamps();
  } else {
    scene_state.hardReset();
  }
}

void SceneStore::loadText(uint8_t sceneIndex, scene_text_t &textOut) {
  memset(textOut, 0, sizeof(scene_text_t));
  if (sceneIndex >= SCENE_SLOTS)
    return;
  char filename[16];
  sprintf(filename, "s_%d.bin", sceneIndex);
  File scenefile = myfs.open(filename, FILE_READ);
  if (scenefile) {
    uint32_t text_offset = sizeof(scene_state_version_t) + file_scripts_size();
    scenefile.seek(text_offset, SeekSet);
    scenefile.read(&textOut, sizeof(scene_text_t));
    scenefile.close();
  }
}

void SceneStore::save(uint8_t sceneIndex) {
  if (sceneIndex >= SCENE_SLOTS)
    return;
  char filename[16];
  sprintf(filename, "s_%d.bin", sceneIndex);
  File scenefile = myfs.open(filename, FILE_WRITE_BEGIN);
  if (scenefile) {
    scenefile.write(&scene_state.version, sizeof(scene_state_version_t));
    scenefile.write(scene_state.ss_scripts_ptr(), file_scripts_size());
    scenefile.write(scene_text, sizeof(scene_text_t));
    scenefile.close();
  }
}

void SceneStore::clearAll(void) {
  scene_state.hardReset();
  memset(scene_text, 0, sizeof(scene_text_t));

  for (uint8_t i = 0; i < SCENE_SLOTS; i++) {
    save(i);
  }
}

size_t SceneStore::file_scripts_size(void) {
  // Exclude TEMP script by subtracting 1 from size
  return Scene::ss_scripts_size() - sizeof(scene_script_t);
}
