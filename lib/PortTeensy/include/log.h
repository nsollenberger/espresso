#ifndef _LOG_H__
#define _LOG_H__

#ifdef LOGGING_ENABLED
#include <Arduino.h>
#define LOG Serial.printf
#else
#define LOG(...)                                                               \
  {}
#endif // LOGGING_ENABLED

#endif