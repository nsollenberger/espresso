#ifndef _CONFIG_H__
#define _CONFIG_H__


constexpr int TRIG_IN1 = 6;
constexpr int TRIG_IN2 = 21;
constexpr int TRIG_IN3 = 23;
constexpr int TRIG_IN4 = 11;
constexpr int TRIG_IN5 = 9;
constexpr int TRIG_IN6 = 22;
constexpr int TRIG_IN7 = 10;
constexpr int TRIG_IN8 = 12;
constexpr int TRIG_OUT1 = 2;
constexpr int TRIG_OUT2 = 3;
constexpr int TRIG_OUT3 = 4;
constexpr int TRIG_OUT4 = 5;
constexpr int BUTTON_PIN = 13;

// Using SPI1
constexpr int SPI_SCK_PIN = 27;
constexpr int SPI_COPI_PIN = 26;
// Teensy4 seems to not allow re-assigning CIPO :/
constexpr int SPI_CIPO_PIN = 1;
constexpr int SPI_CS_OLED_PIN = 24;
constexpr int SPI_CS_DAC1_PIN = 25;
constexpr int SPI_CS_DAC2_PIN = 17;

constexpr int OLED_DC_PIN = 20;
constexpr int OLED_RES_PIN = 30;

// Using Serial1
constexpr int WS_LED_PIN = 8;

// Using I2C0
constexpr int I2C_SDA_PIN = 18;
constexpr int I2C_SCL_PIN = 19;

// Using Serial7
constexpr int UART_TX_PIN = 29;
constexpr int UART_RX_PIN = 28;

constexpr int CV_IN1 = 16;
constexpr int CV_IN2 = 14;
constexpr int PARAM = 15;
constexpr int UNUSED_AIN = 24;

#endif
