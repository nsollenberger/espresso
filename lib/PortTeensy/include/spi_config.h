#ifndef _SPI_CONFIG_H__
#define _SPI_CONFIG_H__

#include <atomic>
#include "avr/pgmspace.h"
#include <SPI.h>

#include "config.h"
#include "gpio.h"
#include "log.h"

struct spi_transaction_t {
  size_t bytes;
  char tx_data[4];
};

class Spi {
public:
  static FLASHMEM void initPort(void) {
    Gpio::setLevel(SPI_CS_DAC1_PIN, HIGH);
    Gpio::setLevel(SPI_CS_DAC2_PIN, HIGH);
    Gpio::setLevel(SPI_CS_OLED_PIN, HIGH);
    callbackHandler.attachImmediate(&callback);
    spi_active = false;
    SPI1.begin();
  }

  static bool sendMessagePolling(spi_transaction_t &message_conf) {
    if (spi_active.load()) {
      return false;
    }
    for (size_t i = 0; i < message_conf.bytes; i++) {
      SPI1.transfer(message_conf.tx_data[i]);
    }
    return true;
  }

  static bool sendMessageAsync(const void *txBuffer, size_t count, int cs_pin) {
    if (spi_active.load()) {
      return false;
    }
    spi_active = true;
    cs = cs_pin;
    Gpio::setLevel(cs, LOW);
    return SPI1.transfer(txBuffer, nullptr, count, callbackHandler);
  }

  static void callback(EventResponderRef eventResponder) {
    Gpio::setLevel(cs, HIGH);
    spi_active = false;
  }

  static void beginTransaction() { SPI1.beginTransaction(settings); }

  static void endTransaction() { SPI1.endTransaction(); }

  static SPISettings settings;
  static EventResponder callbackHandler;
  static int cs;
  static volatile std::atomic<bool> spi_active;

  enum SpiDevice {
    dac1_dev,
    dac2_dev,
    oled_dev,
  };
};

#endif
