#ifndef _TC_H__
#define _TC_H__

#include <TimerOne.h>

typedef void timer_isr_cb_t(void);

class Tc {
public:
  static void init(void) {
    Timer1.initialize(1000);
  }

  static void startTimer(timer_isr_cb_t &tickISR) {
    Timer1.attachInterrupt(tickISR);
  }
};

#endif