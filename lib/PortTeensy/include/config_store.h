#ifndef _CONFIG_STORE_H__
#define _CONFIG_STORE_H__

#include "avr/pgmspace.h"
#include <stdint.h>

#include <config_types.h>

struct device_config_t {
  uint8_t flip;
};

class ConfigStore {
public:
  FLASHMEM bool is_flash_fresh(void);
  FLASHMEM void resetAll(void);
  FLASHMEM uint8_t flash_last_saved_scene(void);
  FLASHMEM void flash_update_last_saved_scene(uint8_t preset_no);
  FLASHMEM void flash_update_cal(cal_data_t *);
  FLASHMEM void flash_get_cal(cal_data_t *);
  FLASHMEM void flash_update_device_config(device_config_t *);
  FLASHMEM void flash_get_device_config(device_config_t *);

private:
  uint8_t fresh{0};

  enum configType : int8_t {
    eFresh,
    eDeviceConfig,
    eLastSavedScene,
    eCalibration,
  };

  FLASHMEM int getConfigAddress(configType);
  FLASHMEM scene_scale_t readScaleValue(int baseAddress);
  FLASHMEM void writeScaleValue(int baseAddress, scene_scale_t value);
};

#endif
