#ifndef _WS_LED_H__
#define _WS_LED_H__

#include "avr/pgmspace.h"
#include <WS2812Serial.h>

#include "config.h"

constexpr int kNumLEDs = 9;

class WSLEDStrip {
public:
  WSLEDStrip()
      : leds(kNumLEDs, displayMemory, drawingMemory, WS_LED_PIN, WS2812_GRB) {
    leds.begin();
  }

  void setColor(uint16_t index, uint8_t red, uint8_t green, uint8_t blue) {
    leds.setPixelColor(index, red, green, blue);
  }

  // color ~= 0xRRGGBB
  void setColor(uint16_t index, int color) {
    leds.setPixel(index, color);
  }

  void flush() {
    leds.show();
  }

  FLASHMEM void setBrightness(uint8_t n) { leds.setBrightness(n); }

private:
  byte drawingMemory[kNumLEDs * 3];
  // Hacky: it seems member variable must be static to allow (DMAMEM) section
  // attribute We only need one instance of this class tho, so /meh
  static DMAMEM byte displayMemory[kNumLEDs * 12];
  WS2812Serial leds;
};

#endif
