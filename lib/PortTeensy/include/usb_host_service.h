#ifndef _USB_HOST_SERVICE_H__
#define _USB_HOST_SERVICE_H__

#include "avr/pgmspace.h"
#include <Arduino.h>
#include <stdlib.h>
#include <USBHost_t36.h>

#include <event_types.h>

class UsbHostService {
public:
  UsbHostService(EventQueue &_e)
      : events(_e), myusb(), hub1(myusb), hub2(myusb), hub3(myusb), hub4(myusb),
        keyboard(myusb), midi1(myusb), midi2(myusb), midi3(myusb) {}
  UsbHostService(const UsbHostService &) = delete;
  UsbHostService &operator=(const UsbHostService &) = delete;

  FLASHMEM void start(void) {
    // USBHost_t36 does not allow attaching context to callback
    // so we'll use a singleton pattern here
    instance = this;
    keyboard.attachRawPress(onRawPress);
    keyboard.attachRawRelease(onRawRelease);
    myusb.begin();
  }
  void task(void) { myusb.Task(); }

  static void onRawPress(uint8_t keycode) {
    instance->handlePress(keycode);
  }

  static void onRawRelease(uint8_t keycode) {
    instance->handleRelease(keycode);
  }

  void handlePress(uint8_t keycode) {
    int eventBuf = EventQueue::createKeyboardEventBuffer(toHidKeycode(keycode), keyboard.getModifiers());
    EventQueue::event_t e = {kEventKeyDown, eventBuf};
    events.postFromISR(e);
  }

  void handleRelease(uint8_t keycode) {
    int eventBuf = EventQueue::createKeyboardEventBuffer(toHidKeycode(keycode), keyboard.getModifiers());
    EventQueue::event_t e = {kEventKeyUp, eventBuf};
    events.postFromISR(e);
  }

  // USBHost_t36 reports modifiers as a keycode value between 103 and 110,
  // corresponding to which modifier bit is set in byte 0 of a HID transfer
  bool isModifier(const uint8_t keycode) {
    return ((keycode >= 103) && (keycode < 111));
  }

  uint8_t toHidKeycode(const uint8_t keycode) {
    if (isModifier(keycode)) return 0;
    return keycode;
  }

private:
  EventQueue &events;
  USBHost myusb;
  USBHub hub1;
  USBHub hub2; // @nate 1k each!
  USBHub hub3;
  USBHub hub4;

  static UsbHostService *instance;

public:
  KeyboardController keyboard;
  MIDIDevice_BigBuffer midi1; // @nate - 4k each!
  MIDIDevice_BigBuffer midi2;
  MIDIDevice_BigBuffer midi3;
};

#endif
