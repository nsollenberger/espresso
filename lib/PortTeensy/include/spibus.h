#ifndef _SPIBUS_H__
#define _SPIBUS_H__

#include "spi_config.h"

typedef spi_transaction_t spi_message_t;

class SpiBus {
public:
  SpiBus(Spi::SpiDevice spiDev) : _dev(spiDev), bus_valid{false} {
    if (!Spi::spi_active.load()) {
      Spi::beginTransaction();
      bus_valid = true;
    }
  }

  ~SpiBus() {
    if (bus_valid)
      Spi::endTransaction();
  }
  SpiBus(const SpiBus &) = delete;
  SpiBus &operator=(const SpiBus &) = delete;

  bool sendMessage(spi_message_t &message_conf, bool keepActive = false) {
    if (!bus_valid)
      return false;
    switch (_dev) {
    case Spi::dac1_dev:
      Gpio::setLevel(SPI_CS_DAC1_PIN, LOW);
      break;
    case Spi::dac2_dev:
      Gpio::setLevel(SPI_CS_DAC2_PIN, LOW);
      break;
    case Spi::oled_dev:
      Gpio::setLevel(SPI_CS_OLED_PIN, LOW);
      break;
    }
    bool success = Spi::sendMessagePolling(message_conf);
    if (!keepActive) {
      switch (_dev) {
      case Spi::dac1_dev:
        Gpio::setLevel(SPI_CS_DAC1_PIN, HIGH);
        break;
      case Spi::dac2_dev:
        Gpio::setLevel(SPI_CS_DAC2_PIN, HIGH);
        break;
      case Spi::oled_dev:
        Gpio::setLevel(SPI_CS_OLED_PIN, HIGH);
        break;
      }
    }
    return success;
  }

  bool sendMessageAsync(const void *txBuffer, size_t count) {
    if (!bus_valid)
      return false;
    int cs_pin;
    switch (_dev) {
    case Spi::dac1_dev:
      cs_pin = SPI_CS_DAC1_PIN;
      break;
    case Spi::dac2_dev:
      cs_pin = SPI_CS_DAC2_PIN;
      break;
    case Spi::oled_dev:
      cs_pin = SPI_CS_OLED_PIN;
      break;
    default:
      return false;
    }
    return Spi::sendMessageAsync(txBuffer, count, cs_pin);
  }

private:
  Spi::SpiDevice _dev;

public:
  bool bus_valid;
};

#endif
