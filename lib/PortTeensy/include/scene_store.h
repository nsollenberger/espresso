#ifndef _SCENE_STORE_H__
#define _SCENE_STORE_H__

#include <LittleFS.h>

#include <region.h>

#define SCENE_SLOTS 32

// Bytes Used: 1114112, Bytes Total:1179648
constexpr int diskSizeBytes = 1152 * 1024;

class Scene;

class SceneStore {
public:
  SceneStore(Scene &_state, scene_text_t &_text);
  SceneStore(const SceneStore &) = delete;
  SceneStore &operator=(const SceneStore &) = delete;

  void load(uint8_t sceneIndex, bool loadPatterns = true);
  void loadText(uint8_t sceneIndex, scene_text_t &textOut);
  void save(uint8_t sceneIndex);
  void clearAll(void);

private:
  Scene &scene_state;
  scene_text_t &scene_text;

  LittleFS_Program myfs;

  size_t file_scripts_size(void);
};

#endif
