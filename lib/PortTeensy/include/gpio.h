#ifndef _GPIO_H__
#define _GPIO_H__

#include <Arduino.h>
#include "ADC.h"
#include "avr/pgmspace.h"
#include <stdint.h>

#include "config.h"

typedef int input_pin_t;
typedef int output_pin_t;

extern input_pin_t triggerInPins[8];
extern output_pin_t triggerOutPins[4];

class Gpio {
public:
  static FLASHMEM void initPins(void);
  static FLASHMEM void initInterrupts(void gate_cb(int));

  static void setLevel(output_pin_t pin, int level) {
    digitalWriteFast(pin, level);
  }

  static int getLevel(input_pin_t pin) { return digitalReadFast(pin); }

  static int readAdc(input_pin_t pin) { return adc->analogRead(pin); }

  // pin0 must be available on ADC0
  // pin1 must be available on ADC1
  static ADC::Sync_result readAdcPair(input_pin_t pin0, input_pin_t pin1) {
    return adc->analogSynchronizedRead(pin0, pin1);
  }

private:
  static ADC *adc;
  static void (*gate_cb)(int);

  static void trigger1(void) { (*gate_cb)(0); }

  static void trigger2(void) { (*gate_cb)(1); }

  static void trigger3(void) { (*gate_cb)(2); }

  static void trigger4(void) { (*gate_cb)(3); }

  static void trigger5(void) { (*gate_cb)(4); }

  static void trigger6(void) { (*gate_cb)(5); }

  static void trigger7(void) { (*gate_cb)(6); }

  static void trigger8(void) { (*gate_cb)(7); }
};

#endif
