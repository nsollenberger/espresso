#ifndef _SCREEN_H__
#define _SCREEN_H__

#include "avr/pgmspace.h"
#include <cstddef>
#include <stdint.h>

#include <region.h>

#include "gpio.h"
#include "spibus.h"

constexpr size_t kBufferRowSize = 64;
// local buffer stores 2 column pixels per byte
constexpr size_t kBufferColSize = 64;
constexpr size_t kBufferSize = kBufferRowSize * kBufferColSize;

enum eCommand1Byte : uint8_t {
  eSleepModeOn = 0xAE,
  eSleepModeOff = 0xAF,
  eSetModeOff = 0xA4,
  eSetModeAllOnGray = 0xA5,
  eSetModeNormal = 0xA6,
  eSetModeInverse = 0xA7,
  eWriteRam = 0x5C,
};

enum eCommand2Bytes : uint8_t {
  eClockFreqDiv = 0xB3,
  eSetMuxRatio = 0xCA,
  eSetDisplayOffset = 0xA2,
  eSelectVddRegulator = 0xAB,
  eSetMasterConstrast = 0xC7,
  eSetContrast = 0xC1,
  eSetPhaseLength = 0xB1,
  eSetPrechargeVoltage = 0xBB,
  eSetVcomHVoltage = 0xBE,
};

enum eCommand3Bytes : uint8_t {
  eSetRemap = 0xA0,
  eSetVslAndQuality = 0xB4,
  eSetColAddress = 0x15,
  eSetRowAddress = 0x75,
};

typedef uint8_t column_units;
typedef uint8_t row_units;

class Screen {
public:
  Screen(screen_buffer_t &_r) : regions(_r) {}

  FLASHMEM void init(void);
  void draw();
  void clearScreen(void);

  screen_buffer_t& getBuffer(void) { return regions; }
  FLASHMEM void setDirection(uint8_t flipped) { is_screen_flipped = flipped; }

private:
  static DMAMEM uint8_t tx_buffer[8192] __attribute__((aligned(32)));
  screen_buffer_t& regions;
  size_t tx_buffer_count;
  uint8_t screenBuf[kBufferSize];
  bool is_screen_flipped;

  void fillTxBuffer(Region &reg);
  void writeScreenBufferAsync(column_units x, row_units y, column_units w,
                              row_units h);
  void setRect(SpiBus &spiBus, column_units x, row_units y, column_units w,
               row_units h);
  void clearScreen(SpiBus &spiBus);

  void writeCommand1Byte(SpiBus &spiBus, eCommand1Byte cmd);
  void writeCommand2Bytes(SpiBus &spiBus, eCommand2Bytes cmd, uint8_t data1);
  void writeCommand3Bytes(SpiBus &spiBus, eCommand3Bytes cmd, uint8_t data1,
                          uint8_t data2);

  void setDC(int data) { Gpio::setLevel(OLED_DC_PIN, data); }
};

#endif
