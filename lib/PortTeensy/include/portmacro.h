#ifndef _PORTMACRO_H__
#define _PORTMACRO_H__

#include <Arduino.h>

namespace Port {

inline void DISABLE_INTERRUPTS(void) { 
	noInterrupts(); 
}
inline void ENABLE_INTERRUPTS(void) { 
	interrupts(); 
}
} // namespace Port

#endif