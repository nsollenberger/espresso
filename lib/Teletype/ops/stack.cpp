#include "ops/stack.h"

#include <espresso/command.h>
#include <espresso/command_state.h>
#include <scene.h>

#include "teletype.h"
#include "teletype_io.h"

using Espresso::Command;
using Espresso::CommandState;
using Espresso::ExecState;

static void mod_S_func(Scene *ss, ExecState *es, CommandState *cs,
                       const Command *post_command);
static void op_S_ALL_get(const void *data, Scene *ss, ExecState *es,
                         CommandState *cs);
static void op_S_POP_get(const void *data, Scene *ss, ExecState *es,
                         CommandState *cs);
static void op_S_CLR_get(const void *data, Scene *ss, ExecState *es,
                         CommandState *cs);
static void op_S_L_get(const void *data, Scene *ss, ExecState *es,
                       CommandState *cs);

const tele_mod_t mod_S = MAKE_MOD(S, mod_S_func, 0);

const tele_op_t op_S_ALL = MAKE_GET_OP(S.ALL, op_S_ALL_get, 0, false);
const tele_op_t op_S_POP = MAKE_GET_OP(S.POP, op_S_POP_get, 0, false);
const tele_op_t op_S_CLR = MAKE_GET_OP(S.CLR, op_S_CLR_get, 0, false);
const tele_op_t op_S_L = MAKE_GET_OP(S.L, op_S_L_get, 0, true);

static void mod_S_func(Scene *ss, ExecState *_es, CommandState *_cs,
                       const Command *post_command) {
  if (ss->stack_op.top < STACK_OP_SIZE) {
    ss->stack_op.commands[ss->stack_op.top] = *post_command;
    ss->stack_op.top++;
    tele_has_stack(ss->stack_op.top > 0);
  }
}

static void op_S_ALL_get(const void *_data, Scene *ss, ExecState *es,
                         CommandState *_cs) {
  for (int16_t i = 0; i < ss->stack_op.top; i++) {
    tele_process_command(ss, es, &ss->stack_op.commands[ss->stack_op.top - i - 1]);
  }
  ss->stack_op.top = 0;
  tele_has_stack(false);
}

static void op_S_POP_get(const void *_data, Scene *ss, ExecState *es,
                         CommandState *_cs) {
  if (ss->stack_op.top) {
    ss->stack_op.top--;
    tele_process_command(ss, es, &ss->stack_op.commands[ss->stack_op.top]);
    if (ss->stack_op.top == 0)
      tele_has_stack(false);
  }
}

static void op_S_CLR_get(const void *_data, Scene *ss, ExecState *_es,
                         CommandState *_cs) {
  ss->stack_op.top = 0;
  tele_has_stack(false);
}

static void op_S_L_get(const void *_data, Scene *ss, ExecState *_es,
                       CommandState *cs) {
  cs->push(ss->stack_op.top);
}
