#include "ops/voice.h"

#include <espresso/command_state.h>
#include <scene.h>
#include <voice.h>

using Espresso::CommandState;
using Espresso::ExecState;

static void op_V_CV_get(const void *data, Scene *ss, ExecState *es,
                        CommandState *cs);
static void op_V_CV_set(const void *data, Scene *ss, ExecState *es,
                        CommandState *cs);
static void op_V_EG_get(const void *data, Scene *ss, ExecState *es,
                        CommandState *cs);
static void op_V_EG_set(const void *data, Scene *ss, ExecState *es,
                        CommandState *cs);
static void op_V_GATE_get(const void *data, Scene *ss, ExecState *es,
                          CommandState *cs);
static void op_V_GATE_set(const void *data, Scene *ss, ExecState *es,
                          CommandState *cs);
static void op_V_MIDI_get(const void *data, Scene *ss, ExecState *es,
                          CommandState *cs);
static void op_V_MIDI_set(const void *data, Scene *ss, ExecState *es,
                          CommandState *cs);
static void op_V_PLAY_get(const void *data, Scene *ss, ExecState *es,
                          CommandState *cs);
static void op_V_PAUSE_get(const void *data, Scene *ss, ExecState *es,
                           CommandState *cs);
static void op_V_MUTE_get(const void *data, Scene *ss, ExecState *es,
                          CommandState *cs);
static void op_V_RESET_get(const void *data, Scene *ss, ExecState *es,
                           CommandState *cs);
static void op_V_LOOP_get(const void *data, Scene *ss, ExecState *es,
                          CommandState *cs);
static void op_V_LOOP_set(const void *data, Scene *ss, ExecState *es,
                          CommandState *cs);
static void op_V_OCT_get(const void *data, Scene *ss, ExecState *es,
                         CommandState *cs);
static void op_V_OCT_set(const void *data, Scene *ss, ExecState *es,
                         CommandState *cs);
static void op_V_LENGTH_get(const void *data, Scene *ss, ExecState *es,
                            CommandState *cs);
static void op_V_LENGTH_set(const void *data, Scene *ss, ExecState *es,
                            CommandState *cs);
static void op_V_VEL_get(const void *data, Scene *ss, ExecState *es,
                         CommandState *cs);
static void op_V_VEL_set(const void *data, Scene *ss, ExecState *es,
                         CommandState *cs);
static void op_V_ENV_get(const void *data, Scene *ss, ExecState *es,
                         CommandState *cs);
static void op_V_ENV_set(const void *data, Scene *ss, ExecState *es,
                         CommandState *cs);

const tele_op_t op_V_CV =
    MAKE_GET_SET_OP(V.CV, op_V_CV_get, op_V_CV_set, 1, true);
const tele_op_t op_V_EG =
    MAKE_GET_SET_OP(V.EG, op_V_EG_get, op_V_EG_set, 1, true);
const tele_op_t op_V_GATE =
    MAKE_GET_SET_OP(V.GATE, op_V_GATE_get, op_V_GATE_set, 1, true);
const tele_op_t op_V_MIDI =
    MAKE_GET_SET_OP(V.MIDI, op_V_MIDI_get, op_V_MIDI_set, 1, true);
const tele_op_t op_V_PLAY = MAKE_GET_OP(V.PLAY, op_V_PLAY_get, 1, false);
const tele_op_t op_V_PAUSE = MAKE_GET_OP(V.PAUSE, op_V_PAUSE_get, 1, false);
const tele_op_t op_V_MUTE = MAKE_GET_OP(V.MUTE, op_V_MUTE_get, 1, false);
const tele_op_t op_V_RESET = MAKE_GET_OP(V.RESET, op_V_RESET_get, 1, false);
const tele_op_t op_V_LOOP =
    MAKE_GET_SET_OP(V.LOOP, op_V_LOOP_get, op_V_LOOP_set, 1, true);
const tele_op_t op_V_OCT =
    MAKE_GET_SET_OP(V.OCT, op_V_OCT_get, op_V_OCT_set, 1, true);
const tele_op_t op_V_LENGTH =
    MAKE_GET_SET_OP(V.LENGTH, op_V_LENGTH_get, op_V_LENGTH_set, 1, true);
const tele_op_t op_V_VEL =
    MAKE_GET_SET_OP(V.VEL, op_V_VEL_get, op_V_VEL_set, 1, true);
const tele_op_t op_V_ENV =
    MAKE_GET_SET_OP(V.ENV, op_V_ENV_get, op_V_ENV_set, 1, true);

static void op_V_CV_get(const void *_data, Scene *ss, ExecState *es,
                        CommandState *cs) {
  auto voice_num = cs->pop();
  if (voice_num < 1 || voice_num > VOICE_COUNT) {
    cs->push(-1);
    return;
  }
  cs->push(ss->voices[voice_num - 1].config().cv_out);
}

static void op_V_CV_set(const void *_data, Scene *ss, ExecState *es,
                        CommandState *cs) {
  auto voice_num = cs->pop();
  auto cv_out = cs->pop();
  if (cv_out < 0 || cv_out > CV_COUNT)
    return;
  if (voice_num < 1 || voice_num > VOICE_COUNT)
    return;
  ss->voices[voice_num - 1].config().cv_out = cv_out;
}

static void op_V_EG_get(const void *_data, Scene *ss, ExecState *es,
                        CommandState *cs) {
  auto voice_num = cs->pop();
  if (voice_num < 1 || voice_num > VOICE_COUNT) {
    cs->push(-1);
    return;
  }
  cs->push(ss->voices[voice_num - 1].config().eg_out);
}

static void op_V_EG_set(const void *_data, Scene *ss, ExecState *es,
                        CommandState *cs) {
  auto voice_num = cs->pop();
  auto eg_out = cs->pop();
  if (eg_out < 0 || eg_out > CV_COUNT)
    return;
  if (voice_num < 1 || voice_num > VOICE_COUNT)
    return;
  if (ss->voices[voice_num - 1].config().eg_out && eg_out == 0) {
    tele_env_config(ss->voices[voice_num - 1].config().eg_out - 1, 0, 0, 0, 0);
  }
  ss->voices[voice_num - 1].config().eg_out = eg_out;
}

static void op_V_GATE_get(const void *_data, Scene *ss, ExecState *es,
                          CommandState *cs) {
  auto voice_num = cs->pop();
  if (voice_num < 1 || voice_num > VOICE_COUNT) {
    cs->push(-1);
    return;
  }
  cs->push(ss->voices[voice_num - 1].config().gate_out);
}

static void op_V_GATE_set(const void *_data, Scene *ss, ExecState *es,
                          CommandState *cs) {
  auto voice_num = cs->pop();
  auto gate_out = cs->pop();
  if (gate_out < 0 || gate_out > CV_COUNT)
    return;
  if (voice_num < 1 || voice_num > VOICE_COUNT)
    return;
  ss->voices[voice_num - 1].config().gate_out = gate_out;
}

static void op_V_MIDI_get(const void *_data, Scene *ss, ExecState *es,
                          CommandState *cs) {
  auto voice_num = cs->pop();
  if (voice_num < 1 || voice_num > VOICE_COUNT) {
    cs->push(-1);
    return;
  }
  cs->push(ss->voices[voice_num - 1].config().midi_out_ch);
}

static void op_V_MIDI_set(const void *_data, Scene *ss, ExecState *es,
                          CommandState *cs) {
  auto voice_num = cs->pop();
  auto midi_out_ch = cs->pop();
  if (midi_out_ch < 0 || midi_out_ch > 16)
    return;
  if (voice_num < 1 || voice_num > VOICE_COUNT)
    return;
  ss->voices[voice_num - 1].config().midi_out_ch = midi_out_ch;
}

static void op_V_PLAY_get(const void *_data, Scene *ss, ExecState *_es,
                          CommandState *cs) {
  auto voice_num = cs->pop();
  if (voice_num < 1 || voice_num > VOICE_COUNT)
    return;
  ss->voices[voice_num - 1].play();
}

static void op_V_PAUSE_get(const void *_data, Scene *ss, ExecState *_es,
                           CommandState *cs) {
  auto voice_num = cs->pop();
  if (voice_num < 1 || voice_num > VOICE_COUNT)
    return;
  ss->voices[voice_num - 1].pause();
}

static void op_V_MUTE_get(const void *_data, Scene *ss, ExecState *_es,
                          CommandState *cs) {
  auto voice_num = cs->pop();
  if (voice_num < 1 || voice_num > VOICE_COUNT)
    return;
  ss->voices[voice_num - 1].mute();
}

static void op_V_RESET_get(const void *_data, Scene *ss, ExecState *_es,
                           CommandState *cs) {
  auto voice_num = cs->pop();
  if (voice_num < 1 || voice_num > VOICE_COUNT)
    return;
  clock_reset_voice(voice_num - 1);
}

static void op_V_LOOP_get(const void *_data, Scene *ss, ExecState *es,
                          CommandState *cs) {
  auto voice_num = cs->pop();
  if (voice_num < 1 || voice_num > VOICE_COUNT) {
    cs->push(-1);
    return;
  }
  cs->push(ss->voices[voice_num - 1].loop ? 1 : 0);
}

static void op_V_LOOP_set(const void *_data, Scene *ss, ExecState *es,
                          CommandState *cs) {
  auto voice_num = cs->pop();
  auto loop = cs->pop();
  if (voice_num < 1 || voice_num > VOICE_COUNT)
    return;
  ss->voices[voice_num - 1].loop = !!loop;
}

static void op_V_OCT_get(const void *_data, Scene *ss, ExecState *es,
                         CommandState *cs) {
  auto voice_num = cs->pop();
  if (voice_num < 1 || voice_num > VOICE_COUNT) {
    cs->push(-1);
    return;
  }
  cs->push(ss->voices[voice_num - 1].config().octave);
}

static void op_V_OCT_set(const void *_data, Scene *ss, ExecState *es,
                         CommandState *cs) {
  auto voice_num = cs->pop();
  auto octave = cs->pop();
  if (voice_num < 1 || voice_num > VOICE_COUNT)
    return;
  if (octave < -1)
    octave = -1;
  if (octave > 9)
    octave = 9;
  ss->voices[voice_num - 1].config().octave = octave;
}

static void op_V_LENGTH_get(const void *_data, Scene *ss, ExecState *es,
                            CommandState *cs) {
  auto voice_num = cs->pop();
  if (voice_num < 1 || voice_num > VOICE_COUNT) {
    cs->push(-1);
    return;
  }
  cs->push(ss->voices[voice_num - 1].config().note_length);
}

static void op_V_LENGTH_set(const void *_data, Scene *ss, ExecState *es,
                            CommandState *cs) {
  auto voice_num = cs->pop();
  auto length = cs->pop();
  if (voice_num < 1 || voice_num > VOICE_COUNT)
    return;
  if (length < 1)
    length = 1;
  if (length > 100)
    length = 100;
  ss->voices[voice_num - 1].config().note_length = length;
  clock_refresh_voice(voice_num - 1);
}

static void op_V_VEL_get(const void *_data, Scene *ss, ExecState *es,
                         CommandState *cs) {
  auto voice_num = cs->pop();
  if (voice_num < 1 || voice_num > VOICE_COUNT) {
    cs->push(-1);
    return;
  }
  cs->push(ss->voices[voice_num - 1].config().note_vel);
}

static void op_V_VEL_set(const void *_data, Scene *ss, ExecState *es,
                         CommandState *cs) {
  auto voice_num = cs->pop();
  auto vel = cs->pop();
  if (voice_num < 1 || voice_num > VOICE_COUNT)
    return;
  if (vel < 1)
    vel = 1;
  if (vel > 127)
    vel = 127;
  ss->voices[voice_num - 1].config().note_vel = vel;
}

static void op_V_ENV_get(const void *_data, Scene *ss, ExecState *es,
                         CommandState *cs) {
  cs->push(-1);
}

static void op_V_ENV_set(const void *_data, Scene *ss, ExecState *es,
                         CommandState *cs) {
  auto voice_num = cs->pop();
  if (voice_num < 1 || voice_num > VOICE_COUNT)
    return;

  int16_t attack = 10;
  int16_t decay = 50;
  int16_t sustain = 80;
  int16_t release = 400;

  auto argc = cs->size();
  if (argc == 1) {
    decay = 0;
    sustain = 100;
    release = cs->pop();
  } else if (argc == 2) {
    attack = cs->pop();
    decay = 0;
    sustain = 100;
    release = cs->pop();
  } else if (argc == 3) {
    attack = cs->pop();
    sustain = cs->pop();
    release = cs->pop();
    decay = release;
  } else if (argc == 4) {
    attack = cs->pop();
    decay = cs->pop();
    sustain = cs->pop();
    release = cs->pop();
  }

  if (sustain < 0)
    sustain = 0;
  if (sustain > 100)
    sustain = 100;

  tele_env_config(ss->voices[voice_num - 1].config().eg_out - 1, attack, decay,
                  (float(sustain) / 100.0) * 16383, release);
}
