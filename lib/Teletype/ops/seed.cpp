#include "ops/seed.h"

#include <espresso/command_state.h>
#include <random.h>
#include <scene.h>

using Espresso::CommandState;
using Espresso::ExecState;
using Port::random_seed;

#define MAKE_SEED_OP(n, v)                                                     \
  {                                                                            \
    .name = #n, .get = op_peek_seed_i16, .set = op_poke_seed_i16, .params = 0, \
    .returns = 1, .data = (void *)offsetof(scene_rand_t, v)                           \
  }

#define MAKE_SEED_ALIAS_OP(n, v) MAKE_SEED_OP(n, v)

static void op_peek_seed_i16(const void *data, Scene *ss, ExecState *es,
                             CommandState *cs);
static void op_poke_seed_i16(const void *data, Scene *ss, ExecState *es,
                             CommandState *cs);
static void op_SEED_get(const void *data, Scene *ss, ExecState *es,
                        CommandState *cs);
static void op_SEED_set(const void *data, Scene *ss, ExecState *es,
                        CommandState *cs);

// clang-format off
const tele_op_t op_SEED			= MAKE_GET_SET_OP(SEED, op_SEED_get, op_SEED_set, 0, true);
const tele_op_t op_RAND_SEED	= MAKE_SEED_OP(RAND.SEED, rand);  
const tele_op_t op_SYM_RAND_SD	= MAKE_SEED_ALIAS_OP(RAND.SD, rand);  
const tele_op_t op_SYM_R_SD		= MAKE_SEED_ALIAS_OP(R.SD, rand);  
const tele_op_t op_TOSS_SEED	= MAKE_SEED_OP(TOSS.SEED, toss);  
const tele_op_t op_SYM_TOSS_SD	= MAKE_SEED_ALIAS_OP(TOSS.SD, toss);  
const tele_op_t op_PROB_SEED	= MAKE_SEED_OP(PROB.SEED, prob);  
const tele_op_t op_SYM_PROB_SD	= MAKE_SEED_ALIAS_OP(PROB.SD, prob);  
const tele_op_t op_DRUNK_SEED	= MAKE_SEED_OP(DRUNK.SEED, drunk);  
const tele_op_t op_SYM_DRUNK_SD	= MAKE_SEED_ALIAS_OP(DRUNK.SD, drunk);  
const tele_op_t op_P_SEED		= MAKE_SEED_OP(P.SEED, pattern);  
const tele_op_t op_SYM_P_SD		= MAKE_SEED_ALIAS_OP(P.SD, pattern);
// clang-format on

static void op_peek_seed_i16(const void *data, Scene *ss, ExecState *_es,
                             CommandState *cs) {
  char *base = (char *)&ss->rand_states;
  size_t offset = (size_t)data;
  tele_rand_t *ptr = (tele_rand_t *)(base + offset);
  cs->push(ptr->seed);
}

static void op_poke_seed_i16(const void *data, Scene *ss, ExecState *_es,
                             CommandState *cs) {
  int16_t s = cs->pop();

  char *base = (char *)&ss->rand_states;
  size_t offset = (size_t)data;
  tele_rand_t *ptr = (tele_rand_t *)(base + offset);

  ptr->seed = s;
  random_seed(&ptr->rand, ptr->seed);
}

static void op_SEED_get(const void *_data, Scene *ss, ExecState *_es,
                        CommandState *cs) {
  cs->push(ss->variables.seed);
}

static void op_SEED_set(const void *_data, Scene *ss, ExecState *_es,
                        CommandState *cs) {
  auto s = cs->pop();
  ss->ss_rand_init(s);
  ss->variables.seed = s;
}
