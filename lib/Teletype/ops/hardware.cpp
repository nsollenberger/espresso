#include "ops/hardware.h"

#include <espresso/command_state.h>
#include <scene.h>
#include <util.h>

#include "teletype.h"
#include "teletype_io.h"

using Util::normalise_value;

using Espresso::CommandState;
using Espresso::ExecState;

static void op_CV_get(const void *data, Scene *ss, ExecState *es,
                      CommandState *cs);
static void op_CV_set(const void *data, Scene *ss, ExecState *es,
                      CommandState *cs);
static void op_CV_SLEW_get(const void *data, Scene *ss, ExecState *es,
                           CommandState *cs);
static void op_CV_SLEW_set(const void *data, Scene *ss, ExecState *es,
                           CommandState *cs);
static void op_CV_OFF_get(const void *data, Scene *ss, ExecState *es,
                          CommandState *cs);
static void op_CV_OFF_set(const void *data, Scene *ss, ExecState *es,
                          CommandState *cs);
static void op_IN_get(const void *_data, Scene *ss, ExecState *_es,
                      CommandState *cs);
static void op_IN_SCALE_set(const void *_data, Scene *ss, ExecState *_es,
                            CommandState *cs);
static void op_IN_CAL_MIN_set(const void *_data, Scene *ss, ExecState *_es,
                              CommandState *cs);
static void op_IN_CAL_MAX_set(const void *_data, Scene *ss, ExecState *_es,
                              CommandState *cs);
static void op_IN_CAL_RESET_set(const void *_data, Scene *ss, ExecState *_es,
                                CommandState *cs);
static void op_PARAM_get(const void *_data, Scene *ss, ExecState *_es,
                         CommandState *cs);
static void op_PARAM_SCALE_set(const void *_data, Scene *ss, ExecState *_es,
                               CommandState *cs);
static void op_PARAM_CAL_MIN_set(const void *_data, Scene *ss, ExecState *_es,
                                 CommandState *cs);
static void op_PARAM_CAL_MAX_set(const void *_data, Scene *ss, ExecState *_es,
                                 CommandState *cs);
static void op_PARAM_CAL_RESET_set(const void *_data, Scene *ss, ExecState *_es,
                                   CommandState *cs);
static void op_CV_SET_get(const void *data, Scene *ss, ExecState *es,
                          CommandState *cs);
static void op_STATE_get(const void *data, Scene *ss, ExecState *es,
                         CommandState *cs);
static void op_DEVICE_FLIP_get(const void *data, Scene *ss, ExecState *es,
                               CommandState *cs);

// clang-format off
const tele_op_t op_CV       = MAKE_GET_SET_OP(CV      , op_CV_get      , op_CV_set     , 1, true);
const tele_op_t op_CV_OFF   = MAKE_GET_SET_OP(CV.OFF  , op_CV_OFF_get  , op_CV_OFF_set , 1, true);
const tele_op_t op_CV_SLEW  = MAKE_GET_SET_OP(CV.SLEW , op_CV_SLEW_get , op_CV_SLEW_set, 1, true);
const tele_op_t op_IN       = MAKE_GET_OP    (IN      , op_IN_get      , 0, true);
const tele_op_t op_IN_SCALE = MAKE_GET_OP    (IN.SCALE, op_IN_SCALE_set, 2, false);
const tele_op_t op_PARAM    = MAKE_GET_OP    (PARAM   , op_PARAM_get   , 0, true);
const tele_op_t op_PARAM_SCALE = MAKE_GET_OP (PARAM.SCALE, op_PARAM_SCALE_set, 2, false);
const tele_op_t op_PRM      = MAKE_ALIAS_OP  (PRM     , op_PARAM_get   , NULL,           0, true);
const tele_op_t op_CV_SET   = MAKE_GET_OP    (CV.SET  , op_CV_SET_get  , 2, false);
const tele_op_t op_STATE    = MAKE_GET_OP    (STATE   , op_STATE_get   , 1, true );
const tele_op_t op_IN_CAL_MIN    = MAKE_GET_OP (IN.CAL.MIN, op_IN_CAL_MIN_set, 0, true);
const tele_op_t op_IN_CAL_MAX    = MAKE_GET_OP (IN.CAL.MAX, op_IN_CAL_MAX_set, 0, true);
const tele_op_t op_IN_CAL_RESET  = MAKE_GET_OP (IN.CAL.RESET, op_IN_CAL_RESET_set, 0, false);
const tele_op_t op_PARAM_CAL_MIN = MAKE_GET_OP (PARAM.CAL.MIN, op_PARAM_CAL_MIN_set, 0, true);
const tele_op_t op_PARAM_CAL_MAX = MAKE_GET_OP (PARAM.CAL.MAX, op_PARAM_CAL_MAX_set, 0, true);
const tele_op_t op_PARAM_CAL_RESET  = MAKE_GET_OP (PARAM.CAL.RESET, op_PARAM_CAL_RESET_set, 0, false);
const tele_op_t op_DEVICE_FLIP   = MAKE_GET_OP (DEVICE.FLIP, op_DEVICE_FLIP_get, 0, false);
// clang-format on

static void op_CV_get(const void *_data, Scene *ss, ExecState *_es,
                      CommandState *cs) {
  int16_t a = cs->pop();
  a--;
  if (a < 0)
    cs->push(0);
  else if (a < 4)
    cs->push(ss->variables.cv[a]);
  else
    cs->push(0);
}

static void op_CV_set(const void *_data, Scene *ss, ExecState *_es,
                      CommandState *cs) {
  int16_t a = cs->pop();
  int16_t b = cs->pop();
  b = normalise_value(0, 16383, 0, b);
  a--;
  if (a < 0)
    return;
  else if (a < 4) {
    ss->variables.cv[a] = b;
    tele_cv(a, b, 1);
  }
}

static void op_CV_SLEW_get(const void *_data, Scene *ss, ExecState *_es,
                           CommandState *cs) {
  int16_t a = cs->pop();
  a--;
  if (a < 0)
    cs->push(0);
  else if (a < 4)
    cs->push(ss->variables.cv_slew[a]);
  else
    cs->push(0);
}

static void op_CV_SLEW_set(const void *_data, Scene *ss, ExecState *_es,
                           CommandState *cs) {
  int16_t a = cs->pop();
  int16_t b = cs->pop();
  b = normalise_value(1, 32767, 0, b); // min slew = 1
  a--;
  if (a < 0)
    return;
  else if (a < 4) {
    ss->variables.cv_slew[a] = b;
    tele_cv_slew(a, b);
  }
}

static void op_CV_OFF_get(const void *_data, Scene *ss, ExecState *_es,
                          CommandState *cs) {
  int16_t a = cs->pop();
  a--;
  if (a < 0)
    cs->push(0);
  else if (a < 4)
    cs->push(ss->variables.cv_off[a]);
  else
    cs->push(0);
}

static void op_CV_OFF_set(const void *_data, Scene *ss, ExecState *_es,
                          CommandState *cs) {
  int16_t a = cs->pop();
  int16_t b = cs->pop();
  ss->variables.cv_off[a] = b;
  a--;
  if (a < 0)
    return;
  else if (a < 4) {
    ss->variables.cv_off[a] = b;
    tele_cv_off(a, b);
    tele_cv(a, ss->variables.cv[a], 1);
  }
}

static void op_IN_get(const void *_data, Scene *ss, ExecState *_es,
                      CommandState *cs) {
  tele_update_adc(0);
  cs->push(ss->ss_get_in());
}

static void op_IN_SCALE_set(const void *_data, Scene *ss, ExecState *_es,
                            CommandState *cs) {
  int16_t min = cs->pop();
  int16_t max = cs->pop();
  ss->ss_set_in_scale(min, max);
}

static void op_IN_CAL_MIN_set(const void *_data, Scene *ss, ExecState *_es,
                              CommandState *cs) {
  ss->ss_set_in_min(ss->variables.in);
  cs->push(ss->variables.in);
}

static void op_IN_CAL_MAX_set(const void *_data, Scene *ss, ExecState *_es,
                              CommandState *cs) {
  ss->ss_set_in_max(ss->variables.in);
  cs->push(ss->variables.in);
}

static void op_IN_CAL_RESET_set(const void *_data, Scene *ss, ExecState *_es,
                                CommandState *_cs) {
  ss->ss_reset_in_cal();
}

static void op_PARAM_get(const void *_data, Scene *ss, ExecState *_es,
                         CommandState *cs) {
  tele_update_adc(0);
  cs->push(ss->ss_get_param());
}

static void op_PARAM_SCALE_set(const void *_data, Scene *ss, ExecState *_es,
                               CommandState *cs) {
  int16_t min = cs->pop();
  int16_t max = cs->pop();
  ss->ss_set_param_scale(min, max);
}

static void op_PARAM_CAL_MIN_set(const void *_data, Scene *ss, ExecState *_es,
                                 CommandState *cs) {
  ss->ss_set_param_min(ss->variables.param);
  cs->push(ss->variables.param);
}

static void op_PARAM_CAL_MAX_set(const void *_data, Scene *ss, ExecState *_es,
                                 CommandState *cs) {
  ss->ss_set_param_max(ss->variables.param);
  cs->push(ss->variables.param);
}

static void op_PARAM_CAL_RESET_set(const void *_data, Scene *ss, ExecState *_es,
                                   CommandState *cs) {
  ss->ss_reset_param_cal();
}

static void op_CV_SET_get(const void *_data, Scene *ss, ExecState *_es,
                          CommandState *cs) {
  int16_t a = cs->pop();
  int16_t b = cs->pop();

  if (b < 0)
    b = 0;
  else if (b > 16383)
    b = 16383;

  a--;
  if (a < 0)
    return;
  else if (a < 4) {
    ss->variables.cv[a] = b;
    tele_cv(a, b, 0);
  }
}

static void op_STATE_get(const void *_data, Scene *_ss, ExecState *_es,
                         CommandState *cs) {
  int16_t a = cs->pop();
  a--;
  if (a < 0)
    cs->push(0);
  else if (a < 8)
    cs->push(tele_get_input_state(a));
  else
    cs->push(0);
}

static void op_DEVICE_FLIP_get(const void *_data, Scene *_ss, ExecState *_es,
                               CommandState *cs) {
  device_flip();
}

