#include <espresso/command_state.h>
#include <scene.h>
#include <util.h>

#include "ops/op.h"
#include "ops/controlflow.h"
#include "ops/hardware.h"
#include "ops/init.h"
#include "ops/maths.h"
#include "ops/metronome.h"
#include "ops/queue.h"
#include "ops/seed.h"
#include "ops/sequence.h"
#include "ops/stack.h"
#include "ops/variables.h"
#include "ops/voice.h"
#include "teletype_io.h"

using Espresso::CommandState;
using Espresso::ExecState;

/////////////////////////////////////////////////////////////////
// OPS //////////////////////////////////////////////////////////

// If you edit this array, you need to run 'utils/op_enums.py' to update the
// values in 'op_enum.h' so that the order matches.
const tele_op_t *tele_ops[E_OP__LENGTH] = {
    // variables
    &op_A, &op_B, &op_C, &op_D, &op_DRUNK, &op_DRUNK_MAX, &op_DRUNK_MIN,
    &op_DRUNK_WRAP, &op_FLIP, &op_I, &op_O, &op_O_INC, &op_O_MAX, &op_O_MIN,
    &op_O_WRAP, &op_T, &op_TIME, &op_TIME_ACT, &op_LAST, &op_X, &op_Y, &op_Z,
    &op_J, &op_K,

    // init
    &op_INIT, &op_INIT_SCENE, &op_INIT_SCRIPT, &op_INIT_SCRIPT_ALL,
    &op_INIT_CV, &op_INIT_CV_ALL, &op_INIT_DATA, &op_INIT_TIME,

    // metronome
    &op_M, &op_M_SYM_EXCLAMATION, &op_M_ACT, &op_M_RESET,

    // queue
    &op_Q, &op_Q_AVG, &op_Q_N, &op_Q_CLR, &op_Q_GRW, &op_Q_SUM, &op_Q_MIN,
    &op_Q_MAX, &op_Q_RND, &op_Q_SRT, &op_Q_REV, &op_Q_SH, &op_Q_ADD, &op_Q_SUB,
    &op_Q_MUL, &op_Q_DIV, &op_Q_MOD, &op_Q_I,

    // hardware
    &op_CV, &op_CV_OFF, &op_CV_SLEW, &op_IN, &op_IN_SCALE, &op_PARAM,
    &op_PARAM_SCALE, &op_IN_CAL_MIN, &op_IN_CAL_MAX, &op_IN_CAL_RESET,
    &op_PARAM_CAL_MIN, &op_PARAM_CAL_MAX, &op_PARAM_CAL_RESET, &op_PRM, &op_CV_SET,
    &op_STATE, &op_DEVICE_FLIP,

    // maths
    &op_ADD, &op_SUB, &op_MUL, &op_DIV, &op_MOD, &op_RAND, &op_RND, &op_RRAND,
    &op_RRND, &op_R, &op_R_MIN, &op_R_MAX, &op_TOSS, &op_MIN, &op_MAX, &op_LIM,
    &op_WRAP, &op_WRP, &op_QT, &op_QT_S, &op_QT_CS, &op_QT_B, &op_QT_BX,
    &op_AVG, &op_EQ, &op_NE, &op_LT, &op_GT, &op_LTE, &op_GTE, &op_INR,
    &op_OUTR, &op_INRI, &op_OUTRI, &op_NZ, &op_EZ, &op_RSH, &op_LSH, &op_LROT,
    &op_RROT, &op_EXP, &op_ABS, &op_SGN, &op_AND, &op_OR, &op_AND3, &op_OR3,
    &op_AND4, &op_OR4, &op_JI, &op_SCALE, &op_SCL, &op_N, &op_VN, &op_HZ,
    &op_N_S, &op_N_C, &op_N_CS, &op_N_B, &op_N_BX, &op_V, &op_VV, &op_ER,
    &op_NR, &op_BPM, &op_BIT_OR, &op_BIT_AND, &op_BIT_NOT, &op_BIT_XOR,
    &op_BSET, &op_BGET, &op_BCLR, &op_BTOG, &op_BREV, &op_XOR, &op_CHAOS,
    &op_CHAOS_R, &op_CHAOS_ALG, &op_SYM_PLUS, &op_SYM_DASH, &op_SYM_STAR,
    &op_SYM_FORWARD_SLASH, &op_SYM_PERCENTAGE, &op_SYM_EQUAL_x2,
    &op_SYM_EXCLAMATION_EQUAL, &op_SYM_LEFT_ANGLED, &op_SYM_RIGHT_ANGLED,
    &op_SYM_LEFT_ANGLED_EQUAL, &op_SYM_RIGHT_ANGLED_EQUAL,
    &op_SYM_RIGHT_ANGLED_LEFT_ANGLED, &op_SYM_LEFT_ANGLED_RIGHT_ANGLED,
    &op_SYM_RIGHT_ANGLED_EQUAL_LEFT_ANGLED,
    &op_SYM_LEFT_ANGLED_EQUAL_RIGHT_ANGLED, &op_SYM_EXCLAMATION,
    &op_SYM_LEFT_ANGLED_x2, &op_SYM_RIGHT_ANGLED_x2, &op_SYM_LEFT_ANGLED_x3,
    &op_SYM_RIGHT_ANGLED_x3, &op_SYM_AMPERSAND_x2, &op_SYM_PIPE_x2,
    &op_SYM_AMPERSAND_x3, &op_SYM_PIPE_x3, &op_SYM_AMPERSAND_x4,
    &op_SYM_PIPE_x4, &op_TIF,

    // stack
    &op_S_ALL, &op_S_POP, &op_S_CLR, &op_S_L,

    // controlflow
    &op_SCRIPT, &op_SYM_DOLLAR, &op_SCRIPT_POL, &op_SYM_DOLLAR_POL, &op_KILL,
    &op_SCENE, &op_SCENE_P, &op_BREAK, &op_BRK, &op_SYNC,

    // seed
    &op_SEED, &op_RAND_SEED, &op_SYM_RAND_SD, &op_SYM_R_SD, &op_TOSS_SEED,
    &op_SYM_TOSS_SD, &op_PROB_SEED, &op_SYM_PROB_SD, &op_DRUNK_SEED,
    &op_SYM_DRUNK_SD, &op_P_SEED, &op_SYM_P_SD,

    &op_SEQ_PROB, &op_SEQ_REV, &op_SEQ_SHUF, &op_SEQ_ROT, &op_SEQ_NROT, &op_SEQ_FAST,
    &op_SEQ_SLOW, &op_SEQ_TRUNC, &op_SEQ_FLIP, &op_SEQ_STUT, &op_SEQ_FRAC, &op_SEQ_HARM,
    &op_SEQ_TRANSP, &op_SEQ_QNT, &op_SEQ_SLEW, &op_SEQ_LOOP,
    // &op_SEQ_SCALE, &op_SEQ_FILL, &op_SEQ_MFILL, &op_SEQ_MFILL_QT

    &op_V_CV, &op_V_EG, &op_V_GATE, &op_V_MIDI, &op_V_PLAY, &op_V_PAUSE, &op_V_MUTE,
    &op_V_RESET, &op_V_LOOP, &op_V_OCT, &op_V_LENGTH, &op_V_VEL, &op_V_ENV

};

/////////////////////////////////////////////////////////////////
// MODS /////////////////////////////////////////////////////////

const tele_mod_t *tele_mods[E_MOD__LENGTH] = {
    // controlflow
    &mod_IF, &mod_ELIF, &mod_ELSE, &mod_L, &mod_W, &mod_EVERY, &mod_EV,
    &mod_SKIP, &mod_MODULO, &mod_OTHER, &mod_PROB,

    // stack
    &mod_S,

    &mod_SEQ,  &mod_SEQ_PROB, &mod_SEQ_FAST, &mod_SEQ_SLOW,
    &mod_SEQ_STUT, &mod_SEQ_FRAC, &mod_SEQ_HARM, &mod_SEQ_TRANSP,
    &mod_SEQ_QNT, &mod_SEQ_SLEW,
};

/////////////////////////////////////////////////////////////////
// HELPERS //////////////////////////////////////////////////////

void op_peek_i16(const void *data, Scene *ss, ExecState *_es,
                 CommandState *cs) {
  char *base = (char *)&ss->variables;
  size_t offset = (size_t)data;
  int16_t *ptr = (int16_t *)(base + offset);
  cs->push(*ptr);
}

void op_poke_i16(const void *data, Scene *ss, ExecState *_es,
                 CommandState *cs) {
  char *base = (char *)&ss->variables;
  size_t offset = (size_t)data;
  int16_t *ptr = (int16_t *)(base + offset);
  *ptr = cs->pop();
  tele_vars_updated();
}
