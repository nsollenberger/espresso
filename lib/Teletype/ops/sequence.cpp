#include <algorithm>

#include <voice.h>
#include <espresso/command.h>
#include <espresso/command_state.h>
#include <espresso/exec_state.h>

#include "ops/sequence.h"
#include "sequence.h"
#include "scene.h"
#include "types.h"

using Espresso::Command;
using Espresso::CommandState;
using Espresso::ExecState;

static void mod_SEQ_func(Scene *ss, ExecState *es, CommandState *cs,
                         const Command *post_command);

static void op_SEQ_NULL_get(const void *data, Scene *ss, ExecState *es,
                            CommandState *cs) {}

static void op_SEQ_REV_set(const void *data, Scene *ss, ExecState *es,
                           CommandState *cs);
static void op_SEQ_SHUF_set(const void *data, Scene *ss, ExecState *es,
                            CommandState *cs);
static void op_SEQ_FLIP_set(const void *data, Scene *ss, ExecState *es,
                            CommandState *cs);

static void op_SEQ_ROT_set(const void *data, Scene *ss, ExecState *es,
                           CommandState *cs);
static void op_SEQ_NROT_set(const void *data, Scene *ss, ExecState *es,
                            CommandState *cs);
static void op_SEQ_TRUNC_set(const void *data, Scene *ss, ExecState *es,
                             CommandState *cs);
static void op_SEQ_LOOP_set(const void *data, Scene *ss, ExecState *es,
                              CommandState *cs);

static void op_SEQ_PROB_set(const void *data, Scene *ss, ExecState *es,
                            CommandState *cs);
static void op_SEQ_FAST_set(const void *data, Scene *ss, ExecState *es,
                            CommandState *cs);
static void op_SEQ_SLOW_set(const void *data, Scene *ss, ExecState *es,
                            CommandState *cs);
static void op_SEQ_STUT_set(const void *data, Scene *ss, ExecState *es,
                            CommandState *cs);
static void op_SEQ_FRAC_set(const void *data, Scene *ss, ExecState *es,
                            CommandState *cs);
static void op_SEQ_HARM_set(const void *data, Scene *ss, ExecState *es,
                            CommandState *cs);
static void op_SEQ_TRANSP_set(const void *data, Scene *ss, ExecState *es,
                              CommandState *cs);
static void op_SEQ_QNT_set(const void *data, Scene *ss, ExecState *es,
                              CommandState *cs);
static void op_SEQ_SLEW_set(const void *data, Scene *ss, ExecState *es,
                              CommandState *cs);

static void mod_SEQ_PROB_func(Scene *ss, ExecState *es, CommandState *cs,
                            const Command *post_command);
static void mod_SEQ_FAST_func(Scene *ss, ExecState *es, CommandState *cs,
                            const Command *post_command);
static void mod_SEQ_SLOW_func(Scene *ss, ExecState *es, CommandState *cs,
                            const Command *post_command);
static void mod_SEQ_STUT_func(Scene *ss, ExecState *es, CommandState *cs,
                            const Command *post_command);
static void mod_SEQ_FRAC_func(Scene *ss, ExecState *es, CommandState *cs,
                            const Command *post_command);
static void mod_SEQ_HARM_func(Scene *ss, ExecState *es, CommandState *cs,
                            const Command *post_command);
static void mod_SEQ_TRANSP_func(Scene *ss, ExecState *es, CommandState *cs,
                            const Command *post_command);
static void mod_SEQ_QNT_func(Scene *ss, ExecState *es, CommandState *cs,
                            const Command *post_command);
static void mod_SEQ_SLEW_func(Scene *ss, ExecState *es, CommandState *cs,
                            const Command *post_command);

const tele_mod_t mod_SEQ = MAKE_MOD_OPT(N, mod_SEQ_func, 0, 1);

const tele_op_t op_SEQ_REV =
    MAKE_GET_OP(REV, op_SEQ_REV_set, 0, false);
const tele_op_t op_SEQ_SHUF =
    MAKE_GET_OP(SHUF, op_SEQ_SHUF_set, 0, false);
const tele_op_t op_SEQ_FLIP =
    MAKE_GET_OP(FLIP, op_SEQ_FLIP_set, 0, false);

const tele_op_t op_SEQ_ROT =
    MAKE_GET_SET_OP(ROT, op_SEQ_NULL_get, op_SEQ_ROT_set, 0, false);
const tele_op_t op_SEQ_NROT =
    MAKE_GET_SET_OP(NROT, op_SEQ_NULL_get, op_SEQ_NROT_set, 0, false);
const tele_op_t op_SEQ_TRUNC =
    MAKE_GET_SET_OP(TRUNC, op_SEQ_NULL_get, op_SEQ_TRUNC_set, 0, false);
const tele_op_t op_SEQ_LOOP =
    MAKE_GET_SET_OP(LOOP, op_SEQ_NULL_get, op_SEQ_LOOP_set, 0, false);

const tele_op_t op_SEQ_PROB =
    MAKE_GET_SET_OP(NPROB, op_SEQ_NULL_get, op_SEQ_PROB_set, 0, true);
const tele_op_t op_SEQ_FAST =
    MAKE_GET_SET_OP(FAST, op_SEQ_NULL_get, op_SEQ_FAST_set, 0, false);
const tele_op_t op_SEQ_SLOW =
    MAKE_GET_SET_OP(SLOW, op_SEQ_NULL_get, op_SEQ_SLOW_set, 0, false);
const tele_op_t op_SEQ_STUT =
    MAKE_GET_SET_OP(STUT, op_SEQ_NULL_get, op_SEQ_STUT_set, 0, false);
const tele_op_t op_SEQ_FRAC =
    MAKE_GET_SET_OP(FRAC, op_SEQ_NULL_get, op_SEQ_FRAC_set, 0, false);
const tele_op_t op_SEQ_HARM =
    MAKE_GET_SET_OP(HARM, op_SEQ_NULL_get, op_SEQ_HARM_set, 0, false);
const tele_op_t op_SEQ_TRANSP =
    MAKE_GET_SET_OP(TRANSP, op_SEQ_NULL_get, op_SEQ_TRANSP_set, 0, false);
const tele_op_t op_SEQ_QNT =
    MAKE_GET_SET_OP(QNT, op_SEQ_NULL_get, op_SEQ_QNT_set, 0, false);
const tele_op_t op_SEQ_SLEW =
    MAKE_GET_SET_OP(SLEW, op_SEQ_NULL_get, op_SEQ_SLEW_set, 0, false);

const tele_mod_t mod_SEQ_PROB = MAKE_MOD(PROB, mod_SEQ_PROB_func, 0);
const tele_mod_t mod_SEQ_FAST = MAKE_MOD(FAST, mod_SEQ_FAST_func, 0);
const tele_mod_t mod_SEQ_SLOW = MAKE_MOD(SLOW, mod_SEQ_SLOW_func, 0);
const tele_mod_t mod_SEQ_STUT = MAKE_MOD(STUT, mod_SEQ_STUT_func, 0);
const tele_mod_t mod_SEQ_FRAC = MAKE_MOD(FRAC, mod_SEQ_FRAC_func, 0);
const tele_mod_t mod_SEQ_HARM = MAKE_MOD(HARM, mod_SEQ_HARM_func, 0);
const tele_mod_t mod_SEQ_TRANSP = MAKE_MOD(TRANSP, mod_SEQ_TRANSP_func, 0);
const tele_mod_t mod_SEQ_QNT = MAKE_MOD(QNT, mod_SEQ_QNT_func, 0);
const tele_mod_t mod_SEQ_SLEW = MAKE_MOD(SLEW, mod_SEQ_SLEW_func, 0);


static void mod_SEQ_func(Scene *ss, ExecState *es, CommandState *cs,
                         const Command *post_command) {
  auto seq_i = cs->pop() - 1;
  if (seq_i < 0) {
    seq_i = es->current()->script_number;
  }
  if (seq_i < 0 || (seq_i >= SEQ_COUNT)) {
    return;
  }
  ss->last_voice = seq_i;
  ss->voices[seq_i].seq.resetData();
  for (int i = 0; i < post_command->length; i++) {
    if (i >= MAX_SEQ_NOTES)
      break;
    if (post_command->data[i].tag != Espresso::ALDA_NOTE)
      break;
    ss->voices[seq_i].seq.unpackData(post_command->data[i]);
  }
  // @nate - why does this crash when Sequence is empty?
  clock_refresh_seq(ss->voices[seq_i].seq);
  // @nate - execute any remaining POST command (after SUB_SEP)
}

static void applyModSeq(Scene *ss, Sequence::ModifierName name, const Command *post_command) {
  int16_t vals[MAX_SEQ_NOTES] = {0};
  uint8_t vals_len = 0;
  for (int i = 0; i < post_command->length; i++) {
    if (i >= MAX_SEQ_NOTES)
      break;
    if (post_command->data[i].tag != Espresso::NUMBER && post_command->data[i].tag != Espresso::XNUMBER && post_command->data[i].tag != Espresso::BNUMBER && post_command->data[i].tag != Espresso::RNUMBER)
      break;
    vals[i] = post_command->data[i].value;
    vals_len++;
  }
  ss->voices[ss->last_voice].seq.applyModSeq(name, vals, vals_len);
}


static void op_SEQ_REV_set(const void *data, Scene *ss, ExecState *es,
                           CommandState *cs) {
  ss->voices[ss->last_voice].seq.applyMod(Sequence::ModReverse);
}

static void op_SEQ_SHUF_set(const void *data, Scene *ss, ExecState *es,
                            CommandState *cs) {
  ss->voices[ss->last_voice].seq.applyMod(Sequence::ModShuffle);
}

static void op_SEQ_FLIP_set(const void *data, Scene *ss, ExecState *es,
                             CommandState *cs) {
  ss->voices[ss->last_voice].seq.applyMod(Sequence::ModFlip);
}


static void op_SEQ_ROT_set(const void *data, Scene *ss, ExecState *es,
                           CommandState *cs) {
  auto r = cs->pop();
  ss->voices[ss->last_voice].seq.applyMod(Sequence::ModRotate, r);
}

static void op_SEQ_NROT_set(const void *data, Scene *ss, ExecState *es,
                            CommandState *cs) {
  auto r = cs->pop();
  ss->voices[ss->last_voice].seq.applyMod(Sequence::ModNRotate, r);
}

static void op_SEQ_TRUNC_set(const void *data, Scene *ss, ExecState *es,
                             CommandState *cs) {
  auto l = cs->pop();
  ss->voices[ss->last_voice].seq.applyMod(Sequence::ModTruncate, l);
}

static void op_SEQ_LOOP_set(const void *data, Scene *ss, ExecState *es,
                             CommandState *cs) {
  ss->voices[ss->last_voice].seq.applyMod(Sequence::ModLoop, cs->pop());
}


static void op_SEQ_PROB_set(const void *_data, Scene *ss, ExecState *es,
                            CommandState *cs) {
  auto p = cs->pop();
  ss->voices[ss->last_voice].seq.applyMod(
      Sequence::ModProbability,
      std::max<uint8_t>(0, std::min<uint8_t>(100, p)));
}

static void op_SEQ_FAST_set(const void *data, Scene *ss, ExecState *es,
                            CommandState *cs) {
  auto m = cs->pop();
  ss->voices[ss->last_voice].seq.applyMod(Sequence::ModFast, m);
}

static void op_SEQ_SLOW_set(const void *data, Scene *ss, ExecState *es,
                            CommandState *cs) {
  auto m = cs->pop();
  ss->voices[ss->last_voice].seq.applyMod(Sequence::ModSlow, m);
}

static void op_SEQ_STUT_set(const void *data, Scene *ss, ExecState *es,
                             CommandState *cs) {
  auto s = cs->pop();
  ss->voices[ss->last_voice].seq.applyMod(Sequence::ModStutter, s);
}

static void op_SEQ_FRAC_set(const void *data, Scene *ss, ExecState *es,
                             CommandState *cs) {
  auto s = cs->pop();
  ss->voices[ss->last_voice].seq.applyMod(Sequence::ModFrac, s);
}

static void op_SEQ_HARM_set(const void *data, Scene *ss, ExecState *es,
                             CommandState *cs) {
  auto s = cs->pop();
  ss->voices[ss->last_voice].seq.applyMod(Sequence::ModHarmonize, s);
}

static void op_SEQ_TRANSP_set(const void *data, Scene *ss, ExecState *es,
                             CommandState *cs) {
  auto s = cs->pop();
  ss->voices[ss->last_voice].seq.applyMod(Sequence::ModTranspose, s);
}

static void op_SEQ_QNT_set(const void *data, Scene *ss, ExecState *es,
                             CommandState *cs) {
  auto root = cs->pop();
  // Optional argument (default value 0 is intentional behavior)
  ss->voices[ss->last_voice].seq.scale_index = cs->pop();
  ss->voices[ss->last_voice].seq.applyMod(Sequence::ModQuantize, root);
}

static void op_SEQ_SLEW_set(const void *data, Scene *ss, ExecState *es,
                             CommandState *cs) {
  auto slew = cs->pop();
  if (cs->size()) {
    ss->voices[ss->last_voice].seq.applyMod(Sequence::ModSlewUp, slew);
    ss->voices[ss->last_voice].seq.applyMod(Sequence::ModSlewDown, cs->pop());
  } else {
    ss->voices[ss->last_voice].seq.applyMod(Sequence::ModSlew, slew);
  }
}

static void mod_SEQ_PROB_func(Scene *ss, ExecState *es, CommandState *cs,
                            const Command *post_command) {
  applyModSeq(ss, Sequence::ModProbability, post_command);
}

static void mod_SEQ_FAST_func(Scene *ss, ExecState *es, CommandState *cs,
                            const Command *post_command) {
  applyModSeq(ss, Sequence::ModFast, post_command);
}

static void mod_SEQ_SLOW_func(Scene *ss, ExecState *es, CommandState *cs,
                            const Command *post_command) {
  applyModSeq(ss, Sequence::ModSlow, post_command);
}

static void mod_SEQ_STUT_func(Scene *ss, ExecState *es, CommandState *cs,
                            const Command *post_command) {
  // who populates post_command?
  applyModSeq(ss, Sequence::ModStutter, post_command);
}

static void mod_SEQ_FRAC_func(Scene *ss, ExecState *es, CommandState *cs,
                            const Command *post_command) {
  applyModSeq(ss, Sequence::ModFrac, post_command);
}

static void mod_SEQ_HARM_func(Scene *ss, ExecState *es, CommandState *cs,
                            const Command *post_command) {
  applyModSeq(ss, Sequence::ModHarmonize, post_command);
}

static void mod_SEQ_TRANSP_func(Scene *ss, ExecState *es, CommandState *cs,
                            const Command *post_command) {
  applyModSeq(ss, Sequence::ModTranspose, post_command);
}

static void mod_SEQ_QNT_func(Scene *ss, ExecState *es, CommandState *cs,
                            const Command *post_command) {
  applyModSeq(ss, Sequence::ModQuantize, post_command);
}

static void mod_SEQ_SLEW_func(Scene *ss, ExecState *es, CommandState *cs,
                            const Command *post_command) {
  applyModSeq(ss, Sequence::ModSlew, post_command);
}
