#include "ops/controlflow.h"

#include <cstdint>
#include <espresso/command_state.h>
#include <espresso/exec_state.h>
#include <random.h>
#include <scene.h>

#include "teletype.h"
#include "teletype_io.h"


using Espresso::Command;
using Espresso::CommandState;
using Espresso::ExecState;
using Port::random_next;

static void mod_PROB_func(Scene *ss, ExecState *es, CommandState *cs,
                          const Command *post_command);
static void mod_IF_func(Scene *ss, ExecState *es, CommandState *cs,
                        const Command *post_command);
static void mod_ELIF_func(Scene *ss, ExecState *es, CommandState *cs,
                          const Command *post_command);
static void mod_ELSE_func(Scene *ss, ExecState *es, CommandState *cs,
                          const Command *post_command);
static void mod_L_func(Scene *ss, ExecState *es, CommandState *cs,
                       const Command *post_command);
static void mod_W_func(Scene *ss, ExecState *es, CommandState *cs,
                       const Command *post_command);
static void mod_EVERY_func(Scene *ss, ExecState *es, CommandState *cs,
                           const Command *post_command);
static void mod_SKIP_func(Scene *ss, ExecState *es, CommandState *cs,
                          const Command *post_command);
static void mod_MODULO_func(Scene *ss, ExecState *es, CommandState *cs,
                          const Command *post_command);
static void mod_OTHER_func(Scene *ss, ExecState *es, CommandState *cs,
                           const Command *post_command);

static void op_SCENE_get(const void *data, Scene *ss, ExecState *es,
                         CommandState *cs);
static void op_SCENE_set(const void *data, Scene *ss, ExecState *es,
                         CommandState *cs);
static void op_SCENE_P_get(const void *data, Scene *ss, ExecState *es,
                           CommandState *cs);
static void op_SCRIPT_get(const void *data, Scene *ss, ExecState *es,
                          CommandState *cs);
static void op_SCRIPT_set(const void *data, Scene *ss, ExecState *es,
                          CommandState *cs);
static void op_SCRIPT_POL_get(const void *data, Scene *ss, ExecState *es,
                              CommandState *cs);
static void op_SCRIPT_POL_set(const void *data, Scene *ss, ExecState *es,
                              CommandState *cs);
static void op_KILL_get(const void *data, Scene *ss, ExecState *es,
                        CommandState *cs);
static void op_BREAK_get(const void *data, Scene *ss, ExecState *es,
                         CommandState *cs);
static void op_SYNC_get(const void *data, Scene *ss, ExecState *es,
                        CommandState *cs);

// clang-format off
const tele_mod_t mod_PROB = MAKE_MOD(PROB, mod_PROB_func, 1);
const tele_mod_t mod_IF = MAKE_MOD(IF, mod_IF_func, 1);
const tele_mod_t mod_ELIF = MAKE_MOD(ELIF, mod_ELIF_func, 1);
const tele_mod_t mod_ELSE = MAKE_MOD(ELSE, mod_ELSE_func, 0);
const tele_mod_t mod_L = MAKE_MOD(L, mod_L_func, 2);
const tele_mod_t mod_W = MAKE_MOD(W, mod_W_func, 1);
const tele_mod_t mod_EVERY = MAKE_MOD(EVERY, mod_EVERY_func, 1);
const tele_mod_t mod_EV = MAKE_MOD(EV, mod_EVERY_func, 1);
const tele_mod_t mod_SKIP = MAKE_MOD(SKIP, mod_SKIP_func, 1);
const tele_mod_t mod_MODULO = MAKE_MOD(MOD, mod_MODULO_func, 2);
const tele_mod_t mod_OTHER = MAKE_MOD(OTHER, mod_OTHER_func, 0);

const tele_op_t op_SCRIPT = MAKE_GET_SET_OP(SCRIPT, op_SCRIPT_get, op_SCRIPT_set, 0, true);
const tele_op_t op_SYM_DOLLAR = MAKE_ALIAS_OP($, op_SCRIPT_get, op_SCRIPT_set, 0, true);
const tele_op_t op_SCRIPT_POL = MAKE_GET_SET_OP(SCRIPT.POL, op_SCRIPT_POL_get, op_SCRIPT_POL_set, 1, true);
const tele_op_t op_SYM_DOLLAR_POL = MAKE_ALIAS_OP($.POL, op_SCRIPT_POL_get, op_SCRIPT_POL_set, 1, true);
const tele_op_t op_KILL = MAKE_GET_OP(KILL, op_KILL_get, 0, false);
const tele_op_t op_SCENE_P = MAKE_GET_OP(SCENE.P, op_SCENE_P_get, 1, false);
const tele_op_t op_SCENE = MAKE_GET_SET_OP(SCENE, op_SCENE_get, op_SCENE_set, 0, true);
const tele_op_t op_BREAK = MAKE_GET_OP(BREAK, op_BREAK_get, 0, false);
const tele_op_t op_BRK = MAKE_ALIAS_OP(BRK, op_BREAK_get, NULL, 0, false);
const tele_op_t op_SYNC = MAKE_GET_OP(SYNC, op_SYNC_get, 1, false);
// clang-format on

static void mod_PROB_func(Scene *ss, ExecState *es, CommandState *cs,
                          const Command *post_command) {
  int16_t a = cs->pop();
  random_state_t *r = &ss->rand_states.prob.rand;

  if (static_cast<int16_t>(random_next(r) % 100) < a) {
    tele_process_command(ss, es, post_command);
  }
}

static void mod_IF_func(Scene *ss, ExecState *es, CommandState *cs,
                        const Command *post_command) {
  int16_t a = cs->pop();

  es->current()->if_else_condition = false;
  if (a) {
    es->current()->if_else_condition = true;
    tele_process_command(ss, es, post_command);
  }
}

static void mod_ELIF_func(Scene *ss, ExecState *es, CommandState *cs,
                          const Command *post_command) {
  int16_t a = cs->pop();

  if (!es->current()->if_else_condition) {
    if (a) {
      es->current()->if_else_condition = true;
      tele_process_command(ss, es, post_command);
    }
  }
}

static void mod_ELSE_func(Scene *ss, ExecState *es, CommandState *_cs,
                          const Command *post_command) {
  if (!es->current()->if_else_condition) {
    es->current()->if_else_condition = true;
    tele_process_command(ss, es, post_command);
  }
}

static void mod_L_func(Scene *ss, ExecState *es, CommandState *cs,
                       const Command *post_command) {
  int16_t a = cs->pop();
  int16_t b = cs->pop();

  // using a pointer means that the loop contents can a interact with the
  // iterator, allowing users to roll back a loop or advance it faster
  int16_t *i = &es->current()->i;
  *i = a;

  // Forward loop
  if (a < b) {
    // continue the loop whenever the _pointed-to_ I meets the condition
    // this means that I can be interacted with inside the loop command

    // iterate with higher precision to account for b == 32767
    for (int32_t l = a; l <= b; l++) {
      tele_process_command(ss, es, post_command);
      if (es->current()->breaking)
        break;
      // the increment statement has careful syntax, because the
      // ++ operator has precedence over the dereference * operator
      (*i)++;
    }

    if (!es->current()->breaking)
      (*i)--; // past end of loop, leave I in the correct state
  }
  // Reverse loop (also works for equal values (either loop would))
  else {
    for (int32_t l = a; l >= b && !es->current()->breaking; l--) {
      tele_process_command(ss, es, post_command);
      (*i)--;
    }
    if (!es->current()->breaking)
      (*i)++;
  }
}

static void mod_W_func(Scene *ss, ExecState *es, CommandState *cs,
                       const Command *post_command) {
  int16_t a = cs->pop();
  if (a) {
    tele_process_command(ss, es, post_command);
    es->current()->while_depth++;
    if (es->current()->while_depth < WHILE_DEPTH)
      es->current()->while_continue = true;
    else
      es->current()->while_continue = false;
  } else
    es->current()->while_continue = false;
}

static void mod_EVERY_func(Scene *ss, ExecState *es, CommandState *cs,
                           const Command *post_command) {
  int16_t mod = cs->pop();
  auto *every = ss->ss_get_every(es->current()->script_number,
                                          es->current()->line_number);
  ss->every_set_mod(every, mod);
  ss->every_tick(every);
  if (ss->every_is_now(every))
    tele_process_command(ss, es, post_command);
}

static void mod_SKIP_func(Scene *ss, ExecState *es, CommandState *cs,
                          const Command *post_command) {
  int16_t mod = cs->pop();
  auto *every = ss->ss_get_every(es->current()->script_number,
                                          es->current()->line_number);
  ss->every_set_mod(every, mod);
  ss->every_tick(every);
  if (ss->skip_is_now(every))
    tele_process_command(ss, es, post_command);
}

static void mod_MODULO_func(Scene *ss, ExecState *es, CommandState *cs,
                          const Command *post_command) {
  int16_t mod = cs->pop();
  int16_t divisor = cs->pop();
  auto *every = ss->ss_get_every(es->current()->script_number,
                                          es->current()->line_number);
  // % 5 3 -- run command last 2 of 5
  // % 3 5 -- run command first 3 of 5
  bool inverted = divisor > mod;
  if (inverted) {
    // use larger value is mod
    ss->every_set_mod(every, divisor);
    // use smaller value as divisor
    divisor = mod;
  } else {
    ss->every_set_mod(every, mod);
  }
  ss->every_tick(every);
  if (ss->modulo_is_now(every, divisor, inverted))
    tele_process_command(ss, es, post_command);
}


static void mod_OTHER_func(Scene *ss, ExecState *es, CommandState *_cs,
                           const Command *post_command) {
  if (!ss->every_last)
    tele_process_command(ss, es, post_command);
}

static void op_SYNC_get(const void *_data, Scene *ss, ExecState *_es,
                        CommandState *cs) {
  int16_t count = cs->pop();
  ss->every_last = false;
  ss->ss_sync_every(count);
}

static void op_SCENE_get(const void *_data, Scene *ss, ExecState *_es,
                         CommandState *cs) {
  cs->push(ss->variables.scene);
}

static void op_SCENE_set(const void *_data, Scene *ss, ExecState *_es,
                         CommandState *cs) {
  int16_t scene = cs->pop();
  if (!ss->initializing) {
    ss->variables.scene = scene;
    tele_scene(scene, true);
  }
}

static void op_SCENE_P_get(const void *_data, Scene *ss, ExecState *_es,
                           CommandState *cs) {
  int16_t scene = cs->pop();
  if (!ss->initializing) {
    ss->variables.scene = scene;
    tele_scene(scene, false);
  }
}

static void op_SCRIPT_get(const void *_data, Scene *ss, ExecState *es,
                          CommandState *cs) {
  int16_t sn = es->current()->script_number + 1;
  if (sn > (INIT_SCRIPT + 1))
    sn = 0;
  cs->push(sn);
}

static void op_SCRIPT_set(const void *_data, Scene *ss, ExecState *es,
                          CommandState *cs) {
  uint16_t a = cs->pop() - 1;
  if (a > INIT_SCRIPT || a < TT_SCRIPT_1)
    return;
  const script_number_t &script_no = static_cast<script_number_t>(a);

  es->push();
  // an overflow causes all future SCRIPT calls to fail
  // indicates a bad user script
  if (!es->overflow)
    tele_run_script(ss, es, script_no);
  es->pop();
}

static void op_SCRIPT_POL_get(const void *_data, Scene *ss, ExecState *_es,
                              CommandState *cs) {
  uint16_t a = cs->pop() - 1;
  if (a > TT_SCRIPT_8 || a < TT_SCRIPT_1) {
    cs->push(0);
    return;
  }
  cs->push(ss->ss_get_script_pol(a));
}

static void op_SCRIPT_POL_set(const void *_data, Scene *ss, ExecState *_es,
                              CommandState *cs) {
  uint8_t a = cs->pop();
  uint8_t pol = cs->pop();
  if (pol > 3)
    return;
  if (a == 0) {
    for (uint8_t i = 0; i < 8; i++) {
      ss->ss_set_script_pol(i, pol);
    }
  } else {
    uint8_t s = a - 1;
    if (s >= TT_SCRIPT_1 && s <= TT_SCRIPT_8) {
      ss->ss_set_script_pol(s, pol);
    }
  }
}

static void op_KILL_get(const void *_data, Scene *ss, ExecState *_es,
                        CommandState *_cs) {
  // clear stack
  ss->stack_op.top = 0;
  tele_has_stack(false);
  // disable metronome - @nate settings.isRunning ?
  ss->variables.m_act = 0;
  tele_metro_updated();
  tele_kill();
}

static void op_BREAK_get(const void *_data, Scene *_ss, ExecState *es,
                         CommandState *_cs) {
  es->current()->breaking = true;
}
