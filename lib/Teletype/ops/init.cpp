#include "ops/init.h"

#include <string.h> // memset()

#include <espresso/command_state.h>
#include <scene.h>

#include "teletype.h"
#include "teletype_io.h"

using Espresso::CommandState;
using Espresso::ExecState;

static void op_INIT_get(const void *data, Scene *ss, ExecState *es,
                        CommandState *cs);
static void op_INIT_SCENE_get(const void *data, Scene *ss, ExecState *es,
                              CommandState *cs);
static void op_INIT_SCRIPT_get(const void *data, Scene *ss, ExecState *es,
                               CommandState *cs);
static void op_INIT_SCRIPT_ALL_get(const void *data, Scene *ss, ExecState *es,
                                   CommandState *cs);
static void op_INIT_CV_get(const void *data, Scene *ss, ExecState *es,
                           CommandState *cs);
static void op_INIT_CV_ALL_get(const void *data, Scene *ss, ExecState *es,
                               CommandState *cs);
static void op_INIT_DATA_get(const void *data, Scene *ss, ExecState *es,
                             CommandState *cs);
static void op_INIT_TIME_get(const void *data, Scene *ss, ExecState *es,
                             CommandState *cs);

const tele_op_t op_INIT = MAKE_GET_OP(INIT, op_INIT_get, 0, false);
const tele_op_t op_INIT_SCENE =
    MAKE_GET_OP(INIT.SCENE, op_INIT_SCENE_get, 0, false);
const tele_op_t op_INIT_SCRIPT =
    MAKE_GET_OP(INIT.SCRIPT, op_INIT_SCRIPT_get, 1, false);
const tele_op_t op_INIT_SCRIPT_ALL =
    MAKE_GET_OP(INIT.SCRIPT.ALL, op_INIT_SCRIPT_ALL_get, 0, false);
const tele_op_t op_INIT_CV = MAKE_GET_OP(INIT.CV, op_INIT_CV_get, 1, false);
const tele_op_t op_INIT_CV_ALL =
    MAKE_GET_OP(INIT.CV.ALL, op_INIT_CV_ALL_get, 0, false);
const tele_op_t op_INIT_DATA =
    MAKE_GET_OP(INIT.DATA, op_INIT_DATA_get, 0, false);
const tele_op_t op_INIT_TIME =
    MAKE_GET_OP(INIT.TIME, op_INIT_TIME_get, 0, false);

static void op_INIT_get(const void *_data, Scene *ss, ExecState *_es,
                        CommandState *_cs) {

  ss->hardReset();
  tele_vars_updated();
  tele_metro_updated();
}

static void op_INIT_SCENE_get(const void *_data, Scene *ss, ExecState *_es,
                              CommandState *_cs) {
  return op_INIT_get(_data, ss, _es, _cs);
}

static void op_INIT_SCRIPT_get(const void *_data, Scene *ss, ExecState *_es,
                               CommandState *cs) {
  int16_t v = cs->pop() - 1;
  if (v >= 0 && v < TEMP_SCRIPT)
    ss->ss_clear_script(v);
}

static void op_INIT_SCRIPT_ALL_get(const void *_data, Scene *ss, ExecState *_es,
                                   CommandState *_cs) {
  for (size_t i = 0; i < TEMP_SCRIPT; i++)
    ss->ss_clear_script(i);
}

static void op_INIT_CV_get(const void *_data, Scene *ss, ExecState *_es,
                           CommandState *cs) {
  int16_t v = cs->pop() - 1;
  if (v >= 0 && v < TR_COUNT) {
    ss->variables.cv[v] = 0;
    ss->variables.cv_off[v] = 0;
    ss->variables.cv_slew[v] = 1;
    tele_cv(v, 0, 1);
  }
}

static void op_INIT_CV_ALL_get(const void *_data, Scene *ss, ExecState *_es,
                               CommandState *_cs) {
  for (size_t i = 0; i < TR_COUNT; i++) {
    ss->variables.cv[i] = 0;
    ss->variables.cv_off[i] = 0;
    ss->variables.cv_slew[i] = 1;
    tele_cv(i, 0, 1);
  }
}

static void op_INIT_DATA_get(const void *_data, Scene *ss, ExecState *_es,
                             CommandState *_cs) {
  ss->ss_variables_init();
  tele_vars_updated();
  tele_metro_updated();
}

static void op_INIT_TIME_get(const void *_data, Scene *ss, ExecState *_es,
                             CommandState *_cs) {
  ss->variables.time = 0;
  uint32_t ticks = tele_get_ticks();
  for (uint8_t i = 0; i < TEMP_SCRIPT; i++)
    ss->scripts[i].last_time = ticks;
  ss->variables.time = 0;
  ss->ss_sync_every(0);
}
