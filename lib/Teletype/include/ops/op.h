#ifndef _OPS_OP_H_
#define _OPS_OP_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "ops/op_enum.h"
#include "types.h"

class Scene;

namespace Espresso {
  class Command;
  class CommandState;
  class ExecState;
}

struct tele_op_t {
  const char *name;
  void (*const get)(const void *data, Scene *ss, Espresso::ExecState *es,
                    Espresso::CommandState *cs);
  void (*const set)(const void *data, Scene *ss, Espresso::ExecState *es,
                    Espresso::CommandState *cs);
  const uint8_t params;
  const bool returns;
  const void *data;
};

struct tele_mod_t {
  const char *name;
  void (*const func)(Scene *ss, Espresso::ExecState *es, Espresso::CommandState *cs,
                     const Espresso::Command *post_command);
  const uint8_t params;
  const uint8_t opt_params;
};

extern const tele_op_t *tele_ops[E_OP__LENGTH];
extern const tele_mod_t *tele_mods[E_MOD__LENGTH];

// Get only ops
#define MAKE_GET_OP(n, g, p, r)                                                \
  { .name = #n, .get = g, .set = NULL, .params = p, .returns = r, .data = NULL }

// Get & set ops
#define MAKE_GET_SET_OP(n, g, s, p, r)                                         \
  { .name = #n, .get = g, .set = s, .params = p, .returns = r, .data = NULL }
#define MAKE_GET_SET_OP_CONTEXT(n, g, s, p, r, d)                              \
  { .name = #n, .get = g, .set = s, .params = p, .returns = r, .data = (void *)d }

// Variables, peek & poke
#define MAKE_SIMPLE_VARIABLE_OP(n, v)                                          \
  {                                                                            \
    .name = #n, .get = op_peek_i16, .set = op_poke_i16, .params = 0,           \
    .returns = 1, .data = (void *)offsetof(scene_variables_t, v)               \
  }

void op_peek_i16(const void *data, Scene *ss, Espresso::ExecState *es, Espresso::CommandState *cs);
void op_poke_i16(const void *data, Scene *ss, Espresso::ExecState *es, Espresso::CommandState *cs);

// Alias one OP to another
#define MAKE_ALIAS_OP(n, g, s, p, r)                                           \
  { .name = #n, .get = g, .set = s, .params = p, .returns = r, .data = NULL }

// Mods
#define MAKE_MOD_OPT(n, f, p, o_p)                                             \
  {                                                                            \
    .name = #n, .func = f, .params = p,                                        \
    .opt_params = o_p                                                          \
  }

#define MAKE_MOD(n, f, p) MAKE_MOD_OPT(n, f, p, 0)


#endif
