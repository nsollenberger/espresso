#ifndef _OPS_VOICE_H_
#define _OPS_VOICE_H_

#include "ops/op.h"

extern const tele_op_t op_V_CV;
extern const tele_op_t op_V_EG;
extern const tele_op_t op_V_GATE;
extern const tele_op_t op_V_MIDI;
extern const tele_op_t op_V_PLAY;
extern const tele_op_t op_V_PAUSE;
extern const tele_op_t op_V_MUTE;
extern const tele_op_t op_V_RESET;
extern const tele_op_t op_V_LOOP;
extern const tele_op_t op_V_OCT;
// extern const tele_op_t op_V_MONO;
// extern const tele_op_t op_V_MONO_LAST;
// extern const tele_op_t op_V_MONO_HIGH;
// extern const tele_op_t op_V_MONO_LOW;
extern const tele_op_t op_V_LENGTH;
extern const tele_op_t op_V_VEL;
extern const tele_op_t op_V_ENV;
// extern const tele_op_t op_V_INIT;


#endif
