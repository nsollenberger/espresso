#ifndef INIT_H
#define INIT_H

#include "ops/op.h"

extern const tele_op_t op_INIT;
extern const tele_op_t op_INIT_SCENE;
extern const tele_op_t op_INIT_SCRIPT;
extern const tele_op_t op_INIT_SCRIPT_ALL;
extern const tele_op_t op_INIT_CV;
extern const tele_op_t op_INIT_CV_ALL;
extern const tele_op_t op_INIT_DATA;
extern const tele_op_t op_INIT_TIME;

#endif
