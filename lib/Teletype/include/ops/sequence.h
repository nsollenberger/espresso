#ifndef _OPS_SEQUENCE_H_
#define _OPS_SEQUENCE_H_

#include "ops/op.h"

extern const tele_mod_t mod_SEQ;

extern const tele_op_t op_SEQ_REV;
extern const tele_op_t op_SEQ_SHUF;
extern const tele_op_t op_SEQ_FLIP;

extern const tele_op_t op_SEQ_ROT;
extern const tele_op_t op_SEQ_NROT;
extern const tele_op_t op_SEQ_TRUNC;
extern const tele_op_t op_SEQ_LOOP;

extern const tele_op_t op_SEQ_PROB;
extern const tele_op_t op_SEQ_FAST;
extern const tele_op_t op_SEQ_SLOW;
extern const tele_op_t op_SEQ_STUT;
extern const tele_op_t op_SEQ_FRAC;
extern const tele_op_t op_SEQ_HARM;
extern const tele_op_t op_SEQ_TRANSP;
extern const tele_op_t op_SEQ_QNT;
extern const tele_op_t op_SEQ_SLEW;

extern const tele_mod_t mod_SEQ_PROB;
extern const tele_mod_t mod_SEQ_FAST;
extern const tele_mod_t mod_SEQ_SLOW;
extern const tele_mod_t mod_SEQ_STUT;
extern const tele_mod_t mod_SEQ_FRAC;
extern const tele_mod_t mod_SEQ_HARM;
extern const tele_mod_t mod_SEQ_TRANSP;
extern const tele_mod_t mod_SEQ_QNT;
extern const tele_mod_t mod_SEQ_SLEW;

#endif
