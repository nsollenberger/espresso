#ifndef TYPES_H__
#define TYPES_H__

#include <stdint.h>

enum script_number_t : int8_t {
  TT_SCRIPT_1 = 0,
  TT_SCRIPT_2,
  TT_SCRIPT_3,
  TT_SCRIPT_4,
  TT_SCRIPT_5,
  TT_SCRIPT_6,
  TT_SCRIPT_7,
  TT_SCRIPT_8,
  INIT_SCRIPT,
  TEMP_SCRIPT
};

struct process_result_t {
  bool has_value;
  int16_t value;
};

#endif
