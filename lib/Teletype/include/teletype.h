#ifndef _TELETYPE_H_
#define _TELETYPE_H_

#include <table.h>

#include "teletype_io.h"
#include "types.h"

class Scene;

namespace Espresso {
  class Command;
  class ExecState;
}

class Teletype {
public:

  // execute(script_number_t);
  process_result_t run_script(Scene &scene, script_number_t script_no);

  // apply(ExecState, script_number_t);
  process_result_t run_script(Scene &ss, Espresso::ExecState &es,
                              script_number_t script_no);

  // apply(ExecState, Command);
  process_result_t process_command(Scene &ss, Espresso::ExecState &es,
                                   const Espresso::Command cmd);
};

#endif
