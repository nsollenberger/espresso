#ifndef _TELETYPE_IO_H_
#define _TELETYPE_IO_H_

#include "types.h"
#include <stdbool.h>
#include <stdint.h>

#define SUB_MODE_OFF 0
#define SUB_MODE_VARS 1
#define SUB_MODE_DASH 2

// These functions are an escape hatch for OPs to access/interact with
// "global state"

class Scene; // Forward declarations
class Sequence;
namespace Espresso {
  class Command;
  class ExecState;
}

void tele_run_script(Scene *ss, Espresso::ExecState *es,
                                     script_number_t script_no);
process_result_t tele_process_command(Scene *ss, Espresso::ExecState *es,
                                      const Espresso::Command *cmd);

// used for TIME and LAST
uint32_t tele_get_ticks(void);

// called when M or M.ACT are updated
void tele_metro_updated(void);

// called by M.RESET
void tele_metro_reset(void);

void tele_tr(uint8_t i, int16_t v);
void tele_cv(uint8_t i, int16_t v, uint8_t s);
void tele_cv_slew(uint8_t i, int16_t v);
void tele_env_config(uint8_t i, uint16_t attack, uint16_t decay, uint16_t sustain, uint16_t release);
void tele_env(uint8_t i, bool high);

void tele_update_adc(bool force);

// inform target if the stack has entries
void tele_has_stack(bool has_stack);

void tele_cv_off(uint8_t i, int16_t v);
void tele_scene(uint8_t sceneIndex, bool loadPattern);

void tele_vars_updated(void);

void tele_kill(void);
bool tele_get_input_state(uint8_t);

void tele_save_calibration(void);

// manage device config
void device_flip(void);

void send_midi_note(uint8_t ch, uint8_t note, uint8_t vel);
void send_midi_cc(uint8_t ch, uint8_t cc_num, uint8_t cc_val);
void send_midi_clock();

void clock_reset_voice(int i);
void clock_refresh_voice(int i);
void clock_refresh_seq(Sequence &seq);

#endif
