#include "teletype.h"

#include <algorithm>
#include <stdint.h>
#include <string.h>

#include <espresso/command.h>
#include <espresso/parser.h>
#include <espresso/command_state.h>
#include <espresso/exec_state.h>
#include <scene.h>

#include "ops/op.h"
#include "ops/op_enum.h"
#include "teletype_io.h"

using Espresso::Command;
using Espresso::CommandState;
using Espresso::ExecState;

/////////////////////////////////////////////////////////////////
// RUN //////////////////////////////////////////////////////////

process_result_t Teletype::run_script(Scene &scene, script_number_t script_no) {
  ExecState es;
  es.push();
  return run_script(scene, es, script_no);
}

// Everything needs to call this to execute code.  An execution
// context is required for proper operation of DEL, THIS, L, W, IF
process_result_t Teletype::run_script(Scene &ss, ExecState &es,
                                            script_number_t script_no) {
  process_result_t result = {.has_value = false, .value = 0};

  es.setScript(script_no);

  for (size_t i = 0; i < ss.ss_get_script_len(script_no); i++) {
    es.setLine(i);

    // Commented code doesn't run.
    if (ss.ss_get_script_comment(script_no, i))
      continue;

    // BREAK implemented with break...
    if (es.current()->breaking)
      break;
    do {
      // TODO: Check for 0-length commands before we bother?
      result = process_command(ss, es, *ss.ss_get_script_command(script_no, i));
      // and WHILE implemented with while!
    } while (es.current()->while_continue &&
             !es.current()->breaking);
  }

  es.current()->breaking = false;
  ss.ss_update_script_last(script_no);

  return result;
}

/////////////////////////////////////////////////////////////////
// PROCESS //////////////////////////////////////////////////////

// run a single command inside a given exec_state
process_result_t Teletype::process_command(Scene &ss, ExecState &es,
                                 const Command cmd) {
  CommandState cs; // initialise this here as well as inside the loop, in case
                   // the command has 0 length

  // 1. Do we have a PRE seperator?
  // ------------------------------
  // if we do then only process the PRE part, the MOD will determine if the
  // POST should be run and take care of running it
  int end_idx = cmd.separator == -1 ? cmd.length : cmd.separator;

  // 2. Determine the location of all the SUB commands
  // -------------------------------------------------
  // an array of structs to hold the start and end of each sub command
  struct sub_idx {
    int start;
    int end;
  } subs[ESPRESSO_COMMAND_MAX_LENGTH];

  int sub_len = 0;
  int sub_start = 0;

  // iterate through cmd.data to find all the SUB_SEPs and add to the array
  for (int idx = 0; idx < end_idx; idx++) {
    Espresso::token_type word_type = cmd.data[idx].tag;
    if (word_type == Espresso::SUB_SEP && idx > sub_start) {
      subs[sub_len].start = sub_start;
      subs[sub_len].end = idx - 1;
      sub_len++;
      sub_start = idx + 1;
    }
  }

  // the last sub command won't have been added, manually add it here
  if (end_idx > sub_start) {
    subs[sub_len].start = sub_start;
    subs[sub_len].end = end_idx - 1;
    sub_len++;
  }

  // 3. Loop through each sub command and execute it
  // -----------------------------------------------
  // iterate through sub commands from left to right
  for (int sub_idx = 0; sub_idx < sub_len && !es.current()->breaking;
       sub_idx++) {
    const int sub_start = subs[sub_idx].start;
    const int sub_end = subs[sub_idx].end;
    int parens_idx = -1;

    // initialise the command state for each sub, otherwise a value left on
    // the stack for the previous sub, can cause the set fn to trigger when
    // it shouldn't
    cs.reset();

    // as we are using a stack based language, we must process commands from
    // right to left
    for (int idx = sub_end; idx >= sub_start; idx--) {
      const Espresso::token_type word_type = cmd.data[idx].tag;
      const int16_t word_value = cmd.data[idx].value;

      if (word_type == Espresso::BRACKETS) {
        auto *every = ss.ss_get_every(es.current()->script_number,
                                       es.current()->line_number);
        ss.every_set_mod(every, word_value);
        ss.every_tick(every);

        int16_t value = 0;
        // Remove all BRACKET values from stack
        for (int b = 0; b < word_value; b++) {
          if (every->count == b) value = cs.pop();
          else cs.pop();
        }
        // Leave only 1 value
        cs.push(value);
      } else if (word_type == Espresso::PARENS) {
        parens_idx = idx;
      } else if (word_type == Espresso::NUMBER || word_type == Espresso::XNUMBER || word_type == Espresso::BNUMBER ||
          word_type == Espresso::RNUMBER) {
        cs.push(word_value);
      } else if (word_type == Espresso::OP) {
        const tele_op_t *op = tele_ops[word_value];

        // if we're in the first command position, and there is a set fn
        // pointer and we have enough params, then run set, else run get
        if (idx == sub_start && op->set != NULL &&
            cs.size() >= op->params + 1)
          op->set(op->data, &ss, &es, &cs);
        else
          op->get(op->data, &ss, &es, &cs);
      } else if (word_type == Espresso::MOD) {
        // Standard MOD, copy everything after the separator
        uint8_t copy_offset = cmd.separator;
        int copy_len = -1;
        // Only copy what is inside the parens
        if (parens_idx > -1) {
          copy_offset = parens_idx;
          copy_len = cmd.data[parens_idx].value;
        }
        Command post_command(cmd, copy_offset, copy_len);
        tele_mods[word_value]->func(&ss, &es, &cs, &post_command);
      }
    }
  }

  // 4. Return
  // ---------
  // sometimes we have single value left of the stack, if so return it
  if (cs.size()) {
    process_result_t o = {.has_value = true, .value = cs.pop()};
    return o;
  } else {
    process_result_t o = {.has_value = false, .value = 0};
    return o;
  }
}

