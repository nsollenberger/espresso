#include "keyboard_helper.h"

constexpr uint8_t SHIFT = HID_MODIFIER_LEFT_SHIFT | HID_MODIFIER_RIGHT_SHIFT;

uint8_t hid_to_ascii(uint8_t data, uint8_t mod) {
  /* upper row of the keyboard, numbers and special symbols */
  if (data >= 0x1e && data <= 0x27) {
    // shift key pressed
    if (mod & SHIFT) {
      switch (data) {
      case HID_1:
        return (0x21);
      case HID_2:
        return (0x40);
      case HID_3:
        return (0x23);
      case HID_4:
        return (0x24);
      case HID_5:
        return (0x25);
      case HID_6:
        return (0x5e);
      case HID_7:
        return (0x26);
      case HID_8:
        return (0x2a);
      case HID_9:
        return (0x28);
      case HID_0:
        return (0x29);
      }
    }
    // numbers
    else {
      if (data == 0x27) { // zero
        return (0x30);
      } else {
        return (data + 0x13);
      }
    }
  }

  /* Letters a-z */
  if (data >= 0x04 && data <= 0x1d) {
    // if((( capsLock == true ) && ( mod & SHIFT ) == 0 ) || (( capsLock ==
    // false ) && ( mod & SHIFT ))) {  //upper case
    return (data + 0x3d);
    // }
    // else {  //lower case
    // 	return( data + 0x5d );
    // }
  }

  /* Other special symbols */
  if (data >= 0x2c && data <= 0x38) {
    switch (data) {
    case HID_SPACEBAR:
      return (0x20);
    case HID_MINUS:
      if ((mod & SHIFT) == false) {
        return (0x2d);
      } else {
        return (0x5f);
      }
    case HID_EQUAL:
      if ((mod & SHIFT) == false) {
        return (0x3d);
      } else {
        return (0x2b);
      }
    case HID_OPEN_BRACKET:
      if ((mod & SHIFT) == false) {
        return (0x5b);
      } else {
        return (0x7b);
      }
    case HID_CLOSE_BRACKET:
      if ((mod & SHIFT) == false) {
        return (0x5d);
      } else {
        return (0x7d);
      }
    case HID_BACKSLASH:
      if ((mod & SHIFT) == false) {
        return (0x5c);
      } else {
        return (0x7c);
      }
    case HID_SEMICOLON:
      if ((mod & SHIFT) == false) {
        return (0x3b);
      } else {
        return (0x3a);
      }
    case HID_APOS:
      if ((mod & SHIFT) == false) {
        return (0x27);
      } else {
        return (0x22);
      }
    case HID_GRAVE:
      if ((mod & SHIFT) == false) {
        return (0x60);
      } else {
        return (0x7e);
      }
    case HID_COMMA:
      if ((mod & SHIFT) == false) {
        return (0x2c);
      } else {
        return (0x3c);
      }
    case HID_DOT:
      if ((mod & SHIFT) == false) {
        return (0x2e);
      } else {
        return (0x3e);
      }
    case HID_SLASH:
      if ((mod & SHIFT) == false) {
        return (0x2f);
      } else {
        return (0x3f);
      }
    default:
      break;
    }
  }

  return (0);
}