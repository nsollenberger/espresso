#ifndef _CHAOS_STATE_H__
#define _CHAOS_STATE_H__

#include <stdint.h>

// constants defining I/O ranges
constexpr int16_t chaos_value_min = -10000;
constexpr int16_t chaos_value_max = 10000;
constexpr int16_t chaos_param_min = 0;
constexpr int16_t chaos_param_max = 10000;
// fixed beta for henon map
constexpr float chaos_henon_b = 0.3;
// cellular automata parameters (1-d, binary)
constexpr int chaos_cell_count = 8;
constexpr int chaos_cell_max = 0xff;

enum chaos_algo_t {
  CHAOS_ALGO_LOGISTIC, // logistic map
  CHAOS_ALGO_CUBIC,    // cubic map
  CHAOS_ALGO_HENON,    // henon map
  CHAOS_ALGO_CELLULAR, // 1-d binary cellular automaton
  CHAOS_ALGO_COUNT     // unused, don't remve
};

class Chaos {
public:
  Chaos(void) {
    hardReset();
  }

  void hardReset(void) {
    ix = 5000;
    ir = 5000;
    alg = CHAOS_ALGO_LOGISTIC;
    chaos_scale_values();
  }

  void chaos_set_val(int16_t);
  int16_t chaos_get_val(void);
  void chaos_set_r(int16_t);
  int16_t chaos_get_r(void);
  void chaos_set_alg(chaos_algo_t);
  chaos_algo_t chaos_get_alg(void);

private:
  int16_t ix;       // state value in integer format
  float fx;         // normalized floating point state value (as needed)
  int16_t ir;       // parameter value in integer format
  float fr;         // floating-point parm value (as needed)
  float fx0;        // state history (as needed)
  float fx1;        // state history (as needed)
  chaos_algo_t alg; // current algorithm

  int16_t cellular_get_val(void);
  int16_t logistic_get_val(void);
  int16_t cubic_get_val(void);
  int16_t henon_get_val(void);
  void chaos_scale_values(void);
};

#endif