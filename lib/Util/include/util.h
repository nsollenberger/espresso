#ifndef _UTIL_H_
#define _UTIL_H_

#include <stdint.h>

namespace Util {

int16_t bit_reverse(int16_t unreversed, int8_t bits_to_reverse);
int16_t rev_bitstring_to_int(const char *token);

char *itoa(int value, char *result, int base);
void itoa_hex(uint16_t value, char *out);
void itoa_bin(uint16_t value, char *out);
void itoa_rbin(uint16_t value, char *out);

int16_t normalise_value(int16_t min, int16_t max, int16_t wrap, int16_t value);

} // namespace Util

#endif
