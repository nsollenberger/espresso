#include "chaos.h"

// scale integer state and param values to float,
// as appropriate for current algorithm
void Chaos::chaos_scale_values(void) {
  switch (alg) {
  case CHAOS_ALGO_HENON:
    // for henon, x in [-1.5, 1.5], r in [1, 1.4]
    fx = ix / (float)chaos_value_max * 1.5;
    fr = 1.f + ir / (float)chaos_param_max * 0.4;
    if (fr < 1.f) {
      fr = 1.f;
    }
    if (fr > 1.4) {
      fr = 1.4f;
    }
    break;
  case CHAOS_ALGO_CELLULAR:
    // 1d binary CA takes binary state and rule
    if (ix > chaos_cell_max) {
      ix = chaos_cell_max;
    }
    if (ix < 0) {
      ix = 0;
    }
    // rule is 8 bits
    if (ir > 0xff) {
      ir = 0xff;
    }
    if (ir < 0) {
      ir = 0;
    }
    break;
  case CHAOS_ALGO_CUBIC:
  case CHAOS_ALGO_LOGISTIC: // fall through
  default:
    // for cubic / logistic, x in [-1, 1] and r in [3.2, 4)
    fx = ix / (float)chaos_value_max;
    fr = ir / (float)chaos_param_max * 0.9999 + 3.0;
    break;
  }
}

void Chaos::chaos_set_val(int16_t val) {
  ix = val;
  chaos_scale_values();
}

int16_t Chaos::logistic_get_val(void) {
  if (fx < 0.f) {
    fx = 0.f;
  }
  fx = fx * fr * (1.f - fx);
  ix = fx * (float)chaos_value_max;
  return ix;
}

int16_t Chaos::cubic_get_val(void) {
  float x3 = fx * fx * fx;
  fx = fr * x3 + fx * (1.f - fr);
  ix = fx * (float)chaos_value_max;
  return ix;
}

int16_t Chaos::henon_get_val(void) {
  float x0_2 = fx0 * fx0;
  float x = 1.f - (x0_2 * fr) + (chaos_henon_b * fx1);
  // reflect bounds to avoid blowup
  while (x < -1.5) {
    x = -1.5 - x;
  }
  while (x > 1.5) {
    x = 1.5 - x;
  }
  fx1 = fx0;
  fx0 = fx;
  fx = x;
  ix = x / 1.5 * (float)chaos_value_max;
  return ix;
}

int16_t Chaos::cellular_get_val(void) {
  uint8_t x = (uint8_t)ix;
  uint8_t y = 0;
  uint8_t code = 0;
  for (int i = 0; i < chaos_cell_count; ++i) {
    // 3-bit code representing cell and neighbor state
    code = 0;
    // LSb in neighbor code = right-side neighor, wrapping
    if (i == 0) {
      if (x & 0x80) {
        code |= 0b001;
      }
    } else {
      if (x & (1 << (i - 1))) {
        code |= 0b001;
      }
    }
    // MSb in neighbor code = left-side neighbor, wrapping
    if (i == chaos_cell_count - 1) {
      if (x & 1) {
        code |= 0b100;
      }
    } else {
      if (x & (1 << (i + 1))) {
        code |= 0b100;
      }
    }
    // middle bit = old value of this cell
    if (x & (1 << i)) {
      code |= 0b010;
    }
    // lookup the bit in the rule specified by this code;
    // this is the new bit value
    if (ir & (1 << code)) {
      y |= (1 << i);
    }
  }
  ix = y;
  return ix;
}

int16_t Chaos::chaos_get_val(void) {
  switch (alg) {
  case CHAOS_ALGO_LOGISTIC:
    return logistic_get_val();
  case CHAOS_ALGO_CUBIC:
    return cubic_get_val();
  case CHAOS_ALGO_HENON:
    return henon_get_val();
  case CHAOS_ALGO_CELLULAR:
    return cellular_get_val();
  default:
    return 0;
  }
}

void Chaos::chaos_set_r(int16_t r) {
  ir = r;
  chaos_scale_values();
}

int16_t Chaos::chaos_get_r(void) { return ir; }

void Chaos::chaos_set_alg(chaos_algo_t a) {
  // if (a < 0) { a = 0; }
  // if (a >= CHAOS_ALGO_COUNT) { a = CHAOS_ALGO_COUNT - 1; }
  alg = a;
  chaos_scale_values();
}

chaos_algo_t Chaos::chaos_get_alg(void) { return alg; }
