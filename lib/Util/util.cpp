#include "util.h"
#include <string.h>

// http://www.jb.man.ac.uk/~slowe/cpp/itoa.html
// http://embeddedgurus.com/stack-overflow/2009/06/division-of-integers-by-constants/
// http://codereview.blogspot.com/2009/06/division-of-integers-by-constants.html
// http://homepage.cs.uiowa.edu/~jones/bcd/divide.html
/**
 * C++ version 0.4 char* style "itoa":
 * Written by Lukás Chmela
 * Released under GPLv3.
 */

char *Util::itoa(int value, char *result, int base) {
  // check that the base if valid
  // removed for optimization
  // if (base < 2 || base > 36) { *result = '\0'; return result; }

  char *ptr = result, *ptr1 = result, tmp_char;
  int tmp_value;
  uint8_t inv = 0;

  // add: opt crashes on negatives
  if (value < 0) {
    value = -value;
    inv++;
  }

  do {
    tmp_value = value;
    // opt-hack for base 10 assumed
    // value = (((uint16_t)value * (uint16_t)0xCD) >> 8) >> 3;
    value = (((uint32_t)value * (uint32_t)0xCCCD) >> 16) >> 3;
    // value /= base;
    *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrst"
             "uvwxyz"[35 + (tmp_value - value * base)];
  } while (value);

  // Apply negative sign
  if (inv)
    *ptr++ = '-';
  *ptr-- = '\0';
  while (ptr1 < ptr) {
    tmp_char = *ptr;
    *ptr-- = *ptr1;
    *ptr1++ = tmp_char;
  }
  return result;
}

void Util::itoa_hex(uint16_t value, char *out) {
  static char num[] = "0123456789ABCDEF";

  out[0] = 'X';
  uint8_t v, index = 1, dont_ignore_zeros = 0;

  for (int8_t i = 3; i >= 0; i--) {
    v = (value >> (i << 2)) & 0xf;
    if (dont_ignore_zeros || v) {
      out[index++] = num[v];
      dont_ignore_zeros = 1;
    }
  }

  if (!dont_ignore_zeros)
    out[index++] = '0';
  out[index] = '\0';
}

void Util::itoa_bin(uint16_t value, char *out) {
  out[0] = 'B';
  uint8_t v, index = 1, dont_ignore_zeros = 0;

  for (int8_t i = 15; i >= 0; i--) {
    v = (value >> i) & 1;
    if (dont_ignore_zeros || v) {
      out[index++] = '0' + v;
      dont_ignore_zeros = 1;
    }
  }

  if (!dont_ignore_zeros)
    out[index++] = '0';
  out[index] = '\0';
}

void Util::itoa_rbin(uint16_t value, char *out) {
  out[0] = 'R';
  uint8_t v, index = 1;

  for (int8_t i = 0; i < 16; i++) {
    v = (value >> i) & 1;
    out[index++] = '0' + v;
  }

  index = 0;
  for (int8_t i = 16; i > 0; i--) {
    if (out[i] == '1') {
      index = i;
      break;
    }
  }
  if (index == 0) {
    index = 1;
    out[index] = '0';
  }
  out[index + 1] = '\0';
}

int16_t Util::bit_reverse(int16_t unreversed, int8_t bits_to_reverse) {
  int16_t reversed = 0;
  for (int i = 0; i < bits_to_reverse; i++) {
    if ((unreversed & (1 << i)))
      reversed |= 1 << ((bits_to_reverse - 1) - i);
  }
  return reversed;
}

int16_t Util::rev_bitstring_to_int(const char *token) {
  int8_t length = strlen(token);
  int16_t value = 0;
  for (int8_t i = 0; i < length; i++) {
    if (token[i] == '1') {
      value += 1 << i;
    }
  }
  return value;
}

int16_t Util::normalise_value(int16_t min, int16_t max, int16_t wrap, int16_t value) {
  if (value >= min && value <= max)
    return value;

  if (wrap) {
    if (value < min)
      return max;
    else
      return min;
  } else {
    if (value < min)
      return min;
    else
      return max;
  }
}
