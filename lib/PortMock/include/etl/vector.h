#ifndef _ETL_VECTOR_H__
#define _ETL_VECTOR_H__

#include <vector>

namespace etl {
template <typename T, int length>
class vector : public std::vector<T> {
public:
  bool full() {
    return this->size() == length;
  }
};
} // namespace etl

#endif