#ifndef _ETL_FORWARD_LIST_H__
#define _ETL_FORWARD_LIST_H__

#include <forward_list>

namespace etl {
template <typename T, int length>
class forward_list : public std::forward_list<T> {
public:
  int size() {
    int s = 0;
    for (auto it = this->begin(); it != this->end(); it++) {
      s++;
    }
    return s;
  }
};
} // namespace etl

#endif