#ifndef _SCREEN_H__
#define _SCREEN_H__

#include <region.h>

class Screen {
public:
  Screen(screen_buffer_t &_b) : buf(_b) {}

  void draw() {}
  screen_buffer_t& getBuffer() { return buf; }

private:
  screen_buffer_t &buf;
};

#endif
