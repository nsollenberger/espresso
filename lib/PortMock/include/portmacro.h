#ifndef _PORTMACRO_H__
#define _PORTMACRO_H__

#ifdef SIMULATOR
#include "tc.h"
#endif

namespace Port {

inline void DISABLE_INTERRUPTS(void) {
#ifdef SIMULATOR
  tc_mutex.lock();
#endif
}

inline void ENABLE_INTERRUPTS(void) {
#ifdef SIMULATOR
  tc_mutex.unlock();
#endif
}

} // namespace Port

#endif