#ifndef _SCENE_STORE_H__
#define _SCENE_STORE_H__

#include <region.h>
#include <scene.h>

#define SCENE_SLOTS 32
constexpr int diskSizeBytes = 1024 * 1024;

class SceneStore {
public:
  SceneStore(Scene &_state, scene_text_t &_text) {}
  SceneStore(const SceneStore &) = delete;
  SceneStore &operator=(const SceneStore &) = delete;

  void load(uint8_t sceneIndex, bool loadPatterns = true) {}
  void loadText(uint8_t sceneIndex, scene_text_t &textOut) {}
  void save(uint8_t sceneIndex) {}
  void clearAll(void) {}
};

#endif
