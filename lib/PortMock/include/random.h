#ifndef _RANDOM_H_
#define _RANDOM_H_

#include <stdint.h>

struct random_state_t {
  uint32_t z, w;
};

namespace Port {
extern uint32_t rand_val;
uint32_t rand(uint32_t min, uint32_t max = 0xffffffff);
uint32_t rand(uint32_t max = 0xffffffff);

void random_seed(random_state_t *r, uint32_t seed);
uint32_t random_next(random_state_t *r);
} // namespace Port

#endif