#ifndef _GPIO_H__
#define _GPIO_H__

#include "config.h"

typedef int input_pin_t;
typedef int output_pin_t;

extern input_pin_t triggerInPins[8];
extern output_pin_t triggerOutPins[4];

namespace ADC { 
struct Sync_result {
  int result_adc0;
  int result_adc1;
};
}

class Gpio {
public:
  static void setLevel(output_pin_t pin, int level) {}

  static int getLevel(input_pin_t pin) { return 1; }

  static int readAdc(input_pin_t pin) { return 0xf00; }

  static ADC::Sync_result readAdcPair(input_pin_t pin0, input_pin_t pin1) {
    ADC::Sync_result res = {
      100,
      200
    };
    return res;
  }

};

#endif