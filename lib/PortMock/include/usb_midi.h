#ifndef _MIDI_H__
#define _MIDI_H__

#include <stdint.h>

#ifdef SIMULATOR
#include "log.h"
#include <RtMidi.h>
#include <cstdlib>
#include <iostream>
#include <vector>

class usb_midi_class {
public:
  usb_midi_class() : active{false} {}

  void initNative() {
    midiout.openVirtualPort("Espresso-Simulator Output");
    active = true;
  }

  void read() {}
  void sendNoteOn(uint8_t note, uint8_t vel, uint8_t ch) {
    if (!active)
      return;
    message.clear();
    message.push_back(0x90 | (ch-1));
    message.push_back(note);
    message.push_back(vel);
    midiout.sendMessage(&message);
  }
  void sendNoteOff(uint8_t note, uint8_t vel, uint8_t ch) {
    if (!active)
      return;
    message.clear();
    message.push_back(0x80 | (ch-1));
    message.push_back(note);
    message.push_back(vel);
    midiout.sendMessage(&message);
  }
  void sendControlChange(uint8_t cc_num, uint8_t cc_val, uint8_t ch) {
    if (!active)
      return;
    message.clear();
    message.push_back(0xB0 | (ch-1));
    message.push_back(cc_num);
    message.push_back(cc_val);
    midiout.sendMessage(&message);
  }
  void sendRealTime(uint8_t) {}

  typedef void (*GenericHandler)(uint8_t, uint8_t, uint8_t);
  typedef void (*VoidHandler)(void);

  void setHandleNoteOn(GenericHandler fptr) {}
  void setHandleNoteOff(GenericHandler fptr) {}
  void setHandleControlChange(GenericHandler fptr) {}
  void setHandleClock(VoidHandler fptr) {}
  void setHandleStart(VoidHandler fptr) {}
  void setHandleStop(VoidHandler fptr) {}
  void setHandleContinue(VoidHandler fptr) {}

private:
  bool active;
  RtMidiOut midiout;
  std::vector<unsigned char> message;
};

#else

class usb_midi_class {
public:
  void read() {}
  void sendNoteOn(uint8_t note, uint8_t vel, uint8_t ch) {}
  void sendNoteOff(uint8_t note, uint8_t vel, uint8_t ch) {}
  void sendControlChange(uint8_t cc_num, uint8_t cc_val, uint8_t ch) {}
  void sendRealTime(uint8_t) {}

  typedef void (*GenericHandler)(uint8_t, uint8_t, uint8_t);
  typedef void (*VoidHandler)(void);

  void setHandleNoteOn(GenericHandler fptr) {}
  void setHandleNoteOff(GenericHandler fptr) {}
  void setHandleControlChange(GenericHandler fptr) {}
  void setHandleClock(VoidHandler fptr) {}
  void setHandleStart(VoidHandler fptr) {}
  void setHandleStop(VoidHandler fptr) {}
  void setHandleContinue(VoidHandler fptr) {}
};

#endif

extern usb_midi_class usbMIDI;

#endif