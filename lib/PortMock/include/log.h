#ifndef _LOG_H__
#define _LOG_H__

#include <stdio.h>

#ifdef LOGGING_ENABLED
#define LOG printf
#else
#define LOG(...)                                                               \
  {}
#endif // LOGGING_ENABLED

#endif