#ifndef _USB_HOST_SERVICE_H__
#define _USB_HOST_SERVICE_H__

#include "usb_midi.h"
#include <events.h>

class KeyboardController {
public:
  int getModifiers() { return 0; }
  void attachRawPress(void fptr(uint8_t));
  void attachRawRelease(void fptr(uint8_t));
};

typedef usb_midi_class MIDIDeviceBase;

class UsbHostService {
public:
  UsbHostService(Events &_e) {}

  KeyboardController keyboard;
  MIDIDeviceBase midi1;
  MIDIDeviceBase midi2;
  MIDIDeviceBase midi3;
};

#endif
