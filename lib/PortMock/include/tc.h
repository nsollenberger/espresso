#ifndef _TC_H__
#define _TC_H__

#ifdef SIMULATOR
#include <chrono>
#include <mutex>
#include <thread>
extern std::recursive_mutex tc_mutex;
#endif

typedef void timer_isr_cb_t(void);

class Tc {
  static timer_isr_cb_t *mCb;

public:
  static void init(void) {}
  static void startTimer(timer_isr_cb_t &cb) {
    mCb = &cb;
#ifdef SIMULATOR
    std::thread t([=]() {
      while (true) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
        Tc::tick();
      }
    });
    t.detach();
#endif
  }
  static void tick(void) {
#ifdef SIMULATOR
    tc_mutex.lock();
#endif
    (*mCb)();
#ifdef SIMULATOR
    tc_mutex.unlock();
#endif
  }
};

#endif