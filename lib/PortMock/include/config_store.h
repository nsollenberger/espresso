#ifndef _CONFIG_STORE_H__
#define _CONFIG_STORE_H__

#include <scene.h>
#include <stdint.h>

struct device_config_t {
  uint8_t flip;
};

class ConfigStore {
public:
  bool is_flash_fresh(void) { return false; }
  void resetAll(void) {}
  uint8_t flash_last_saved_scene(void) { return 0; }
  void flash_update_last_saved_scene(uint8_t preset_no) {}
  void flash_update_cal(cal_data_t *) {}
  void flash_get_cal(cal_data_t *) {}
  void flash_update_device_config(device_config_t *) {}
  void flash_get_device_config(device_config_t *) {}
};

#endif