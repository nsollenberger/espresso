#ifndef _CONFIG_H__
#define _CONFIG_H__


constexpr int TRIG_IN1 = 1;
constexpr int TRIG_IN2 = 2;
constexpr int TRIG_IN3 = 3;
constexpr int TRIG_IN4 = 4;
constexpr int TRIG_IN5 = 5;
constexpr int TRIG_IN6 = 6;
constexpr int TRIG_IN7 = 7;
constexpr int TRIG_IN8 = 8;
constexpr int TRIG_OUT1 = 9;
constexpr int TRIG_OUT2 = 10;
constexpr int TRIG_OUT3 = 11;
constexpr int TRIG_OUT4 = 12;
constexpr int BUTTON_PIN = 13;

constexpr int OLED_DC_PIN = 14;
constexpr int OLED_RES_PIN = 15;

constexpr int CV_IN1 = 16;
constexpr int CV_IN2 = 17;
constexpr int PARAM = 18;

#endif
