#ifndef _SPIBUS_H__
#define _SPIBUS_H__

#include "spi_config.h"

typedef spi_transaction_t spi_message_t;

class SpiBus {
public:
  SpiBus(Spi::SpiDevice spiDev) {}
  SpiBus(const SpiBus &) = delete;
  SpiBus &operator=(const SpiBus &) = delete;

  void sendMessage(spi_message_t &message_conf, bool keepActive = false) {}
  void sendMessageAsync(const void *txBuffer, size_t count) {}
};

#endif