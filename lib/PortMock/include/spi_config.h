#ifndef _SPI_CONFIG_H__
#define _SPI_CONFIG_H__

#include <cstddef>

struct spi_transaction_t {
  size_t bytes;
  char tx_data[4];
  // char rx_data[4]; Unused in this application
};

class Spi {
public:
  enum SpiDevice {
    dac1_dev,
    dac2_dev,
    oled_dev,
  };
};

#endif