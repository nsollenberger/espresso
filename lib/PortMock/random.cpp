#include "random.h"

uint32_t Port::rand_val = 18474;

#ifdef SIMULATOR

#include <random>

std::default_random_engine r1;

uint32_t Port::rand(uint32_t max) {
  return r1() * (float(max) / r1.max());
}

uint32_t Port::rand(uint32_t min, uint32_t max) {
  return min + (r1() * (float(max - min) / r1.max()));
}

#else

uint32_t Port::rand(uint32_t max) { return rand_val; }

uint32_t Port::rand(uint32_t min, uint32_t max) { return rand_val; }

#endif

/** The next two functions are duplicated from src. Ideally that would be
 * avoided **/

void Port::random_seed(random_state_t *r, uint32_t seed) {
  r->z = 12345;
  r->w = seed;
}

uint32_t Port::random_next(random_state_t *r) {
  r->z = 36969 * (r->z & 65535) + (r->z >> 16);
  r->w = 18000 * (r->w & 65535) + (r->w >> 16);
  return ((r->z << 16) + r->w) & 0x7FFFFFFF;
}