#include "gpio.h"

input_pin_t triggerInPins[] = {TRIG_IN1, TRIG_IN2, TRIG_IN3, TRIG_IN4, TRIG_IN5, TRIG_IN6, TRIG_IN7, TRIG_IN8};
output_pin_t triggerOutPins[] = {TRIG_OUT1, TRIG_OUT2, TRIG_OUT3, TRIG_OUT4};