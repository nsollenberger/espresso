#include "region.h"

const eRegionColor& operator|=(eRegionColor& lhv, eRegionColor rhv) {
  if (rhv > lhv) lhv = rhv;
  return lhv;
}
