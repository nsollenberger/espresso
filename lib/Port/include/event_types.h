#ifndef _EVENT_TYPES_H__
#define _EVENT_TYPES_H__

#include <stdint.h>

enum etype {
  kEventNone,
  kEventPollADC,
  kEventDACRefresh,
  kEventScreenRefresh,
  kEventScreenSleep,
  kEventTrigger,
  kEventClock,
  kEventKeyDown,
  kEventKeyUp,
  kEventKeyTimer,
};

class EventQueue {
public:
  typedef struct {
    etype type;
    int32_t data;
  } event_t;

  virtual bool postFromISR(const event_t &e) = 0;

  static int createKeyboardEventBuffer(uint8_t keycode, uint8_t modifiers) {
    return keycode | (modifiers << 16);
  }

  /**
   * Parses the keycode from a keyboard event buffer
   */
  static uint8_t eventKeycode(int eventBuf) {
    return eventBuf & 0xFF;
  }

  /**
   * Parses modifiers from a keyboard event buffer
   */
  static uint8_t eventModifiers(int eventBuf) {
    return (eventBuf >> 16) & 0xFF;
  }
};

#endif
