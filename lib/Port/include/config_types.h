#ifndef _CONFIG_TYPES_H__
#define _CONFIG_TYPES_H__

#include <stdint.h>

typedef int16_t scene_scale_t;

struct cal_data_t {
  scene_scale_t p_min;
  scene_scale_t p_max;
  scene_scale_t i_min;
  scene_scale_t i_max;
};

#endif
