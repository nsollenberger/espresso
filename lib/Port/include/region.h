#ifndef _REGION_TYPES_H__
#define _REGION_TYPES_H__

#include <assert.h>
#include <cstddef>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

constexpr int kLineEditorSize = 32; // 31 characters + null termination
constexpr int kSceneTextChars = kLineEditorSize;
constexpr int kSceneTextLines = 32;

typedef char scene_text_t[kSceneTextLines][kSceneTextChars];

class Region;
constexpr int kRegionRows = 8;
typedef Region screen_buffer_t[kRegionRows];


enum eRegionColor : uint8_t {
 eColorBlank = 0,
 eColorLight1 = 0x11,
 eColorLight2 = 0x33,
 eColorMedium1 = 0x44,
 eColorMedium2 = 0x77,
 eColorDark1 = 0xAA,
 eColorDark2 = 0xFF,
};

const eRegionColor& operator|=(eRegionColor& lhv, eRegionColor rhv);

class Region {
public:
  Region(uint8_t _x = 0, uint8_t _y = 0, uint8_t _w = 128, uint8_t _h = 8)
      : x(_x), y(_y), w(_w), h(_h) {
    len = w * h;
    data = static_cast<eRegionColor *>(calloc(len, sizeof(eRegionColor)));
    assert(data != nullptr);
    dirty = 0;
  }

  ~Region() { free(data); }
  Region(const Region &) = delete;
  Region &operator=(const Region &) = delete;

  void fill(eRegionColor color) {
    for (int i = 0; i < len; i++) {
      data[i] = color;
    }
    dirty = 1;
  }

  void fill(int x1, int y1, int x2, int y2, eRegionColor color) {
    const auto width = x2 - x1;
    for (auto _y = y1; _y < y2; _y++) {
      auto startingOffset = x1 + (w * _y);
      for (auto _i = startingOffset; _i < startingOffset + width; _i++) {
        if (_i < len)
          data[_i] = color;
      }
    }
    dirty = true;
  }

  // offsets
  uint8_t x;
  uint8_t y;
  // dimensions
  uint8_t w;
  uint8_t h;
  int len;
  uint8_t dirty;
  // row-first
  eRegionColor *data;
};


#endif
