#include "scene.h"

#include <util.h>

using Port::random_seed;
using Util::bit_reverse;

void Scene::hardReset(void) {
  ss_variables_init();
  ss_rand_init();
  memset(&scripts, 0, ss_scripts_size());
  uint32_t ticks = tele_get_ticks();
  for (size_t i = 0; i < TEMP_SCRIPT; i++)
    scripts[i].last_time = ticks;
  every_last = false;
  chaos.hardReset();
}

void Scene::ss_set_scene(int16_t value) { variables.scene = value; }

int16_t Scene::ss_get_in(void) {
  return scale_get(variables.in_scale, variables.in);
}

void Scene::ss_set_in(int16_t value) { variables.in = value; }

int16_t Scene::ss_get_param(void) {
  return scale_get(variables.param_scale, variables.param);
}

void Scene::ss_set_param(int16_t value) { variables.param = value; }

void Scene::ss_reset_in_cal(void) {
  cal.i_max = 16383;
  cal.i_min = 0;
  ss_update_in_scale();
  tele_save_calibration();
}

void Scene::ss_set_in_scale(int16_t min, int16_t max) {
  variables.in_range.out_min = min;
  variables.in_range.out_max = max;
  ss_update_in_scale();
}

void Scene::ss_update_in_scale(void) {
  variables.in_scale =
      scale_init(cal.i_min, cal.i_max, variables.in_range.out_min,
                 variables.in_range.out_max);
}

void Scene::ss_set_in_min(int16_t min) {
  cal.i_min = min;
  ss_update_in_scale();
  tele_save_calibration();
}

void Scene::ss_set_in_max(int16_t max) {
  cal.i_max = max;
  ss_update_in_scale();
  tele_save_calibration();
}

void Scene::ss_reset_param_cal(void) {
  cal.p_max = 16383;
  cal.p_min = 0;
  ss_update_param_scale();
  tele_save_calibration();
}

void Scene::ss_set_param_scale(int16_t min, int16_t max) {
  variables.param_range.out_min = min;
  variables.param_range.out_max = max;
  ss_update_param_scale();
}

void Scene::ss_update_param_scale(void) {
  variables.param_scale =
      scale_init(cal.p_min, cal.p_max, variables.param_range.out_min,
                 variables.param_range.out_max);
}

void Scene::ss_set_param_min(int16_t min) {
  cal.p_min = min;
  ss_update_param_scale();
  tele_save_calibration();
}

void Scene::ss_set_param_max(int16_t max) {
  cal.p_max = max;
  ss_update_param_scale();
  tele_save_calibration();
}

int16_t Scene::ss_get_script_last(script_number_t idx) {
  if (idx < TT_SCRIPT_1)
    return 0;
  if (idx > INIT_SCRIPT)
    return 0;
  uint32_t delta = (tele_get_ticks() - scripts[idx].last_time) & 0x7fff;
  return delta;
}

void Scene::ss_update_script_last(script_number_t idx) {
  scripts[idx].last_time = tele_get_ticks();
}

every_count_t *Scene::ss_get_every(script_number_t idx, uint8_t line_number) {
  return &scripts[idx].every[line_number];
}

void Scene::ss_sync_every(int16_t count) {
  for (int script = 0; script < SCRIPT_COUNT; script++)
    for (int line = 0; line < SCRIPT_MAX_COMMANDS; line++) {
      int16_t count_e = count;
      if (scripts[script].every[line].mod == 0)
        scripts[script].every[line].mod = 1; // lazy init
      while (count_e < 0)
        count_e += scripts[script].every[line].mod;
      scripts[script].every[line].count = count_e;
    }
}

bool Scene::every_is_now(every_count_t *e) {
  every_last = e->count == 0;
  return every_last;
}

bool Scene::skip_is_now(every_count_t *e) {
  every_last = e->count != 0;
  return every_last;
}

bool Scene::modulo_is_now(every_count_t *e, int16_t divisor, bool inverted) {
  if (divisor < 0)
    divisor = -divisor;
  // Using a "greedy remainder", to allow user to specify custom intervals
  // (ie, MOD 5 2 -> play last 3, instead of last 1 like a true modulo operator)
  auto remainder = e->mod - divisor;
  every_last = e->count >= e->mod - remainder;
  if (inverted) every_last = !every_last;
  return every_last;
}

void Scene::every_tick(every_count_t *e) {
  (e->count)++;
  e->count %= e->mod;
}

void Scene::every_set_mod(every_count_t *e, int16_t mod) {
  if (mod < 0)
    mod = -mod;
  else if (mod == 0)
    mod = 1; // lazy initialization
  e->mod = mod;
  e->count %= e->mod;
}

uint8_t Scene::ss_get_script_pol(size_t idx) {
  return variables.script_pol[idx];
}

void Scene::ss_set_script_pol(size_t idx, uint8_t value) {
  variables.script_pol[idx] = value;
  // TODO redraw indicators
}

uint8_t Scene::ss_get_script_len(script_number_t idx) { return scripts[idx].l; }

void Scene::ss_set_script_len(script_number_t idx, uint8_t l) {
  scripts[idx].l = l;
}

void Scene::ss_clear_script(size_t script_idx) {
  memset(&scripts[script_idx], 0, sizeof(scene_script_t));
  variables.j[script_idx] = 0;
  variables.k[script_idx] = 0;
}

const Command *Scene::ss_get_script_command(script_number_t script_idx,
                                                   size_t c_idx) {
  return &scripts[script_idx].c[c_idx];
}

void Scene::ss_insert_script_command(script_number_t script_idx,
                                     size_t command_idx,
                                     const Command *cmd) {
  if (command_idx >= SCRIPT_MAX_COMMANDS)
    return;

  uint8_t script_len = ss_get_script_len(script_idx);
  if (script_len == SCRIPT_MAX_COMMANDS) {                // no room to insert
    ss_delete_script_command(script_idx, script_len - 1); // make room
    script_len = ss_get_script_len(script_idx);
  }

  // shuffle down
  for (size_t i = script_len; i > command_idx; i--) {
    const Command *cmd = ss_get_script_command(script_idx, i - 1);
    ss_set_script_command(script_idx, i, cmd);
  }

  // increase length
  ss_set_script_len(script_idx, script_len + 1);

  // overwrite at command_idx
  ss_overwrite_script_command(script_idx, command_idx, cmd);
}

void Scene::ss_delete_script_command(script_number_t script_idx,
                                     size_t command_idx) {
  uint8_t script_len = ss_get_script_len(script_idx);
  if (command_idx >= SCRIPT_MAX_COMMANDS || command_idx >= script_len)
    return;

  if (script_len && ss_get_script_command(script_idx, command_idx)->length) {
    script_len--;
    ss_set_script_len(script_idx, script_len);

    for (size_t n = command_idx; n < script_len; n++) {
      const Command *cmd = ss_get_script_command(script_idx, n + 1);
      ss_set_script_command(script_idx, n, cmd);
    }

    Command blank_command;
    blank_command.length = 0;
    blank_command.comment = false;
    ss_set_script_command(script_idx, script_len, &blank_command);
  }
}

void Scene::ss_copy_script_command(Command *dest,
                                   script_number_t script_idx, size_t c_idx) {
  memcpy(dest, &scripts[script_idx].c[c_idx], sizeof(Command));
}

void Scene::ss_overwrite_script_command(script_number_t script_idx,
                                        size_t command_idx,
                                        const Command *cmd) {
  // Few of the commands in this file bounds-check.
  // Are we trusting calling code in this file or not?
  // If so, why here?  If not, we need much more bounds-checking
  // If we start running up against processor limits, we should not
  // Well-validated upstream code doesn't _need_ bounds-checking here IMO
  // -- burnsauce (sliderule)

  // TODO: why check upper bound here but not lower?
  if (command_idx >= SCRIPT_MAX_COMMANDS)
    return;

  ss_set_script_command(script_idx, command_idx, cmd);

  const uint8_t script_len = ss_get_script_len(script_idx);

  if (script_len < SCRIPT_MAX_COMMANDS && command_idx >= script_len) {
    ss_set_script_len(script_idx, script_len + 1);
  }
}

bool Scene::ss_get_script_comment(script_number_t script_idx, size_t c_idx) {
  return scripts[script_idx].c[c_idx].comment;
}

void Scene::ss_set_script_comment(script_number_t script_idx, size_t c_idx,
                                  uint8_t on) {
  scripts[script_idx].c[c_idx].comment = on;
}

void Scene::ss_toggle_script_comment(script_number_t script_idx, size_t c_idx) {
  scripts[script_idx].c[c_idx].comment = !scripts[script_idx].c[c_idx].comment;
}

const scene_script_t *Scene::ss_scripts_ptr(void) const { return scripts; }
scene_script_t *Scene::ss_scripts_ptr(void) { return scripts; }

size_t Scene::ss_scripts_size(void) {
  return sizeof(scene_script_t) * SCRIPT_COUNT;
}

void Scene::resetTimestamps(void) {
  uint32_t ticks = tele_get_ticks();
  for (size_t i = 0; i < TEMP_SCRIPT; i++)
    scripts[i].last_time = ticks;
  variables.time = 0;
}

void Scene::ss_variables_init(void) {
  const scene_variables_t default_variables = {
      .a = 1,
      .x = 0,
      .b = 2,
      .y = 0,
      .c = 3,
      .z = 0,
      .d = 4,
      .t = 0,
      .j = {0},
      .k = {0},
      .cv = {0},
      .cv_off = {0},
      .cv_slew = {1, 1, 1, 1},
      .drunk = 0,
      .drunk_max = 255,
      .drunk_min = 0,
      .drunk_wrap = 0,
      .flip = 0,
      .in = 0,
      .m = 1000,
      .m_act = 1,
      .mutes = {0},
      .o = 0,
      .o_inc = 1,
      .o_min = 0,
      .o_max = 63,
      .o_wrap = 1,
      .p_n = 0,
      .param = 0,
      .q = {0},
      .q_n = 1,
      .q_grow = 0,
      .r_min = 0,
      .r_max = 16383,
      .n_scale_bits = {0},
      .n_scale_root = {0},
      .scene = 0,
      .script_pol = {1, 1, 1, 1, 1, 1, 1, 1},
      .time = 0,
      .time_act = 1,
      .seed = 0,
      .in_range = {0, 16383},
      .in_scale = {},
      .param_range = {0, 16383},
      .param_scale = {},
  };
  memcpy(&variables, &default_variables, sizeof(default_variables));

  for (size_t i = 0; i < NB_NBX_SCALES; i++) {
    variables.n_scale_bits[i] = bit_reverse(0b101011010101, 12);
  }
  ss_update_in_scale();
  ss_update_param_scale();
}

void Scene::ss_rand_init(uint32_t seed) {
  ss_rand_init_field(rand_states.rand, seed ? seed : Port::rand());
  ss_rand_init_field(rand_states.prob, seed ? seed : Port::rand());
  ss_rand_init_field(rand_states.toss, seed ? seed : Port::rand());
  ss_rand_init_field(rand_states.pattern, seed ? seed : Port::rand());
  ss_rand_init_field(rand_states.drunk, seed ? seed : Port::rand());
}

void Scene::ss_rand_init_field(tele_rand_t &field, uint32_t seed) {
  field.seed = seed;
  random_seed(&field.rand, seed);
}

scale_t Scene::scale_init(scene_scale_t izero, scene_scale_t imax, scene_scale_t ozero,
                          scene_scale_t omax) {
  scale_t ret;
  if (izero == imax)
    imax = 1;
  // Impart 16 bits of precision
  ret.m = TO_Q15(omax - ozero) / (imax - izero);
  ret.b = ozero - FROM_Q15(ret.m * izero);
  return ret;
}

scene_scale_t Scene::scale_get(scale_t scale, scene_scale_t x) {
  return FROM_Q15(scale.m * x) + scale.b;
}

void Scene::ss_set_script_command(script_number_t script_idx, size_t c_idx,
                                  const Command *cmd) {
  memcpy(&scripts[script_idx].c[c_idx], cmd, sizeof(Command));
}
