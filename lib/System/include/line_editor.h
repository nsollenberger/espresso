#ifndef _LINE_EDITOR_H__
#define _LINE_EDITOR_H__

#include <stddef.h>
#include <stdint.h>

#include "region.h"
#include "scene.h"

typedef char copy_buffer_t[kSceneTextLines][kSceneTextChars];

namespace Espresso {
class Command;
}

class LineEditor {
public:
  void line_editor_set(const char value[kLineEditorSize]);
  void line_editor_set_command(const Espresso::Command *command);
  char *line_editor_get(void);
  bool line_editor_process_keys(uint8_t key, uint8_t mod_key, bool is_key_held);
  void line_editor_draw(char prefix, Region *reg);

  copy_buffer_t copy_buffer = {};
  uint8_t copy_buffer_len = {0};

private:
  char buffer[kLineEditorSize] = {};
  size_t cursor = {0};
  size_t length = {0};
};

#endif
