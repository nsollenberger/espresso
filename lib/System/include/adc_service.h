#ifndef _ADC_SERVICE_H__
#define _ADC_SERVICE_H__

#include <stdint.h>

// Renamed file due to PIO LDF case-insensitivity causing conflicts with ADC.h

class Adc {
public:
  static void sampleInputs(uint16_t (&result)[2]);
  static uint16_t sampleParam();
};

#endif