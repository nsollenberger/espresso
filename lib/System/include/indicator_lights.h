#ifndef _INDICATOR_LIGHTS_H__
#define _INDICATOR_LIGHTS_H__

#include <stdint.h>
#include <ws_leds.h>

class IndicatorLights {
public:
  IndicatorLights(WSLEDStrip _l) : leds(_l) {}

  void setTrigger(int i, bool high) {
    leds.setColor(i+1, high ? 0x111100 : 0);
    leds.flush();
  }

  void setCV(uint16_t val1, uint16_t val2, uint16_t val3, uint16_t val4) {
    leds.setColor(8, 0, 0, val1 >> 6);
    leds.setColor(7, 0, 0, val2 >> 6);
    leds.setColor(6, 0, 0, val3 >> 6);
    leds.setColor(5, 0, 0, val4 >> 6);
    leds.flush();
  }

  void displayCountdown(float percentRemaining, int color) {
    for (int i = 1; i < 9; i++) {
      int ledI = i;
      if (i > 4)
        ledI = 13 - i;
      if (percentRemaining >= (0.125 * i)) {
        leds.setColor(ledI, color);
      } else if (percentRemaining <= (0.125 * (i - 1))) {
        leds.setColor(ledI, 0);
      } else {
        float fade = (percentRemaining - 0.125 * (i - 1)) / 0.125;
        int r = (color & 0xff0000) >> 16;
        int g = (color & 0x00ff00) >> 8;
        int b = (color & 0x0000ff);
        leds.setColor(ledI, r * fade, g * fade, b * fade);
      }
      leds.flush();
    }
  }

private:
  WSLEDStrip leds;
};

#endif