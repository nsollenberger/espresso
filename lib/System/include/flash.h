#ifndef _FLASH_H__
#define _FLASH_H__

#include "avr/pgmspace.h"
#include "region.h"
#include <Bounce2.h>
#include <config_store.h>
#include <indicator_lights.h>
#include <scene_store.h>
#include <screen.h>
#include <timers.h>

constexpr int kFlashPrepareTimeoutMs = 8000;

class Flash {
public:
  static FLASHMEM void prepareFresh(SceneStore &sceneStore, ConfigStore &configStore,
                           Bounce &button, Timers &timers, Screen &screen,
                           IndicatorLights &indicators);
};

#endif
