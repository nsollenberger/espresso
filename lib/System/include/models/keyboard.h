#ifndef _MODELS_KEYBOARD_H__
#define _MODELS_KEYBOARD_H__

#include <cstddef>
#include <etl/forward_list.h>
#include <stdint.h>

namespace Models {

class Keyboard {
public:
  bool keyDown(const uint8_t keycode, const uint8_t modifiers) {
    mod_key = modifiers;
    hold_key_count = 0;
    if (keycode) {
      held_keys.push_front(keycode);
      return true;
    }
    return false;
  }

  void keyUp(const uint8_t keycode, const uint8_t modifiers) {
    mod_key = modifiers;
    hold_key_count = 0;
    if (keycode) {
      held_keys.remove(keycode);
    }
  }

  uint8_t getHeldKey(void) {
    if (held_keys.size()) {
      if (hold_key_count > 4) {
        return held_keys.front();
      } else {
        hold_key_count++;
      }
    }
    return 0;
  }

  uint8_t mod_key = {0};

private:
  etl::forward_list<uint8_t, 8> held_keys;
  uint8_t hold_key_count = {0};
};

} // namespace Models

#endif
