#ifndef _MODELS_MODE_MANAGER_H__
#define _MODELS_MODE_MANAGER_H__

#include "models/mode.h"

namespace Models {

class Settings;
class HelpMode;
class PresetRMode;
class PresetWMode;
class EditMode;
class VoiceSettings;

class ModeManager {
public:
  ModeManager(Settings &_sm, VoiceSettings &_vs, EditMode &_e, HelpMode &_h,
              PresetRMode &_pr, PresetWMode &_pw)
      : settings(_sm), voiceSettings(_vs), editMode(_e), helpMode(_h),
        presetRMode(_pr), presetWMode(_pw) {}

  CurrentVoiceMode& getMode(model_current_voice_mode_t m);
  Mode& getMode(model_mode_t m);

  Mode& activeMode() {
    return getMode(mode);
  }

  void activateMode(model_current_voice_mode_t m) {
    activateMode(static_cast<model_mode_t>(m));
  }

  void activateMode(model_mode_t m) {
    last_mode = mode;
    mode = m;
    getMode(m).activate();
  }

  void activatePriorMode(void) {
    if (mode == last_mode)
      return;

    activateMode(last_mode);
  }

  model_mode_t mode = {M_SETTINGS_1};

private:
  Settings &settings;
  VoiceSettings &voiceSettings;
  EditMode &editMode;
  HelpMode &helpMode;
  PresetRMode &presetRMode;
  PresetWMode &presetWMode;

  model_mode_t last_mode = {M_SETTINGS_1};
};

} // namespace Models

#endif
