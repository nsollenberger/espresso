#ifndef _MODELS_PRESET_R_MODE_H__
#define _MODELS_PRESET_R_MODE_H__

#include <stdint.h>

#include <config_store.h>
#include <scene_store.h>
#include <teletype.h>

#include "models/mode.h"

class Scene;

namespace Models {

class ModeManager; // Forward-declaration

class PresetRMode : public Mode {
public:
  PresetRMode(Teletype &_t, Scene &_s,
              SceneStore &_sStore, ConfigStore &_cStore)
      : tt(_t), scene(_s), sceneStore(_sStore),
        configStore(_cStore), modeManager{nullptr} {}

  void setModeManager(ModeManager &_m) { modeManager = &_m; }
  uint8_t get_preset(void);
  void activate();
  void set_preset_last(uint8_t preset);
  void process_preset_r_preset(uint8_t preset);
  void preset_line_down(void);
  void preset_line_up(void);
  void process_preset_r_load(void);
  void processKeypress(uint8_t key, uint8_t mod_key, bool is_held_key);

  uint8_t preset_select;

  Teletype &tt;
  Scene &scene;
  SceneStore &sceneStore;
  ConfigStore &configStore;
  ModeManager *modeManager;
  scene_text_t text_buffer;

  uint8_t offset;
  uint8_t preset_last;
  bool dirty;

private:
  void load_text(void);
  void do_preset_read(void);
};

} // namespace Models

#endif
