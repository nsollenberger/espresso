#ifndef _MODELS_SETTINGS_MODE_H__
#define _MODELS_SETTINGS_MODE_H__

#include <stdint.h>

#include "models/mode.h"

constexpr int MIN_BPM = 20;
constexpr int MAX_ROWS = 3;
constexpr int MAX_COLS = 2;

namespace Models {

class ModeManager;

class Settings : public Mode {
public:

  void setModeManager(ModeManager &_m) { modeManager = &_m; }

  void activate() {
    dirty = true;
    isEditing = false;
  }

  void processKeypress(uint8_t key, uint8_t mod_key, bool is_held_key);

  uint8_t row_index = {1};
  uint8_t col_index = {0};

  enum SettingSelection {
    PLAY_PAUSE, RESET, CLOCK_TEMPO, LOOP_LENGTH,
    LOAD, SAVE
  };

private:
  ModeManager *modeManager = {nullptr};
public:
  bool dirty;
  bool isRunning = {false};
  bool isEditing = {false};
  bool shouldReset = {false};
  bool backToTop = {true};
  float clockTempo = {120.0};
  int loopLength = {16};

  void setIsRunning(bool run) {
    if (run != isRunning) {
      dirty = true;
      isRunning = run;
    }
  }

  SettingSelection getSelection() {
    if (row_index == 0) {
      switch (col_index) {
        case 0: return LOAD;
        case 1:
        default: return SAVE;
      }
    } else if (row_index == 1) {
      switch (col_index) {
        case 0: return PLAY_PAUSE;
        case 1:
        default: return CLOCK_TEMPO;
      }
    } else {
      switch (col_index) {
        case 0: return RESET;
        case 1:
        default: return LOOP_LENGTH;
      }
    }
  }

private:

  void decrementClock() {
    if (clockTempo > MIN_BPM) {
      dirty = true;
      clockTempo -= 1.0;
    }
  }

  void incrementClock() {
    dirty = true;
    clockTempo += 1.0;
  }

  void decrementLoopLength() {
    if (loopLength > 2) {
      dirty = true;
      loopLength--;
    }
  }

  void incrementLoopLength() {
    dirty = true;
    loopLength++;
  }
};
} // namespace Models

#endif
