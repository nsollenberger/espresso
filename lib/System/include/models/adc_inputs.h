#ifndef _CONTROLLERS_ADC_INPUTS_H__
#define _CONTROLLERS_ADC_INPUTS_H__

#include <stdint.h>

class Scene;
class Timers;

namespace Models {

class ModeManager;
class PresetRMode;
class Screensaver;

class AdcInputs {
public:
  AdcInputs(Scene &_state, ModeManager &_mm,
            PresetRMode &_pr, Screensaver &_ss, Timers &_t)
      : scene_state(_state), modeManager(_mm),
        presetRMode(_pr), screensaver(_ss), timers(_t) {}

  void pollInputs(void);
  void sampleInputs(bool force);

  int16_t getRawParam() {
    return param;
  }

private:
  Scene &scene_state;
  ModeManager &modeManager;
  PresetRMode &presetRMode;
  Screensaver &screensaver;
  Timers &timers;

  int16_t last_knob = {0};
  uint16_t adc[2];
  uint16_t param;
  uint64_t last_adc_tick = 0;
};
} // namespace Models

#endif
