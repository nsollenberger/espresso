#ifndef _MODELS_EDIT_MODE_H__
#define _MODELS_EDIT_MODE_H__

#include <stdint.h>

#include <scene.h>
#include <types.h>

#include "models/mode.h"
#include "espresso/parser.h"

#define UNDO_DEPTH 3

class LineEditor;
class Scene;

namespace Espresso {
class Command;
}

namespace Models {

class ModeManager;

class EditMode : public CurrentVoiceMode {
public:
  EditMode(Scene &_s, LineEditor &_le) : scene(_s), le(_le) {}

  void setModeManager(ModeManager &_m) { modeManager = &_m; }

  void activate();
  void processKeypress(uint8_t key, uint8_t mod_key, bool is_held_key);
  void setScript(script_number_t new_script);

  Scene &scene;
  LineEditor &le;
  ModeManager *modeManager = {nullptr};
  uint8_t line_no1, line_no2;
  script_number_t script;
  Espresso::e_parse_error status;
  char error_msg[PARSE_ERROR_STR_LENGTH];

private:
  Espresso::Command undo_buffer[UNDO_DEPTH][SCRIPT_MAX_COMMANDS];
  uint8_t undo_comments[UNDO_DEPTH][SCRIPT_MAX_COMMANDS];
  uint8_t undo_length[UNDO_DEPTH];
  uint8_t undo_line_no1[UNDO_DEPTH];
  uint8_t undo_line_no2[UNDO_DEPTH];
  uint8_t undo_count;
  uint8_t undo_pos;

public:
  const uint8_t D_INPUT = {1 << 0};
  const uint8_t D_LIST = {1 << 1};
  const uint8_t D_MESSAGE = {1 << 2};
  const uint8_t D_ALL = {0xFF};
  uint8_t dirty;

private:
  void save_undo(void);
  void undo(void);
};
} // namespace Models

#endif
