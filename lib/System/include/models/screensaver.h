#ifndef _MODELS_SCREENSAVER_H__
#define _MODELS_SCREENSAVER_H__

#include <portmacro.h>
#include <stdint.h>

#define SLEEP_CHECK_MS 15000

constexpr int kScreensaverTimeoutMs = 15 * 60 * 1000;

namespace Models {

class Screensaver {
public:
  Screensaver()
      : screensaverTimeoutTicks(kScreensaverTimeoutMs / SLEEP_CHECK_MS) {}
  bool incrementInactive(void) {
    const bool beginScreensaver =
        (++inactiveCounter >= screensaverTimeoutTicks);
    return beginScreensaver;
  }

  bool resetTimer(void) {
    const bool exitingScreensaver = inactiveCounter >= screensaverTimeoutTicks;
    inactiveCounter = 0;
    return exitingScreensaver;
  }

  bool active() { return inactiveCounter >= screensaverTimeoutTicks; }

private:
  uint32_t screensaverTimeoutTicks;
  uint32_t inactiveCounter = {0};
};

} // namespace Models

#endif
