#ifndef _MODELS_PRESET_W_MODE_H_
#define _MODELS_PRESET_W_MODE_H_

#include "models/mode.h"
#include "line_editor.h"
#include <config_store.h>
#include <scene_store.h>
#include <stdint.h>

namespace Models {

class ModeManager; // Forward-declarations
class PresetRMode;

class PresetWMode : public Mode {
public:
  PresetWMode(PresetRMode &_pr, SceneStore &_sStore, ConfigStore &_cStore,
              scene_text_t &_text, LineEditor &_le)
      :  presetRMode(_pr), sceneStore(_sStore), configStore(_cStore), scene_text(_text),
      le(_le), modeManager{nullptr} {}

  void setModeManager(ModeManager &_m) { modeManager = &_m; }
  void activate();
  void processKeypress(uint8_t key, uint8_t mod_key, bool is_held_key);

  PresetRMode &presetRMode;
  SceneStore &sceneStore;
  ConfigStore &configStore;
  scene_text_t &scene_text;
  LineEditor &le;
  ModeManager *modeManager;

  uint8_t edit_line;
  uint8_t edit_offset;

  const uint8_t D_INPUT = {1 << 0};
  const uint8_t D_LIST = {1 << 1};
  const uint8_t D_ALL = {0xFF};
  uint8_t dirty;
};

} // namespace Models

#endif
