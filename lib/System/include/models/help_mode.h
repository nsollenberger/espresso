#ifndef _MODELS_HELP_MODE_H__
#define _MODELS_HELP_MODE_H__

#include "models/mode.h"
#include "line_editor.h"
#include "region.h"
#include <stdint.h>

#define HELP_PAGES 15

#define HELP1_LENGTH 63
#define HELP2_LENGTH 23
#define HELP3_LENGTH 56
#define HELP4_LENGTH 78
#define HELP5_LENGTH 128
#define HELP6_LENGTH 48
#define HELP7_LENGTH 43
#define HELP9_LENGTH 17
#define HELP10_LENGTH 67
#define HELP11_LENGTH 39
#define HELP12_LENGTH 36
#define HELP13_LENGTH 164
#define HELP14_LENGTH 140
#define HELP15_LENGTH 85
#define HELP16_LENGTH 141

namespace Models {

class HelpMode : public Mode {
public:
  HelpMode(LineEditor &_le) : le(_le) {}

  void activate();
  void processKeypress(uint8_t key, uint8_t mod_key, bool is_held_key);

  LineEditor &le;

  uint8_t page_no = {0};
  uint8_t offset = {0};
  enum search_mode_t {
    SEARCH_MODE_NONE,
    SEARCH_MODE_FWD,
    SEARCH_MODE_REV,
  } search_mode = {SEARCH_MODE_NONE};
  struct search_state_t {
    int line;
    int ch;
  } search_state = {0,0};
  enum search_result_t {
    SEARCH_RESULT_NONE,
    SEARCH_RESULT_HIT,
    SEARCH_RESULT_MISS,
  } search_result;
  int prev_hit = {0};
  bool dirty;

private:
  bool text_search_forward(search_state_t *state, const char *needle,
                           const char **haystack, int haystack_len);
  bool text_search_reverse(search_state_t *state, const char *needle,
                           const char **haystack, int haystack_len);

public:
  static const char **help_pages[HELP_PAGES];
  static const uint8_t help_length[HELP_PAGES];
  static const char *help1[HELP1_LENGTH];
  static const char *help2[HELP2_LENGTH];
  static const char *help3[HELP3_LENGTH];
  static const char *help4[HELP4_LENGTH];
  static const char *help5[HELP5_LENGTH];
  static const char *help6[HELP6_LENGTH];
  static const char *help7[HELP7_LENGTH];
  static const char *help9[HELP9_LENGTH];
  static const char *help10[HELP10_LENGTH];
  static const char *help11[HELP11_LENGTH];
  static const char *help12[HELP12_LENGTH];
  static const char *help13[HELP13_LENGTH];
  static const char *help14[HELP14_LENGTH];
  static const char *help15[HELP15_LENGTH];
  static const char *help16[HELP16_LENGTH];
};
} // namespace Models

#endif
