#ifndef _MODELS_CV_OUTPUTS_H__
#define _MODELS_CV_OUTPUTS_H__

#include <stdint.h>

#include "dac.h"
#include <config_store.h>
#include <scene.h>

// Refresh rate, in ticks: assumes Timers are running at 1000Hz
#define RATE_CV 6

enum EnvelopeState : int8_t {
  EnvelopeOff = 0,
  EnvelopeIdle,
  EnvelopeAttack,
  EnvelopeDecay,
  EnvelopeSustain,
  EnvelopeRelease
};

struct aout_t {
  int32_t delta;
  uint32_t a;
  uint16_t now;
  uint16_t off;
  uint16_t target;
  uint16_t slew;
  uint16_t step;
  uint16_t env_attack;
  uint16_t env_decay;
  uint16_t env_sustain;
  uint16_t env_release;
  EnvelopeState env_state;
};

namespace Models {
template <class T, class I>
class CvOutputs {
public:
  CvOutputs(T &_t, device_config_t &_cfg, I _i)
      : tt(_t), device_config(_cfg), indicators(_i) {
    for (int i = 0; i < 4; i++) {
      aout[i].slew = 1;
      aout[i].step = 0;
      aout[i].now = 0;
      aout[i].off = 0;
      aout[i].env_state = EnvelopeOff;
    }
  }

  void refresh(void) {
    bool updated = false;

    for (int i = 0; i < 4; i++) {
      if (aout[i].env_state) {
        switch (aout[i].env_state) {
          case EnvelopeOff:
          case EnvelopeIdle:
            break;

          case EnvelopeAttack:
            // 0 = has not started yet
            if (aout[i].step == 0) {
              setSlewDuration(i, aout[i].env_attack);
              setValue(i, 16383, true);
            }
            // 1 = just about to finish phase
            else if (aout[i].step == 1) {
              aout[i].env_state = EnvelopeDecay;
            }
            break;

          case EnvelopeDecay:
            if (aout[i].step == 0) {
              setSlewDuration(i, aout[i].env_decay);
              setValue(i, aout[i].env_sustain, aout[i].env_decay);
            }
            else if (aout[i].step == 1) {
              aout[i].env_state = EnvelopeSustain;
            }
            break;

          case EnvelopeSustain:
            break;

          case EnvelopeRelease:
            if (aout[i].step == 0) {
              setSlewDuration(i, aout[i].env_release);
              setValue(i, 0, true);
            }
            else if (aout[i].step == 1) {
              aout[i].env_state = EnvelopeIdle;
            }
            break;
        }
      }

      if (aout[i].step) {
        aout[i].step--;
        if (aout[i].step == 0) {
          aout[i].now = aout[i].target;
        } else {
          aout[i].a += aout[i].delta;
          aout[i].now = aout[i].a >> 16;
        }
        updated = true;
      }
    }

    if (updated) {
      uint16_t a0, a1, a2, a3;

      if (device_config.flip) {
        a0 = aout[3].now >> 2;
        a1 = aout[2].now >> 2;
        a2 = aout[1].now >> 2;
        a3 = aout[0].now >> 2;
      } else {
        a0 = aout[0].now >> 2;
        a1 = aout[1].now >> 2;
        a2 = aout[2].now >> 2;
        a3 = aout[3].now >> 2;
      }

      Dac::updateOutputs(a0, a1, a2, a3);
      indicators.setCV(a0, a1, a2, a3);
    }
  }

  void setValue(uint8_t index, int16_t value, bool slew) {
    int16_t target = value + aout[index].off;
    if (target < 0)
      target = 0;
    else if (target > 16383)
      target = 16383;
    aout[index].target = target;
    if (slew) {
      aout[index].step = aout[index].slew;
      aout[index].delta =
          ((aout[index].target - aout[index].now) << 16) / aout[index].step;
    } else {
      aout[index].step = 1;
      aout[index].now = aout[index].target;
    }

    aout[index].a = aout[index].now << 16;
  }

  void setOffset(uint8_t index, int16_t offset) { aout[index].off = offset; }

  void setStep(uint8_t index, int16_t step) { aout[index].step = step; }

  void setSlewDuration(uint8_t index, int16_t durationMs) {
    aout[index].slew = durationMs / RATE_CV;
    if (aout[index].slew == 0)
      aout[index].slew = 1;
  }

  void enableEnvelope(uint8_t index, uint16_t attack, uint16_t decay, uint16_t sustain, uint16_t release) {
    aout[index].env_attack = attack;
    aout[index].env_decay = decay;
    aout[index].env_sustain = sustain;
    aout[index].env_release = release;
    aout[index].env_state = EnvelopeIdle;
  }

  void disableEnvelope(uint8_t index) {
    aout[index].env_state = EnvelopeOff;
    setSlewDuration(index, 0);
  }

  void envelopeGate(uint8_t index, bool high) {
    switch (aout[index].env_state) {
      case EnvelopeIdle:
        if (high) aout[index].env_state = EnvelopeAttack;
        break;

      case EnvelopeAttack:
        if (!high) {
          // step to 0, so "start of release" is detected
          aout[index].env_state = EnvelopeRelease;
          aout[index].step = 0;
        }
        break;

      case EnvelopeDecay:
        if (!high) {
          aout[index].env_state = EnvelopeRelease;
          aout[index].step = 0;
        }
        break;

      case EnvelopeSustain:
        if (!high) {
          aout[index].env_state = EnvelopeRelease;
          aout[index].step = 0;
        }
        break;

      case EnvelopeRelease:
        if (high) {
          aout[index].env_state = EnvelopeAttack;
          aout[index].step = 0;
        }
        break;

      case EnvelopeOff:
        break;
    }
  }

private:
  T &tt;
  device_config_t &device_config;
  I indicators;
  aout_t aout[4];
};
} // namespace Models

#endif
