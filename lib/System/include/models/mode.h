#ifndef _MODELS_MODE_H__
#define _MODELS_MODE_H__

#include <stdint.h>

#include <types.h>

enum model_mode_t : int8_t {
  M_SETTINGS_1,
  M_PRESET_R, M_PRESET_W,
  M_HELP
};

enum model_current_voice_mode_t : int8_t {
  M_EDIT = M_HELP + 1,
  M_VOICE_SETTINGS_1
};

namespace Models {

class Mode {
public:
  virtual void activate() = 0;
  virtual void processKeypress(uint8_t key, uint8_t mod_key, bool is_held_key) = 0;
};

/**
 * Also keeps track of the "current voice", for showing editor/settings per-voice
 */
class CurrentVoiceMode : public Mode {
public:
  virtual void setScript(script_number_t new_script) = 0;
};

} // namespace Models

#endif
