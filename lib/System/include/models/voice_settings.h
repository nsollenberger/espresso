#ifndef _MODELS_VOICE_SETTINGS_MODE_H__
#define _MODELS_VOICE_SETTINGS_MODE_H__

#include <stdint.h>

#include "models/mode.h"
#include "types.h"

#define VOICE_COUNT 8
#define SEQ_COUNT VOICE_COUNT

class Voice;
typedef Voice voice_collection_t[VOICE_COUNT];

namespace Models {

class ModeManager;

constexpr int kVoiceSettingsSelectableCount = 6;

class VoiceSettings : public CurrentVoiceMode {
public:
  VoiceSettings() { resetAll(); }

  void setModeManager(ModeManager &_m) { modeManager = &_m; }

  void activate() {
    dirty = true;
    is_editing = false;
  }

  void processKeypress(uint8_t key, uint8_t mod_key, bool is_held_key);

  void setScript(script_number_t new_script) {
    if (script != new_script) {
      script = new_script;
      dirty = true;
    }
  }

  // @nate - sync VoiceSettings with Scene (save/load at the same time)
  void resetAll();

  struct VoiceConfig {
    int8_t cv_out;
    int8_t eg_out;
    int8_t gate_out;
    int8_t midi_out_ch;

    int8_t octave;

    int8_t note_length;
    int8_t note_vel;
  };

  VoiceConfig configs[VOICE_COUNT];
  script_number_t script = {TT_SCRIPT_1};
  int8_t selected_index = {0};
  bool back_to_top = {true};
  bool is_editing;
  bool dirty;

private:
  ModeManager *modeManager = {nullptr};

  bool incrementSelected();
  bool decrementSelected();

  bool increment_range(int8_t &value, const int8_t max) {
    const bool _dirty = (value < max);
    if (_dirty) value += 1;
    return _dirty;
  }

  bool decrement_range(int8_t &value, const int8_t min) {
    const bool _dirty = (value > min);
    if (_dirty) value -= 1;
    return _dirty;
  }
};
} // namespace Models

#endif
