#ifndef _SCENE_STATE_H__
#define _SCENE_STATE_H__

#include <avr/pgmspace.h> // FLASHMEM
#include <cstddef>
#include <cstring>
#include <stdint.h>

#include <config_types.h>
#include <random.h>
#include <chaos.h>
#include <teletype_io.h>
#include <types.h>

#include "espresso/command.h"

using Espresso::Command;

#define SCRIPT_MAX_COMMANDS 6
#define SCRIPT_COUNT 10

#include "models/voice_settings.h"

#define CV_COUNT 4
#define Q_LENGTH 64
#define TR_COUNT 4
#define TRIGGER_INPUTS 8
#define STACK_OP_SIZE 16
#define PATTERN_COUNT 4
#define PATTERN_LENGTH 64

#define WHILE_DEPTH 10000
#define RAND_STATES_COUNT 5

#define METRO_MIN_MS 25
#define METRO_MIN_UNSUPPORTED_MS 2

#define NB_NBX_SCALES 16

#define TO_Q15(x) ((x) << 15)
#define FROM_Q15(x) ((((x) >> 14) + 1) >> 1)


typedef int32_t scene_scale_double_t;

typedef uint16_t scene_state_version_t;

struct scale_data_t {
  scene_scale_t out_min;
  scene_scale_t out_max;
};

struct scale_t {
  scene_scale_double_t b;
  scene_scale_double_t m;
};

struct scene_variables_t {
  int16_t a;
  int16_t x;
  int16_t b;
  int16_t y;
  int16_t c;
  int16_t z;
  int16_t d;
  int16_t t;
  int16_t j[SCRIPT_COUNT];
  int16_t k[SCRIPT_COUNT];
  int16_t cv[CV_COUNT];
  int16_t cv_off[CV_COUNT];
  int16_t cv_slew[CV_COUNT];
  int16_t drunk;
  int16_t drunk_max;
  int16_t drunk_min;
  int16_t drunk_wrap;
  int16_t flip;
  int16_t in;
  int16_t m;
  bool m_act;
  bool mutes[TRIGGER_INPUTS];
  int16_t o;
  int16_t o_inc;
  int16_t o_min;
  int16_t o_max;
  int16_t o_wrap;
  int16_t p_n;
  int16_t param;
  int16_t q[Q_LENGTH];
  int16_t q_n;
  int16_t q_grow;
  int16_t r_min;
  int16_t r_max;
  int16_t n_scale_bits[NB_NBX_SCALES];
  int16_t n_scale_root[NB_NBX_SCALES];
  int16_t scene;
  uint8_t script_pol[8];
  int64_t time;
  uint8_t time_act;
  int16_t seed;
  scale_data_t in_range;
  scale_t in_scale;
  scale_data_t param_range;
  scale_t param_scale;
};

struct scene_stack_op_t {
  Command commands[STACK_OP_SIZE];
  uint8_t top;
};

struct every_count_t {
  int16_t count;
  int16_t mod;
};

struct scene_script_t {
  uint8_t l;
  Command c[SCRIPT_MAX_COMMANDS];
  every_count_t every[SCRIPT_MAX_COMMANDS];
  uint32_t last_time;
};

struct tele_rand_t {
  random_state_t rand;
  int16_t seed;
};

union scene_rand_t {
  tele_rand_t rand;
  tele_rand_t prob;
  tele_rand_t toss;
  tele_rand_t pattern;
  tele_rand_t drunk;
};

class Scene {
public:
  Scene(voice_collection_t &_v): voices(_v) { hardReset(); }

  FLASHMEM void hardReset(void);
  FLASHMEM void ss_set_scene(int16_t value);

  int16_t ss_get_in(void);
  FLASHMEM void ss_set_in(int16_t value);
  int16_t ss_get_param(void);
  FLASHMEM void ss_set_param(int16_t value);

  FLASHMEM void ss_reset_in_cal(void);
  FLASHMEM void ss_set_in_scale(int16_t, int16_t);
  FLASHMEM void ss_update_in_scale(void);
  FLASHMEM void ss_set_in_min(int16_t);
  FLASHMEM void ss_set_in_max(int16_t);

  FLASHMEM void ss_reset_param_cal(void);
  FLASHMEM void ss_set_param_scale(int16_t, int16_t);
  FLASHMEM void ss_update_param_scale(void);
  FLASHMEM void ss_set_param_min(int16_t);
  FLASHMEM void ss_set_param_max(int16_t);

  int16_t ss_get_script_last(script_number_t idx);
  void ss_update_script_last(script_number_t idx);

  every_count_t *ss_get_every(script_number_t idx, uint8_t line);
  void ss_sync_every(int16_t count);
  bool every_is_now(every_count_t *e);
  bool skip_is_now(every_count_t *e);
  bool modulo_is_now(every_count_t *e, int16_t, bool);
  void every_tick(every_count_t *);
  void every_set_mod(every_count_t *, int16_t);

  uint8_t ss_get_script_pol(size_t idx);
  void ss_set_script_pol(size_t idx, uint8_t pol);

  uint8_t ss_get_script_len(script_number_t idx);
  void ss_clear_script(size_t script_idx);


  const Command *ss_get_script_command(script_number_t script_idx,
                                              size_t c_idx);
  void ss_insert_script_command(script_number_t script_idx, size_t command_idx,
                                const Command *cmd);
  void ss_delete_script_command(script_number_t script_idx, size_t command_idx);
  void ss_copy_script_command(Command *dest, script_number_t script_idx,
                              size_t c_idx);
  void ss_overwrite_script_command(script_number_t script_idx,
                                   size_t command_idx,
                                   const Command *cmd);




  FLASHMEM bool ss_get_script_comment(script_number_t script_idx, size_t c_idx);
  FLASHMEM void ss_set_script_comment(script_number_t script_idx, size_t c_idx,
                                      uint8_t on);
  FLASHMEM void ss_toggle_script_comment(script_number_t script_idx, size_t c_idx);





  FLASHMEM const scene_script_t *ss_scripts_ptr(void) const;
  FLASHMEM scene_script_t *ss_scripts_ptr(void);
  FLASHMEM static size_t ss_scripts_size(void);


  FLASHMEM void resetTimestamps(void);

  FLASHMEM scene_scale_t scale_get(scale_t scale, scene_scale_t x);
  FLASHMEM void ss_variables_init(void);
  FLASHMEM void ss_rand_init(uint32_t seed = 0);



  voice_collection_t &voices;
  cal_data_t cal;
  Chaos chaos;
  scene_rand_t rand_states;
  scene_variables_t variables;
  scene_script_t scripts[SCRIPT_COUNT];
  scene_stack_op_t stack_op;
  uint8_t last_voice = {0};
  bool every_last;
  bool initializing = {true};
  const scene_state_version_t version = {1};

private:

  FLASHMEM void ss_rand_init_field(tele_rand_t &field, uint32_t seed);
  
  FLASHMEM scale_t scale_init(scene_scale_t izero, scene_scale_t imax, scene_scale_t ozero, scene_scale_t omax);

  FLASHMEM void ss_set_script_command(script_number_t script_idx, size_t c_idx,
                             const Command *cmd);
  FLASHMEM void ss_set_script_len(script_number_t idx, uint8_t l);
};

#endif
