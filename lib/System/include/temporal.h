#ifndef _TEMPORAL_H__
#define _TEMPORAL_H__

#include <sequence.h>

class Temporal {

  static float beat_interval_ms;

public:
  static void setBeatInterval(float interval) {
    beat_interval_ms = interval;
  }

  static float getBeatInterval() {
    return beat_interval_ms;
  }

  static float durationInMs(int duration, int denom, Sequence::DurationUnit unit, uint8_t dots) {
    float dur_ms = {0};
    switch (unit) {
    case Sequence::DurRel:
      // Relational time is fractional (hence inverted num/denom)
      dur_ms = (float(denom) / duration) * 4 * beat_interval_ms;
      break;

    // Subharmonicon style "tempo divisions"

    // 1T = 16
    // 2T = 8
    // 3T = 8.
    // 4T = 4
    // 5T = --
    // 6T = 4.
    // 7T = 4..
    // 8T = 2
    // 9T = --
    // 10T = --
    // 11T = --
    // 12T = 2.
    // 13T = --
    // 14T = 2..
    // 15T = 2...
    // 16T = 1

    // Seq mod: D x
    // Changes duration of notes in the sequence
    // (most useful if pattern-able)

      // N(C E G B A B)
      // DUR(8 16 0 8 4)

    // NTH: can patterns be combined?

      // N(C2 D2)
      // N(+ G E A B)       <-- concat/extend
      // N(* B8. E4.. D8 D) <-- merge/overlay?

    // (Are we re-implementing Tidal cycles now?)

    case Sequence::DurMs:
      dur_ms = float(duration) / denom;
      break;

    case Sequence::DurSec:
      dur_ms = float(duration * 1000) / denom;
      break;

    default:
      return dur_ms;
    }

    float dottedMs = dur_ms;
    while (dots > 0) {
      dur_ms += dottedMs / 2;
      dottedMs /= 2;
      dots--;
    }

    return dur_ms;
  }

  static int durationInTicks(int duration, int denom, Sequence::DurationUnit unit, uint8_t dots) {
    float dur_ticks = {0};
    switch (unit) {
    case Sequence::DurRel:
      // Relational time is fractional (hence inverted num/denom)
      dur_ticks = (float(denom) / duration) * 4 * 24;
      break;

    default:
      return dur_ticks;
    }

    float dottedTicks = dur_ticks;
    while (dots > 0) {
      dur_ticks += dottedTicks / 2;
      dottedTicks /= 2;
      dots--;
    }

    return dur_ticks;
  }

  static float noteLengthMs(float dur_ms, uint8_t note_length, bool legato) {
    if (legato)
      return dur_ms;
    return (note_length / 100.0) * dur_ms;
  }

  static int noteLengthTicks(int dur_ticks, uint8_t note_length, bool legato) {
    if (legato)
      return dur_ticks;
    return (note_length / 100.0) * (float)dur_ticks;
  }
};

#endif