#ifndef _CONTROLLERS_MIDI_H__
#define _CONTROLLERS_MIDI_H__

#include "avr/pgmspace.h"
#include <scene.h>
#include <stdint.h>

namespace Controllers {
template <class T, class S>
class Midi {
public:
  Midi(T &tele, S &service)
      : tt(tele), midiService(service) {
    instance = this;
  }

  FLASHMEM void registerHandlers(void);

  // MidiInterface does not support passing context to callbacks
  static Midi &getInstance(void) { return *instance; }

  static void midi_clock_tick(void) { getInstance().clock_tick(); }

  static void midi_seq_start(void) { getInstance().seq_start(); }

  static void midi_seq_stop(void) { getInstance().seq_stop(); }

  static void midi_seq_continue(void) { getInstance().seq_continue(); }

private:
  T &tt;
  S &midiService;

  static Midi *instance;

  void clock_tick(void);
  void seq_start(void);
  void seq_stop(void);
  void seq_continue(void);
};
} // namespace Controllers

#include <controllers/midi.hpp>

#endif
