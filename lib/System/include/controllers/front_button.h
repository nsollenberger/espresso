#ifndef _CONTROLLERS_FRONT_BUTTON_H__
#define _CONTROLLERS_FRONT_BUTTON_H__

#include "models/adc_inputs.h"
#include "models/mode_manager.h"
#include "models/preset_r_mode.h"
#include "models/screensaver.h"
#include <stdint.h>

namespace Controllers {
class FrontButton {
public:
  FrontButton(Models::AdcInputs &_adc, Models::ModeManager &_mm,
              Models::PresetRMode &_pr, Models::Screensaver &_ss)
      : adcInputs(_adc), modeManager(_mm), presetRMode(_pr), screensaver(_ss) {}

  void handleButtonPress(bool isPressed) {
    if (screensaver.resetTimer())
      return;

    if (isPressed == 0) {
      if (modeManager.mode != M_PRESET_R) {
        front_timer = 0;
        presetRMode.set_preset_last(adcInputs.getRawParam() >> 7);
        modeManager.activateMode(M_PRESET_R);
      } else
        front_timer = 15;
    } else {
      if (front_timer) {
        modeManager.activatePriorMode();
      }
      front_timer = 0;
    }
  }

  void checkHeldButton(void) {
    if (front_timer) {
      if (front_timer == 1) {
        if (modeManager.mode == M_PRESET_R) {
          presetRMode.process_preset_r_load();
        }
        front_timer = 0;
      } else
        front_timer--;
    }
  }

private:
  Models::AdcInputs &adcInputs;
  Models::ModeManager &modeManager;
  Models::PresetRMode &presetRMode;
  Models::Screensaver &screensaver;

  uint8_t front_timer = {0};
};
} // namespace Controllers

#endif