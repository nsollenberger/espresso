#ifndef _CONTROLLERS_HID_KEYBOARD_H__
#define _CONTROLLERS_HID_KEYBOARD_H__

#include <stdint.h>

#include "models/voice_settings.h"

class Teletype;
class IndicatorLights;

namespace Models {
  class Keyboard;
  class Screensaver;
  class Settings;
  class ModeManager;

  template <class T, class I>
  class CvOutputs;

  class EditMode;
  class VoiceSettings;
}

namespace Controllers {
class HidKeyboard {
public:
  HidKeyboard(voice_collection_t &_v, Models::EditMode &_e, Models::Screensaver &_s,
              Models::ModeManager &_m, Models::CvOutputs<Teletype, IndicatorLights> &_cv,
              Models::Keyboard &_kbd, Models::Settings &_set, Models::VoiceSettings &_vset)
      : voices(_v), editMode(_e), screensaver(_s),
        modeManager(_m), cvOutputs(_cv), kbd(_kbd), settings(_set), voiceSettings(_vset) {}

  void checkHeldKeys(void);
  void processKeypress(uint8_t key, uint8_t mod_key, bool is_held_key);

private:
  voice_collection_t &voices;
  Models::EditMode &editMode;
  Models::Screensaver &screensaver;
  Models::ModeManager &modeManager;
  Models::CvOutputs<Teletype, IndicatorLights> &cvOutputs;
  Models::Keyboard &kbd;
  Models::Settings &settings;
  Models::VoiceSettings &voiceSettings;

  bool processKeypressGlobal(uint8_t key, uint8_t mod_key, bool is_held_key);
};
} // namespace Controllers

#endif
