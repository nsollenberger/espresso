#ifndef _SEQUENCE_H__
#define _SEQUENCE_H__

#include "espresso/command.h"
#include <algorithm>
#include <array>
#include <etl/forward_list.h>
#include <stdint.h>
#include <string.h> // memcpy

#include <random.h>
#include <table.h>
#include <types.h>
#include <util.h>

#define MAX_CHORD_NOTES 4
#define MAX_SEQ_NOTES 12
#define MAX_REPEATS 8
#define DEFAULT_DURATION_REL 4
#define MIN_NOTE_LENGTH_MS 50

using Util::bit_reverse;

namespace Espresso {
class Token;
}

class Sequence {
public:
  Sequence() { hardReset(); }

  void hardReset();

  enum DurationUnit : uint8_t {
    DurNull = 0,
    DurRel,
    DurMs,
    DurSec,
  };

  enum NoteName : int8_t {
    NoteRest = 0,
    NoteC,
    NoteDb,
    NoteD,
    NoteEb,
    NoteE,
    NoteF,
    NoteGb,
    NoteG,
    NoteAb,
    NoteA,
    NoteBb,
    NoteB,
  };

  enum ModifierName : uint8_t {
    ModNone = 0,
    ModFast,
    ModFlip, // Invert note sequence
    ModFrac, // Like stutter, but variable
    ModHarmonize, // Adds interval relative to note[0]
    ModLoop,
    ModNRotate,
    ModProbability,
    ModQuantize,
    ModReverse,
    ModRotate,
    ModShuffle,
    ModSlew,
    ModSlewDown,
    ModSlewUp,
    ModSlow,
    ModStutter,
    ModTranspose,
    ModTruncate,
    ModMaxCount
  };

  static void packNoteName(uint8_t &outData, NoteName note);
  static void packNoteOctave(uint8_t &outData, int8_t octave);
  static void packNoteDuration(int16_t &outValue, int duration,
                               DurationUnit unit);
  static void packNoteDots(int16_t &outValue, uint8_t dots);
  static void packNoteLegato(int16_t &outValue, bool legato);
  static void packNoteRepeats(int16_t &outValue, uint8_t repeats);

  static const char* getNoteString(NoteName note) {
    switch (note) {
      case NoteC: return "C";
      case NoteDb: return "D-";
      case NoteD: return "D";
      case NoteEb: return "E-";
      case NoteE: return "E";
      case NoteF: return "F";
      case NoteGb: return "G-";
      case NoteG: return "G";
      case NoteAb: return "A-";
      case NoteA: return "A";
      case NoteBb: return "B-";
      case NoteB: return "B";
      case NoteRest:
      default: return "R";
    }
  }

  static const char* getDurationUnitString(DurationUnit unit) {
    switch (unit) {
      case DurRel: return "";
      case DurMs: return "MS";
      case DurSec: return "S";
      case DurNull:
      default: return "";
    }
  }

  struct NoteGroup {
    NoteName notes[MAX_CHORD_NOTES];
    uint16_t duration;
    uint16_t duration_denom;
    uint16_t slew_up;
    uint16_t slew_dn;
    DurationUnit duration_unit;
    int8_t octave_offset[MAX_CHORD_NOTES];
    uint8_t note_count;
    uint8_t dots;
    uint8_t repeats;
    bool legato;
    bool silent;
  };

  static const NoteGroup quarterNoteRest;

  void unpackData(const Espresso::Token &tele_data);
  void resetData() {
    _length = 0;
    scale_index = 0;
    resetMods();
  }

  struct Modifier {
    int16_t args[MAX_SEQ_NOTES];
    ModifierName name;
    uint8_t length;
  };

  void applyMod(ModifierName m, int16_t v = 0) {
    applyModSeq(m, &v, 1);
  }

  void applyModSeq(ModifierName m, int16_t *v, int length) {
    if (mod_count < ModMaxCount) {
      memcpy(mods[mod_count].args, v, length * sizeof(int16_t));
      mods[mod_count].name = m;
      mods[mod_count].length = length;
      mod_count++;
      _length_processed = 0;
    }
  }

  void resetMods() {
    mod_count = 0;
    _length_processed = 0;
  }

  NoteGroup* getNotesAtStep(uint8_t index) {
    auto &ns = _ns;
    if (!_length_processed) {
      memcpy(&ns, &steps, sizeof(NoteGroup) * MAX_SEQ_NOTES);
      auto len = _length;
      initializeSlew(ns, len);
      preserveDuration(ns, len);
      accumulateOctaveOffsets(ns, len);
      len = expandRepeats(ns, len);

      for (int m_i = 0; m_i < mod_count; m_i++) {
        auto &mod = mods[m_i];
        if (mod.length > 1) {
          int n_i = 0;
          int arg_i = 0;
          while (n_i < len) {
            auto processed = processMod(ns, n_i, n_i, len, mod.name, mod.args[arg_i % mod.length]);
            n_i += processed;
            len += processed - 1; // Only increment if processing resulted in additional notes
            arg_i++;
          }
        } else {
          auto processed = processMod(ns, 0, len - 1, len, mod.name, mod.args[0]);
          len = processed;
        }
      }

      _length_processed = len;
    }

    return &ns[index];
  }

  uint8_t length() {
    if (_length && !_length_processed) {
      getNotesAtStep(0);
    }
    return _length_processed;
  }

  uint8_t raw_length() {
    return _length;
  }

  NoteGroup steps[MAX_SEQ_NOTES];
private:
  NoteGroup _ns[MAX_SEQ_NOTES];
public:
  Modifier mods[ModMaxCount];
  int16_t scale_index;
  uint8_t mod_count;

private:
  uint8_t _length;
  uint8_t _length_processed;

  void initializeSlew(NoteGroup ns[], uint8_t length) {
    for (int i = 0; i < length; i++) {
      ns[i].slew_up = 0;
      ns[i].slew_dn = 0;
    }
  }

  void preserveDuration(NoteGroup ns[], uint8_t length) {
    // Preserve duration before mutating to keep sequence length the same
    int prev_duration = DEFAULT_DURATION_REL;
    for (int i = 0; i < length; i++) {
      if (ns[i].duration == 0) {
        ns[i].duration = prev_duration;
        ns[i].duration_unit = DurRel;
      }
      prev_duration = ns[i].duration;
    }
  }

  void accumulateOctaveOffsets(NoteGroup ns[], uint8_t length) {
    // Octave offsets are accumulated, so notes do not change register when being reordered
    int oct_offset_acc = 0;
    for (int i = 0; i < length; i++) {
      for (int n = 0; n < ns[i].note_count; n++) {
        oct_offset_acc += ns[i].octave_offset[n];
        ns[i].octave_offset[n] = oct_offset_acc;
      }
    }
  }

  // Must be called with complete `ns[MAX_SEQ_NOTES]` to avoid array overflow
  uint8_t expandRepeats(NoteGroup ns[MAX_SEQ_NOTES], uint8_t length) {
    int i = 0;
    while (i < length) {
      auto repeats = ns[i].repeats;
      if (repeats > 1) {
        for (int x = MAX_SEQ_NOTES - 1; x >= (i + repeats); x--) {
          ns[x] = ns[x - (repeats - 1)];
        }
        ns[i].repeats = 1;
        for (int r = 1; r < repeats; r++) {
          ns[i+r] = ns[i];
        }
        // advance past the expanded repeats
        i += repeats - 1;
        length += repeats - 1;
      }
      i++;
    }

    return length;
  }

  uint8_t processMod(NoteGroup ns[], uint8_t start_index, uint8_t stop_index, uint8_t ns_len, ModifierName m, int16_t arg) {
    const uint8_t process_len = (stop_index - start_index) + 1;
    uint8_t processed = process_len;
    auto *process_ns = &ns[start_index];
    auto repeats_set = false;
    switch (m) {
      case ModFast:
        modFast(process_ns, process_len, arg);
        break;

      case ModFlip:
        modFlip(process_ns, process_len);
        break;

      case ModFrac:
        modFrac(process_ns, process_len, arg, repeats_set);
        break;

      case ModHarmonize:
        modHarmonize(process_ns, process_len, arg);
        break;

      case ModLoop:
        // ModLoop requires full ns[MAX_SEQ_NOTES]
        if (start_index > 0) break;
        processed = modLoop(process_ns, process_len, arg);
        break;

      case ModNRotate:
        modNRot(process_ns, process_len, arg);
        break;

      case ModProbability:
        modProbability(process_ns, process_len, arg);
        break;

      case ModQuantize:
        modQuantize(process_ns, process_len, arg);
        break;

      case ModReverse:
        modRev(process_ns, process_len);
        break;

      case ModRotate:
        modRot(process_ns, process_len, arg);
        break;

      case ModShuffle:
        modShuf(process_ns, process_len);
        break;

      case ModSlew:
        modSlewUp(process_ns, process_len, arg);
        modSlewDown(process_ns, process_len, arg);
        break;

      case ModSlewDown:
        modSlewDown(process_ns, process_len, arg);
        break;

      case ModSlewUp:
        modSlewUp(process_ns, process_len, arg);
        break;

      case ModSlow:
        modSlow(process_ns, process_len, arg);
        break;

      case ModStutter:
        modStutter(process_ns, process_len, arg, repeats_set);
        break;

      case ModTranspose:
        modTranspose(process_ns, process_len, arg);
        break;

      case ModTruncate:
        modTruncate(process_ns, process_len, arg);
        break;

      case ModNone:
      case ModMaxCount:
        break;
    }

    if (repeats_set) {
      // subtract process_len b/c it is included in ns_len
      auto result = expandRepeats(ns, ns_len + processed - process_len);
      processed += result - ns_len;
    }
    return processed;
  }

  void modFast(NoteGroup ns[], uint8_t length, int16_t arg) {
    auto f = std::min<int16_t>(arg, 32);

    if (f > 1) {
      for (int i = 0; i < length; i++) {
        if (ns[i].duration_unit == DurRel) {
          ns[i].duration *= f;
        } else {
          ns[i].duration_denom *= f;
        }
      }
    }
  }

  void modFlip(NoteGroup ns[], uint8_t length) {
    etl::forward_list<NoteName, MAX_SEQ_NOTES> ms = {};
    for (int i = 0; i < length; i++) {
      if (ns[i].silent || ns[i].note_count == 0) continue;
      ms.push_front(ns[i].notes[0]);
    }
    ms.sort();
    ms.unique();
    for (int i = 0; i < length; i++) {
      if (ns[i].silent || ns[i].note_count == 0) continue;
      int fi = 0;
      for (const auto &n : ms) {
        if (ns[i].notes[0] == n) break;
        fi++;
      }
      const int ti = ms.size() - 1 - fi;
      fi = 0;
      for (const auto &n : ms) {
        if (fi == ti) {
          ns[i].notes[0] = n;
          ns[i].note_count = 1; // ignore chords for now
          break;
        }
        fi++;
      }
    }
  }

  void modFrac(NoteGroup ns[], uint8_t length, int16_t arg, bool &repeats_set) {
    if (arg > 1) {
      for (int i = 0; i < length; i++) {
        if (ns[i].silent || ns[i].note_count == 0) continue;
        auto stut = Port::rand(1, std::min<int16_t>(9, arg + 1));
        if (stut <= 1) continue;
        if (ns[i].duration_unit == DurRel) {
          ns[i].duration *= stut;
        } else {
          ns[i].duration_denom *= stut;
        }
        ns[i].repeats *= stut;
        repeats_set = true;
      }
    }
  }

  void modHarmonize(NoteGroup ns[], uint8_t length, int16_t arg) {
    for (int i = 0; i < length; i++) {
      if (ns[i].silent || ns[i].note_count == 0) continue;
      if (ns[i].note_count < MAX_CHORD_NOTES) {
        ns[i].notes[ns[i].note_count] = static_cast<NoteName>(ns[i].notes[0] + arg);
        ns[i].note_count++;
      }
    }
  }

  // Must be called with complete `ns[MAX_SEQ_NOTES]` to avoid array overflow
  uint8_t modLoop(NoteGroup ns[MAX_SEQ_NOTES], uint8_t length, int16_t loop_length) {
    if (loop_length > length || loop_length < 1) return length;

    float seq_dur = 0;
    float loop_dur = 0;
    float remaining_dur = 0;

    for (auto i = 0; i < length; i++) {
      auto d = getNoteDurationMs(ns[i]);
      seq_dur += d;

      if (i < loop_length)
        loop_dur = seq_dur;
    }
    remaining_dur = seq_dur - loop_dur;
    length = loop_length;

    int n_i = 0;
    auto next_note_dur = getNoteDurationMs(ns[n_i]);
    while (length < MAX_SEQ_NOTES && remaining_dur >= next_note_dur) {
      ns[length] = ns[n_i];
      length++;
      n_i = (n_i + 1) % loop_length;
      remaining_dur -= next_note_dur;
      next_note_dur = getNoteDurationMs(ns[n_i]);
    }

    if (length < MAX_SEQ_NOTES && remaining_dur) {
      if (remaining_dur > MIN_NOTE_LENGTH_MS) {
        ns[length] = ns[n_i];
      } else {
        ns[length].notes[0] = NoteRest;
        ns[length].note_count = 0;
      }
      ns[length].duration = remaining_dur;
      ns[length].duration_unit = DurMs;
      length++;
      remaining_dur = 0;
    }

    return length;
  }

  void modNRot(NoteGroup ns[], uint8_t length, int16_t arg) {
    auto r = arg % length;
    // in this form, only the Notes are rotated, not the note length/etc
    if (r > 0) {
      NoteGroup ms[MAX_SEQ_NOTES] = {};
      for (int i = 0; i < length; i++) {
        memcpy(ms[i].notes, ns[i].notes, sizeof(NoteName) * MAX_CHORD_NOTES);
        memcpy(ms[i].octave_offset, ns[i].octave_offset, sizeof(NoteName) * MAX_CHORD_NOTES);
        ms[i].note_count = ns[i].note_count;
      }
      for (int i = 0; i < length; i++) {
        auto ir = (i + r) % length;
        memcpy(ns[i].notes, ms[ir].notes, sizeof(NoteName) * MAX_CHORD_NOTES);
        memcpy(ns[i].octave_offset, ms[ir].octave_offset, sizeof(NoteName) * MAX_CHORD_NOTES);
        ns[i].note_count = ms[ir].note_count;        
      }
    }
  }

  void modProbability(NoteGroup ns[], uint8_t length, int16_t arg) {
    // apply probability as a property on NoteGroup
    for (int ni = 0; ni < length; ni++) {
      ns[ni].silent = static_cast<int16_t>(Port::rand(1, 101)) > arg;
    }
  }

  void modQuantize(NoteGroup ns[], uint8_t length, int16_t root) {
    for (int i = 0; i < length; i++) {
      if (ns[i].silent || ns[i].note_count == 0) continue;
      for (int ni = 0; ni < ns[i].note_count; ni++) {
        // NTH: take octave in to consideration, when snapping to nearest note
        ns[i].notes[ni] = quantizeNote(scale_index, root, ns[i].notes[ni]);
      }
    }
  }

  void modRev(NoteGroup ns[], uint8_t length) {
    std::reverse(ns, ns + length);
  }

  void modRot(NoteGroup ns[], uint8_t length, int16_t arg) {
    auto r = arg % length;
    if (r > 0)
      std::rotate(ns, ns + r, ns + length);
  }

  void modShuf(NoteGroup ns[], uint8_t length) {
    for (int ni = 0; ni < length; ni++) {
      int mi = Port::rand() % length;
      if (ni != mi)
        std::swap(ns[ni], ns[mi]);
    }
  }

  void modSlewDown(NoteGroup ns[], uint8_t length, uint16_t arg) {
    for (int i = 0; i < length; i++) {
      ns[i].slew_dn = arg;
    }
  }

  void modSlewUp(NoteGroup ns[], uint8_t length, uint16_t arg) {
    for (int i = 0; i < length; i++) {
      ns[i].slew_up = arg;
    }
  }

  void modSlow(NoteGroup ns[], uint8_t length, int16_t arg) {
    auto f = std::min<int16_t>(arg, 32);

    if (f > 1) {
      for (int i = 0; i < length; i++) {
        if (ns[i].duration_unit == DurRel) {
          ns[i].duration_denom *= f;
        } else {
          ns[i].duration *= f;
        }
      }
    }
  }

  void modStutter(NoteGroup ns[], uint8_t length, int16_t arg, bool &repeats_set) {
    if (arg > 1) {
      auto stut = std::min<int16_t>(8, arg);
      for (int i = 0; i < length; i++) {
        if (ns[i].silent || ns[i].note_count == 0) continue;
        if (ns[i].duration_unit == DurRel) {
          ns[i].duration *= stut;
        } else {
          ns[i].duration_denom *= stut;
        }
        ns[i].repeats *= stut;
      }
      repeats_set = true;
    }
  }

  void modTranspose(NoteGroup ns[], uint8_t length, int16_t arg) {
    for (int i = 0; i < length; i++) {
      if (ns[i].silent || ns[i].note_count == 0) continue;
      for (int ni = 0; ni < ns[i].note_count; ni++) {
        auto note_index = Sequence::note_to_index(ns[i].notes[ni]);
        auto transposed = note_index + arg;
        while (transposed < 0) {
          transposed += 12;
          ns[i].octave_offset[ni] -= 1;
        }
        while (transposed > 11) {
          transposed -= 12;
          ns[i].octave_offset[ni] += 1;
        }
        ns[i].notes[ni] = Sequence::index_to_note(transposed);
      }
    }
  }

  void modTruncate(NoteGroup ns[], uint8_t length, int16_t arg) {
    if (arg < 0) {
      arg = length + arg;
      for (int i = 0; i < arg; i++) {
        ns[i].silent = true;
      }      
    } else {
      for (int i = arg; i < length; i++) {
        ns[i].silent = true;
      }
    }
  }

  NoteName quantizeNote(int16_t scale, int16_t root, NoteName input) {
    int16_t mask = Sequence::scale_to_bitmask(scale);
    return Sequence::quantize_to_bitmask_scale(mask, root, input);
  }

  float getNoteDurationMs(NoteGroup &ns);

  // @nate - refactor to utility class

  static int16_t scale_to_bitmask(int16_t scale) {
    // look up a 12-bit scale mask (LSB = root, reversed for human readability)
    return bit_reverse(table_n_b[scale % nb_nbx_scale_presets], 12);
  }

  static int16_t clamp_index(int16_t index) {
    auto i = index % 12;
    if (i < 0) i += 12;
    return i;
  }

  /**
   * scale_bits is a bitwise value, indicating which semitones are "in key"
   * root offsets or transposes the scale from C (0), in semitones
   */
  static NoteName quantize_to_bitmask_scale(int16_t scale_bits, int16_t root,
                                           NoteName input) {
    int16_t target = Sequence::clamp_index(
      Sequence::note_to_index(input) - root
    );

    for (int16_t i = 0; i < 7; i++) {
      auto search_up = Sequence::clamp_index(target + i);
      if (scale_bits & (1 << search_up)) {
        return Sequence::index_to_note(
          Sequence::clamp_index(search_up + root)
        );
      }
      auto search_dn = Sequence::clamp_index(target - i);
      if (scale_bits & (1 << search_dn)) {
        return Sequence::index_to_note(
          Sequence::clamp_index(search_dn + root)
        );
      }
    }
    return input;
  }

  static NoteName index_to_note(int16_t index) {
    //    index: C = 0, D = 3, ... B = 12
    // NoteName: C = 1, D = 4, ... B = 13
    return static_cast<NoteName>(index + 1);
  }

  static int16_t note_to_index(NoteName note) {
    // NoteName: C = 1, D = 4, ... B = 13
    //    index: C = 0, D = 3, ... B = 12
    return note - 1;
  }

};

#endif
