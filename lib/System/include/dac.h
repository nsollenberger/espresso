#ifndef _DAC_H__
#define _DAC_H__

#include <stdint.h>

class Dac {
public:
  static void updateOutputs(uint16_t a0, uint16_t a1, uint16_t a2, uint16_t a3);

private:
  static void prepareForTx(uint16_t value, uint8_t (&result)[2]) {
    value = 4095 - value; // Invert output
    result[0] = (value >> 8) & 0x0F; // Keep top 4 bits (12 total) for MCP4822
    result[1] = value & 0xFF;
  }
};

#endif