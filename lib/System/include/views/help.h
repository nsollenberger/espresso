#ifndef _VIEWS_HELP__
#define _VIEWS_HELP__

#include <font.h>
#include <models/help_mode.h>
#include <screen.h>

namespace Views {
class Help {
public:
  Help(Models::HelpMode &_model) : model(_model) {}

  void render(Screen &screen) {
    auto &line = screen.getBuffer();

    if (!model.dirty) {
      return;
    }

    uint8_t help_line_ct = 8;
    if (model.search_mode != Models::HelpMode::SEARCH_MODE_NONE)
      help_line_ct--;
    if (model.search_result == Models::HelpMode::SEARCH_RESULT_MISS)
      help_line_ct--;

    // clamp value of model.page_no
    if (model.page_no >= HELP_PAGES)
      model.page_no = HELP_PAGES - 1;

    // clamp value of offset
    if (model.offset >= model.help_length[model.page_no] - help_line_ct)
      model.offset = model.help_length[model.page_no] - help_line_ct;

    const char **text = model.help_pages[model.page_no];

    for (uint8_t y = 0; y < help_line_ct; y++) {
      if (model.search_result == Models::HelpMode::SEARCH_RESULT_HIT &&
          (y + model.offset) == model.search_state.line) {
        line[y].fill(eColorLight1);
        Font::string_region_clip(line[y], text[y + model.offset], 2, 0, eColorDark1, eColorLight1);
      } else {
        line[y].fill(eColorBlank);
        Font::string_region_clip(line[y], text[y + model.offset], 2, 0, eColorDark1, eColorBlank);
      }
    }

    if (model.search_result == Models::HelpMode::SEARCH_RESULT_MISS) {
      char s[36];
      strcpy(s, model.le.line_editor_get());
      strcat(s, ": NOT FOUND");
      line[6].fill(eColorBlank);
      Font::string_region_clip(line[6], s, 0, 0, eColorMedium1, eColorBlank);
    }

    switch (model.search_mode) {
    case Models::HelpMode::SEARCH_MODE_FWD:
      model.le.line_editor_draw('>', &line[7]);
      break;
    case Models::HelpMode::SEARCH_MODE_REV:
      model.le.line_editor_draw('<', &line[7]);
      break;
    default:
      break;
    }

    model.dirty = false;

    screen.draw();
  }

private:
  Models::HelpMode &model;
};

} // namespace Views

#endif
