#ifndef _PRESET_READ_VIEW__
#define _PRESET_READ_VIEW__

#include <region.h>
#include <stdio.h>
#include <font.h>
#include "models/preset_r_mode.h"
#include <screen.h>

namespace Views {
class PresetRead {
public:
  PresetRead(Models::PresetRMode &_m) : model(_m) {}

  void render(Screen &screen) {
    if (!model.dirty) return;

    auto &line = screen.getBuffer();

    for (int i = 0; i < 8; i++) {
      line[i].fill(eColorBlank);
    }

    char s[32];
    Util::itoa(model.preset_select, s, 10);
    line[0].fill(eColorLight1);
    Font::string_region_clip_right(line[0], s, 126, 0, eColorDark2, eColorLight1);
    Font::string_region_clip(line[0], model.text_buffer[0], 2, 0, eColorDark2, eColorLight1);

    for (uint8_t y = 1; y < 8; y++) {
      line[y].fill(eColorBlank);
      Font::string_region_clip(line[y], model.text_buffer[model.offset + y], 2, 0, eColorDark1, eColorBlank);
    }

    model.dirty = false;
    screen.draw();
  }

private:
  Models::PresetRMode& model;
};

} // namespace Views

#endif
