#ifndef _SETTINGS_1_VIEW__
#define _SETTINGS_1_VIEW__

#include <cstdint>
#include <stdio.h>

#include "font.h"
#include "region.h"
#include "models/settings.h"
#include "screen.h"
#include "./scrollable_view.h"

namespace Views {

using Models::Settings;

class Settings_1 : public ScrollableView {

/**
 * Number of "content" rows, where each row is a Screen buffer Region
 */
static constexpr uint8_t kTotalRegionCount = 11;
static constexpr uint8_t kBaseOffset = 3;

public:
  Settings_1(Settings &_m) : ScrollableView(kTotalRegionCount), model(_m) {}

  void render(Screen &screen) {
    if (!model.dirty) return;

    auto &buf = screen.getBuffer();
    for (int i = 0; i < 8; i++) {
      buf[i].fill(eColorBlank);
    }

    const auto selected = model.getSelection();
    const uint8_t col2Offset = 60;
    uint8_t index;

    if (selected == Settings::LOAD || selected == Settings::SAVE) {
      offset = 0;
    } else {
      offset = kBaseOffset;
    }

    if (onscreen(3, index)) {
      buf[index].fill(0, 6, buf[index].w, 7, eColorMedium2);
      buf[index].fill(0, 7, buf[index].w, 8, eColorLight1);
    }

    char str[8];
    const auto fractional = (static_cast<int>(model.clockTempo) * 10) % 10;
    sprintf(str, "%d.%d", static_cast<int>(model.clockTempo), fractional);
    if (onscreen(kBaseOffset + 2, index)) {
      Font::string_region_clip(buf[index], "BPM", col2Offset, 4, eColorDark2, eColorBlank);
      renderSelectableText(buf[index], str, col2Offset + 20, 0, Settings::CLOCK_TEMPO);
    }
    if (onscreen(kBaseOffset + 3, index)) {
      Font::string_region_clip(buf[index], "BPM", col2Offset, -4, eColorDark2, eColorBlank);
      renderSelectableText(buf[index], str, col2Offset + 20, -4, Settings::CLOCK_TEMPO);
    }

    sprintf(str, "%d", model.loopLength);
    if (onscreen(kBaseOffset + 5, index)) {
      Font::string_region_clip(buf[index], "Length", col2Offset, 4, eColorDark2, eColorBlank);
      renderSelectableText(buf[index], str, col2Offset + 27, 0, Settings::LOOP_LENGTH);
    }
    if (onscreen(kBaseOffset + 6, index)) {
      Font::string_region_clip(buf[index], "Length", col2Offset, -4, eColorDark2, eColorBlank);
      renderSelectableText(buf[index], str, col2Offset + 27, -4, Settings::LOOP_LENGTH);
    }

    renderSaveLoadButtons(buf);
    renderPlayPauseIcon(buf, model.isRunning, selected == Settings::PLAY_PAUSE);
    renderStopIcon(buf, selected == Settings::RESET);
    renderScrollIndicator(buf);

    model.dirty = false;
    screen.draw();
  }

private:
  Settings& model;


  /**
   * Renders a selectable item, highlighting the current selection.
   */
  void renderSelectableText(Region &reg, const char *str, const int x_off, const int y_off, const Settings::SettingSelection index) {
    const bool selected = index == model.getSelection();
    const auto fg = selected ? eColorDark2 : eColorDark1;
    const auto bg = selected ? (model.isEditing ? eColorLight2 : eColorLight1) : eColorBlank;
    Font::string_region_clip(reg, str, x_off, y_off, fg, bg, Font::Size16);
  }

  void renderSelectedBox(screen_buffer_t &buf, const uint8_t region_index, const int xoff_1, const int xoff_2) {
    uint8_t index;
    if (onscreen(region_index, index)) {
      auto &reg1 = buf[index];
      reg1.fill(xoff_1-1, 0, xoff_2+2, 1, eColorLight2);
      reg1.fill(xoff_1-2, 1, xoff_2+3, 2, eColorLight2);
      reg1.fill(xoff_1-3, 2, xoff_2+4, 8, eColorLight2);
    }
    if (onscreen(region_index + 1, index)) {
      auto &reg2 = buf[index];
      reg2.fill(xoff_1-3, 0, xoff_2+4, 6, eColorLight2);
      reg2.fill(xoff_1-2, 6, xoff_2+3, 7, eColorLight2);
      reg2.fill(xoff_1-1, 7, xoff_2+2, 8, eColorLight2);
    }

  }

  void renderSaveLoadButtons(screen_buffer_t &buf) {
    const auto selected = model.getSelection();
    uint8_t index;

    if (selected == Settings::LOAD) renderSelectedBox(buf, 1, 20, 46);
    if (selected == Settings::SAVE) renderSelectedBox(buf, 1, 70, 96);

    if (onscreen(1, index)) {
      Font::string_region_clip(buf[index], "Load", 20, 0, eColorDark1, eColorBlank, Font::Size16);
      Font::string_region_clip(buf[index], "Save", 70, 0, eColorDark1, eColorBlank, Font::Size16);
    }
    if (onscreen(2, index)) {
      Font::string_region_clip(buf[index], "Load", 20, -4, eColorDark1, eColorBlank, Font::Size16);
      Font::string_region_clip(buf[index], "Save", 70, -4, eColorDark1, eColorBlank, Font::Size16);
    }
  }

  void renderPlayPauseIcon(screen_buffer_t &buf, const bool is_running, const bool is_selected) {
    int xoff = 12;
    uint8_t index;

    if (is_selected) {
      renderSelectedBox(buf, kBaseOffset + 2, xoff, xoff + 22);
    }

    const auto playColor = !is_running ? eColorDark1 : eColorMedium1;

    if (onscreen(kBaseOffset + 2, index)) {
      auto &reg1 = buf[index];
      reg1.fill(xoff, 2, xoff+1, 3, playColor);
      reg1.fill(xoff, 3, xoff+2, 4, playColor);
      reg1.fill(xoff, 4, xoff+3, 5, playColor);
      reg1.fill(xoff, 5, xoff+4, 6, playColor);
      reg1.fill(xoff, 6, xoff+5, 7, playColor);
      reg1.fill(xoff, 7, xoff+6, 8, playColor);
    }

    if (onscreen(kBaseOffset + 3, index)) {
      auto &reg2 = buf[index];
      reg2.fill(xoff, 0, xoff+7, 1, playColor);
      reg2.fill(xoff, 1, xoff+6, 2, playColor);
      reg2.fill(xoff, 2, xoff+5, 3, playColor);
      reg2.fill(xoff, 3, xoff+4, 4, playColor);
      reg2.fill(xoff, 4, xoff+3, 5, playColor);
      reg2.fill(xoff, 5, xoff+2, 6, playColor);
      reg2.fill(xoff, 6, xoff+1, 7, playColor);
    }

    const auto pauseColor = is_running ? eColorDark1 : eColorMedium1;
    xoff += 14;

    if (onscreen(kBaseOffset + 2, index)) {
      auto &reg1 = buf[index];
      reg1.fill(xoff, 2, xoff+3, 8, pauseColor);
    }

    if (onscreen(kBaseOffset + 3, index)) {
      auto &reg2 = buf[index];
      reg2.fill(xoff, 0, xoff+3, 6, pauseColor);
    }

    xoff += 6;

    if (onscreen(kBaseOffset + 2, index)) {
      auto &reg1 = buf[index];
      reg1.fill(xoff, 2, xoff+3, 8, pauseColor);
    }

    if (onscreen(kBaseOffset + 3, index)) {
      auto &reg2 = buf[index];
      reg2.fill(xoff, 0, xoff+3, 6, pauseColor);
    }
  }

  void renderStopIcon(screen_buffer_t &buf, bool is_selected) {
    int xoff = 17;
    uint8_t index;

    if (is_selected) {
      renderSelectedBox(buf, kBaseOffset + 5, xoff - 5, xoff + 17);
    }

    xoff += 1;

    if (onscreen(kBaseOffset + 5, index)) {
      auto &reg1 = buf[index];
      reg1.fill(xoff, 2, xoff+11, 8, eColorMedium2);
    }

    if (onscreen(kBaseOffset + 6, index)) {
      auto &reg2 = buf[index];
      reg2.fill(xoff, 0, xoff+11, 6, eColorMedium2);
    }
  }
};

} // namespace Views

#endif
