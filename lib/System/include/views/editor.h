#ifndef _EDITOR_VIEW__
#define _EDITOR_VIEW__

#include <algorithm>

#include "espresso/validator.h"
#include "espresso/command.h"
#include "font.h"
#include "line_editor.h"
#include "models/edit_mode.h"
#include "screen.h"
#include "voice.h"

using std::max;
using std::min;

namespace Models {
  class ModeManager;
}

namespace Views {

class Editor {
public:
  Editor(Models::EditMode &_model) : model(_model) {}

  void render(Screen &screen) {
    auto &line = screen.getBuffer();

    uint8_t sel1 = min(model.line_no1, model.line_no2);
    uint8_t sel2 = max(model.line_no1, model.line_no2);

    if (model.dirty & model.D_INPUT) {
      bool muted = false;
      char prefix = model.script + '1';
      if (model.script == INIT_SCRIPT)
        prefix = 'I';
      else if (model.script <= TT_SCRIPT_8)
        muted = model.scene.voices[model.script].is_muted;

      if (sel1 == sel2)
        model.le.line_editor_draw(prefix, &line[7]);
      else
        line[7].fill(eColorBlank);

      char script_no[2] = {prefix, '\0'};
      Font::string_region_clip(line[7], script_no, 0, 0,
                               muted ? eColorMedium1 : eColorDark2,
                               eColorBlank);
      model.dirty &= ~model.D_INPUT;
    }

    if (model.dirty & model.D_MESSAGE) {
      char s[32];
      if (model.status != Espresso::E_OK) {
        strcpy(s, parse_error_str(model.status));
        if (model.error_msg[0]) {
          size_t len = strlen(s);
          strcat(s, ": ");
          strncat(s, model.error_msg, 32 - len - 3);
          model.error_msg[0] = 0;
        }
        model.status = Espresso::E_OK;
      } else {
        s[0] = 0;
      }

      line[6].fill(eColorBlank);
      Font::string_region_clip(line[6], s, 0, 0, eColorMedium1, eColorBlank);
      model.dirty &= ~model.D_MESSAGE;
    }

    char s[32];
    if (model.dirty & model.D_LIST) {
      for (int i = 0; i < 6; i++) {
        auto fg = model.scene.ss_get_script_comment(model.script, i)
                      ? eColorMedium2
                      : eColorDark2;
        auto bg = (i >= sel1 && i <= sel2) ? eColorLight1 : eColorBlank;
        line[i].fill(bg);
        if (model.scene.ss_get_script_len(model.script) > i) {
          model.scene.ss_get_script_command(model.script, i)->print(s);
          Font::string_region_clip(line[i], s, 2, 0, fg, bg);
        }
      }
      model.dirty &= ~model.D_LIST;
    }

    screen.draw();
  }

private:
  Models::EditMode &model;
};

} // namespace Views

#endif
