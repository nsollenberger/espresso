#ifndef _SCROLLABLE_VIEW_H__
#define _SCROLLABLE_VIEW_H__

#include <algorithm>

#include "font.h"
#include "region.h"

namespace Views {

class ScrollableView {
public:
  ScrollableView(uint8_t total_region_count) {
    scroll_height = 64 * (8.0 / static_cast<float>(total_region_count));
    scroll_constant = (64 - scroll_height) / (std::max<int>(total_region_count, 9) - 8);
  }

  uint8_t offset = {0};
  uint8_t scroll_height;
  uint8_t scroll_constant;

  /**
   * Detects if a "content index" is visible, given the current offset.
   *
   * Passes back the current index if visible, otherwise the offset delta
   * required to make the target index visible.
   */
  bool onscreen(int8_t target, uint8_t &out_index) {
    if (offset > target) {
      out_index = target - offset;
      return false;
    }
    if (target > 7 && offset < target - 7) {
      out_index = (target - 7) - offset;
      return false;
    }
    out_index = target - offset;
    return true;
  }

  void renderScrollIndicator(screen_buffer_t &buf) {
    const auto handle_y0 = offset * scroll_constant;
    const auto handle_y1 = handle_y0 + scroll_height;

    for (int ri = 0; ri < 8; ri++) {
      const auto &reg = buf[ri];
      const int xoff = 127;
      auto *p = reg.data + xoff;
      const auto *max = reg.data + reg.len;
      const auto width = static_cast<uint32_t>(reg.w);

      for (int yoff = 0; yoff < kFontCharH; yoff++) {
        const auto y_pos = (ri * 8) + yoff;
        const auto is_handle = (handle_y0 <= y_pos && y_pos < handle_y1);
        *p = is_handle ? eColorMedium1 : eColorLight1;
        p += width;
        if (p > max) break;
      }
    }
  }
};

} // namespace Views

#endif
