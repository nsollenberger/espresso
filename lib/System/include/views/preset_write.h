#ifndef _PRESET_WRITE_VIEW__
#define _PRESET_WRITE_VIEW__

#include <region.h>
#include <stdio.h>
#include <font.h>
#include "models/preset_r_mode.h"
#include <models/preset_w_mode.h>
#include <screen.h>

namespace Views {
class PresetWrite {
public:
  PresetWrite(Models::PresetWMode &_m) : model(_m) {}

  void render(Screen &screen) {
    if (!model.dirty) return;

    auto &line = screen.getBuffer();

    if (model.dirty == model.D_ALL) {
      for (int i = 0; i < 8; i++) {
        line[i].fill(eColorBlank);
      }
    }

    if (model.dirty & model.D_LIST) {
      char header[6] = ">>> ";
      Util::itoa(model.presetRMode.preset_select, header + 4, 10);
      line[0].fill(eColorLight1);
      Font::string_region_clip_right(line[0], header, 126, 0, eColorDark2, eColorLight1);
      Font::string_region_clip(line[0], "WRITE", 2, 0, eColorDark2, eColorLight1);

      for (uint8_t y = 1; y < 7; y++) {
        auto bg = (model.edit_line == (y - 1)) ? eColorLight1 : eColorBlank;
        line[y].fill(bg);
        auto fg = (bg != eColorBlank) ? eColorDark2 : eColorDark1;
        Font::string_region_clip(line[y], model.scene_text[model.edit_offset + y - 1], 2, 0,
                                 fg, bg);
      }
    }

    if (model.dirty & model.D_INPUT) {
      model.le.line_editor_draw('+', &line[7]);
    }

    model.dirty = false;
    screen.draw();
  }

private:
  Models::PresetWMode& model;
};

} // namespace Views

#endif
