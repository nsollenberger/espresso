#ifndef _VOICE_SETTINGS_1_VIEW__
#define _VOICE_SETTINGS_1_VIEW__

#include <cstdint>

#include "font.h"
#include "models/voice_settings.h"
#include "region.h"
#include "screen.h"
#include "./scrollable_view.h"

namespace Models {
  class ModeManager;
}

namespace Views {


class VoiceSettings_1 : public ScrollableView {

/**
 * Number of "content" rows, where each row is a Screen buffer Region
 */
static constexpr uint8_t kTotalRegionCount = 11;

public:
  VoiceSettings_1(Models::VoiceSettings &_m) : ScrollableView(kTotalRegionCount), model(_m) {}

  void render(Screen &screen) {
    if (!model.dirty) return;

    auto &buf = screen.getBuffer();
    auto &config = model.configs[model.script];

    for (int i = 0; i < 8; i++) {
      buf[i].fill(eColorBlank);
    }

    constexpr uint8_t p_2 = 2;
    constexpr uint8_t p_3 = 4;
    constexpr uint8_t settings_align_1 = 64;
    constexpr uint8_t settings_align_2 = 64;

    // Fixed header
    {
      char str[3];
      sprintf(str, "v%d", model.script + 1);
      Font::string_region_clip_right(buf[0], str, 128 - p_2, 0, eColorMedium2, eColorBlank);
    }

    if (model.back_to_top) {
      offset = 0;
    }
    // Update scroll offset (if needed) when selection changes
    else if (selected_cache != model.selected_index) {
      selected_cache = model.selected_index;
      const auto content_index = map_selected(selected_cache);
      uint8_t difference;
      if (!onscreen(content_index, difference)) {
        offset += difference;
      }
    }

    uint8_t index;

    // Scrollable contents
    if (onscreen(0, index)) {
      Font::string_region_clip(buf[index], "CV", p_2, 0, eColorDark2, eColorBlank);
    }

    if (onscreen(1, index)) {
      Font::string_region_clip_right(buf[index], "v/oct:", settings_align_1, 0, eColorDark1, eColorBlank);

      char str[3] = "--";
      if (config.cv_out > 0) sprintf(str, "%d", config.cv_out);
      renderSelectable(buf[index], str, settings_align_1 + p_3, 0, 0);
    }

    if (onscreen(2, index)) {
      Font::string_region_clip_right(buf[index], "gate:", settings_align_1, 0, eColorDark1, eColorBlank);

      char str[3] = "--";
      if (config.gate_out > 0) sprintf(str, "%d", config.gate_out);
      renderSelectable(buf[index], str, settings_align_1 + p_3, 0, 1);
    }

    if (onscreen(4, index)) {
      Font::string_region_clip(buf[index], "MIDI", p_2, 0, eColorDark2, eColorBlank);
    }

    if (onscreen(5, index)) {
      Font::string_region_clip_right(buf[index], "channel:", settings_align_1, 0, eColorDark1, eColorBlank);

      char str[3] = "--";
      if (config.midi_out_ch > 0) sprintf(str, "%d", config.midi_out_ch);
      renderSelectable(buf[index], str, settings_align_1 + p_3, 0, 2);
    }

    if (onscreen(6, index)) {
      Font::string_region_clip_right(buf[index], "octave:", settings_align_1, 0, eColorDark1, eColorBlank);

      char str[4];
      sprintf(str, "%d", config.octave);
      renderSelectable(buf[index], str, settings_align_1 + p_3, 0, 3);
    }

    if (onscreen(8, index)) {
      Font::string_region_clip(buf[index], "General", p_2, 0, eColorDark2, eColorBlank);
    }

    if (onscreen(9, index)) {
      Font::string_region_clip_right(buf[index], "note length:", settings_align_2, 0, eColorDark1, eColorBlank);

      char str[4];
      sprintf(str, "%d", config.note_length);
      renderSelectable(buf[index], str, settings_align_2 + p_3, 0, 4);
    }

    if (onscreen(10, index)) {
      Font::string_region_clip_right(buf[index], "default velocity:", settings_align_2, 0, eColorDark1, eColorBlank);

      char str[4];
      sprintf(str, "%d", config.note_vel);
      renderSelectable(buf[index], str, settings_align_2 + p_3, 0, 5);
    }

    // Scroll indicator
    renderScrollIndicator(buf);

    model.dirty = false;
    screen.draw();
  }

private:
  Models::VoiceSettings& model;
  uint8_t selected_cache = {0};

  /**
   * Maps from selection index to "content" index.
   *
   * Must be kept in sync with view logic in render method
   */
  uint8_t map_selected(uint8_t index) {
    switch (index) {
    case 0: return 1;
    case 1: return 2;
    case 2: return 5;
    case 3: return 6;
    case 4: return 9;
    case 5: return 10;
    }
    return 0;
  }

  /**
   * Renders a selectable item, highlighting the current selection.
   */
  void renderSelectable(Region &reg, const char *str, const int x_off, const int y_off, const int index) {
    const bool selected = index == model.selected_index;
    const auto fg = selected ? eColorDark2 : eColorDark1;
    const auto bg = selected ? (model.is_editing ? eColorLight2 : eColorLight1) : eColorBlank;
    Font::string_region_clip(reg, str, x_off, y_off, fg, bg);
  }
};

} // namespace Views

#endif
