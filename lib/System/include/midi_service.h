#ifndef _MIDI_SERVICE_H__
#define _MIDI_SERVICE_H__

#include <stdint.h>
#include <usb_host_service.h>

class MidiService {
public:
  MidiService(UsbHostService &usbHost)
      : hostMIDI1(usbHost.midi1), hostMIDI2(usbHost.midi2),
        hostMIDI3(usbHost.midi3) {}

  void read() {
    hostMIDI1.read();
    hostMIDI2.read();
    hostMIDI3.read();
    usbMIDI.read();
  }

  void sendNoteOn(uint8_t ch, uint8_t note, uint8_t vel) {
    if (ch == 0) {
      ch = 1;
    }
    if (vel == 0) {
      sendNoteOff(ch, note, vel);
    } else {
      // Teensy MIDI implementations do not use same argument order as Arduino MIDI Library
      hostMIDI1.sendNoteOn(note, vel, ch);
      hostMIDI2.sendNoteOn(note, vel, ch);
      hostMIDI3.sendNoteOn(note, vel, ch);
      usbMIDI.sendNoteOn(note, vel, ch);
    }
  }

  void sendNoteOff(uint8_t ch, uint8_t note, uint8_t vel) {
    if (ch == 0) {
      ch = 1;
    }
    hostMIDI1.sendNoteOff(note, vel, ch);
    hostMIDI2.sendNoteOff(note, vel, ch);
    hostMIDI3.sendNoteOff(note, vel, ch);
    usbMIDI.sendNoteOff(note, vel, ch);
  }

  void sendControlChange(uint8_t ch, uint8_t cc_num, uint8_t cc_val) {
    if (ch == 0) {
      ch = 1;
    }
    hostMIDI1.sendControlChange(cc_num, cc_val, ch);
    hostMIDI2.sendControlChange(cc_num, cc_val, ch);
    hostMIDI3.sendControlChange(cc_num, cc_val, ch);
    usbMIDI.sendControlChange(cc_num, cc_val, ch);
  }

  enum RealTimeEvents : uint8_t {
    RealTimeEventClock = 0xF8,
    RealTimeEventStart = 0xFA,
    RealTimeEventContinue = 0xFB,
    RealTimeEventStop = 0xFC,
    RealTimeEventActiveSensing = 0xFE,
    RealTimeEventSystemReset = 0xFF,
  };

  void sendClock() {
    hostMIDI1.sendRealTime(RealTimeEventClock);
    hostMIDI2.sendRealTime(RealTimeEventClock);
    hostMIDI3.sendRealTime(RealTimeEventClock);
    usbMIDI.sendRealTime(RealTimeEventClock);
  }

  typedef void (*GenericHandler)(uint8_t, uint8_t, uint8_t);
  typedef void (*VoidHandler)(void);

  void setHandleNoteOn(GenericHandler fptr) {
    hostMIDI1.setHandleNoteOn(fptr);
    hostMIDI2.setHandleNoteOn(fptr);
    hostMIDI3.setHandleNoteOn(fptr);
    usbMIDI.setHandleNoteOn(fptr);
  }
  void setHandleNoteOff(GenericHandler fptr) {
    hostMIDI1.setHandleNoteOff(fptr);
    hostMIDI2.setHandleNoteOff(fptr);
    hostMIDI3.setHandleNoteOff(fptr);
    usbMIDI.setHandleNoteOff(fptr);
  }
  void setHandleControlChange(GenericHandler fptr) {
    hostMIDI1.setHandleControlChange(fptr);
    hostMIDI2.setHandleControlChange(fptr);
    hostMIDI3.setHandleControlChange(fptr);
    usbMIDI.setHandleControlChange(fptr);
  }
  void setHandleClock(VoidHandler fptr) {
    hostMIDI1.setHandleClock(fptr);
    hostMIDI2.setHandleClock(fptr);
    hostMIDI3.setHandleClock(fptr);
    usbMIDI.setHandleClock(fptr);
  }
  void setHandleStart(VoidHandler fptr) {
    hostMIDI1.setHandleStart(fptr);
    hostMIDI2.setHandleStart(fptr);
    hostMIDI3.setHandleStart(fptr);
    usbMIDI.setHandleStart(fptr);
  }
  void setHandleStop(VoidHandler fptr) {
    hostMIDI1.setHandleStop(fptr);
    hostMIDI2.setHandleStop(fptr);
    hostMIDI3.setHandleStop(fptr);
    usbMIDI.setHandleStop(fptr);
  }
  void setHandleContinue(VoidHandler fptr) {
    hostMIDI1.setHandleContinue(fptr);
    hostMIDI2.setHandleContinue(fptr);
    hostMIDI3.setHandleContinue(fptr);
    usbMIDI.setHandleContinue(fptr);
  }

  MIDIDeviceBase &hostMIDI1;
  MIDIDeviceBase &hostMIDI2;
  MIDIDeviceBase &hostMIDI3;
};

#endif