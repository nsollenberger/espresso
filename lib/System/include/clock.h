#ifndef _CLOCK_H__
#define _CLOCK_H__

#include "models/settings.h"
#include "temporal.h"

#include <sequence.h>
#include <voice.h>

class Scene;

struct SequenceEvent {
  float duration_ms;
  float release_ms;
  int duration_ticks;
  int release_ticks;
  bool is_abs;
  bool started;
  bool playing;
  bool released;
};

enum ClockState {
  ClockRunning,
  ClockStopping,
  ClockStopped
};

constexpr int TEMPO_CACHE_THROTTLE_MS = 20;

template <class T, class S> class Clock {

  struct SequenceSchedule {
    Voice *voice;
    SequenceEvent events[MAX_SEQ_NOTES * MAX_REPEATS];
    unsigned long prev_ended_at_ms;
    uint32_t prev_ended_at_ticks;
    uint8_t event_count;
    uint8_t event_current;
    bool started;
  };

  // This manages the overall system reset behavior, named to avoid confusion with other "reset/refresh" methods
  struct ChimeSchedule {
    SequenceEvent event;
    unsigned long prev_ended_at_ms;
    uint32_t prev_ended_at_ticks;
    uint32_t loop_count;
    bool started;
  };

public:
  Clock(T &_tt, Scene &_s, S &_m, Models::Settings &_settings)
      : chime_schedule{{0}, 0, 0, 0, 0}, tt(_tt), scene(_s), midiService(_m), settings(_settings), cached_tempo{0}, cached_tempo_at{0}, ticks_in{0},
      sync{SyncOff}, state{ClockStopped} {
    for (auto &sched : seq_schedules) {
      sched.voice = nullptr;
      sched.event_count = 0;
    }
    setBPM(settings.clockTempo);
    refreshChimeScheduleReset();
  }

  void setVoice(Voice &v, int i) {
    if (i >= VOICE_COUNT)
      return;
    seq_schedules[i].voice = &v;
    refreshScheduleReset(seq_schedules[i]);
  }

  bool tick(unsigned long t) {
    time = t;

    if (settings.shouldReset) {
      settings.shouldReset = false;
      reset();
    }

    if (settings.clockTempo != cached_tempo) {
      if (time > cached_tempo_at + TEMPO_CACHE_THROTTLE_MS) {
        cached_tempo_at = time;
        setBPM(settings.clockTempo);
      }
    }

    if (!settings.isRunning && state == ClockStopped)
      return false;

    // Settings overrides running state (in transition to OFF), regardless of Sync direction
    if (!settings.isRunning && state == ClockRunning)
      state = ClockStopping;

    // Settings overrides running state (in transition to ON), when not SyncIn
    if (settings.isRunning && state != ClockRunning && sync != SyncIn) {
      state = ClockRunning;
    }

    if (sync == SyncOut) {
      if (time >= (last_sync_out + sync_interval_ms)) {
        midiService.sendClock();
        last_sync_out = time;
      }
    }

    if (state == ClockStopping)
      state = ClockStopped;

    bool event_flag = false;

    {
      PROCESS_CHIME:

      if (!chime_schedule.started) {
        chime_schedule.started = true;
        chime_schedule.prev_ended_at_ms = time;
        chime_schedule.prev_ended_at_ticks = ticks_in;
      }

      if (!chime_schedule.prev_ended_at_ms) {
        // Used when resuming from paused state
        chime_schedule.prev_ended_at_ms = time;
      }

      auto &current = chime_schedule.event;

      if (state == ClockStopped) {
        uint32_t time_passed_ms = time - chime_schedule.prev_ended_at_ms;
        chime_schedule.prev_ended_at_ms = 0;
        current.duration_ms -= time_passed_ms;
      }
      else {
        bool endOfStepMs =
            (sync != SyncIn || (sync == SyncIn && current.is_abs)) &&
            (time >= (chime_schedule.prev_ended_at_ms + current.duration_ms));
        bool endOfStepTicks =
            sync == SyncIn && !current.is_abs &&
            (ticks_in >= (chime_schedule.prev_ended_at_ticks + current.duration_ticks));
        bool endOfStep = endOfStepMs || endOfStepTicks;

        if (endOfStep) {
          if (static_cast<int>(++chime_schedule.loop_count) >= settings.loopLength)
            reset();
          else
            refreshChimeScheduleReset();
          goto PROCESS_CHIME;
        }
      }
    }

    for (auto &sched : seq_schedules) {
      if (sched.voice == nullptr)
        continue;

      if (!sched.event_count)
        refreshScheduleReset(sched);

      if (!sched.voice->is_playing)
        continue;

      bool sendPriorLegatoNoteOff = false;

    PROCESS_EVENT:

      if (!sched.started) {
        sched.started = true;
        sched.event_current = 0;
        sched.prev_ended_at_ms = time;
        sched.prev_ended_at_ticks = ticks_in;
      }

      if (!sched.prev_ended_at_ms) {
        // Used when resuming from paused state
        sched.prev_ended_at_ms = time;
      }

      auto &current = sched.events[sched.event_current];

      if (state == ClockStopped || sched.voice->should_stop) {
        uint32_t time_passed_ms = time - sched.prev_ended_at_ms;
        sched.prev_ended_at_ms = 0;
        current.duration_ms -= time_passed_ms;
        if (sendPriorLegatoNoteOff || (current.playing && !current.released)) {
          sched.voice->notesOff();
          current.release_ms -= time_passed_ms;
          current.started = false;
          current.playing = false;
          event_flag = true;
        }
        if (sched.voice->should_stop) {
          sched.voice->is_playing = false;
        }
        continue;
      }

      bool noteOffMs = (sync != SyncIn || (sync == SyncIn && current.is_abs)) &&
                       (time >= (sched.prev_ended_at_ms + current.release_ms));
      bool noteOffTicks =
          sync == SyncIn && !current.is_abs &&
          (ticks_in >= (sched.prev_ended_at_ticks + current.release_ticks));
      bool sendNoteOff =
          current.playing && !current.released && (noteOffMs || noteOffTicks);

      bool endOfStepMs =
          (sync != SyncIn || (sync == SyncIn && current.is_abs)) &&
          (time >= (sched.prev_ended_at_ms + current.duration_ms));
      bool endOfStepTicks =
          sync == SyncIn && !current.is_abs &&
          (ticks_in >= (sched.prev_ended_at_ticks + current.duration_ticks));
      bool endOfStep = endOfStepMs || endOfStepTicks;

      if (!current.started) {
        current.playing = sched.voice->notesOn(sched.event_current);
        current.started = true;
        event_flag = event_flag || current.playing;

        if (sendPriorLegatoNoteOff) {
          sched.voice->notesOff();
        }
        continue;
      } else if (sendNoteOff) {
        if (endOfStep) {
          // Defer NoteOff for legato notes
          sendPriorLegatoNoteOff = true;
        } else {
          sched.voice->notesOff();
          current.released = true;
          event_flag = true;
        }
      }

      bool stepsRemaining = (sched.event_current + 1) < sched.event_count;

      if (endOfStep) {
        if (stepsRemaining) {
          sched.event_current += 1;
          sched.prev_ended_at_ms = time;
          sched.prev_ended_at_ticks = ticks_in;
          goto PROCESS_EVENT;
        } else {
          nextSequence(*sched.voice);
          refreshScheduleReset(sched);
          if (sched.event_count) {
            goto PROCESS_EVENT;
          } else if (sendPriorLegatoNoteOff) {
            // If not looping, be sure that a deferred NoteOff still gets sent
            sched.voice->notesOff();
            event_flag = true;
          }
        }
      }
    }
    return event_flag;
  }

  enum MidiSync { SyncOff, SyncIn, SyncOut };
  void setMidiSync(MidiSync s) {
    sync = s;
    if (s == SyncOut) {
      sync_interval_ms = Temporal::getBeatInterval() / 24;
      last_sync_out = 0;
    } else if (s == SyncIn) {
      state = ClockStopping;
    }
  }

  void reset() {
    // Force reset state, useful for tests
    if (settings.clockTempo != cached_tempo) {
      setBPM(settings.clockTempo);
    }
    chime_schedule.loop_count = 0;
    refreshChimeScheduleReset();
    for (auto &sched : seq_schedules) {
      resetSchedule(sched);
    }
  }

  void resetVoice(int i) {
    if (i < 0 || i >= VOICE_COUNT)
      return;
    resetSchedule(seq_schedules[i]);
  }

  // Recalculate a schedule without reset
  void refreshVoice(int i) {
    if (i < 0 || i >= VOICE_COUNT)
      return;
    refreshScheduleRunning(seq_schedules[i]);
  }

  // Recalculate a schedule without reset
  void refreshSequence(Sequence &seq) {
    for (auto &sched : seq_schedules) {
      if (sched.voice != nullptr && &sched.voice->seq == &seq)
        refreshScheduleSeqRunning(sched);
    }
  }

  void midiClockTick() {
    if (sync == SyncIn && settings.isRunning) {
      state = ClockRunning;
      ticks_in++;
    }
  }

  void midiSeqStop() {
    if (sync == SyncIn)
      state = ClockStopping;
  }

  void midiSeqStart() {
    if (sync == SyncIn)
      reset();
    midiSeqContinue();
  }

  void midiSeqContinue() {
    if (sync == SyncIn && settings.isRunning)
      state = ClockRunning;
  }

  SequenceSchedule seq_schedules[VOICE_COUNT];
  ChimeSchedule chime_schedule;

private:
  T &tt;
  Scene &scene;
  S &midiService;
  Models::Settings &settings;

  float sync_interval_ms;
  float last_sync_out;
  float cached_tempo;
  unsigned long time;
  unsigned long cached_tempo_at;
  uint32_t ticks_in;
  MidiSync sync;
  ClockState state;

  void setBPM(float bpm) {
    cached_tempo = bpm;
    setIntervalMillis(60000.0 / bpm);
  }

  void setIntervalMillis(float interval) {
    Temporal::setBeatInterval(interval);
    if (sync == SyncOut)
      sync_interval_ms = interval / 24;
    refreshChimeScheduleRunning();
    for (auto &sched : seq_schedules) {
      refreshScheduleRunning(sched);
    }
  }

  void resetSchedule(SequenceSchedule &sched) {
    if (sched.voice != nullptr && sched.event_count) {
      if (sched.events[sched.event_current].playing &&
          !sched.events[sched.event_current].released) {
        sched.voice->notesOff();
      }
      nextSequence(*sched.voice);
      // Refresh schedule in case it was mutated by pausing clock
      refreshScheduleReset(sched);
    }
  }

  void populateEvent(SequenceEvent &event, const Voice *voice,
                     const Sequence::NoteGroup *note) {
    if (note == nullptr) {
      event.duration_ms = 0;
      event.release_ms = 0;
      event.duration_ticks = 0;
      event.release_ticks = 0;
      event.is_abs = 0;
    } else {
      event.duration_ms =
          Temporal::durationInMs(note->duration, note->duration_denom,
                                 note->duration_unit, note->dots);
      event.duration_ticks =
          Temporal::durationInTicks(note->duration, note->duration_denom,
                                    note->duration_unit, note->dots);

      if (voice == nullptr) {
        event.release_ms = 0;
        event.release_ticks = 0;
      } else {
        event.release_ms = Temporal::noteLengthMs(
            event.duration_ms, voice->config().note_length, note->legato);

        event.release_ticks = Temporal::noteLengthTicks(
            event.duration_ticks, voice->config().note_length, note->legato);
      }

      event.is_abs = note->duration_unit != Sequence::DurRel;
    }
    event.started = false;
    event.playing = false;
    event.released = false;
  }

  void refreshScheduleReset(SequenceSchedule &sched) {
    if (sched.voice == nullptr)
      return;
    auto &seq = sched.voice->seq;

    auto length = seq.length();
    if (length > 0) {
      for (uint8_t i = 0; i < length; i++) {
        populateEvent(sched.events[i], sched.voice, seq.getNotesAtStep(i));
      }
    } else {
      populateEvent(sched.events[0], sched.voice, &Sequence::quarterNoteRest);
      length = 1;
    }

    sched.event_count = length;
    sched.started = false;
  }

  void refreshChimeScheduleReset() {
    populateEvent(chime_schedule.event, nullptr, &Sequence::quarterNoteRest);
    chime_schedule.started = false;
  }

  /**
   * Updates schedule timing when tempo or note length changes
   */
  void refreshScheduleRunning(SequenceSchedule &sched) {
    if (sched.voice == nullptr)
      return;
    auto &seq = sched.voice->seq;

    auto &event = sched.events[sched.event_current];
    const auto *note = seq.getNotesAtStep(sched.event_current);
    if (note != nullptr) {
      // Adjust current note's new duration + release time to account for percent
      // of note that has already been played
      prorateRemainingDuration(*note, event, sched.prev_ended_at_ms, sched.voice);
    }

    auto length = seq.length();
    for (uint8_t i = sched.event_current + 1; i < length; i++) {
      populateEvent(sched.events[i], sched.voice, seq.getNotesAtStep(i));
    }
  }

  void refreshChimeScheduleRunning() {
    prorateRemainingDuration(Sequence::quarterNoteRest, chime_schedule.event, chime_schedule.prev_ended_at_ms, nullptr);
  }


  void prorateRemainingDuration(const Sequence::NoteGroup &note, SequenceEvent &event, unsigned long prev_ended_at_ms, Voice* voice) {
    int a = time - prev_ended_at_ms;
    float b = 1 - ((float)a / event.duration_ms);
    float new_dur = Temporal::durationInMs(note.duration, note.duration_denom,
                                           note.duration_unit, note.dots);
    event.duration_ms = a + b * new_dur;

    if (voice != nullptr) {
      float rel_rem = (voice->config().note_length / 100.0) - b;
      if (rel_rem > 0) {
        event.release_ms = a + rel_rem * new_dur;
      } else {
        event.release_ms = a;
      }
    }
  }

  /**
   * Re-calculates schedule when Sequence changes
   */
  void refreshScheduleSeqRunning(SequenceSchedule &sched) {
    auto a = time - sched.prev_ended_at_ms;
    auto b = a;
    for (int i = 0; i < sched.event_current; i++) {
      b += sched.events[i].duration_ms;
    }
    sched.voice->notesOff();
    refreshScheduleReset(sched);
    auto &seq = sched.voice->seq;

    if (!sched.event_count) {
      // Current sequence has been truncated, move on to next (??)
      nextSequence(*sched.voice);
      return;
    }

    float offset_new = 0;
    float next_dur = 0;
    int new_step = -1;
    auto length = seq.length();

    do {
      new_step = (new_step + 1) % length;
      offset_new += next_dur;
      next_dur = sched.events[new_step].duration_ms;
    } while ((offset_new + next_dur) < b);

    sched.started = true;
    sched.event_current = new_step;
    sched.prev_ended_at_ms = time - (b - offset_new);
    // @nate -- if SyncIn && !sched.events[sched.event_curr].is_abs,
    // need prev_ended_at_ticks !!

    // Skip the current note if it should have already started
    if ((time - sched.prev_ended_at_ms) > 0)
      sched.events[sched.event_current].started = true;
  }

  void nextSequence(Voice &voice) {
    tt.run_script(scene, voice.nextSequence());
  }
};

#endif
