#ifndef _VOICE_H__
#define _VOICE_H__

#include <algorithm>
#include <array>
#include <etl/vector.h>
#include <stdint.h>

#include <table.h>
#include <teletype_io.h>
#include <types.h>

#include "sequence.h"
#include "models/voice_settings.h"

#define MAX_SEQ_CHAIN_LENGTH 128

using std::min;

class Voice {
public:
  Voice(Models::VoiceSettings &_settings, script_number_t _script) : settings(_settings), cb_script(_script), note_lists{} { hardReset(); }

  void hardReset();

  Models::VoiceSettings::VoiceConfig& config() {
    return settings.configs[cb_script];
  }

  const Models::VoiceSettings::VoiceConfig& config() const {
    return settings.configs[cb_script];
  }

  void play() {
    should_stop = false;
    is_muted = false;
    is_playing = true;
  }

  void pause() {
    should_stop = true;
  }

  void mute() {
    is_muted = true;
  }

  script_number_t nextSequence() {
    if (!loop) {
      should_stop = true;
    }
    // Reset Sequence before re-populating it via user script
    seq.resetData();
    return cb_script;
  }

  typedef std::array<int8_t, MAX_CHORD_NOTES> NoteList;

  bool notesOn(int seq_step) {
    if (should_stop || is_muted)
      return false;
    
    auto &conf_ = config();
    auto *notes = getNotesAtStep(seq_step);
    if (notes == nullptr || !notes->note_count)
      return false;

    if (conf_.gate_out != last_gate_out) {
      if (last_gate_out)
        sendGateOff(last_gate_out);
      last_gate_out = conf_.gate_out;
    }

    if (conf_.midi_out_ch != last_midi_out_ch) {
      if (last_midi_out_ch) {
        sendMidiOff(1, last_midi_out_ch);
        sendMidiOff(0, last_midi_out_ch);
      }
      last_midi_out_ch = conf_.midi_out_ch;
    }

    if (note_lists.full())
      notesOff();

    if (!notes->silent) {
      auto new_notes = toNoteList(notes);
      note_lists.insert(note_lists.begin(), new_notes);

      if (conf_.midi_out_ch) {
        for (int i = 0; i < notes->note_count; i++)
          send_midi_note(conf_.midi_out_ch, new_notes[i], conf_.note_vel);
      }

      if (conf_.cv_out) {
        auto new_note = getMonoNote(new_notes);
        auto slew = new_note > last_note ? notes->slew_up : notes->slew_dn;
        last_note = new_note;
        if (slew > 1) tele_cv_slew(conf_.cv_out - 1, slew);
        tele_cv(conf_.cv_out - 1, table_n[new_note], slew > 1);
      }

      if (conf_.gate_out) {
        // @nate - should take trigger polarity in to account
        tele_tr(conf_.gate_out - 1, 1);
        gate_note_count++;
      }

      if (conf_.eg_out) {
        tele_env(conf_.eg_out - 1, 1);
      }
    }
    return !notes->silent;
  }

  void notesOff() {
    // Clean up on the appropriate channel
    notesOff(last_gate_out, last_midi_out_ch);
  }

  void notesOff(int8_t gate_out, int8_t midi_out_ch) {
    if (!note_lists.empty()) {
      if (midi_out_ch)
        sendMidiOff(note_lists.size() - 1, midi_out_ch);

      if (gate_out) {
        gate_note_count--;
        if (gate_note_count <= 0) {
          sendGateOff(gate_out);
        }
      }

      note_lists.pop_back();
    }
  }

  Sequence seq;
  Models::VoiceSettings &settings;

  int16_t seq_step;
  const script_number_t cb_script;

  bool is_playing;
  bool is_muted;
  bool should_stop;
  bool loop;

private:
  etl::vector<NoteList, 2> note_lists;
  int8_t gate_note_count;
  int8_t last_note;

  int8_t last_gate_out;
  int8_t last_midi_out_ch;

  NoteList toNoteList(const Sequence::NoteGroup* notes) {
    NoteList new_notes = {-1, -1, -1, -1};
    for (int i = 0; i < notes->note_count; i++) {
      new_notes[i] = toMidiNote(notes->notes[i], notes->octave_offset[i]);
    }
    return new_notes;
  }

  int8_t toMidiNote(const Sequence::NoteName note, const int8_t octave_offset) {
    // middle C is MIDI note 60
    return min(60 + ((config().octave - 4) * 12) + (octave_offset * 12) + note - 1, 127);
  }

  void sendMidiOff(int index, int midi_out_ch) {
    if (static_cast<int>(note_lists.size()) > index) {
      NoteList &notes = note_lists.at(index);

      if (midi_out_ch) {
        for (auto n : notes) {
          if (n == -1)
            break;
          send_midi_note(midi_out_ch, n, 0);
        }
      }
    }
  }

  void sendGateOff(int gate_out) {
    tele_tr(gate_out - 1, 0);
    gate_note_count = 0;
  }

  /**
   * Gets the latest note
   */
  int8_t getMonoNote(const NoteList notes) {
    for (auto it = notes.rbegin(); it != notes.rend(); it++) {
      if (*it != -1)
        return *it;
    }
    return 0;
  }

  const Sequence::NoteGroup* getNotesAtStep(int seq_step) {
    if (seq_step >= seq.length())
      return nullptr;

    return seq.getNotesAtStep(seq_step);
  }
};

#endif
