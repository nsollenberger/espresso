#ifndef _TIMERS_H__
#define _TIMERS_H__

#include "avr/pgmspace.h"
#include <stdint.h>

class Timers {
public:
  typedef bool(timer_callback_t)(void *userArg);

  typedef volatile struct _softTimer {
    uint32_t ticksRemain;
    uint32_t ticks;
    timer_callback_t *callback;
    volatile struct _softTimer *next;
    volatile struct _softTimer *prev;
    void *userArg;
  } softTimer_t;

  FLASHMEM void initAndRun(void);

  uint64_t getTicks(void) { return tcTicks; }

  bool add(softTimer_t &t, uint32_t ticks, timer_callback_t &callback,
           void *userArg);
  bool remove(softTimer_t &t);
  void clearAll(void);

  void update(softTimer_t &timer, uint32_t ticks) {
    timer.ticks = ticks;
    if (timer.ticksRemain > ticks) {
      timer.ticksRemain = ticks;
    }
  }

  void reset(softTimer_t &timer) { timer.ticksRemain = timer.ticks; }

  void resetSet(softTimer_t &timer, uint32_t ticks) {
    timer.ticks = ticks;
    timer.ticksRemain = ticks;
  }

  void immediate(softTimer_t &timer) { timer.ticksRemain = 1; }

private:
  volatile uint64_t tcTicks = {0};
  volatile softTimer_t *listHead = {nullptr};
  volatile softTimer_t *listTail = {nullptr};
  volatile uint32_t listLength = {0};

  static Timers *instance;

  static void tickISR(void) {
    instance->processTimersISR();
  }

  void processTimersISR(void);
  bool isAlreadyLinked(softTimer_t &t);
};

#endif
