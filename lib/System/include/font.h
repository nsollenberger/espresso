#ifndef _FONT_H__
#define _FONT_H__

#include "stdint.h"

#include "region.h"

constexpr uint8_t kFontCharW = 6;
constexpr uint8_t kFontCharH = 8;
// glyph table doesn't include the initial non-ascii chars
constexpr uint8_t kFontAsciiOffset = 32;
constexpr uint8_t kFontTableSize = 95;

constexpr uint8_t kTabPosition = 48;
constexpr uint8_t kDefaultPaddingRight = 7; // determines where to clip text

// columns/pixels of space between chars
constexpr uint8_t kKerningSize = 1;

struct glyph_t {
  uint8_t first;            // column inset from left side in proportional mode
  uint8_t last;             // column inset from right side in proportional mode
  uint8_t data[kFontCharW]; // column data
};

class Font {
public:
  enum FontSize {
    Size8 = 1,
    Size16 = 2,
  };

  static uint8_t string_position(const char *str, uint8_t pos = 0xff);

  static void string_region_clip(Region &reg, const char *str, uint8_t xoff,
                                 int yoff, eRegionColor fg, eRegionColor bg,
                                 FontSize size = Size8, uint32_t paddingRight = kDefaultPaddingRight,
                                 int8_t highlightIndex = -1,
                                 eRegionColor highlightBg = eColorBlank);

  static void
  string_region_clip_right(Region &reg, const char *str, uint8_t _xoff,
                           int yoff, eRegionColor fg, eRegionColor bg,
                           FontSize size = Size8,
                           uint32_t paddingRight = kDefaultPaddingRight - 1) {
    int8_t xoff = _xoff - string_position(str);
    if (xoff < 0)
      xoff = 0;

    return string_region_clip(reg, str, xoff, yoff, fg, bg, size, paddingRight);
  }

  static void string_region_clip_hid(Region &reg, const char *str, uint8_t xoff,
                                     int yoff, eRegionColor fg, eRegionColor bg,
                                     uint8_t highlightIndex, eRegionColor highlightBg,
                                     FontSize size = Size8) {
    return string_region_clip(reg, str, xoff, yoff, fg, bg, size,
                              kDefaultPaddingRight, highlightIndex,
                              highlightBg);
  }

private:
  static uint8_t getFontIndex(uint8_t _char) {
    if ((_char < kFontAsciiOffset) ||
        (_char >= kFontAsciiOffset + kFontTableSize)) {
      return 31; // render '?' for non-printable chars
    }
    return _char - kFontAsciiOffset;
  }

  static bool hasTabDelimiter(const char *str) {
    while (*str != 0) {
      if (*str == '\t') {
        return true;
      }
      str++;
    }
    return false;
  }

  static uint8_t glyph(char ch, eRegionColor *buf, uint8_t w, eRegionColor fg,
                       eRegionColor bg, int yoff);
  static uint8_t glyph_big(char ch, eRegionColor *buf, uint8_t w, eRegionColor fg, eRegionColor bg, int yoff);
};

#endif
