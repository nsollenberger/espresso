#ifndef _EXEC_STATE_H__
#define _EXEC_STATE_H__

#include <stdint.h>

#include <types.h>

namespace Espresso {

constexpr int kExecDepth = 8;

struct exec_vars_t {
  int16_t i;
  uint16_t while_depth;
  script_number_t script_number;
  uint8_t line_number;
  bool if_else_condition;
  bool while_continue;
  bool breaking;
};

class ExecState {
public:
  void setScript(script_number_t script_number) {
    current()->script_number = script_number;
  }

  void setLine(uint8_t line_number) {
    current()->line_number = line_number;
  }

  int push(void) {
    if (exec_depth < kExecDepth) {
      variables[exec_depth].while_depth = 0;
      variables[exec_depth].while_continue = false;
      if (exec_depth > 0) {
        variables[exec_depth].if_else_condition =
            variables[exec_depth - 1].if_else_condition;
        variables[exec_depth].i = variables[exec_depth - 1].i;
      } else {
        variables[exec_depth].if_else_condition = true;
        variables[exec_depth].i = 0;
      }
      variables[exec_depth].breaking = false;
      exec_depth += 1; // exec_depth = 1 at the root
    } else
      overflow = true;
    return exec_depth;
  }

  int pop(void) {
    if (exec_depth > 0)
      exec_depth -= 1;
    return exec_depth;
  }

  exec_vars_t *current(void) {
    return &variables[exec_depth - 1]; // array is 0-indexed
  }

  bool overflow = {false};

private:
  uint8_t exec_depth = {0};
  exec_vars_t variables[kExecDepth];
};

}

#endif
