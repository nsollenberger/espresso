#ifndef _COMMAND_STATE_H__
#define _COMMAND_STATE_H__

#include <stdint.h>

namespace Espresso {

constexpr int kStackSize = 16;

class CommandState {
public:
  CommandState(void) : top{0}, values{0} {}

  void reset(void) { top = 0; }

  int16_t size(void) { return top; }

  int16_t pop(void) {
    if (top) {
      top--;
      return values[top];
    }
    return 0;
  }

  void push(int16_t data) {
    if (top < kStackSize) {
      values[top] = data;
      top++;
    }
  }

private:
  int16_t top;
  int16_t values[kStackSize];
};

}

#endif
