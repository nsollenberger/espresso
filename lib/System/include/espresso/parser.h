#ifndef _ESPRESSO_PARSER_H__
#define _ESPRESSO_PARSER_H__

#include "espresso/validator.h"

namespace Espresso {

class Command;

e_parse_error parser(const char *cmd, Command &out,
                char error_msg[PARSE_ERROR_STR_LENGTH]);

};
#endif
