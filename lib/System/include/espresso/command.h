#ifndef _ESPRESSO_EXRPESSION_H__
#define _ESPRESSO_EXRPESSION_H__

#include <algorithm>
#include <cstring>
#include <initializer_list>
#include <stdint.h>

#define ESPRESSO_COMMAND_MAX_LENGTH 16

namespace Espresso {

enum token_type : uint8_t {
  NUMBER,
  XNUMBER,
  BNUMBER,
  RNUMBER,
  ALDA_NOTE,
  OP,
  MOD,
  PRE_SEP,
  SUB_SEP,
  PARENS,
  BRACKETS
};

struct Token {
  token_type tag;
  int16_t value;
  uint8_t data[4];
};

class Command {
public:
  Command() : length{0}, separator{-1}, comment{false}, data{} {}
  Command(uint8_t _l, int8_t _s, bool _c, std::initializer_list<Token> _d) : length{_l}, separator{_s}, comment{_c}, data{} {
    // https://stackoverflow.com/a/5549918
    std::copy(_d.begin(), _d.end(), data);
  }

  Command(const Command &src, uint8_t offset, int len = -1) {
    if (len < 0) {
      len = src.length - offset - 1;
    }
    length = len;
    if (length) memcpy(data, &src.data[offset + 1], length * sizeof(Token));
  }

  void print(char *out) const;

  uint8_t length;
  int8_t separator;
  bool comment;
  Token data[ESPRESSO_COMMAND_MAX_LENGTH];
};

}

#endif
