#ifndef _ESPRESSO_VALIDATOR_H__
#define _ESPRESSO_VALIDATOR_H__

#define PARSE_ERROR_STR_LENGTH 21

namespace Espresso {

class Command;

enum e_parse_error {
  E_OK,
  E_PARSE,
  E_LENGTH,
  E_NEED_PARAMS,
  E_EXTRA_PARAMS,
  E_NO_MOD_HERE,
  E_MANY_PRE_SEP,
  E_NEED_PRE_SEP,
  E_PLACE_PRE_SEP,
  E_NO_SUB_SEP_IN_PRE,
  E_NOT_LEFT,
  E_UNKNOWN,
  E_ERRORS_LENGTH
};

e_parse_error validate(const Command *c, char error_msg[PARSE_ERROR_STR_LENGTH]);

const char *parse_error_str(e_parse_error);

};

#endif
