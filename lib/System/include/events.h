#ifndef _EVENTS_H__
#define _EVENTS_H__

#include <atomic>
#include <cstddef>
#include <portmacro.h>
#include <stdint.h>

#include <event_types.h>

constexpr size_t kEventsBufferLength{40};

class Events : public EventQueue {
public:
  // Lock-free implementation assumes nested interrupts are not used (!)
  bool postFromISR(const event_t &e) {
    uint32_t prevIndex = head;
    bool status = false;

    increment(head);
    if (head != tail) {
      events[head].type = e.type;
      events[head].data = e.data;
      status = true;
    } else {
      // Queue is full, abort
      head = prevIndex;
    }

    return status;
  }

  bool getNext(event_t &e) {
    bool status;

    if (tail != head) {
      increment(tail);
      e.type = events[tail].type;
      e.data = events[tail].data;
      status = true;
    } else {
      // Queue is empty
      e.type = kEventNone;
      e.data = 0;
      status = false;
    }

    return status;
  }

private:
  // Owned by ISR
  volatile std::atomic<uint32_t> head = {0};
  // Owned by Main
  volatile std::atomic<uint32_t> tail = {0};

  volatile event_t events[kEventsBufferLength] = {};

  void increment(volatile std::atomic<uint32_t> &buf_index) {
    if (++buf_index >= kEventsBufferLength)
      buf_index = 0;
  }
};

#endif
