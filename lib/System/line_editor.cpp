#include "line_editor.h"

#include <string.h>
#include <hid_keys.h>
#include <keyboard_helper.h>

#include "espresso/command.h"
#include "font.h"

void LineEditor::line_editor_set(const char value[kLineEditorSize]) {
  size_t _length = strlen(value);
  if (_length < kLineEditorSize) {
    strcpy(buffer, value);
    cursor = _length;
    length = _length;
  } else {
    buffer[0] = 0;
    cursor = 0;
    length = 0;
  }
}

void LineEditor::line_editor_set_command(const Espresso::Command *command) {
  command->print(buffer);
  length = strlen(buffer);
  cursor = length;
}

char *LineEditor::line_editor_get(void) { return buffer; }

bool LineEditor::line_editor_process_keys(uint8_t k, uint8_t m,
                                          bool is_key_held) {
  // <left> or ctrl-b: move cursor left
  if (match_no_mod(m, k, HID_LEFT) || match_ctrl(m, k, HID_B)) {
    if (cursor) {
      cursor--;
    }
    return true;
  }
  // <right> or ctrl-f: move cursor right
  else if (match_no_mod(m, k, HID_RIGHT) || match_ctrl(m, k, HID_F)) {
    if (cursor < length) {
      cursor++;
    }
    return true;
  }
  // <home> or ctrl-a: move to beginning of line
  else if (match_no_mod(m, k, HID_HOME) || match_ctrl(m, k, HID_A)) {
    cursor = 0;
    return true;
  }
  // <end> or ctrl-e: move to end of line
  else if (match_no_mod(m, k, HID_END) || match_ctrl(m, k, HID_E)) {
    cursor = length;
    return true;
  }
  // ctrl-<left> or alt-b: move cursor to previous word
  else if (match_ctrl(m, k, HID_LEFT) || match_alt(m, k, HID_B)) {
    bool encountered_word = false;
    while (cursor) {
      if (buffer[cursor - 1] == ' ') {
        if (encountered_word)
          break;
      } else {
        encountered_word = true;
      }
      cursor--;
    }
    return true;
  }
  // ctrl-<right> or alt-f: move cursor to next word
  else if (match_ctrl(m, k, HID_RIGHT) || match_alt(m, k, HID_F)) {
    bool encountered_word = false;
    while (cursor < length) {
      if (buffer[cursor] == ' ') {
        if (encountered_word)
          break;
      } else {
        encountered_word = true;
      }
      cursor++;
    }
    return true;
  }
  // <backspace> or ctrl-h: backwards delete one character
  else if (match_no_mod(m, k, HID_BACKSPACE) || match_ctrl(m, k, HID_H)) {
    if (cursor) {
      cursor--;
      for (size_t x = cursor; x < kLineEditorSize - 1; x++) {
        buffer[x] = buffer[x + 1];
      }
      length--;
    }
    return true;
  }
  // <delete> or ctrl-d: forwards delete one character
  else if (match_no_mod(m, k, HID_DELETE) || match_ctrl(m, k, HID_D)) {
    if (cursor < length) {
      for (size_t x = cursor; x < kLineEditorSize - 1; x++) {
        buffer[x] = buffer[x + 1];
      }
      length--;
    }
    return true;
  }
  // shift-<backspace> or ctrl-u: delete from cursor to beginning
  else if (match_shift(m, k, HID_BACKSPACE) || match_ctrl(m, k, HID_U)) {
    // strings will overlap, so we need to use an intermediate buffer
    char temp[kLineEditorSize];
    strcpy(temp, &buffer[cursor]);
    line_editor_set(temp);
    cursor = 0;
    return true;
  }
  // shift-<delete> or ctrl-k: delete from cursor to end
  else if (match_shift(m, k, HID_DELETE) || match_ctrl(m, k, HID_K)) {
    buffer[cursor] = 0;
    length = cursor;
    return true;
  }
  // alt-<backspace> or ctrl-w: delete from cursor to beginning of word
  else if (match_alt(m, k, HID_BACKSPACE) || match_ctrl(m, k, HID_W)) {
    bool encountered_word = false;
    while (cursor) {
      // Stop after reaching space on the other side of the word.
      if (buffer[cursor - 1] == ' ') {
        if (encountered_word)
          break;
      } else {
        encountered_word = true;
      }
      // Delete preceeding character by shifting buffer down.
      cursor--;
      for (size_t x = cursor; x < kLineEditorSize - 1; x++) {
        buffer[x] = buffer[x + 1];
      }
      length--;
    }
    return true;
  }
  // alt-d: delete from cursor to end of word.
  else if (match_alt(m, k, HID_D)) {
    bool encountered_word = false;
    while (cursor < length) {
      // Stop after reaching space on the other side of the word.
      if (buffer[cursor] == ' ') {
        if (encountered_word)
          break;
      } else {
        encountered_word = true;
      }
      // Delete this character by shifting the buffer down.
      for (size_t x = cursor; x < kLineEditorSize - 1; ++x) {
        buffer[x] = buffer[x + 1];
      }
      length--;
    }
    return true;
  }
  // ctrl-x or alt-x: cut
  else if (match_ctrl(m, k, HID_X) || match_alt(m, k, HID_X)) {
    strcpy(copy_buffer[0], buffer);
    copy_buffer_len = 1;
    line_editor_set("");
    return true;
  }
  // ctrl-c or alt-c: copy
  else if (match_ctrl(m, k, HID_C) || match_alt(m, k, HID_C)) {
    strcpy(copy_buffer[0], buffer);
    copy_buffer_len = 1;
    return true;
  }
  // ctrl-v or alt-v: paste
  else if (match_ctrl(m, k, HID_V) || match_alt(m, k, HID_V)) {
    line_editor_set(copy_buffer[0]);
    return true;
  } else if (no_mod(m) || mod_only_shift(m)) {
    if (length < kLineEditorSize - 2) { // room for another char & 0
      uint8_t n = hid_to_ascii(k, m);
      if (n) {
        for (size_t x = kLineEditorSize - 1; x > cursor; x--) {
          buffer[x] = buffer[x - 1]; // shuffle forwards
        }

        buffer[cursor] = n;
        cursor++;
        length++;
        return true;
      }
    }
  }

  // did't process a key
  return false;
}

void LineEditor::line_editor_draw(char prefix, Region *reg) {
  // kLineEditorSize includes space for null, need to also include space for
  // the prefix, the space after the prefix and a space at the very end
  char s[kLineEditorSize + 3] = {prefix, ' ', 0};
  strcat(s, buffer);
  strcat(s, " ");

  reg->fill(eColorBlank);
  Font::string_region_clip_hid(*reg, s, 0, 0, eColorDark2, eColorBlank, cursor + 2, eColorLight2);
}
