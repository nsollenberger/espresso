#include "voice.h"

void Voice::hardReset() {
  notesOff();
  notesOff();
  
  seq_step = 0;
  last_note = 0;

  is_playing = true;
  is_muted = false;
  should_stop = false;
  loop = true;

  gate_note_count = 0;

  seq.hardReset();
}
