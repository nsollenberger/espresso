#include "timers.h"
#include <cstdint>
#include <portmacro.h>
#include <tc.h>

Timers *Timers::instance = nullptr;

void Timers::initAndRun(void) {
  instance = this;
  Tc::init();
  Tc::startTimer(tickISR);
}

bool Timers::add(softTimer_t &t_, uint32_t ticks, timer_callback_t &callback,
                 void *userArg) {
  if (ticks < 1) {
    ticks = 1;
  }

  Port::DISABLE_INTERRUPTS();
  if (isAlreadyLinked(t_)) {
    Port::ENABLE_INTERRUPTS();
    return false;
  }

  if ((listHead == nullptr) || (listTail == nullptr) || (listLength == 0)) {
    // List is empty
    listHead = listTail = &t_;
    t_.next = t_.prev = &t_;
    listLength = 1;
  } else {
    listTail->next = &t_;
    listHead->prev = &t_;
    t_.prev = listTail;
    t_.next = listHead;
    listTail = &t_;
    ++listLength;
  }

  t_.ticksRemain = ticks;
  t_.ticks = ticks;
  t_.callback = &callback;
  t_.userArg = userArg;
  Port::ENABLE_INTERRUPTS();

  return true;
}

bool Timers::remove(softTimer_t &t_) {
  uint32_t searchIndex;
  volatile softTimer_t *ptr = listHead->next;
  bool found = false;

  Port::DISABLE_INTERRUPTS();
  if ((t_.next == nullptr) || (t_.prev == nullptr)) {
    Port::ENABLE_INTERRUPTS();
    return false;
  }

  if (&t_ == listHead) {
    found = true;
    listHead = t_.next;
  } else if (&t_ == listTail) {
    found = true;
    listTail = t_.prev;
  } else {
    for (searchIndex = 1; searchIndex < listLength; searchIndex++) {
      if (&t_ == ptr) {
        found = true;
        break;
      }
      ptr = ptr->next;
    }
  }

  if (found) {
    (t_.next)->prev = t_.prev;
    (t_.prev)->next = t_.next;
    t_.next = t_.prev = nullptr;
    --listLength;
    if (listLength == 0) {
      listHead = nullptr;
      listTail = nullptr;
    }
  }
  Port::ENABLE_INTERRUPTS();

  return found;
}

void Timers::clearAll(void) {
  Port::DISABLE_INTERRUPTS();
  listHead = listTail = nullptr;
  listLength = 0;
  Port::ENABLE_INTERRUPTS();
}

void Timers::processTimersISR(void) {
  tcTicks++;

  uint32_t searchIndex;
  volatile softTimer_t *ptr = listHead;

  if ((listHead == nullptr) || (listTail == nullptr) || (listLength == 0)) {
    return;
  }

  for (searchIndex = 0; searchIndex < listLength; searchIndex++) {
    --(ptr->ticksRemain);
    if (ptr->ticksRemain == 0) {
      ptr->ticksRemain = ptr->ticks;
      (*(ptr->callback))(ptr->userArg);
    }
    ptr = ptr->next;
  }
}

bool Timers::isAlreadyLinked(softTimer_t &t_) {
  softTimer_t *ptr = listHead;
  for (uint32_t searchIndex = 0; searchIndex < listLength; searchIndex++) {
    if (ptr == &t_) {
      return true;
    }
    ptr = ptr->next;
  }
  return false;
}
