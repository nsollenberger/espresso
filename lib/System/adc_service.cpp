#include "adc_service.h"
#include <gpio.h>

void Adc::sampleInputs(uint16_t (&result)[2]) {
  auto reading = Gpio::readAdcPair(CV_IN1, CV_IN2);
  // Inputs are inverted
  result[0] = 4095 - reading.result_adc0;
  result[1] = 4095 - reading.result_adc1;
}

uint16_t Adc::sampleParam() {
  return Gpio::readAdc(PARAM);
}