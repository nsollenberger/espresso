#include "sequence.h"
#include "temporal.h"

const Sequence::NoteGroup Sequence::quarterNoteRest = {
  { Sequence::NoteRest },
  4, 1, 0, 0, Sequence::DurRel,
  { 0 }, 0, 0, 0, 1, 0
};

void Sequence::hardReset() {
  resetData();
}

// - 4 bits (13 values) - note
// - 4 bits (-8..7) - octave_offset

void Sequence::packNoteName(uint8_t &outData, NoteName note) {
  outData |= note << 4;
}

void Sequence::packNoteOctave(uint8_t &outData, int8_t octave) {
  // clamp to signed 4-bit range
  if (octave > 7)
    octave = 7;
  if (octave < -8)
    octave = -8;
  // shift sign bit down
  octave = (octave << 4) >> 4;
  // reset octave bits, in case shifting multiple octaves
  outData &= ~0x0f;
  outData |= octave & 0x0f;
}

// - 8 bits (0..255) - duration (<< 2, when in ms)
// - 2 bits (4 values) - duration_unit (rel, ms, sec, fsec)
// - 2 bits (up to 3 dots) - duration_dots -- integer count of dots (eg c4.. is double-dotted quarter)
// - 1 bit - legato
// - 3 bits (0..8) - repeats

void Sequence::packNoteDuration(int16_t &outValue, int duration, DurationUnit unit) {
  outValue |= (unit - 1) << 6;
  if (unit == DurMs)
    duration >>= 2;
  if (duration > 255)
    duration = 255;
  outValue |= (duration & 0xff) << 8;
}

void Sequence::packNoteDots(int16_t &outValue, uint8_t dots) {
    if (dots > 3)
      dots = 3;
    outValue |= dots << 4;
}

void Sequence::packNoteLegato(int16_t &outValue, bool legato) {
  if (legato)
    outValue |= 1 << 3;
  else
    outValue &= ~(1 << 3);
}

void Sequence::packNoteRepeats(int16_t &outValue, uint8_t repeats) {
    if (repeats < 1)
      repeats = 0;
    if (repeats > MAX_REPEATS)
      repeats = MAX_REPEATS;
    outValue |= (repeats - 1);
}

void Sequence::unpackData(const Espresso::Token &tele_data) {
  auto &note_group = steps[_length];
  auto &note_count = note_group.note_count;
  note_count = 0;

  for (auto packed : tele_data.data) {
    auto note = static_cast<NoteName>(packed >> 4);
    note_group.notes[note_count] = note;

    // shift sign bit back to the top
    int8_t octave = (packed & 0xf) << 4;
    note_group.octave_offset[note_count] = octave >> 4;

    if (note == NoteRest) {
      break;
    }
    note_count++;
  }

  note_group.duration = (tele_data.value >> 8) & 0xff;
  note_group.duration_denom = 1;
  note_group.duration_unit = static_cast<DurationUnit>(((tele_data.value >> 6) & 0b11) + 1);
  if (note_group.duration_unit == DurMs) {
    note_group.duration <<= 2;
  }
  note_group.dots = (tele_data.value >> 4) & 0b11;
  note_group.legato = (tele_data.value >> 3) & 1;
  note_group.repeats = (tele_data.value & 0b111) + 1;
  note_group.silent = false;

  _length++;
  _length_processed = 0;
}


float Sequence::getNoteDurationMs(NoteGroup &ns) {
  auto note_duration = Temporal::durationInMs(ns.duration, ns.duration_denom, ns.duration_unit, ns.dots);
  return note_duration * ns.repeats;
}

