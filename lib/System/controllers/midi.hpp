template <class T, class S>
Controllers::Midi<T, S> *Controllers::Midi<T, S>::instance = {nullptr};

template <class T, class S>
void Controllers::Midi<T, S>::registerHandlers() {
  midiService.setHandleClock(&midi_clock_tick);
  midiService.setHandleStart(&midi_seq_start);
  midiService.setHandleStop(&midi_seq_stop);
  midiService.setHandleContinue(&midi_seq_continue);
}

template <class T, class S>
void Controllers::Midi<T, S>::clock_tick(void) {
  //
}

template <class T, class S>
void Controllers::Midi<T, S>::seq_start(void) {
  //
}

template <class T, class S>
void Controllers::Midi<T, S>::seq_stop(void) {
  //
}

template <class T, class S>
void Controllers::Midi<T, S>::seq_continue(void) {
  //
}
