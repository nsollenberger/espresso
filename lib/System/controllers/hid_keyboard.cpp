#include "controllers/hid_keyboard.h"

#include <hid_keys.h>
#include <keyboard_helper.h>

#include "models/keyboard.h"
#include "models/edit_mode.h"
#include "models/mode.h"
#include "models/mode_manager.h"
#include "models/screensaver.h"
#include "models/settings.h"
#include "voice.h"

using namespace Controllers;

void HidKeyboard::checkHeldKeys(void) {
  uint8_t held_key = kbd.getHeldKey();
  if (held_key) {
    processKeypress(held_key, kbd.mod_key, true);
  }
}

void HidKeyboard::processKeypress(uint8_t key, uint8_t mod_key,
                                   bool is_held_key) {
  if (screensaver.resetTimer())
    return;

  // first try global keys
  if (processKeypressGlobal(key, mod_key, is_held_key)) {
    return;
  }

  modeManager.activeMode().processKeypress(key, mod_key, is_held_key);
}

bool HidKeyboard::processKeypressGlobal(uint8_t k, uint8_t m, bool is_held_key) {
  if (is_held_key)
    return false;

  // <esc> = Jump to Settings
  if (match_no_mod(m, k, HID_ESCAPE)) {
    modeManager.activateMode(M_SETTINGS_1);
    return true;
  }

  // ctrl-<esc> = Play/Pause
  else if (match_ctrl(m, k, HID_ESCAPE)) {
    settings.setIsRunning(!settings.isRunning);
    return true;
  }

  // <alt>-? or <alt>-H = Toggle Help
  else if (match_shift_alt(m, k, HID_SLASH) || match_alt(m, k, HID_H)) {
    if (modeManager.mode == M_HELP)
      modeManager.activatePriorMode();
    else {
      modeManager.activateMode(M_HELP);
    }
    return true;
  }

  // <F1> through <F8>: Jump to Voice
  else if (no_mod(m) && k >= HID_F1 && k <= HID_F8) {
    editMode.setScript(static_cast<script_number_t>(k - HID_F1));
    modeManager.activateMode(M_EDIT);
    return true;
  }

  // alt-<F1> through alt-<F8>: Jump to Voice Settings
  else if (mod_only_alt(m) && k >= HID_F1 && k <= HID_F8) {
    voiceSettings.setScript(static_cast<script_number_t>(k - HID_F1));
    modeManager.activateMode(M_VOICE_SETTINGS_1);
    return true;
  }

  // ctrl-<F1> through ctrl-<F8> = Mute Voice
  else if (mod_only_ctrl(m) && k >= HID_F1 && k <= HID_F8) {
    auto v_num = static_cast<script_number_t>(k - HID_F1);
    auto &voice = voices[v_num];
    if (voice.is_muted)
      voice.play();
    else
      voice.mute();
    return true;
  }

  return false;
}
