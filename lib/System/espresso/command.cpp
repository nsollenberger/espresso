#include "espresso/command.h"

#include <algorithm>

#include <util.h>

#include "ops/op.h"
#include "sequence.h"

using std::max;

void Espresso::Command::print(char *out) const {
  out[0] = 0;

  int chars_until_closing_parens = 0;
  int chars_until_closing_bracket = 0;

  for (int index = 0; index < length; index++) {
    const Espresso::token_type &tag = data[index].tag;
    const int16_t &value = data[index].value;

    switch (tag) {
    case Espresso::OP:
      strcat(out, tele_ops[value]->name);
      break;
    case Espresso::NUMBER: {
      char number[8];
      Util::itoa(value, number, 10);
      strcat(out, number);
      break;
    }
    case Espresso::XNUMBER: {
      char number[6];
      Util::itoa_hex(value, number);
      strcat(out, number);
      break;
    }
    case Espresso::BNUMBER: {
      char number[18];
      Util::itoa_bin(value, number);
      strcat(out, number);
      break;
    }
    case Espresso::RNUMBER: {
      char number[18];
      Util::itoa_rbin(value, number);
      strcat(out, number);
      break;
    }
    case Espresso::ALDA_NOTE: {
      Sequence seq;
      seq.unpackData(data[index]);
      auto length = seq.raw_length();
      for (int s = 0; s < length; s++) {
        for (int n = 0; n < max<uint8_t>(seq.steps[s].note_count, 1); n++) {
          if (seq.steps[s].octave_offset[n]) {
            auto oct_sym = seq.steps[s].octave_offset[n] > 0 ? ">" : "<";
            for (int o = 0; o < abs(seq.steps[s].octave_offset[n]); o++) {
              strcat(out, oct_sym);
            }
          }
          strcat(out, Sequence::getNoteString(seq.steps[s].notes[n]));
        }
        if (seq.steps[s].duration) {
          char number[8];
          Util::itoa(seq.steps[s].duration, number, 10);
          strcat(out, number);
          strcat(out, Sequence::getDurationUnitString(seq.steps[s].duration_unit));
        }
        if (seq.steps[s].dots) {
          for (int d = 0; d < seq.steps[s].dots; d++) {
            strcat(out, ".");
          }
        }
        if (seq.steps[s].repeats > 1) {
          strcat(out, "*");
          char number[4];
          Util::itoa(seq.steps[s].repeats, number, 10);
          strcat(out, number);
        }
        if (seq.steps[s].legato) {
          strcat(out, "~");
        }
      }
      break;
    }
    case Espresso::MOD:
      strcat(out, tele_mods[value]->name);
      break;
    case Espresso::PRE_SEP:
      strcat(out, ":");
      break;
    case Espresso::SUB_SEP:
      strcat(out, ";");
      break;
    case Espresso::PARENS:
      strcat(out, "(");
      chars_until_closing_parens = value + 1;
      break;
    case Espresso::BRACKETS:
      strcat(out, "[");
      chars_until_closing_bracket = value + 1;
      break;
    }

    if (chars_until_closing_parens > 0) {
      if (--chars_until_closing_parens == 0) {
        strcat(out, ")");
      }
    }

    if (chars_until_closing_bracket > 0) {
      if (--chars_until_closing_bracket == 0) {
        strcat(out, "]");
      }
    }

    // do we need to add a space?
    // first check if we're not at the end
    if (index < static_cast<int>(length - 1)) {
      // otherwise, only add a space if the next tag is a not a seperator
      Espresso::token_type next_tag = data[index + 1].tag;
      if (next_tag != Espresso::PRE_SEP && next_tag != Espresso::SUB_SEP && tag != Espresso::PARENS && next_tag != Espresso::PARENS && tag != Espresso::BRACKETS) {
        strcat(out, " ");
      }
    }
  }
}
