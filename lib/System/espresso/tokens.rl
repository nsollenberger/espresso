%%{
    machine parser;

    action push_num { buf[buf_i++ % kMaxTokenLength] = *p; }
    action proc_num {
        char *token = buf;

        tele_data.tag = Espresso::NUMBER;
        uint8_t base = 0;
        uint8_t binhex = 0;
        uint8_t bitrev = 0;
        if (token[0] == 'X') {
            tele_data.tag = Espresso::XNUMBER;
            binhex = 1;
            base = 16;
            token++;
        }
        else if (token[0] == 'B') {
            tele_data.tag = Espresso::BNUMBER;
            binhex = 1;
            base = 2;
            token++;
        }
        else if (token[0] == 'R') {
            tele_data.tag = Espresso::RNUMBER;
            binhex = 1;
            bitrev = 1;
            base = 2;
            token++;
        }
        int32_t val = strtol(token, NULL, base);
        if (binhex) val = (int16_t)((uint16_t)val);
        if (bitrev) val = rev_bitstring_to_int(token);
        val = val > INT16_MAX ? INT16_MAX : val;
        val = val < INT16_MIN ? INT16_MIN : val;
        tele_data.value = val;

        RECORD_TOKEN();
    }

    action pre_sep {
        result.data[result.length].tag = Espresso::PRE_SEP;
        result.data[result.length].value = 0;
        result.separator = result.length;
        result.length++;
        if (result.length >= ESPRESSO_COMMAND_MAX_LENGTH) {
            return E_LENGTH;
        }
    }

    action sub_sep {
        result.data[result.length].tag = Espresso::SUB_SEP;
        result.data[result.length].value = 0;
        result.length++;
        if (result.length >= ESPRESSO_COMMAND_MAX_LENGTH) {
            return E_LENGTH;
        }
    }

    action parens_open {
        parens_start = result.length;
        result.data[result.length].tag = Espresso::PARENS;
        result.data[result.length].value = 0;
        result.length++;

        if (result.length >= ESPRESSO_COMMAND_MAX_LENGTH) {
            return E_LENGTH;
        }
    }

    action parens_close {
        result.data[parens_start].value = result.length - parens_start - 1;
        parens_start = -1;
    }

    action bracket_open {
        bracket_start = result.length;
        result.data[result.length].tag = Espresso::BRACKETS;
        result.data[result.length].value = 0;
        result.length++;

        if (result.length >= ESPRESSO_COMMAND_MAX_LENGTH) {
            return E_LENGTH;
        }
    }

    action bracket_close {
        result.data[bracket_start].value = result.length - bracket_start - 1;
        bracket_start = -1;
    }

    number = ((('-')? [0-9]+) | ([X] [0-9A-F]+) | ([B|R] [0-1]+)) >{ reset_token(tele_data, buf, buf_i); } $push_num %proc_num;
    sep = [ \n\t];
    pre_sep = ':' @pre_sep;
    sub_sep = ';' @sub_sep;
    p_open = '(' %parens_open;
    p_close = ')' %parens_close;
    b_open = '[' %bracket_open;
    b_close = ']' %bracket_close;

    op_A = "A"i                     %{ MATCH_OP(E_OP_A); };
    op_B = "B"i                     %{ MATCH_OP(E_OP_B); };
    op_C = "C"i                     %{ MATCH_OP(E_OP_C); };
    op_D = "D"i                     %{ MATCH_OP(E_OP_D); };
    op_DRUNK = "DRUNK"i             %{ MATCH_OP(E_OP_DRUNK); };
    op_DRUNK_MAX = "DRUNK.MAX"i     %{ MATCH_OP(E_OP_DRUNK_MAX); };
    op_DRUNK_MIN = "DRUNK.MIN"i     %{ MATCH_OP(E_OP_DRUNK_MIN); };
    op_DRUNK_WRAP = "DRUNK.WRAP"i   %{ MATCH_OP(E_OP_DRUNK_WRAP); };
    op_FLIP = "FFLIP"i              %{ MATCH_OP(E_OP_FLIP); };
    op_I = "I"i                     %{ MATCH_OP(E_OP_I); };
    op_O = "O"i                     %{ MATCH_OP(E_OP_O); };
    op_O_INC = "O.INC"i             %{ MATCH_OP(E_OP_O_INC); };
    op_O_MAX = "O.MAX"i             %{ MATCH_OP(E_OP_O_MAX); };
    op_O_MIN = "O.MIN"i             %{ MATCH_OP(E_OP_O_MIN); };
    op_O_WRAP = "O.WRAP"i           %{ MATCH_OP(E_OP_O_WRAP); };
    op_T = "T"i                     %{ MATCH_OP(E_OP_T); };
    op_TIME = "TIME"i               %{ MATCH_OP(E_OP_TIME); };
    op_TIME_ACT = "TIME.ACT"i       %{ MATCH_OP(E_OP_TIME_ACT); };
    op_LAST = "LAST"i               %{ MATCH_OP(E_OP_LAST); };
    op_X = "X"i                     %{ MATCH_OP(E_OP_X); };
    op_Y = "Y"i                     %{ MATCH_OP(E_OP_Y); };
    op_Z = "Z"i                     %{ MATCH_OP(E_OP_Z); };
    op_J = "J"i                     %{ MATCH_OP(E_OP_J); };
    op_K = "K"i                     %{ MATCH_OP(E_OP_K); };

    # init
    op_INIT = "INIT"i               %{ MATCH_OP(E_OP_INIT); };
    op_INIT_SCENE = "INIT.SCENE"i   %{ MATCH_OP(E_OP_INIT_SCENE); };
    op_INIT_SCRIPT = "INIT.SCRIPT"i %{ MATCH_OP(E_OP_INIT_SCRIPT); };
    op_INIT_SCRIPT_ALL = "INIT.SCRIPT.ALL"i %{ MATCH_OP(E_OP_INIT_SCRIPT_ALL); };
    op_INIT_CV = "INIT.CV"i         %{ MATCH_OP(E_OP_INIT_CV); };
    op_INIT_CV_ALL = "INIT.CV.ALL"i %{ MATCH_OP(E_OP_INIT_CV_ALL); };
    op_INIT_DATA = "INIT.DATA"i     %{ MATCH_OP(E_OP_INIT_DATA); };
    op_INIT_TIME = "INIT.TIME"i     %{ MATCH_OP(E_OP_INIT_TIME); };

    # metronome
    op_M = "M"i                     %{ MATCH_OP(E_OP_M); };
    op_M_SYM_EXCLAMATION = "M!"i    %{ MATCH_OP(E_OP_M_SYM_EXCLAMATION); };
    op_M_ACT = "M.ACT"i             %{ MATCH_OP(E_OP_M_ACT); };
    op_M_RESET = "M.RESET"i         %{ MATCH_OP(E_OP_M_RESET); };

    # queue
    op_Q = "Q"i                     %{ MATCH_OP(E_OP_Q); };
    op_Q_AVG = "Q.AVG"i             %{ MATCH_OP(E_OP_Q_AVG); };
    op_Q_N = "Q.N"i                 %{ MATCH_OP(E_OP_Q_N); };
    op_Q_CLR = "Q.CLR"i             %{ MATCH_OP(E_OP_Q_CLR); };
    op_Q_GRW = "Q.GRW"i             %{ MATCH_OP(E_OP_Q_GRW); };
    op_Q_SUM = "Q.SUM"i             %{ MATCH_OP(E_OP_Q_SUM); };
    op_Q_MIN = "Q.MIN"i             %{ MATCH_OP(E_OP_Q_MIN); };
    op_Q_MAX = "Q.MAX"i             %{ MATCH_OP(E_OP_Q_MAX); };
    op_Q_RND = "Q.RND"i             %{ MATCH_OP(E_OP_Q_RND); };
    op_Q_SRT = "Q.SRT"i             %{ MATCH_OP(E_OP_Q_SRT); };
    op_Q_REV = "Q.REV"i             %{ MATCH_OP(E_OP_Q_REV); };
    op_Q_SH = "Q.SH"i               %{ MATCH_OP(E_OP_Q_SH); };
    op_Q_ADD = "Q.ADD"i             %{ MATCH_OP(E_OP_Q_ADD); };
    op_Q_SUB = "Q.SUB"i             %{ MATCH_OP(E_OP_Q_SUB); };
    op_Q_MUL = "Q.MUL"i             %{ MATCH_OP(E_OP_Q_MUL); };
    op_Q_DIV = "Q.DIV"i             %{ MATCH_OP(E_OP_Q_DIV); };
    op_Q_MOD = "Q.MOD"i             %{ MATCH_OP(E_OP_Q_MOD); };
    op_Q_I = "Q.I"i                 %{ MATCH_OP(E_OP_Q_I); };

    # hardware
    op_CV = "CV"i                   %{ MATCH_OP(E_OP_CV); };
    op_CV_OFF = "CV.OFF"i           %{ MATCH_OP(E_OP_CV_OFF); };
    op_CV_SLEW = "CV.SLEW"i         %{ MATCH_OP(E_OP_CV_SLEW); };
    op_IN = "IN"i                   %{ MATCH_OP(E_OP_IN); };
    op_IN_SCALE = "IN.SCALE"i       %{ MATCH_OP(E_OP_IN_SCALE); };
    op_IN_CAL_MIN = "IN.CAL.MIN"i   %{ MATCH_OP(E_OP_IN_CAL_MIN); };
    op_IN_CAL_MAX = "IN.CAL.MAX"i   %{ MATCH_OP(E_OP_IN_CAL_MAX); };
    op_IN_CAL_RESET = "IN.CAL.RESET"i   %{ MATCH_OP(E_OP_IN_CAL_RESET); };
    op_PARAM = "PARAM"i             %{ MATCH_OP(E_OP_PARAM); };
    op_PARAM_SCALE = "PARAM.SCALE"i %{ MATCH_OP(E_OP_PARAM_SCALE); };
    op_PARAM_CAL_MIN = "PARAM.CAL.MIN"i %{ MATCH_OP(E_OP_PARAM_CAL_MIN); };
    op_PARAM_CAL_MAX = "PARAM.CAL.MAX"i %{ MATCH_OP(E_OP_PARAM_CAL_MAX); };
    op_PARAM_CAL_RESET = "PARAM.CAL.RESET"i %{ MATCH_OP(E_OP_PARAM_CAL_RESET); };
    op_PRM = "PRM"i                 %{ MATCH_OP(E_OP_PRM); };
    op_CV_SET = "CV.SET"i           %{ MATCH_OP(E_OP_CV_SET); };
    op_STATE = "STATE"i             %{ MATCH_OP(E_OP_STATE); };
    op_DEVICE_FLIP = "DEVICE.FLIP"i %{ MATCH_OP(E_OP_DEVICE_FLIP); };

    # maths
    op_ADD = "ADD"i                 %{ MATCH_OP(E_OP_ADD); };
    op_SUB = "SUB"i                 %{ MATCH_OP(E_OP_SUB); };
    op_MUL = "MUL"i                 %{ MATCH_OP(E_OP_MUL); };
    op_DIV = "DIV"i                 %{ MATCH_OP(E_OP_DIV); };
    op_MOD = "MOD"i                 %{ MATCH_OP(E_OP_MOD); };
    op_RAND = "RAND"i               %{ MATCH_OP(E_OP_RAND); };
    op_RND = "RND"i                 %{ MATCH_OP(E_OP_RND); };
    op_RRAND = "RRAND"i             %{ MATCH_OP(E_OP_RRAND); };
    op_RRND = "RRND"i               %{ MATCH_OP(E_OP_RRND); };
    op_R = "R"i                     %{ MATCH_OP(E_OP_R); };
    op_R_MIN = "R.MIN"i             %{ MATCH_OP(E_OP_R_MIN); };
    op_R_MAX = "R.MAX"i             %{ MATCH_OP(E_OP_R_MAX); };
    op_TOSS = "TOSS"i               %{ MATCH_OP(E_OP_TOSS); };
    op_MIN = "MIN"i                 %{ MATCH_OP(E_OP_MIN); };
    op_MAX = "MAX"i                 %{ MATCH_OP(E_OP_MAX); };
    op_LIM = "LIM"i                 %{ MATCH_OP(E_OP_LIM); };
    op_WRAP = "WRAP"i               %{ MATCH_OP(E_OP_WRAP); };
    op_WRP = "WRP"i                 %{ MATCH_OP(E_OP_WRP); };
    op_QT = "QT"i                   %{ MATCH_OP(E_OP_QT); };
    op_QT_S = "QT.S"i               %{ MATCH_OP(E_OP_QT_S); };
    op_QT_CS = "QT.CS"i             %{ MATCH_OP(E_OP_QT_CS); };
    op_QT_B = "QT.B"i               %{ MATCH_OP(E_OP_QT_B); };
    op_QT_BX = "QT.BX"i             %{ MATCH_OP(E_OP_QT_BX); };
    op_AVG = "AVG"i                 %{ MATCH_OP(E_OP_AVG); };
    op_EQ = "EQ"i                   %{ MATCH_OP(E_OP_EQ); };
    op_NE = "NE"i                   %{ MATCH_OP(E_OP_NE); };
    op_LT = "LT"i                   %{ MATCH_OP(E_OP_LT); };
    op_GT = "GT"i                   %{ MATCH_OP(E_OP_GT); };
    op_LTE = "LTE"i                 %{ MATCH_OP(E_OP_LTE); };
    op_GTE = "GTE"i                 %{ MATCH_OP(E_OP_GTE); };
    op_INR = "INR"i                 %{ MATCH_OP(E_OP_INR); };
    op_OUTR = "OUTR"i               %{ MATCH_OP(E_OP_OUTR); };
    op_INRI = "INRI"i               %{ MATCH_OP(E_OP_INRI); };
    op_OUTRI = "OUTRI"i             %{ MATCH_OP(E_OP_OUTRI); };
    op_NZ = "NZ"i                   %{ MATCH_OP(E_OP_NZ); };
    op_EZ = "EZ"i                   %{ MATCH_OP(E_OP_EZ); };
    op_RSH = "RSH"i                 %{ MATCH_OP(E_OP_RSH); };
    op_LSH = "LSH"i                 %{ MATCH_OP(E_OP_LSH); };
    op_RROT = "RROT"i               %{ MATCH_OP(E_OP_RROT); };
    op_LROT = "LROT"i               %{ MATCH_OP(E_OP_LROT); };
    op_EXP = "EXP"i                 %{ MATCH_OP(E_OP_EXP); };
    op_ABS = "ABS"i                 %{ MATCH_OP(E_OP_ABS); };
    op_SGN = "SGN"i                 %{ MATCH_OP(E_OP_SGN); };
    op_AND = "AND"i                 %{ MATCH_OP(E_OP_AND); };
    op_OR = "OR"i                   %{ MATCH_OP(E_OP_OR); };
    op_AND3 = "AND3"i               %{ MATCH_OP(E_OP_AND3); };
    op_OR3 = "OR3"i                 %{ MATCH_OP(E_OP_OR3); };
    op_AND4 = "AND4"i               %{ MATCH_OP(E_OP_AND4); };
    op_OR4 = "OR4"i                 %{ MATCH_OP(E_OP_OR4); };
    op_JI = "JI"i                   %{ MATCH_OP(E_OP_JI); };
    op_SCALE = "SCALE"i             %{ MATCH_OP(E_OP_SCALE); };
    op_SCL = "SCL"i                 %{ MATCH_OP(E_OP_SCL); };
    op_N = "NN"i                    %{ MATCH_OP(E_OP_N); };
    op_VN = "VN"i                   %{ MATCH_OP(E_OP_VN); };
    op_HZ = "HZ"i                   %{ MATCH_OP(E_OP_HZ); };
    op_N_S = "N.S"i                 %{ MATCH_OP(E_OP_N_S); };
    op_N_C = "N.C"i                 %{ MATCH_OP(E_OP_N_C); };
    op_N_CS = "N.CS"i               %{ MATCH_OP(E_OP_N_CS); };
    op_N_B = "N.B"i                 %{ MATCH_OP(E_OP_N_B); };
    op_N_BX = "N.BX"i               %{ MATCH_OP(E_OP_N_BX); };
    op_V = "V"i                     %{ MATCH_OP(E_OP_V); };
    op_VV = "VV"i                   %{ MATCH_OP(E_OP_VV); };
    op_ER = "ER"i                   %{ MATCH_OP(E_OP_ER); };
    op_NR = "NR"i                   %{ MATCH_OP(E_OP_NR); };
    op_BPM = "BPM"i                 %{ MATCH_OP(E_OP_BPM); };
    op_BIT_OR = "|"                 %{ MATCH_OP(E_OP_BIT_OR); };
    op_BIT_AND = "&"                %{ MATCH_OP(E_OP_BIT_AND); };
    op_BIT_NOT = "~"                %{ MATCH_OP(E_OP_BIT_NOT); };
    op_BIT_XOR = "^"                %{ MATCH_OP(E_OP_BIT_XOR); };
    op_BSET = "BSET"i               %{ MATCH_OP(E_OP_BSET); };
    op_BGET = "BGET"i               %{ MATCH_OP(E_OP_BGET); };
    op_BCLR = "BCLR"i               %{ MATCH_OP(E_OP_BCLR); };
    op_BTOG = "BTOG"i               %{ MATCH_OP(E_OP_BTOG); };
    op_BREV = "BREV"i               %{ MATCH_OP(E_OP_BREV); };
    op_XOR = "XOR"i                 %{ MATCH_OP(E_OP_XOR); };
    op_CHAOS = "CHAOS"i             %{ MATCH_OP(E_OP_CHAOS); };
    op_CHAOS_R = "CHAOS.R"i         %{ MATCH_OP(E_OP_CHAOS_R); };
    op_CHAOS_ALG = "CHAOS.ALG"i     %{ MATCH_OP(E_OP_CHAOS_ALG); };
    op_TIF = "?"                    %{ MATCH_OP(E_OP_TIF); };
    op_SYM_PLUS = "+"               %{ MATCH_OP(E_OP_SYM_PLUS); };
    op_SYM_DASH = "-"               %{ MATCH_OP(E_OP_SYM_DASH); };
    op_SYM_STAR = "*"               %{ MATCH_OP(E_OP_SYM_STAR); };
    op_SYM_FORWARD_SLASH = "/"      %{ MATCH_OP(E_OP_SYM_FORWARD_SLASH); };
    op_SYM_PERCENTAGE = "%"         %{ MATCH_OP(E_OP_SYM_PERCENTAGE); };
    op_SYM_EQUAL_x2 = "=="          %{ MATCH_OP(E_OP_SYM_EQUAL_x2); };
    op_SYM_EXCLAMATION_EQUAL = "!=" %{ MATCH_OP(E_OP_SYM_EXCLAMATION_EQUAL); };
    op_SYM_LEFT_ANGLED = "<"        %{ MATCH_OP(E_OP_SYM_LEFT_ANGLED); };
    op_SYM_RIGHT_ANGLED = ">"       %{ MATCH_OP(E_OP_SYM_RIGHT_ANGLED); };
    op_SYM_LEFT_ANGLED_EQUAL = "<="                 %{ MATCH_OP(E_OP_SYM_LEFT_ANGLED_EQUAL); };
    op_SYM_RIGHT_ANGLED_EQUAL = ">="                %{ MATCH_OP(E_OP_SYM_RIGHT_ANGLED_EQUAL); };
    op_SYM_RIGHT_ANGLED_LEFT_ANGLED = "><"          %{ MATCH_OP(E_OP_SYM_RIGHT_ANGLED_LEFT_ANGLED); };
    op_SYM_LEFT_ANGLED_RIGHT_ANGLED = "<>"          %{ MATCH_OP(E_OP_SYM_LEFT_ANGLED_RIGHT_ANGLED); };
    op_SYM_RIGHT_ANGLED_EQUAL_LEFT_ANGLED = ">=<"   %{ MATCH_OP(E_OP_SYM_RIGHT_ANGLED_EQUAL_LEFT_ANGLED); };
    op_SYM_LEFT_ANGLED_EQUAL_RIGHT_ANGLED = "<=>"   %{ MATCH_OP(E_OP_SYM_LEFT_ANGLED_EQUAL_RIGHT_ANGLED); };
    op_SYM_EXCLAMATION = "!"        %{ MATCH_OP(E_OP_SYM_EXCLAMATION); };
    op_SYM_LEFT_ANGLED_x2 = "<<"    %{ MATCH_OP(E_OP_SYM_LEFT_ANGLED_x2); };
    op_SYM_RIGHT_ANGLED_x2 = ">>"   %{ MATCH_OP(E_OP_SYM_RIGHT_ANGLED_x2); };
    op_SYM_LEFT_ANGLED_x3 = "<<<"   %{ MATCH_OP(E_OP_SYM_LEFT_ANGLED_x3); };
    op_SYM_RIGHT_ANGLED_x3 = ">>>"  %{ MATCH_OP(E_OP_SYM_RIGHT_ANGLED_x3); };
    op_SYM_AMPERSAND_x2 = "&&"      %{ MATCH_OP(E_OP_SYM_AMPERSAND_x2); };
    op_SYM_PIPE_x2 = "||"           %{ MATCH_OP(E_OP_SYM_PIPE_x2); };
    op_SYM_AMPERSAND_x3 = "&&&"     %{ MATCH_OP(E_OP_SYM_AMPERSAND_x3); };
    op_SYM_PIPE_x3 = "|||"          %{ MATCH_OP(E_OP_SYM_PIPE_x3); };
    op_SYM_AMPERSAND_x4 = "&&&&"    %{ MATCH_OP(E_OP_SYM_AMPERSAND_x4); };
    op_SYM_PIPE_x4 = "||||"         %{ MATCH_OP(E_OP_SYM_PIPE_x4); };

    # stack
    op_S_ALL = "S.ALL"i             %{ MATCH_OP(E_OP_S_ALL); };
    op_S_POP = "S.POP"i             %{ MATCH_OP(E_OP_S_POP); };
    op_S_CLR = "S.CLR"i             %{ MATCH_OP(E_OP_S_CLR); };
    op_S_L = "S.L"i                 %{ MATCH_OP(E_OP_S_L); };

    # controlflow
    op_SCRIPT = "SCRIPT"i           %{ MATCH_OP(E_OP_SCRIPT); };
    op_SYM_DOLLAR = "$"             %{ MATCH_OP(E_OP_SYM_DOLLAR); };
    op_SCRIPT_POL = "SCRIPT.POL"i   %{ MATCH_OP(E_OP_SCRIPT_POL); };
    op_SYM_DOLLAR_POL = "$.POL"i    %{ MATCH_OP(E_OP_SYM_DOLLAR_POL); };
    op_KILL = "KILL"i               %{ MATCH_OP(E_OP_KILL); };
    op_SCENE = "SCENE"i             %{ MATCH_OP(E_OP_SCENE); };
    op_SCENE_P = "SCENE.P"i         %{ MATCH_OP(E_OP_SCENE_P); };
    op_BREAK = "BREAK"i             %{ MATCH_OP(E_OP_BREAK); };
    op_BRK = "BRK"i                 %{ MATCH_OP(E_OP_BRK); };
    op_SYNC = "SYNC"i               %{ MATCH_OP(E_OP_SYNC); };

    # seed
    op_SEED = "SEED"i               %{ MATCH_OP(E_OP_SEED); };
    op_RAND_SEED = "RAND.SEED"i     %{ MATCH_OP(E_OP_RAND_SEED); };
    op_SYM_RAND_SD = "RAND.SD"i     %{ MATCH_OP(E_OP_SYM_RAND_SD); };
    op_SYM_R_SD = "R.SD"i           %{ MATCH_OP(E_OP_SYM_R_SD); };
    op_TOSS_SEED = "TOSS.SEED"i     %{ MATCH_OP(E_OP_TOSS_SEED); };
    op_SYM_TOSS_SD = "TOSS.SD"i     %{ MATCH_OP(E_OP_SYM_TOSS_SD); };
    op_PROB_SEED = "PROB.SEED"i     %{ MATCH_OP(E_OP_PROB_SEED); };
    op_SYM_PROB_SD = "PROB.SD"i     %{ MATCH_OP(E_OP_SYM_PROB_SD); };
    op_DRUNK_SEED = "DRUNK.SEED"i   %{ MATCH_OP(E_OP_DRUNK_SEED); };
    op_SYM_DRUNK_SD = "DRUNK.SD"i   %{ MATCH_OP(E_OP_SYM_DRUNK_SD); };
    op_P_SEED = "P.SEED"i           %{ MATCH_OP(E_OP_P_SEED); };
    op_SYM_P_SD = "P.SD"i           %{ MATCH_OP(E_OP_SYM_P_SD); };

    op_SEQ_PROB = "NPROB"i          %{ MATCH_OP(E_OP_SEQ_PROB); };
    op_SEQ_REV = "REV"i             %{ MATCH_OP(E_OP_SEQ_REV); };
    op_SEQ_SHUF = "SHUF"i           %{ MATCH_OP(E_OP_SEQ_SHUF); };
    op_SEQ_ROT = "ROT"i             %{ MATCH_OP(E_OP_SEQ_ROT); };
    op_SEQ_NROT = "NROT"i           %{ MATCH_OP(E_OP_SEQ_NROT); };
    op_SEQ_FAST = "FAST"i           %{ MATCH_OP(E_OP_SEQ_FAST); };
    op_SEQ_SLEW = "SLEW"i           %{ MATCH_OP(E_OP_SEQ_SLEW); };
    op_SEQ_SLOW = "SLOW"i           %{ MATCH_OP(E_OP_SEQ_SLOW); };
    op_SEQ_TRUNC = "TRUNC"i         %{ MATCH_OP(E_OP_SEQ_TRUNC); };
    op_SEQ_FLIP = "FLIP"i           %{ MATCH_OP(E_OP_SEQ_FLIP); };
    op_SEQ_STUT = "STUT"i           %{ MATCH_OP(E_OP_SEQ_STUT); };
    op_SEQ_FRAC = "FRAC"i           %{ MATCH_OP(E_OP_SEQ_FRAC); };
    op_SEQ_HARM = "HARM"i           %{ MATCH_OP(E_OP_SEQ_HARM); };
    op_SEQ_TRANSP = "TRANSP"i       %{ MATCH_OP(E_OP_SEQ_TRANSP); };
    op_SEQ_QNT = "QNT"i             %{ MATCH_OP(E_OP_SEQ_QNT); };
    op_SEQ_LOOP = "LOOP"i           %{ MATCH_OP(E_OP_SEQ_LOOP); };
    
    op_V_CV = "V.CV"i               %{ MATCH_OP(E_OP_V_CV); };
    op_V_EG = "V.EG"i               %{ MATCH_OP(E_OP_V_EG); };
    op_V_GATE = "V.GATE"i           %{ MATCH_OP(E_OP_V_GATE); };
    op_V_MIDI = "V.MIDI"i           %{ MATCH_OP(E_OP_V_MIDI); };
    op_V_PLAY = "V.PLAY"i           %{ MATCH_OP(E_OP_V_PLAY); };
    op_V_PAUSE = "V.PAUSE"i         %{ MATCH_OP(E_OP_V_PAUSE); };
    op_V_MUTE = "V.MUTE"i           %{ MATCH_OP(E_OP_V_MUTE); };
    op_V_RESET = "V.RESET"i         %{ MATCH_OP(E_OP_V_RESET); };
    op_V_LOOP = "V.LOOP"i           %{ MATCH_OP(E_OP_V_LOOP); };
    op_V_OCT = "V.OCT"i             %{ MATCH_OP(E_OP_V_OCT); };
    op_V_LENGTH = "V.LENGTH"i       %{ MATCH_OP(E_OP_V_LENGTH); };
    op_V_VEL = "V.VEL"i             %{ MATCH_OP(E_OP_V_VEL); };
    op_V_ENV = "V.ENV"i             %{ MATCH_OP(E_OP_V_ENV); };

    # controlflow
    mod_IF = "IF"i                  %{ MATCH_MOD(E_MOD_IF); };
    mod_ELIF = "ELIF"i              %{ MATCH_MOD(E_MOD_ELIF); };
    mod_ELSE = "ELSE"i              %{ MATCH_MOD(E_MOD_ELSE); };
    mod_L = "L"i                    %{ MATCH_MOD(E_MOD_L); };
    mod_W = "W"i                    %{ MATCH_MOD(E_MOD_W); };
    mod_EVERY = "EVERY"i            %{ MATCH_MOD(E_MOD_EVERY); };
    mod_EV = "EV"i                  %{ MATCH_MOD(E_MOD_EV); };
    mod_SKIP = "SKIP"i              %{ MATCH_MOD(E_MOD_SKIP); };
    mod_MODULO = "MOD"i             %{ MATCH_MOD(E_MOD_MODULO); };
    mod_OTHER = "OTHER"i            %{ MATCH_MOD(E_MOD_OTHER); };
    mod_PROB = "PROB"i              %{ MATCH_MOD(E_MOD_PROB); };

    # stack
    mod_S = "S"i                    %{ MATCH_MOD(E_MOD_S); };

    mod_SEQ = "N"i                  %{ MATCH_MOD(E_MOD_SEQ); };
    mod_SEQ_PROB = "PROB"i          %{ MATCH_MOD(E_MOD_SEQ_PROB); };
    mod_SEQ_FAST = "FAST"i          %{ MATCH_MOD(E_MOD_SEQ_FAST); };
    mod_SEQ_SLOW = "SLOW"i          %{ MATCH_MOD(E_MOD_SEQ_SLOW); };
    mod_SEQ_STUT = "STUT"i          %{ MATCH_MOD(E_MOD_SEQ_STUT); };
    mod_SEQ_FRAC = "FRAC"i          %{ MATCH_MOD(E_MOD_SEQ_FRAC); };
    mod_SEQ_HARM = "HARM"i          %{ MATCH_MOD(E_MOD_SEQ_HARM); };
    mod_SEQ_TRANSP = "TRANSP"i      %{ MATCH_MOD(E_MOD_SEQ_TRANSP); };
    mod_SEQ_QNT = "QNT"i            %{ MATCH_MOD(E_MOD_SEQ_QNT); };
    mod_SEQ_SLEW = "SLEW"i          %{ MATCH_MOD(E_MOD_SEQ_SLEW); };

    op = number | 
        op_A |
        op_B |
        op_C |
        op_D |
        op_DRUNK |
        op_DRUNK_MAX |
        op_DRUNK_MIN |
        op_DRUNK_WRAP |
        op_FLIP |
        op_I |
        op_O |
        op_O_INC |
        op_O_MAX |
        op_O_MIN |
        op_O_WRAP |
        op_T |
        op_TIME |
        op_TIME_ACT |
        op_LAST |
        op_X |
        op_Y |
        op_Z |
        op_J |
        op_K |
        op_INIT |
        op_INIT_SCENE |
        op_INIT_SCRIPT |
        op_INIT_SCRIPT_ALL |
        op_INIT_CV |
        op_INIT_CV_ALL |
        op_INIT_DATA |
        op_INIT_TIME |
        op_M |
        op_M_SYM_EXCLAMATION |
        op_M_ACT |
        op_M_RESET |
        op_Q |
        op_Q_AVG |
        op_Q_N |
        op_Q_CLR |
        op_Q_GRW |
        op_Q_SUM |
        op_Q_MIN |
        op_Q_MAX |
        op_Q_RND |
        op_Q_SRT |
        op_Q_REV |
        op_Q_SH |
        op_Q_ADD |
        op_Q_SUB |
        op_Q_MUL |
        op_Q_DIV |
        op_Q_MOD |
        op_Q_I |
        op_CV |
        op_CV_OFF |
        op_CV_SLEW |
        op_IN |
        op_IN_SCALE |
        op_IN_CAL_MIN |
        op_IN_CAL_MAX |
        op_IN_CAL_RESET |
        op_PARAM |
        op_PARAM_SCALE |
        op_PARAM_CAL_MIN |
        op_PARAM_CAL_MAX |
        op_PARAM_CAL_RESET |
        op_PRM |
        op_CV_SET |
        op_STATE |
        op_DEVICE_FLIP |
        op_ADD |
        op_SUB |
        op_MUL |
        op_DIV |
        #op_MOD | @nate - remove this (or fix parser ambiguity)
        op_RAND |
        op_RND |
        op_RRAND |
        op_RRND |
        op_R |
        op_R_MIN |
        op_R_MAX |
        op_TOSS |
        op_MIN |
        op_MAX |
        op_LIM |
        op_WRAP |
        op_WRP |
        op_QT |
        op_QT_S |
        op_QT_CS |
        op_QT_B |
        op_QT_BX |
        op_AVG |
        op_EQ |
        op_NE |
        op_LT |
        op_GT |
        op_LTE |
        op_GTE |
        op_INR |
        op_OUTR |
        op_INRI |
        op_OUTRI |
        op_NZ |
        op_EZ |
        op_RSH |
        op_LSH |
        op_RROT |
        op_LROT |
        op_EXP |
        op_ABS |
        op_SGN |
        op_AND |
        op_OR |
        op_AND3 |
        op_OR3 |
        op_AND4 |
        op_OR4 |
        op_JI |
        op_SCALE |
        op_SCL |
        op_N |
        op_VN |
        op_HZ |
        op_N_S |
        op_N_C |
        op_N_CS |
        op_N_B |
        op_N_BX |
        op_V |
        op_VV |
        op_ER |
        op_NR |
        op_BPM |
        op_BIT_OR |
        op_BIT_AND |
        op_BIT_NOT |
        op_BIT_XOR |
        op_BSET |
        op_BGET |
        op_BCLR |
        op_BTOG |
        op_BREV |
        op_XOR |
        op_CHAOS |
        op_CHAOS_R |
        op_CHAOS_ALG |
        op_TIF |
        op_SYM_PLUS |
        op_SYM_DASH |
        op_SYM_STAR |
        op_SYM_FORWARD_SLASH |
        op_SYM_PERCENTAGE |
        op_SYM_EQUAL_x2 |
        op_SYM_EXCLAMATION_EQUAL |
        op_SYM_LEFT_ANGLED |
        op_SYM_RIGHT_ANGLED |
        op_SYM_LEFT_ANGLED_EQUAL |
        op_SYM_RIGHT_ANGLED_EQUAL |
        op_SYM_RIGHT_ANGLED_LEFT_ANGLED |
        op_SYM_LEFT_ANGLED_RIGHT_ANGLED |
        op_SYM_RIGHT_ANGLED_EQUAL_LEFT_ANGLED |
        op_SYM_LEFT_ANGLED_EQUAL_RIGHT_ANGLED |
        op_SYM_EXCLAMATION |
        op_SYM_LEFT_ANGLED_x2 |
        op_SYM_RIGHT_ANGLED_x2 |
        op_SYM_LEFT_ANGLED_x3 |
        op_SYM_RIGHT_ANGLED_x3 |
        op_SYM_AMPERSAND_x2 |
        op_SYM_PIPE_x2 |
        op_SYM_AMPERSAND_x3 |
        op_SYM_PIPE_x3 |
        op_SYM_AMPERSAND_x4 |
        op_SYM_PIPE_x4 |
        op_S_ALL |
        op_S_POP |
        op_S_CLR |
        op_S_L |
        op_SCRIPT |
        op_SYM_DOLLAR |
        op_SCRIPT_POL |
        op_SYM_DOLLAR_POL |
        op_KILL |
        op_SCENE |
        op_SCENE_P |
        op_BREAK |
        op_BRK |
        op_SYNC |
        op_SEED |
        op_RAND_SEED |
        op_SYM_RAND_SD |
        op_SYM_R_SD |
        op_TOSS_SEED |
        op_SYM_TOSS_SD |
        op_PROB_SEED |
        op_SYM_PROB_SD |
        op_DRUNK_SEED |
        op_SYM_DRUNK_SD |
        op_P_SEED |
        op_SYM_P_SD |
        op_SEQ_PROB |
        op_SEQ_REV |
        op_SEQ_SHUF |
        op_SEQ_ROT |
        op_SEQ_NROT |
        op_SEQ_FAST |
        op_SEQ_SLEW |
        op_SEQ_SLOW |
        op_SEQ_TRUNC |
        op_SEQ_FLIP |
        op_SEQ_STUT |
        op_SEQ_FRAC |
        op_SEQ_HARM |
        op_SEQ_TRANSP |
        op_SEQ_QNT |
        op_SEQ_LOOP |
        op_V_CV |
        op_V_EG |
        op_V_GATE |
        op_V_MIDI |
        op_V_PLAY |
        op_V_PAUSE |
        op_V_MUTE |
        op_V_RESET |
        op_V_LOOP |
        op_V_OCT |
        op_V_LENGTH |
        op_V_VEL |
        op_V_ENV;

    mod = mod_IF |
        mod_ELIF |
        mod_ELSE |
        mod_L |
        mod_W |
        mod_EVERY |
        mod_EV |
        mod_SKIP |
        mod_MODULO |
        mod_OTHER |
        mod_PROB |
        mod_S;

    mod_alda = mod_SEQ;

    patternable = mod |
        mod_SEQ_PROB |
        mod_SEQ_FAST |
        mod_SEQ_SLOW |
        mod_SEQ_STUT |
        mod_SEQ_FRAC |
        mod_SEQ_HARM |
        mod_SEQ_TRANSP |
        mod_SEQ_QNT |
        mod_SEQ_SLEW;

}%%
