#include "espresso/command.h"
#include "espresso/validator.h"

#include <algorithm>

#include <ops/op.h>

using std::min;

Espresso::e_parse_error Espresso::validate(const Command *c,
                 char error_msg[PARSE_ERROR_STR_LENGTH]) {
  error_msg[0] = 0;
  int16_t stack_depth = 0;
  uint8_t idx = c->length;
  int8_t sep_count = 0;
  bool has_parens = false;

  while (idx--) { // process words right to left
    token_type word_type = c->data[idx].tag;
    int16_t word_value = c->data[idx].value;
    // A first_cmd is either at the beginning of the command or immediately
    // after the PRE_SEP or COMMAND_SEP
    bool first_cmd = idx == 0 || c->data[idx - 1].tag == PRE_SEP ||
                     c->data[idx - 1].tag == SUB_SEP;

    if (word_type == BRACKETS) {
      // BRACKETS evaluate to a single value
      stack_depth -= word_value - 1;
    } else if (word_type == PARENS) {
      has_parens = true;
    } else if (word_type == NUMBER || word_type == XNUMBER || word_type == BNUMBER ||
        word_type == RNUMBER) {
      stack_depth++;
    } else if (word_type == OP) {
      const tele_op_t *op = tele_ops[word_value];

      // if we're not a first_cmd we need to return something
      if (!first_cmd && !op->returns) {
        strcpy(error_msg, op->name);
        return E_NOT_LEFT;
      }

      stack_depth -= op->params;

      // Envelope OP accepts up to 4 arguments
      if (stack_depth > 0 && word_value == E_OP_V_ENV) {
        stack_depth -= min<int16_t>(stack_depth, 3);
      }

      if (stack_depth < 0) {
        strcpy(error_msg, op->name);
        return E_NEED_PARAMS;
      }

      stack_depth += op->returns ? 1 : 0;

      // if we are in the first_cmd position and there is a set fn
      // decrease the stack depth
      // TODO this is technically wrong. the only reason we get away with
      // it is that it's idx == 0, and the while loop is about to end.
      if (first_cmd && op->set != NULL)
        stack_depth--;
    } else if (word_type == MOD) {
      e_parse_error mod_error = E_OK;

      auto max_params = tele_mods[word_value]->opt_params
        ? tele_mods[word_value]->opt_params : tele_mods[word_value]->params;

      if (!has_parens && idx != 0)
        mod_error = E_NO_MOD_HERE;
      else if (!has_parens && c->separator == -1)
        mod_error = E_NEED_PRE_SEP;
      else if (stack_depth < tele_mods[word_value]->params)
        mod_error = E_NEED_PARAMS;
      else if (!has_parens && stack_depth > max_params)
        mod_error = E_EXTRA_PARAMS;

      if (mod_error != E_OK) {
        strcpy(error_msg, tele_mods[word_value]->name);
        return mod_error;
      }

      stack_depth = 0;
    } else if (word_type == PRE_SEP) {
      sep_count++;
      if (sep_count > 1)
        return E_MANY_PRE_SEP;

      if (idx == 0)
        return E_PLACE_PRE_SEP;

      if (c->data[0].tag != MOD)
        return E_PLACE_PRE_SEP;

      if (stack_depth > 1) {
        return E_EXTRA_PARAMS;
      }

      // reset the stack depth
      stack_depth = 0;
    } else if (word_type == SUB_SEP) {
      if (sep_count > 0)
        return E_NO_SUB_SEP_IN_PRE;

      if (stack_depth > 1)
        return E_EXTRA_PARAMS;

      // reset the stack depth
      stack_depth = 0;
      has_parens = false;
    }
  }

  if (stack_depth > 1)
    return E_EXTRA_PARAMS;
  else
    return E_OK;
}

const char *Espresso::parse_error_str(e_parse_error e) {
  const char *error_string[] = {"OK",
                                "UNKNOWN WORD",
                                "COMMAND TOO LONG",
                                "NOT ENOUGH PARAMS",
                                "TOO MANY PARAMS",
                                "MOD NOT ALLOWED HERE",
                                "EXTRA PRE SEPARATOR",
                                "NEED PRE SEPARATOR",
                                "BAD PRE SEPARATOR",
                                "NO SUB SEP IN PRE",
                                "MOVE LEFT",
                                "UNKNOWN ERROR",
                              };

  if (e >= E_ERRORS_LENGTH)
    e = E_UNKNOWN;
  return error_string[e];
}
