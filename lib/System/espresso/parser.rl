#include "espresso/command.h"
#include "espresso/parser.h"

#include <stdlib.h>
#include <string.h>

#include <log.h>
#include <util.h>
#include <ops/op_enum.h>
#include <teletype.h>

#include "sequence.h"

const size_t kMaxTokenLength = 32;

using Util::rev_bitstring_to_int;

%%{
    machine parser;

    include "tokens.rl";
    include "alda.rl";

    action error {
        // fhold;
        char msg[kMaxTokenLength] = {0};
        int i = 0;
        auto *ptr = p;
        while (ptr > data) {
            auto *prev = ptr - 1;
            if (*prev == ' ' || *prev == '\t' || *prev == ':' || *prev == ';' || *prev == '(' || *prev == ')') break;
            ptr--;
        }

        while (ptr < eof) {
            if (*ptr == ' ' || *ptr == '\t') break;
            msg[i++] = *ptr++;
        }
        msg[i] = 0;
        LOG("\nParse error: %s\n", msg);
        strcpy(error_msg, msg);
        return E_PARSE;
    }

    series_op = b_open sep* op (sep+ op)* sep* b_close;
    op_or_series = (op | series_op);

    pre = mod (sep+ op)*;
    pre_alda = mod_alda (sep+ op)*;

    pattern = (pre sep* | patternable) p_open sep* op (sep+ op)* sep* p_close;
    pattern_alda = pre_alda sep* p_open sep* alda (sep+ alda)* sep* p_close;

    ops_or_pattern = op_or_series (sep+ op_or_series)* | pattern | pattern_alda;

    main := (sep*
        (
            (pre sep* pre_sep)? sep* ops_or_pattern sep* |
            pre_alda sep* pre_sep sep* alda (sep+ alda)* sep*
        )
        (sep* sub_sep sep* ops_or_pattern)* sep* ''
    ) $!error;
    
    write data;
}%%


#define MATCH_OP(op) {                              \
    LOG("\nMATCH_OP %d\n", op);                     \
    reset_token(tele_data, buf, buf_i);             \
    tele_data.tag = Espresso::OP; tele_data.value = op;       \
    RECORD_TOKEN();                                 \
}

#define MATCH_MOD(mod) {                            \
    LOG("\nMATCH_MOD %d\n", mod);                   \
    reset_token(tele_data, buf, buf_i);             \
    tele_data.tag = Espresso::MOD; tele_data.value = mod;     \
    RECORD_TOKEN();                                 \
}

#define RECORD_TOKEN() {                            \
    {                                               \
        auto recorded = record_token(result, tele_data);    \
        if (recorded != E_OK) return recorded;      \
    }                                               \
}

void reset_buf(char *buf, int &buf_i) {
    memset(buf, 0, kMaxTokenLength);
    buf_i = 0;
}

void reset_token(Espresso::Token &tele_data, char *buf, int &buf_i) {
    LOG("\nreset_token\n");
    memset(&tele_data, 0, sizeof(Espresso::Token));
    reset_buf(buf, buf_i);
}

Espresso::e_parse_error record_token(Espresso::Command &result, Espresso::Token &tele_data) {
    LOG("\nrecord_token\n");
    result.data[result.length] = tele_data;
    result.length++;
    if (result.length >= ESPRESSO_COMMAND_MAX_LENGTH) {
        return Espresso::E_LENGTH;
    }
    return Espresso::E_OK;
}

void record_note(Espresso::Token &tele_data, const Sequence::NoteName note, int &note_count) {
    tele_data.tag = Espresso::ALDA_NOTE;
    if (note_count < 4) {
        Sequence::packNoteName(tele_data.data[note_count], note);
    }
    note_count++;
}

void record_octave(Espresso::Token &tele_data, int8_t &current_octave, const int8_t offset, const int note_count) {
    current_octave += offset;
    if (note_count < 4) {
        Sequence::packNoteOctave(tele_data.data[note_count], current_octave);
    }
}

Espresso::e_parse_error Espresso::parser(const char *data, Command &result,
                char error_msg[PARSE_ERROR_STR_LENGTH]) {
    // Ragel vars
    int cs;
    const char *p = data;
    const char *pe = p + strlen(p);
    const char *eof = pe;

    // Token buffer
    Token tele_data;
    char buf[kMaxTokenLength] = {};
    int buf_i = 0;
    memset(buf, 0, kMaxTokenLength);

    // Alda-related vars
    int note_count = 0;
    int8_t current_octave = 0;

    int parens_start = -1;
    int bracket_start = -1;

    error_msg[0] = 0;
    result.length = 0;
    result.separator = -1;

    if (strlen(p) == 0) {
      return E_OK;
    }

    %%{
        write init; # write initialisation
        write exec; # run the machine
    }%%

    if (cs == parser_error || cs < parser_first_final) {
        strcpy(error_msg, "SCAN!");
        return E_PARSE;
    }

    return E_OK;
}
