%%{
    machine parser;

    action reset_alda {
        reset_token(tele_data, buf, buf_i);
        note_count = 0;
        current_octave = 0;
    }

    action push_dur { buf[buf_i++ % kMaxTokenLength] = fc; }
    action proc_dur {
        int val = strtol(buf, nullptr, 0);
        auto unit = Sequence::DurRel;
        if (buf_i > 2 && buf[buf_i - 2] == 'M' && buf[buf_i - 1] == 'S') {
            unit = Sequence::DurMs;
        } else if (buf_i > 1 && buf[buf_i - 1] == 'S') {
            unit = Sequence::DurSec;
        }
        Sequence::packNoteDuration(tele_data.value, val, unit);
        reset_buf(buf, buf_i);
    }

    action push_dots {
        LOG("\npush_dots %d\n", buf[0]);
        buf[0]++;
    }
    action proc_dots {
        LOG("\nproc_docs %d\n", buf[0]);
        Sequence::packNoteDots(tele_data.value, buf[0]);
        reset_buf(buf, buf_i);
    }

    action push_reps { buf[buf_i++ % kMaxTokenLength] = fc; }
    action proc_reps {
        int repeats = strtol(buf+1, nullptr, 0);
        Sequence::packNoteRepeats(tele_data.value, repeats);
        reset_buf(buf, buf_i);
    }

    octave = "<" @{ record_octave(tele_data, current_octave, -1, note_count); }
        | ">" @{ record_octave(tele_data, current_octave, 1, note_count); };

    note_sep = "/";
    note = "A-"i @{ record_note(tele_data, Sequence::NoteAb, note_count); }
        | "A"i   %{ record_note(tele_data, Sequence::NoteA, note_count); }
        | "A+"i  @{ record_note(tele_data, Sequence::NoteBb, note_count); }
        | "B-"i  @{ record_note(tele_data, Sequence::NoteBb, note_count); }
        | "B"i   %{ record_note(tele_data, Sequence::NoteB, note_count); }
        | "C-"i  @{ record_note(tele_data, Sequence::NoteB, note_count); }
        | "B+"i  @{ record_note(tele_data, Sequence::NoteC, note_count); }
        | "C"i   %{ record_note(tele_data, Sequence::NoteC, note_count); }
        | "C+"i  @{ record_note(tele_data, Sequence::NoteDb, note_count); }
        | "D-"i  @{ record_note(tele_data, Sequence::NoteDb, note_count); }
        | "D"i   %{ record_note(tele_data, Sequence::NoteD, note_count); }
        | "D+"i  @{ record_note(tele_data, Sequence::NoteEb, note_count); }
        | "E-"i  @{ record_note(tele_data, Sequence::NoteEb, note_count); }
        | "E"i   %{ record_note(tele_data, Sequence::NoteE, note_count); }
        | "F-"i  @{ record_note(tele_data, Sequence::NoteE, note_count); }
        | "E+"i  @{ record_note(tele_data, Sequence::NoteF, note_count); }
        | "F"i   %{ record_note(tele_data, Sequence::NoteF, note_count); }
        | "F+"i  @{ record_note(tele_data, Sequence::NoteGb, note_count); }
        | "G-"i  @{ record_note(tele_data, Sequence::NoteGb, note_count); }
        | "G"i   %{ record_note(tele_data, Sequence::NoteG, note_count); }
        | "G+"i  @{ record_note(tele_data, Sequence::NoteAb, note_count); };

    rest = "R"i  @{ record_note(tele_data, Sequence::NoteRest, note_count); };

    duration = ([1-9][0-9]*("MS"|"S")?) $push_dur %proc_dur;

    legato = "~" @{ 
        Sequence::packNoteLegato(tele_data.value, true); 
    };
    dots = ("."+) $push_dots %proc_dots;
    legato_or_dots = (legato? dots? | dots? legato?);

    repeats = ("*"[1-9][0-9]*) $push_reps %proc_reps;

    alda = ((((octave* note) (note_sep? octave* note)*) | rest)
        duration? legato_or_dots? repeats? "") >reset_alda %{ RECORD_TOKEN(); };

}%%