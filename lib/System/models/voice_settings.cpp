#include <hid_keys.h>
#include <keyboard_helper.h>

#include "types.h"
#include "models/mode_manager.h"
#include "models/voice_settings.h"

#define NUM_VOICES 8

void Models::VoiceSettings::resetAll() {
  for (int ci = 0; ci < VOICE_COUNT; ci++) {
    auto &config = configs[ci];

    if (ci < 4) {
      config.cv_out = ci + 1;
      config.gate_out = ci + 1;
    }
    config.eg_out = 0;
    config.midi_out_ch = 0;

    config.octave = 1;
    config.note_length = 80;
    config.note_vel = 100;
  }
}

void Models::VoiceSettings::processKeypress(uint8_t k, uint8_t m, bool is_held_key) {
  // <tab>: Jump to editor
  if (match_no_mod(m, k, HID_TAB) || match_shift(m, k, HID_TAB)) {
    auto &editMode = modeManager->getMode(M_EDIT);
    editMode.setScript(script);
    modeManager->activateMode(M_EDIT);
  }

  // meta-shift-[: previous script
  else if (match_win_shift(m, k, HID_OPEN_BRACKET)) {
    if (script)
      script = static_cast<script_number_t>(script - 1);
    else
      script = static_cast<script_number_t>(NUM_VOICES - 1);
    dirty = true;
  }
  // meta-shift-]: next script
  else if (match_win_shift(m, k, HID_CLOSE_BRACKET)) {
    script = static_cast<script_number_t>(script + 1);
    if (script >= NUM_VOICES)
      script = TT_SCRIPT_1;
    dirty = true;
  }

  // @nate - META + UP/DOWN == jump to start/end
  else if (!is_editing && match_no_mod(m, k, HID_DOWN)) {
    if (selected_index + 1 < kVoiceSettingsSelectableCount) {
      back_to_top = false;
      selected_index++;
      dirty = true;
    }
  }
  else if (!is_editing && match_no_mod(m, k, HID_UP)) {
    if (selected_index > 0) {
      selected_index--;
      dirty = true;
    } else if (!back_to_top) {
      back_to_top = true;
      dirty = true;
    }
  }

  else if (match_no_mod(m, k, HID_ENTER)) {
    is_editing = !is_editing;
    dirty = true;
  }

  else if (is_editing && (match_no_mod(m, k, HID_DOWN) || match_no_mod(m, k, HID_MINUS))) {
    dirty = decrementSelected();
  }
  else if (is_editing && (match_no_mod(m, k, HID_UP) || match_shift(m, k, HID_EQUAL))) {
    dirty = incrementSelected();
  }

  // ctrl OR option +
    // auto-increment (pick next available slot for CV, GATE, MIDI channel)
    // increment by 10 (for others)

  // meta +
    // increase to max

  // numbers
    // if not editing field, start editing field (line_editor to track state?)
    // push number on line_editor

}


bool Models::VoiceSettings::incrementSelected() {
  auto &config = configs[script];
  switch (selected_index) {
  case 0:
    return increment_range(config.cv_out, 4);
  case 1:
    return increment_range(config.gate_out, 4);
  case 2:
    return increment_range(config.midi_out_ch, 16);
  case 3:
    return increment_range(config.octave, 5);
  case 4:
    return increment_range(config.note_length, 100);
  case 5:
    return increment_range(config.note_vel, 127);
  }
  return false;
}

bool Models::VoiceSettings::decrementSelected() {
  auto &config = configs[script];
  switch (selected_index) {
  case 0:
    return decrement_range(config.cv_out, 0);
  case 1:
    return decrement_range(config.gate_out, 0);
  case 2:
    return decrement_range(config.midi_out_ch, 0);
  case 3:
    return decrement_range(config.octave, -2);
  case 4:
    return decrement_range(config.note_length, 1);
  case 5:
    return decrement_range(config.note_vel, 0);
  }
  return false;
}
