#include "models/preset_w_mode.h"
#include "models/mode_manager.h"
#include "models/preset_r_mode.h"
#include <hid_keys.h>
#include <keyboard_helper.h>
#include <string.h>
#include <util.h>

using namespace Models;

void PresetWMode::activate() {
  edit_line = 0;
  edit_offset = 0;
  le.line_editor_set(scene_text[0]);
  dirty = D_ALL;
}

void PresetWMode::processKeypress(uint8_t k, uint8_t m,
                                        bool is_held_key) {
  // <down> or C-n: line down
  if (match_no_mod(m, k, HID_DOWN) || match_ctrl(m, k, HID_N)) {
    if ((edit_offset + edit_line) < 31) {
      if (edit_line == 5)
        edit_offset++;
      else
        edit_line++;
      le.line_editor_set(scene_text[edit_line + edit_offset]);
      dirty |= D_LIST;
      dirty |= D_INPUT;
    }
  }
  // <up> or C-p: line up
  else if (match_no_mod(m, k, HID_UP) || match_ctrl(m, k, HID_P)) {
    if (edit_line + edit_offset) {
      if (edit_line)
        edit_line--;
      else
        edit_offset--;
      le.line_editor_set(scene_text[edit_line + edit_offset]);
      dirty |= D_LIST;
      dirty |= D_INPUT;
    }
  }
  // [: preset down
  else if (match_no_mod(m, k, HID_OPEN_BRACKET)) {
    if (presetRMode.preset_select)
      presetRMode.preset_select--;
    dirty |= D_LIST;
  }
  // ]: preset up
  else if (match_no_mod(m, k, HID_CLOSE_BRACKET)) {
    if (presetRMode.preset_select < SCENE_SLOTS - 1)
      presetRMode.preset_select++;
    dirty |= D_LIST;
  }
  // <enter>: enter text
  else if (match_no_mod(m, k, HID_ENTER)) {
    strcpy(scene_text[edit_line + edit_offset], le.line_editor_get());
    if (edit_line + edit_offset < 31) {
      if (edit_line == 5)
        edit_offset++;
      else
        edit_line++;
    }
    le.line_editor_set(scene_text[edit_line + edit_offset]);
    dirty |= D_LIST;
    dirty |= D_INPUT;
  }
  // shift-<enter>: insert text
  else if (match_shift(m, k, HID_ENTER)) {
    for (uint8_t i = kSceneTextLines - 1; i > edit_line + edit_offset; i--)
      strcpy(scene_text[i], scene_text[i - 1]); // overwrites final line!
    strcpy(scene_text[edit_line + edit_offset], le.line_editor_get());
    dirty |= D_LIST;
  }
  // alt-<enter>: save preset
  else if (match_alt(m, k, HID_ENTER)) {
    if (!is_held_key) {
      strcpy(scene_text[edit_line + edit_offset], le.line_editor_get());
      sceneStore.save(presetRMode.preset_select);
      configStore.flash_update_last_saved_scene(presetRMode.preset_select);
      modeManager->activatePriorMode();
    }
  } else { // pass to line editor
    bool processed = le.line_editor_process_keys(k, m, is_held_key);
    if (processed)
      dirty |= D_INPUT;
  }
}
