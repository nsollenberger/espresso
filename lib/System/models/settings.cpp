#include <hid_keys.h>
#include <keyboard_helper.h>

#include "models/mode_manager.h"
#include "models/settings.h"

void Models::Settings::processKeypress(uint8_t key, uint8_t mod_key, bool is_held_key) {

  if (match_no_mod(mod_key, key, HID_SPACEBAR) || match_no_mod(mod_key, key, HID_ENTER)) {
    switch (getSelection()) {
    case PLAY_PAUSE:
      setIsRunning(!isRunning);
      break;
    case RESET:
      setIsRunning(false);
      shouldReset = true;
      break;
    case CLOCK_TEMPO:
    case LOOP_LENGTH:
      isEditing = !isEditing;
      dirty = true;
      break;
    case LOAD:
      modeManager->activateMode(M_PRESET_R);
      break;
    case SAVE:
      modeManager->activateMode(M_PRESET_W);
      break;
    default:
      break;
    }
  }

  if (isEditing) {
    if (match_no_mod(mod_key, key, HID_DOWN)) {
      switch (getSelection()) {
      case CLOCK_TEMPO:
        decrementClock();
        break;
      case LOOP_LENGTH:
        decrementLoopLength();
        break;
      default:
        break;
      }
    } else if (match_no_mod(mod_key, key, HID_UP)) {
      switch (getSelection()) {
      case CLOCK_TEMPO:
        incrementClock();
        break;
      case LOOP_LENGTH:
        incrementLoopLength();
        break;
      default:
        break;
      }
    }
  }

  else if (match_no_mod(mod_key, key, HID_DOWN)) {
    if (row_index + 1 < MAX_ROWS) {
      row_index++;
      dirty = true;
    }
  }

  else if (match_no_mod(mod_key, key, HID_UP)) {
    if (row_index > 0) {
      row_index--;
      dirty = true;
    }
  }

  else if (match_no_mod(mod_key, key, HID_RIGHT)) {
    if (col_index + 1 < MAX_COLS) {
      col_index++;
      dirty = true;
    }
  }

  else if (match_no_mod(mod_key, key, HID_LEFT)) {
    if (col_index > 0) {
      col_index--;
      dirty = true;
    }
  }

}
