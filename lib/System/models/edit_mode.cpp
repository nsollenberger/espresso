#include <algorithm>
#include <string.h>

#include <hid_keys.h>
#include <keyboard_helper.h>

#include "line_editor.h"
#include "models/edit_mode.h"
#include "models/mode_manager.h"
#include "espresso/command.h"
#include "espresso/parser.h"
#include "espresso/validator.h"
#include "scene.h"

using std::max;
using std::min;
using Espresso::parser;
using Espresso::validate;

void Models::EditMode::activate() {
  status = Espresso::E_OK;
  error_msg[0] = 0;
  line_no2 = line_no1 = 0;
  le.line_editor_set_command(
      scene.ss_get_script_command(script, line_no1));
  undo_count = 0;
  dirty = D_ALL;
}

void Models::EditMode::setScript(script_number_t s) {
  script = s;
  if (script > INIT_SCRIPT)
    script = INIT_SCRIPT;
  undo_count = 0;
  dirty = D_ALL;
}

void Models::EditMode::processKeypress(uint8_t k, uint8_t m, bool is_held_key) {

  // <tab>: Jump to voice settings
  if (match_no_mod(m, k, HID_TAB) || match_shift(m, k, HID_TAB)) {
    auto &voiceSettings = modeManager->getMode(M_VOICE_SETTINGS_1);
    voiceSettings.setScript(script);
    modeManager->activateMode(M_VOICE_SETTINGS_1);
  }

  // C-z: undo
  else if (match_ctrl(m, k, HID_Z)) {
    undo();
  }

  // <down> or C-n: line down
  else if (match_no_mod(m, k, HID_DOWN) || match_ctrl(m, k, HID_N)) {
    if (line_no1 < (SCRIPT_MAX_COMMANDS - 1) &&
        line_no1 < scene.ss_get_script_len(script)) {
      line_no1++;
      le.line_editor_set_command(
          scene.ss_get_script_command(script, line_no1));
      dirty |= D_LIST | D_INPUT;
    }
    // reset selection
    if (line_no2 != line_no1) {
      line_no2 = line_no1;
      dirty |= D_LIST | D_INPUT;
    }
  }
  // <up> or C-p: line up
  else if (match_no_mod(m, k, HID_UP) || match_ctrl(m, k, HID_P)) {
    if (line_no1) {
      line_no1--;
      le.line_editor_set_command(
          scene.ss_get_script_command(script, line_no1));
      dirty |= D_LIST | D_INPUT;
    }
    // reset selection
    if (line_no2 != line_no1) {
      line_no2 = line_no1;
      dirty |= D_LIST | D_INPUT;
    }
  }

  // shift-<down> or C-S-n: expand selection down
  else if (match_shift(m, k, HID_DOWN) || match_shift_ctrl(m, k, HID_N)) {
    if (line_no2 < (SCRIPT_MAX_COMMANDS - 1) &&
        line_no2 < max<uint8_t>(1, scene.ss_get_script_len(script)) - 1) {
      line_no2++;
      dirty |= D_LIST | D_INPUT;
    }
  }
  // shift-<up> or C-S-p: expand selection up
  else if (match_shift(m, k, HID_UP) || match_shift_ctrl(m, k, HID_P)) {
    if (line_no1 >= scene.ss_get_script_len(script)) {
      // if currently on blank line move up first
      if (line_no1) {
        line_no2 = --line_no1;
        le.line_editor_set_command(
            scene.ss_get_script_command(script, line_no1));
        dirty |= D_LIST | D_INPUT;
      }
    } else if (line_no2) {
      line_no2--;
      dirty |= D_LIST | D_INPUT;
    }
  }

  // meta-shift-[: previous script
  else if (match_win_shift(m, k, HID_OPEN_BRACKET)) {
    status = Espresso::E_OK;
    error_msg[0] = 0;
    if (script)
      script = (script_number_t)(script - 1);
    else
      script =
          static_cast<script_number_t>(SCRIPT_COUNT - 2); // due to TEMP_SCRIPT
    if (line_no1 > scene.ss_get_script_len(script))
      line_no1 = scene.ss_get_script_len(script);
    le.line_editor_set_command(
        scene.ss_get_script_command(script, line_no1));
    line_no2 = line_no1;
    dirty |= D_LIST | D_INPUT;
    undo_count = 0;
  }
  // meta-shift-]: next script
  else if (match_win_shift(m, k, HID_CLOSE_BRACKET)) {
    status = Espresso::E_OK;
    error_msg[0] = 0;
    script = (script_number_t)(script + 1);
    if (script >= SCRIPT_COUNT - 1)
      script = (script_number_t)0; // due to TEMP_SCRIPT
    if (line_no1 > scene.ss_get_script_len(script))
      line_no1 = scene.ss_get_script_len(script);
    le.line_editor_set_command(
        scene.ss_get_script_command(script, line_no1));
    line_no2 = line_no1;
    dirty |= D_LIST | D_INPUT;
    undo_count = 0;
  }

  // alt-<down>: move selected lines down
  else if (match_alt(m, k, HID_DOWN)) {
    uint8_t l1 = min(line_no1, line_no2);
    uint8_t l2 = max(line_no1, line_no2);
    if (l2 < SCRIPT_MAX_COMMANDS - 1 &&
        l2 < max<uint8_t>(1, scene.ss_get_script_len(script)) - 1) {
      save_undo();
      Command temp;
      scene.ss_copy_script_command(&temp, script, l2 + 1);
      scene.ss_delete_script_command(script, l2 + 1);
      scene.ss_insert_script_command(script, l1, &temp);
      line_no1++;
      line_no2++;
      dirty |= D_LIST | D_INPUT;
    }
  }
  // alt-<up>: move selected lines up
  else if (match_alt(m, k, HID_UP)) {
    uint8_t l1 = min(line_no1, line_no2);
    uint8_t l2 = max(line_no1, line_no2);
    if (l1 && l2 < scene.ss_get_script_len(script)) {
      save_undo();
      Command temp;
      scene.ss_copy_script_command(&temp, script, l1 - 1);
      scene.ss_delete_script_command(script, l1 - 1);
      scene.ss_insert_script_command(script, l2, &temp);
      line_no1--;
      line_no2--;
      dirty |= D_LIST | D_INPUT;
    }
  }

  // ctrl-x or alt-x: override line editors cut
  else if (match_ctrl(m, k, HID_X) || match_alt(m, k, HID_X)) {
    if (line_no1 == line_no2) {
      if (line_no1 < scene.ss_get_script_len(script)) {
        save_undo();
        strcpy(le.copy_buffer[0], le.line_editor_get());
        le.copy_buffer_len = 1;
        scene.ss_delete_script_command(script, line_no1);
      }
    } else {
      save_undo();
      uint8_t l1 = min(line_no1, line_no2);
      uint8_t l2 = max(line_no1, line_no2);
      le.copy_buffer_len = 0;
      for (uint8_t l = l1; l <= l2; l++)
        scene.ss_get_script_command(script, l)->print(le.copy_buffer[le.copy_buffer_len++]);
      for (int8_t l = l2; l >= l1; l--)
        scene.ss_delete_script_command(script, l);
    }

    if (line_no1 > scene.ss_get_script_len(script)) {
      line_no1 = scene.ss_get_script_len(script);
    }
    le.line_editor_set_command(
        scene.ss_get_script_command(script, line_no1));
    line_no2 = line_no1;

    dirty |= D_LIST | D_INPUT;
  }
  // ctrl-c or alt-c: override line editors copy for multi selection
  else if (match_ctrl(m, k, HID_C) || match_alt(m, k, HID_C)) {
    if (line_no1 == line_no2) {
      // not a multi line selection, pass it to line editor
      bool processed = le.line_editor_process_keys(k, m, is_held_key);
      if (processed)
        dirty |= D_INPUT;
    } else {
      save_undo();
      le.copy_buffer_len = 0;
      for (uint8_t l = min(line_no1, line_no2); l <= max(line_no1, line_no2);
           l++)
        scene.ss_get_script_command(script, l)->print(le.copy_buffer[le.copy_buffer_len++]);
    }
  }
  // ctrl-v or alt-v: override line editors paste for multi selection
  else if (match_ctrl(m, k, HID_V) || match_alt(m, k, HID_V)) {
    if (le.copy_buffer_len == 0)
      return;

    save_undo();
    uint8_t idx = min(line_no1, line_no2);
    line_no1 = idx;
    Command command;
    command.comment = false;
    for (uint8_t i = 0; i < le.copy_buffer_len; i++) {
      if (parser(le.copy_buffer[i], command, error_msg) != Espresso::E_OK)
        continue;
      if (validate(&command, error_msg) != Espresso::E_OK)
        continue;
      if (command.length == 0)
        continue;

      scene.ss_insert_script_command(script, idx++, &command);
      if (line_no2 < (SCRIPT_MAX_COMMANDS - 1))
        line_no2++;
      if (idx >= SCRIPT_MAX_COMMANDS)
        break;
    }
    line_no2 = idx > line_no1 ? idx - 1 : line_no1;
    le.line_editor_set_command(
        scene.ss_get_script_command(script, line_no1));
    dirty |= D_LIST | D_INPUT;
  }

  // alt-delete: remove selected lines
  else if (match_alt(m, k, HID_DELETE)) {
    uint8_t l1 = min(line_no1, line_no2);
    uint8_t l2 = max(line_no1, line_no2);
    if (l1 < scene.ss_get_script_len(script)) {
      save_undo();
      for (int8_t l = l2; l >= l1; l--)
        scene.ss_delete_script_command(script, l);
      if (line_no1 > scene.ss_get_script_len(script))
        line_no1 = scene.ss_get_script_len(script);
      le.line_editor_set_command(
          scene.ss_get_script_command(script, line_no1));
      line_no2 = line_no1;
      dirty |= D_LIST | D_INPUT;
    }
  }

  // <enter>: enter command
  else if (match_no_mod(m, k, HID_ENTER)) {
    if (line_no1 != line_no2) {
      line_no2 = line_no1;
      le.line_editor_set_command(
          scene.ss_get_script_command(script, line_no1));
      dirty |= D_LIST | D_INPUT;
      return;
    }

    dirty |= D_MESSAGE; // something will happen

    Command command;
    command.comment = false;
    status = parser(le.line_editor_get(), command, error_msg);

    if (status != Espresso::E_OK)
      return; // quit, refreshScreen will display the error message

    status = validate(&command, error_msg);
    if (status != Espresso::E_OK)
      return; // quit, refreshScreen will display the error message

    save_undo();
    if (command.length == 0) { // blank line, delete the command
      scene.ss_delete_script_command(script, line_no1);
      if (line_no1 > scene.ss_get_script_len(script)) {
        line_no1 = scene.ss_get_script_len(script);
      }
    } else {
      scene.ss_overwrite_script_command(script, line_no1, &command);
      if (line_no1 < SCRIPT_MAX_COMMANDS - 1) {
        line_no1++;
      }
    }
    le.line_editor_set_command(
        scene.ss_get_script_command(script, line_no1));
    line_no2 = line_no1;
    dirty |= D_LIST | D_INPUT;
  }
  // shift-<enter>: insert command
  else if (match_shift(m, k, HID_ENTER)) {
    if (line_no1 != line_no2)
      return;

    dirty |= D_MESSAGE; // something will happen

    Command command;
    command.comment = false;
    status = parser(le.line_editor_get(), command, error_msg);

    if (status != Espresso::E_OK)
      return; // quit, refreshScreen will display the error message

    status = validate(&command, error_msg);
    if (status != Espresso::E_OK)
      return; // quit, refreshScreen will display the error message

    save_undo();
    if (command.length > 0) {
      scene.ss_insert_script_command(script, line_no1, &command);
      if (line_no1 < (SCRIPT_MAX_COMMANDS - 1)) {
        line_no1++;
      }
    }

    le.line_editor_set_command(
        scene.ss_get_script_command(script, line_no1));
    line_no2 = line_no1;
    dirty |= D_LIST | D_INPUT;
  }

  // alt-slash comment toggle selected lines
  else if (match_alt(m, k, HID_SLASH)) {
    if (line_no1 >= scene.ss_get_script_len(script))
      return;
    save_undo();
    for (uint8_t l = min(line_no1, line_no2); l <= max(line_no1, line_no2); l++)
      scene.ss_toggle_script_comment(script, l);
    dirty |= D_LIST;
  }

  else { // pass the key though to the line editor
    if (line_no1 != line_no2) {
      line_no2 = line_no1;
      le.line_editor_set_command(
          scene.ss_get_script_command(script, line_no1));
      dirty |= D_LIST | D_INPUT;
      return;
    }

    bool processed = le.line_editor_process_keys(k, m, is_held_key);
    if (processed)
      dirty |= D_INPUT;
  }
}

void Models::EditMode::save_undo(void) {
  if (++undo_count > 3)
    undo_count = 3;
  undo_pos = (undo_pos + 1) % UNDO_DEPTH;
  undo_line_no1[undo_pos] = line_no1;
  undo_line_no2[undo_pos] = line_no2;
  undo_length[undo_pos] = scene.ss_get_script_len(script);
  for (uint8_t l = 0; l < undo_length[undo_pos]; l++) {
    undo_comments[undo_pos][l] = scene.ss_get_script_comment(script, l);
    scene.ss_copy_script_command(&undo_buffer[undo_pos][l], script, l);
  }
}

void Models::EditMode::undo(void) {
  if (undo_count == 0)
    return;
  undo_count--;

  scene.ss_clear_script(script);
  for (uint8_t l = 0; l < undo_length[undo_pos]; l++) {
    scene.ss_insert_script_command(script, l, &undo_buffer[undo_pos][l]);
    scene.ss_set_script_comment(script, l, undo_comments[undo_pos][l]);
  }

  line_no1 = undo_line_no1[undo_pos];
  line_no2 = undo_line_no2[undo_pos];

  undo_pos = (undo_pos + UNDO_DEPTH - 1) % UNDO_DEPTH;

  le.line_editor_set_command(
      scene.ss_get_script_command(script, line_no1));
  dirty = D_ALL;
}
