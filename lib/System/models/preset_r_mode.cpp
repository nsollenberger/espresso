#include <hid_keys.h>
#include <keyboard_helper.h>
#include <teletype.h>
#include <util.h>

#include "models/preset_r_mode.h"
#include "models/mode_manager.h"
#include "scene.h"

using namespace Models;

uint8_t PresetRMode::get_preset() { return preset_last; }

void PresetRMode::activate() {
  offset = 0;
  load_text();
  dirty = true;
}

void PresetRMode::set_preset_last(uint8_t preset) { preset_last = preset; }

void PresetRMode::process_preset_r_preset(uint8_t preset) {
  if (preset != preset_last) {
    preset_select = preset;
    preset_last = preset;
    load_text();
    dirty = true;
  }
}

void PresetRMode::preset_line_down() {
  if (offset < kSceneTextLines - 8) {
    offset++;
    dirty = true;
  }
}

void PresetRMode::preset_line_up() {
  if (offset) {
    offset--;
    dirty = true;
  }
}

void PresetRMode::process_preset_r_load() { do_preset_read(); }

void PresetRMode::processKeypress(uint8_t k, uint8_t m,
                                        bool is_held_key) {
  // <down> or C-n: line down
  if (match_no_mod(m, k, HID_DOWN) || match_ctrl(m, k, HID_N)) {
    preset_line_down();
  }
  // <up> or C-p: line up
  else if (match_no_mod(m, k, HID_UP) || match_ctrl(m, k, HID_P)) {
    preset_line_up();
  }
  // <left> or [: preset down
  else if (match_no_mod(m, k, HID_LEFT) ||
           match_no_mod(m, k, HID_OPEN_BRACKET)) {
    if (preset_select) {
      preset_select--;
      offset = 0;
      load_text();
      dirty = true;
    }
  }
  // <right> or ]: preset up
  else if (match_no_mod(m, k, HID_RIGHT) ||
           match_no_mod(m, k, HID_CLOSE_BRACKET)) {
    if (preset_select < SCENE_SLOTS - 1) {
      preset_select++;
      offset = 0;
      load_text();
      dirty = true;
    }
  }
  // <enter>: load preset
  else if (match_no_mod(m, k, HID_ENTER) && !is_held_key) {
    do_preset_read();
  }
}

void PresetRMode::load_text(void) {
  sceneStore.loadText(preset_select, text_buffer);
}

void PresetRMode::do_preset_read() {
  sceneStore.load(preset_select);
  configStore.flash_update_last_saved_scene(preset_select);
  scene.ss_set_scene(preset_select);

  scene.initializing = true;
  tt.run_script(scene, INIT_SCRIPT);
  scene.initializing = false;

  modeManager->activatePriorMode();
}
