#include "models/adc_inputs.h"

#include "adc_service.h"
#include "models/mode_manager.h"
#include "models/preset_r_mode.h"
#include "models/screensaver.h"
#include "scene.h"
#include "timers.h"

void Models::AdcInputs::pollInputs(void) {
  sampleInputs(false);

  if (param >> 8 != last_knob >> 8) {
    if (screensaver.resetTimer())
      return;
  }

  last_knob = param;

  if (modeManager.mode == M_PRESET_R) {
    uint8_t preset = param >> 6;
    uint8_t deadzone = preset & 1;
    preset >>= 1;
    if (!deadzone || abs(preset - presetRMode.get_preset()) > 1)
      presetRMode.process_preset_r_preset(preset);
  }
}

void Models::AdcInputs::sampleInputs(bool force)  {
  if (!force && timers.getTicks() == last_adc_tick)
    return;
  last_adc_tick = timers.getTicks();
  Adc::sampleInputs(adc);
  scene_state.ss_set_in(adc[0] << 2);
  param = Adc::sampleParam();
  scene_state.ss_set_param(param << 2);
}
