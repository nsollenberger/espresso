#include "models/mode_manager.h"

#include "models/edit_mode.h"
#include "models/help_mode.h"
#include "models/mode.h"
#include "models/preset_r_mode.h"
#include "models/preset_w_mode.h"
#include "models/settings.h"
#include "models/voice_settings.h"

Models::CurrentVoiceMode& Models::ModeManager::getMode(const model_current_voice_mode_t m) {
  auto &mode_mode = getMode(static_cast<model_mode_t>(m));
  auto *current_voice_mode = static_cast<Models::CurrentVoiceMode*>(&mode_mode);
  return *current_voice_mode;
}

Models::Mode& Models::ModeManager::getMode(model_mode_t m) {
  switch (static_cast<model_mode_t>(m)) {
  case M_SETTINGS_1:
    return settings;
  case M_PRESET_W:
    return presetWMode;
  case M_PRESET_R:
    return presetRMode;
  case M_HELP:
    return helpMode;
  }
  switch (static_cast<model_current_voice_mode_t>(m)) {
  case M_EDIT:
    return editMode;
  case M_VOICE_SETTINGS_1:
  default:
    return voiceSettings;
  }
}
