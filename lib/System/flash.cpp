#include "flash.h"
#include "font.h"
#include <arduino.h>
#include <cstdint>
#include <log.h>
#include <portmacro.h>
#include <string.h>

void Flash::prepareFresh(SceneStore &sceneStore, ConfigStore &configStore,
                         Bounce &button, Timers &timers, Screen &screen,
                         IndicatorLights &indicators) {
  if (!configStore.is_flash_fresh())
    return;

  auto &lines = screen.getBuffer();
  char s[36];
  strcpy(s, "SCENES WILL BE OVERWRITTEN!");
  lines[5].fill(eColorBlank);
  Font::string_region_clip(lines[5], s, 0, 0, eColorMedium1, eColorBlank);
  LOG("%s\n", s);

  strcpy(s, "PRESS TO CONFIRM");
  lines[6].fill(eColorBlank);
  Font::string_region_clip(lines[6], s, 0, 0, eColorMedium1, eColorBlank);
  LOG("%s\n", s);

  strcpy(s, "DO NOT PRESS OTHERWISE!");
  lines[7].fill(eColorBlank);
  Font::string_region_clip(lines[7], s, 0, 0, eColorMedium1, eColorBlank);
  LOG("%s\n", s);

  screen.draw();

  int startedAt = timers.getTicks();
  int ledUpdatedAt = startedAt;
  bool confirmed = {false};

  while (timers.getTicks() < static_cast<uint64_t>(startedAt + kFlashPrepareTimeoutMs)) {
    button.update();
    if (button.fell()) {
      confirmed = true;
      break;
    }
    if (timers.getTicks() > static_cast<uint64_t>(ledUpdatedAt + 17)) {
      ledUpdatedAt = timers.getTicks();
      float remaining =
          1 - (float(ledUpdatedAt - startedAt) / kFlashPrepareTimeoutMs);
      indicators.displayCountdown(remaining, 0x000b0f);
    }
  }
  // Clean up after ourselves
  indicators.displayCountdown(0, 0);
  for (int i = 5; i < 8; i++) {
    lines[i].fill(eColorBlank);
  }

  if (confirmed) {
    strcpy(s, "OVERWRITING SCENE MEMORY");
  } else {
    strcpy(s, "SKIPPING...");
  }
  Font::string_region_clip(lines[7], s, 0, 0, eColorMedium1, eColorBlank);
  screen.draw();
  LOG("%s\n", s);

  delay(1000);

  if (!confirmed)
    return;

  sceneStore.clearAll();
  configStore.resetAll();
}
