#include "dac.h"
#include <spibus.h>

void Dac::updateOutputs(uint16_t a0, uint16_t a1, uint16_t a2, uint16_t a3) {
  spi_message_t command_conf = {
    .bytes = 2,
  };
  uint8_t bits[2]{};
  prepareForTx(a0, bits);

  {
    SpiBus spiBus(Spi::dac1_dev);

    // CV 1 + 2 are reversed (ch 1 + 0) on HW rev3
    command_conf.tx_data[0] = bits[0] | 0xB0; // Channel 1, 1x Gain
    command_conf.tx_data[1] = bits[1];
    spiBus.sendMessage(command_conf);

    prepareForTx(a1, bits);
    command_conf.tx_data[0] = bits[0] | 0x30; // Channel 0, 1x Gain
    command_conf.tx_data[1] = bits[1];
    spiBus.sendMessage(command_conf);
  }

  {
    SpiBus spiBus(Spi::dac2_dev);

    prepareForTx(a2, bits);
    command_conf.tx_data[0] = bits[0] | 0x30;
    command_conf.tx_data[1] = bits[1];
    spiBus.sendMessage(command_conf);

    prepareForTx(a3, bits);
    command_conf.tx_data[0] = bits[0] | 0xB0;
    command_conf.tx_data[1] = bits[1];
    spiBus.sendMessage(command_conf);
  }
}