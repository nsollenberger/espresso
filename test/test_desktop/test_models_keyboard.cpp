#include <hid_keys.h>
#include <models/keyboard.h>
#include <stdint.h>
#include <unity.h>

using Models::Keyboard;

namespace TestModelsKeyboard {

const int8_t emptyFrame[8] = {0};
const int8_t aFrame[8] = {0x00, 0x00, HID_A, 0x00, 0x00, 0x00, 0x00, 0x00};
const int8_t bFrame[8] = {0x00, 0x00, HID_B, 0x00, 0x00, 0x00, 0x00, 0x00};
const int8_t abFrame[8] = {0x00, 0x00, HID_A, HID_B, 0x00, 0x00, 0x00, 0x00};
const int8_t caFrame[8] = {0x00, 0x00, HID_C, HID_A, 0x00, 0x00, 0x00, 0x00};

void test_held_keys() {
  Keyboard kbd;

  int8_t changed[8] = {0};

  TEST_ASSERT_EQUAL(kbd.keyDown(HID_A, 0), true);
  TEST_ASSERT_EQUAL(kbd.mod_key, 0);

  // Key is considered "held" after 5 ticks
  TEST_ASSERT_EQUAL(kbd.getHeldKey(), 0);
  TEST_ASSERT_EQUAL(kbd.getHeldKey(), 0);
  TEST_ASSERT_EQUAL(kbd.getHeldKey(), 0);
  TEST_ASSERT_EQUAL(kbd.getHeldKey(), 0);
  TEST_ASSERT_EQUAL(kbd.getHeldKey(), 0);
  TEST_ASSERT_EQUAL(kbd.getHeldKey(), HID_A);

  kbd.keyUp(HID_A, 0);
  TEST_ASSERT_EQUAL(kbd.getHeldKey(), 0);

  TEST_ASSERT_EQUAL(kbd.keyDown(HID_A, 0), true);
  TEST_ASSERT_EQUAL(kbd.keyDown(HID_B, 0), true);
  kbd.getHeldKey();
  kbd.getHeldKey();
  kbd.getHeldKey();
  kbd.getHeldKey();
  kbd.getHeldKey();
  TEST_ASSERT_EQUAL(kbd.getHeldKey(), HID_B);

  kbd.keyUp(HID_B, 0);
  // "Held" count is reset after keyUp
  TEST_ASSERT_EQUAL(kbd.getHeldKey(), 0);
  TEST_ASSERT_EQUAL(kbd.getHeldKey(), 0);
  TEST_ASSERT_EQUAL(kbd.getHeldKey(), 0);
  TEST_ASSERT_EQUAL(kbd.getHeldKey(), 0);
  TEST_ASSERT_EQUAL(kbd.getHeldKey(), 0);
  TEST_ASSERT_EQUAL(kbd.getHeldKey(), HID_A);

  kbd.keyUp(HID_A, 0);
  TEST_ASSERT_EQUAL(kbd.getHeldKey(), 0);
  TEST_ASSERT_EQUAL(kbd.getHeldKey(), 0);
  TEST_ASSERT_EQUAL(kbd.getHeldKey(), 0);
  TEST_ASSERT_EQUAL(kbd.getHeldKey(), 0);
  TEST_ASSERT_EQUAL(kbd.getHeldKey(), 0);
  TEST_ASSERT_EQUAL(kbd.getHeldKey(), 0);
  TEST_ASSERT_EQUAL(kbd.getHeldKey(), 0);
}

int main() {
  UNITY_BEGIN();
  RUN_TEST(test_held_keys);
  UNITY_END();
  return 0;
}

}