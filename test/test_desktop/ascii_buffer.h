#ifndef _ASCII_BUFFER_H__
#define _ASCII_BUFFER_H__

#include <cstdlib>
#include <iostream>
#include <stdint.h>
#include <region.h>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <unity.h>

class AsciiBuffer {
public:
  AsciiBuffer(Region &_reg) : reg(&_reg) {
    output = static_cast<char *>(calloc(reg->h * rowWidth(), sizeof(char)));
  }

  ~AsciiBuffer() { free(output); }

  void setRegion(Region &_reg) {
    reg = &_reg;
  }

  const char *stringVal() {
    char *outputPtr = output;
    for (int row = 0; row < reg->h; row++) {
      *outputPtr = ':';
      outputPtr++;
      for (int col = 0; col < reg->w; col++) {
        const char _val_1 = toAscii(reg->data[col + row * reg->w], 0);
        const char _val_2 = toAscii(reg->data[col + row * reg->w], 1);
        // print each "pixel" two chars wide, for improved ascii-art readability
        *outputPtr = _val_1;
        outputPtr++;
        *outputPtr = _val_2;
        outputPtr++;
      }
      *outputPtr = ':';
      outputPtr++;
      *outputPtr = '\n';
      outputPtr++;
    }
    return output;
  }

  void print() {
    stringVal();
    char *outputPtr = output;
    char *last = output + (reg->h * rowWidth());
    while (outputPtr != last) {
      std::cout << *outputPtr;
      outputPtr++;
    }
  }

private:
  char *output;
  Region *reg;

  const int rowWidth() {
    // two chars per pixel, plus extra chars at beginning and end
    return (2 * reg->w) + 3;
  }

  const char toAscii(const uint8_t data, int column) {
    switch (data) {
    case eColorLight1:
      return column ? ' ' : '.';
    case eColorLight2:
      return '.';
    case eColorMedium1:
      return column ? '-' : '-';
    case eColorMedium2:
      return column ? '-' : 'x';
    case eColorDark1:
      return column ? 'x' : 'X';
    case eColorDark2:
      return 'X';
    default:
    case eColorBlank:
      return ' ';
    }
  }
};


#define SNAPSHOT(filename) (static_cast<std::string>("./test/test_desktop/snapshots/") + filename).c_str()

class AsciiFileSnapshot {
public:
  AsciiFileSnapshot(screen_buffer_t &_buf) : buf(_buf) {}

  bool save(const char *filename) {
    std::ofstream out;
    out.open(SNAPSHOT(filename));
    AsciiBuffer asciiBuf(buf[0]);

    for (auto &reg : buf) {
      asciiBuf.setRegion(reg);
      out << asciiBuf.stringVal();
    }
    out.close();
    return true;
  }

  bool match(const char *filename) {
    std::ifstream in;
    in.open(SNAPSHOT(filename));
    AsciiBuffer asciiBuf(buf[0]);
    std::string line;
    std::string contents;

    for (auto &reg : buf) {
      contents.clear();
      for (int ri = 0; ri < 8; ri++) {
        getline(in, line);
        contents += line + "\n";
      }

      asciiBuf.setRegion(reg);
      TEST_ASSERT_EQUAL_STRING(asciiBuf.stringVal(), contents.c_str());
    }
    in.close();
    return true;
  }

private:
  screen_buffer_t &buf;
};

#endif
