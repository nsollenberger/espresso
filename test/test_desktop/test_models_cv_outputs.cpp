#include <stdint.h>
#include <unity.h>
#include <vector>

#include <models/cv_outputs.h>

using Models::CvOutputs;

namespace TestModelsCvOutputs {

class TMock {};

struct CvState {
  uint16_t a0;
  uint16_t a1;
  uint16_t a2;
  uint16_t a3;
};

std::vector<CvState> cv_state;

class IMock {
public:
  void setCV(uint16_t a0, uint16_t a1, uint16_t a2, uint16_t a3) {
    cv_state.push_back({a0, a1, a2, a3});
  }
};

void test_set_value() {
  cv_state.clear();
  device_config_t device_config{0};
  TMock t;
  IMock i;
  CvOutputs<TMock, IMock> cvOutputs(t, device_config, i);

  cvOutputs.setValue(0, 100, 0);
  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 1);
  // Output is 12bit (>> 2)
  TEST_ASSERT_EQUAL(cv_state[0].a0, 25);
  TEST_ASSERT_EQUAL(cv_state[0].a1, 0);
  TEST_ASSERT_EQUAL(cv_state[0].a2, 0);
  TEST_ASSERT_EQUAL(cv_state[0].a3, 0);
}

void test_set_value_slewed() {
  cv_state.clear();
  device_config_t device_config{0};
  TMock t;
  IMock i;
  CvOutputs<TMock, IMock> cvOutputs(t, device_config, i);

  cvOutputs.setSlewDuration(0, RATE_CV * 4);

  cvOutputs.setValue(0, 400, 1);
  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 1);
  TEST_ASSERT_EQUAL(cv_state[0].a0, 25);

  cv_state.clear();
  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 1);
  TEST_ASSERT_EQUAL(cv_state[0].a0, 50);

  cv_state.clear();
  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 1);
  TEST_ASSERT_EQUAL(cv_state[0].a0, 75);

  cv_state.clear();
  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 1);
  TEST_ASSERT_EQUAL(cv_state[0].a0, 100);

  cv_state.clear();
  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 0);
}

void test_set_value_offset() {
  cv_state.clear();
  device_config_t device_config{0};
  TMock t;
  IMock i;
  CvOutputs<TMock, IMock> cvOutputs(t, device_config, i);

  cvOutputs.setOffset(0, 400);
  cvOutputs.setValue(0, 100, 0);
  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 1);
  TEST_ASSERT_EQUAL(cv_state[0].a0, 125);
}

void test_envelope() {
  cv_state.clear();
  device_config_t device_config{0};
  TMock t;
  IMock i;
  CvOutputs<TMock, IMock> cvOutputs(t, device_config, i);

  cvOutputs.enableEnvelope(0, RATE_CV * 4, RATE_CV * 2, 0.5 * 16383, RATE_CV * 8);

  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 0);

  cvOutputs.envelopeGate(0, 1);
  // Attack
  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 1);
  TEST_ASSERT_EQUAL(cv_state[0].a0, 1023);

  cv_state.clear();
  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 1);
  TEST_ASSERT_EQUAL(cv_state[0].a0, 2047);

  cv_state.clear();
  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 1);
  TEST_ASSERT_EQUAL(cv_state[0].a0, 3071);

  cv_state.clear();
  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 1);
  TEST_ASSERT_EQUAL(cv_state[0].a0, 4095);

  // Decay
  cv_state.clear();
  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 1);
  TEST_ASSERT_EQUAL(cv_state[0].a0, 3071);

  cv_state.clear();
  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 1);
  TEST_ASSERT_EQUAL(cv_state[0].a0, 2047);

  // Sustain
  cv_state.clear();
  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 0);

  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 0);

  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 0);

  cvOutputs.envelopeGate(0, 0);
  // Release
  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 1);
  TEST_ASSERT_EQUAL(cv_state[0].a0, 1791);

  cv_state.clear();
  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 1);
  TEST_ASSERT_EQUAL(cv_state[0].a0, 1535);

  cv_state.clear();
  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 1);
  TEST_ASSERT_EQUAL(cv_state[0].a0, 1279);

  cv_state.clear();
  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 1);
  TEST_ASSERT_EQUAL(cv_state[0].a0, 1023);

  cv_state.clear();
  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 1);
  TEST_ASSERT_EQUAL(cv_state[0].a0, 767);

  cv_state.clear();
  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 1);
  TEST_ASSERT_EQUAL(cv_state[0].a0, 511);

  cv_state.clear();
  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 1);
  TEST_ASSERT_EQUAL(cv_state[0].a0, 255);

  cv_state.clear();
  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 1);
  TEST_ASSERT_EQUAL(cv_state[0].a0, 0);

  // Idle
  cv_state.clear();
  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 0);

  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 0);

  cvOutputs.refresh();
  TEST_ASSERT_EQUAL(cv_state.size(), 0);
}

int main() {
  UNITY_BEGIN();
  RUN_TEST(test_set_value);
  RUN_TEST(test_set_value_slewed);
  RUN_TEST(test_set_value_offset);
  RUN_TEST(test_envelope);
  UNITY_END();
  return 0;
}

} // namespace TestModelsCvOutputs
