#include <unity.h>

namespace TestClock {
int main();
}

namespace TestFont {
int main();
}

namespace TestModelsCvOutputs {
int main();
}

namespace TestModelsKeyboard {
int main();
}

namespace TestModelsVoiceSettings {
int main();
}

namespace TestSequence {
int main();
}

namespace TestTeletypeOps {
int main();
}

namespace TestTeletypePrintCommand {
int main();
}

namespace TestEspressoParser {
int main();
}

namespace TestEspressoValidator {
int main();
}

namespace TestTimers {
int main();
}

namespace TestVoice {
int main();
}

namespace TestViews {
int main();
}

int main() {
  TestClock::main();
  TestFont::main();
  TestModelsCvOutputs::main();
  TestModelsKeyboard::main();
  TestModelsVoiceSettings::main();
  TestSequence::main();
  TestTeletypeOps::main();
  TestTeletypePrintCommand::main();
  TestEspressoParser::main();
  TestEspressoValidator::main();
  TestTimers::main();
  TestVoice::main();
  TestViews::main();
  return 0;
}
