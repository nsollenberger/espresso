#include <unity.h>

#include <espresso/command.h>
#include <sequence.h>
#include <temporal.h>
#include <random.h>

namespace TestVoice {
Espresso::Token generate_seq_data_basic(Sequence::NoteName note, int duration,
                                    Sequence::DurationUnit unit);
}

namespace TestSequence {

using TestVoice::generate_seq_data_basic;

void test_seq_probability() {
  Sequence seq1;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurMs));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteC, 16, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteD, 12, Sequence::DurSec));


  seq1.applyMod(Sequence::ModProbability, 50);
  Port::rand_val = 60;
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->silent, true);

  // re-apply mod to bust cache (since we're using a static Port mock)
  seq1.resetMods();
  seq1.applyMod(Sequence::ModProbability, 50);
  Port::rand_val = 40;
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->silent, false);

  seq1.resetMods();
  seq1.applyMod(Sequence::ModProbability, 50);
  Port::rand_val = 2;
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->silent, false);

  seq1.resetMods();
  seq1.applyMod(Sequence::ModProbability, 50);
  Port::rand_val = 93;
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->silent, true);
}

/*
  N(C >G <B E)
  QNT 3 -13
  // alternatively, specify scale separately
  SCALE -13
  QNT 3
*/
void test_seq_quantization() {
  Sequence seq1;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteAb, 4, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteBb, 8, Sequence::DurMs));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteC, 16, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteDb, 12, Sequence::DurSec));

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteAb);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->duration, 4);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteBb);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->duration, 8);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->duration, 16);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteDb);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->duration, 12);

  seq1.applyMod(Sequence::ModQuantize, 10);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->duration, 4);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteBb);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->duration, 8);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->duration, 16);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->duration, 12);
}

void test_seq_quant_adv() {
  Sequence seq1;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteAb, 4, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteBb, 8, Sequence::DurMs));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteC, 16, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteDb, 12, Sequence::DurSec));

  seq1.applyMod(Sequence::ModTranspose, -5);
  seq1.applyMod(Sequence::ModQuantize, 2);
  // Ab -5 semitones == Eb -> E
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteE);
  // Bb -> F -> Gb
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteGb);
  // C -> G
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteG);
  // Db -> A
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteA);
}

void test_seq_slew() {
  Sequence seq1;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteAb, 4, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteBb, 8, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteC, 16, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteDb, 0, Sequence::DurRel));

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteAb);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->slew_up, 0);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->slew_dn, 0);

  seq1.applyMod(Sequence::ModSlewDown, 80);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteBb);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->slew_up, 0);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->slew_dn, 80);

  seq1.applyMod(Sequence::ModSlewUp, 90);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->slew_up, 90);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->slew_dn, 80);

  seq1.applyMod(Sequence::ModSlewDown, 0);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteDb);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->slew_up, 90);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->slew_dn, 0);
}

void test_seq_repeats() {
  Sequence seq1;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
  auto dat1 = generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurMs);
  Sequence::packNoteRepeats(dat1.value, 3);
  seq1.unpackData(dat1);
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteC, 16, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteD, 12, Sequence::DurSec));

  TEST_ASSERT_EQUAL(seq1.length(), 6);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->duration, 4);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->duration, 8);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->duration, 8);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->duration, 8);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(4)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(4)->duration, 16);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(5)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(5)->duration, 12);
}

void test_seq_repeats_adv() {
  Sequence seq1;

  auto dat1 = generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurMs);
  Sequence::packNoteRepeats(dat1.value, 2);
  seq1.unpackData(dat1);

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));

  auto dat2 = generate_seq_data_basic(Sequence::NoteC, 16, Sequence::DurRel);
  Sequence::packNoteRepeats(dat2.value, 4);
  seq1.unpackData(dat2);

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteD, 12, Sequence::DurSec));

  TEST_ASSERT_EQUAL(seq1.length(), 8);

  seq1.applyMod(Sequence::ModNRotate, 1);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->duration, 8);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->duration, 8);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->duration, 4);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->duration, 16);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(4)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(4)->duration, 16);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(5)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(5)->duration, 16);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(6)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(6)->duration, 16);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(7)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(7)->duration, 12);
}

void test_seq_rests() {
  Sequence seq1;

  // Janky fixture: simulate uninitialized object
  seq1.steps[0].octave_offset[0] = -65;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteRest, 0, Sequence::DurRel));

  TEST_ASSERT_EQUAL(seq1.length(), 1);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteRest);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->octave_offset[0], 0);
}

void test_seq_rev() {
  Sequence seq1;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurMs));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteC, 16, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteD, 12, Sequence::DurSec));

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->duration, 4);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->duration, 8);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->duration, 16);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->duration, 12);

  seq1.applyMod(Sequence::ModReverse);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->duration, 12);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->duration, 16);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->duration, 8);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->duration, 4);
}

void test_seq_shuf() {
  Sequence seq1;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurMs));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteC, 16, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteD, 12, Sequence::DurSec));

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->duration, 4);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->duration, 8);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->duration, 16);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->duration, 12);

  seq1.applyMod(Sequence::ModShuffle);
  Port::rand_val = 1;
  // A, B, C, D swap(0,1)
  // B, A, C, D swap(2,1)
  // B, C, A, D swap(3,1)
  // B, D, A, C 
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteC);
}

void test_seq_rot() {
  Sequence seq1;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurMs));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteC, 16, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteD, 12, Sequence::DurSec));

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->duration, 4);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->duration, 8);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->duration, 16);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->duration, 12);

  seq1.applyMod(Sequence::ModRotate, 5);
  // B, C, D, A
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->duration, 8);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->duration, 16);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->duration, 12);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->duration, 4);

  // Can apply mod multiple times
  seq1.applyMod(Sequence::ModRotate, 2);
  // D, A, B, C
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->duration, 12);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->duration, 4);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->duration, 8);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->duration, 16);
}

void test_seq_nrot() {
  Sequence seq1;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurMs));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteC, 16, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteD, 12, Sequence::DurSec));

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->duration, 4);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->duration, 8);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->duration, 16);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->duration, 12);

  seq1.applyMod(Sequence::ModNRotate, 5);
  // B, C, D, A
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->duration, 4);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->duration, 8);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->duration, 16);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->duration, 12);

  // Can apply mod multiple times
  seq1.applyMod(Sequence::ModNRotate, 2);
  // D, A, B, C
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->duration, 4);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->duration, 8);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->duration, 16);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->duration, 12);
}

void test_seq_fast() {
  Sequence seq1;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteC, 80, Sequence::DurMs));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteD, 2, Sequence::DurSec));

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->duration, 4);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->duration_denom, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->duration, 8);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->duration_denom, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->duration, 80);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->duration_denom, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->duration, 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->duration_denom, 1);

  seq1.applyMod(Sequence::ModFast, 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->duration, 8);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->duration_denom, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->duration, 16);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->duration_denom, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->duration, 80);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->duration_denom, 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->duration, 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->duration_denom, 2);
}

void test_seq_slow() {
  Sequence seq1;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteC, 80, Sequence::DurMs));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteD, 2, Sequence::DurSec));

  seq1.applyMod(Sequence::ModSlow, 3);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->duration, 4);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->duration_denom, 3);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->duration, 8);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->duration_denom, 3);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->duration, 240);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->duration_denom, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->duration, 6);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->duration_denom, 1);
}

void test_seq_trunc() {
  Sequence seq1;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteC, 80, Sequence::DurMs));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteD, 2, Sequence::DurSec));

  seq1.applyMod(Sequence::ModTruncate, 3);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->silent, false);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->silent, false);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->silent, false);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->silent, true);

  seq1.applyMod(Sequence::ModTruncate, -3);
  
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->silent, true);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->silent, false);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->silent, false);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->silent, true);
}

void test_seq_flip() {
  Sequence seq1;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteD, 80, Sequence::DurMs));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteC, 2, Sequence::DurSec));

  seq1.applyMod(Sequence::ModFlip);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteB);

  seq1.applyMod(Sequence::ModTruncate, 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->silent, false);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->silent, false);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->silent, true);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->silent, true);

  seq1.applyMod(Sequence::ModFlip);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteB);
}

void test_seq_stut() {
  Sequence seq1;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteD, 80, Sequence::DurMs));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteC, 2, Sequence::DurSec));

  TEST_ASSERT_EQUAL(seq1.length(), 4);
  seq1.applyMod(Sequence::ModStutter, 2);
  TEST_ASSERT_EQUAL(seq1.length(), 8);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->repeats, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->repeats, 1);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->repeats, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->repeats, 1);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(4)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(4)->repeats, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(5)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(5)->repeats, 1);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(6)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(6)->repeats, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(7)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(7)->repeats, 1);

  seq1.applyMod(Sequence::ModTruncate, 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->silent, false);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->silent, false);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->silent, true);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->silent, true);

  seq1.applyMod(Sequence::ModStutter, 3);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->repeats, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->repeats, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->repeats, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->repeats, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(4)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(4)->repeats, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(5)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(5)->repeats, 1);

  // stutter is not applied to silent notes (meh)
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(6)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(6)->repeats, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(7)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(7)->repeats, 1);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(8)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(8)->repeats, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(9)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(9)->repeats, 1);
}

void test_seq_frac() {
  Sequence seq1;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteD, 80, Sequence::DurMs));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteC, 2, Sequence::DurSec));

  seq1.applyMod(Sequence::ModFrac, 3);
  Port::rand_val = 2;

  TEST_ASSERT_EQUAL(seq1.length(), 8);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->repeats, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->repeats, 1);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->repeats, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->repeats, 1);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(4)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(4)->repeats, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(5)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(5)->repeats, 1);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(6)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(6)->repeats, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(7)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(7)->repeats, 1);
}

void test_seq_harm() {
  Sequence seq1;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteD, 80, Sequence::DurMs));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteC, 2, Sequence::DurSec));

  seq1.applyMod(Sequence::ModHarmonize, 3);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->note_count, 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->note_count, 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->note_count, 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->note_count, 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[1], Sequence::NoteA + 3);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[1], Sequence::NoteB + 3);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[1], Sequence::NoteD + 3);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[1], Sequence::NoteC + 3);

  seq1.applyMod(Sequence::ModTruncate, 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->silent, false);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->silent, false);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->silent, true);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->silent, true);

  seq1.applyMod(Sequence::ModHarmonize, 5);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->note_count, 3);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->note_count, 3);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->note_count, 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->note_count, 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[1], Sequence::NoteA + 3);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[2], Sequence::NoteA + 5);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[1], Sequence::NoteB + 3);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[2], Sequence::NoteB + 5);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[1], Sequence::NoteD + 3);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[1], Sequence::NoteC + 3);
}

void test_seq_harm_adv() {
  Sequence seq1;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteD, 80, Sequence::DurMs));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteC, 2, Sequence::DurSec));

  seq1.applyMod(Sequence::ModHarmonize, -2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->note_count, 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->note_count, 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->note_count, 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->note_count, 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[1], Sequence::NoteA - 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[1], Sequence::NoteB - 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[1], Sequence::NoteD - 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[1], Sequence::NoteC - 2);

  seq1.applyMod(Sequence::ModTruncate, 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->silent, false);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->silent, false);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->silent, true);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->silent, true);

  seq1.applyMod(Sequence::ModHarmonize, 3);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->note_count, 3);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->note_count, 3);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->note_count, 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->note_count, 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[1], Sequence::NoteA - 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[2], Sequence::NoteA + 3);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[1], Sequence::NoteB - 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[2], Sequence::NoteB + 3);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[1], Sequence::NoteD - 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[1], Sequence::NoteC - 2);

  seq1.applyMod(Sequence::ModHarmonize, 12);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->note_count, 4);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->note_count, 4);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->note_count, 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->note_count, 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[1], Sequence::NoteA - 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[2], Sequence::NoteA + 3);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[3], Sequence::NoteA + 12);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[1], Sequence::NoteB - 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[2], Sequence::NoteB + 3);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[3], Sequence::NoteB + 12);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[1], Sequence::NoteD - 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[1], Sequence::NoteC - 2);

  seq1.applyMod(Sequence::ModProbability, 100);
  seq1.applyMod(Sequence::ModHarmonize, 5);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->note_count, 4);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->note_count, 4);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->note_count, 3);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->note_count, 3);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[1], Sequence::NoteA - 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[2], Sequence::NoteA + 3);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[3], Sequence::NoteA + 12);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[1], Sequence::NoteB - 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[2], Sequence::NoteB + 3);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[3], Sequence::NoteB + 12);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[1], Sequence::NoteD - 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[2], Sequence::NoteD + 5);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[1], Sequence::NoteC - 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[2], Sequence::NoteC + 5);
}

void test_seq_loop() {
  Sequence seq1;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel));
  seq1.unpackData(
    generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel));
  seq1.unpackData(
    generate_seq_data_basic(Sequence::NoteC, 16, Sequence::DurRel));
  seq1.unpackData(
    generate_seq_data_basic(Sequence::NoteD, 0, Sequence::DurRel));
  seq1.unpackData(
    generate_seq_data_basic(Sequence::NoteE, 4, Sequence::DurRel));
  seq1.unpackData(
    generate_seq_data_basic(Sequence::NoteF, 0, Sequence::DurRel));

  seq1.applyMod(Sequence::ModLoop, 4);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->duration, 4);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->duration, 8);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->duration, 16);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->duration, 16);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(4)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(4)->duration, 4);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(5)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(5)->duration, 8);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(6)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(6)->duration, 16);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(7)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(7)->duration, 16);
}

void test_seq_loop_adv() {
  Sequence seq1;
  Temporal::setBeatInterval(1000);

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel));
  seq1.unpackData(
    generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel));
  seq1.unpackData(
    generate_seq_data_basic(Sequence::NoteC, 16, Sequence::DurRel));
  seq1.unpackData(
    generate_seq_data_basic(Sequence::NoteD, 0, Sequence::DurRel));
  seq1.unpackData(
    generate_seq_data_basic(Sequence::NoteE, 4, Sequence::DurRel));
  seq1.unpackData(
    generate_seq_data_basic(Sequence::NoteF, 0, Sequence::DurRel));

  seq1.applyMod(Sequence::ModLoop, 3);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->duration, 4);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->duration, 8);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->duration, 16);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->duration, 4);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(4)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(4)->duration, 8);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(5)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(5)->duration, 16);
  // 8th note left, but NoteA was a quarter note
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(6)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(6)->duration, 500);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(6)->duration_unit, Sequence::DurMs);
}

void test_seq_loop_adv2() {
  Sequence seq1;
  Temporal::setBeatInterval(500);

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel));
  seq1.unpackData(
    generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel));
  seq1.unpackData(
    generate_seq_data_basic(Sequence::NoteC, 240, Sequence::DurMs));
  // Loop duration = 500 + 250 + 240 = 990
  seq1.unpackData(
    generate_seq_data_basic(Sequence::NoteD, 108, Sequence::DurMs));
  seq1.unpackData(
    generate_seq_data_basic(Sequence::NoteE, 400, Sequence::DurMs));
  seq1.unpackData(
    generate_seq_data_basic(Sequence::NoteF, 500, Sequence::DurMs));
  // Sequence duration = 500 + 250 + 240 + 108 + 400 + 500 = 1998

  seq1.applyMod(Sequence::ModLoop, 3);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->duration, 4);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->duration, 8);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->duration, 240);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->duration, 4);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(4)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(4)->duration, 8);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(5)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(5)->duration, 240);
  // below MIN_NOTE_LENGTH_MS threshold is added as a rest instead
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(6)->notes[0], Sequence::NoteRest);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(6)->duration, 18);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(6)->duration_unit, Sequence::DurMs);
}

void test_seq_transp() {
  Sequence seq1;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel));
  auto data1 = generate_seq_data_basic(Sequence::NoteD, 80, Sequence::DurMs);
  Sequence::packNoteOctave(data1.data[0], 1);
  seq1.unpackData(data1);
  auto data2 = generate_seq_data_basic(Sequence::NoteC, 2, Sequence::DurRel);
  Sequence::packNoteOctave(data2.data[0], -1);
  seq1.unpackData(data2);

  seq1.applyMod(Sequence::ModTranspose, 3);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->note_count, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->note_count, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->note_count, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->note_count, 1);
  // A + 3 semitones = C
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteC);
  // Should have rolled in to the next octave
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->octave_offset[0], 1);
  // B -> D
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->octave_offset[0], 1);
  // D -> F
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteF);
  // This note did not roll in to the next octave, but had an octave offset to begin with
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->octave_offset[0], 1);
  // C -> Eb
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteEb);
  // This note did not roll in to the next octave either, also had an offset 
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->octave_offset[0], 0);

  seq1.applyMod(Sequence::ModTruncate, 2);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->silent, false);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->silent, false);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->silent, true);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->silent, true);

  seq1.applyMod(Sequence::ModTranspose, 5);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->note_count, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->note_count, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->note_count, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->note_count, 1);
  // C + 5 = F
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteF);
  // D -> G
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteG);
  // Silent notes are unchanged
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteF);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteEb);
}

void test_multiple_seqs_ok() {
  Sequence seq1;
  Sequence seq2;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel));
  seq1.unpackData(
    generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel));
  seq1.unpackData(
    generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel));
  seq1.unpackData(
    generate_seq_data_basic(Sequence::NoteD, 0, Sequence::DurRel));

  seq2.unpackData(
      generate_seq_data_basic(Sequence::NoteAb, 0, Sequence::DurRel));
  seq2.unpackData(
    generate_seq_data_basic(Sequence::NoteBb, 0, Sequence::DurRel));
  seq2.unpackData(
    generate_seq_data_basic(Sequence::NoteDb, 0, Sequence::DurRel));

  TEST_ASSERT_EQUAL(seq1.length(), 4);
  TEST_ASSERT_EQUAL(seq2.length(), 3);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteD);

  TEST_ASSERT_EQUAL(seq2.getNotesAtStep(0)->notes[0], Sequence::NoteAb);
  TEST_ASSERT_EQUAL(seq2.getNotesAtStep(1)->notes[0], Sequence::NoteBb);
  TEST_ASSERT_EQUAL(seq2.getNotesAtStep(2)->notes[0], Sequence::NoteDb);
}

void test_sequenced_mod() {
  Sequence seq1;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteD, 80, Sequence::DurMs));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteC, 2, Sequence::DurSec));

  TEST_ASSERT_EQUAL(seq1.length(), 4);

  int16_t values[4] = { 1, 2, 0, 3 };

  seq1.applyModSeq(Sequence::ModStutter, values, 4);

  TEST_ASSERT_EQUAL(seq1.length(), 7);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->repeats, 1);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->repeats, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->repeats, 1);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->repeats, 1);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(4)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(4)->repeats, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(5)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(5)->repeats, 1);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(6)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(6)->repeats, 1);
}

void test_sequenced_mod2() {
  Sequence seq1;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteD, 80, Sequence::DurMs));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteC, 2, Sequence::DurSec));

  TEST_ASSERT_EQUAL(seq1.length(), 4);

  int16_t values[4] = { 50, 0, 90 };

  seq1.applyModSeq(Sequence::ModSlew, values, 3);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->slew_up, 50);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(0)->slew_dn, 50);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->slew_up, 0);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(1)->slew_dn, 0);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->slew_up, 90);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(2)->slew_dn, 90);

  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->slew_up, 50);
  TEST_ASSERT_EQUAL(seq1.getNotesAtStep(3)->slew_dn, 50);
}

int main() {
  UNITY_BEGIN();
  RUN_TEST(test_seq_probability);
  RUN_TEST(test_seq_quantization);
  RUN_TEST(test_seq_quant_adv);
  RUN_TEST(test_seq_slew);
  RUN_TEST(test_seq_repeats);
  RUN_TEST(test_seq_repeats_adv);
  RUN_TEST(test_seq_rests);
  RUN_TEST(test_seq_rev);
  RUN_TEST(test_seq_shuf);
  RUN_TEST(test_seq_rot);
  RUN_TEST(test_seq_nrot);
  RUN_TEST(test_seq_fast);
  RUN_TEST(test_seq_slow);
  RUN_TEST(test_seq_trunc);
  RUN_TEST(test_seq_flip);
  RUN_TEST(test_seq_stut);
  RUN_TEST(test_seq_frac);
  RUN_TEST(test_seq_harm);
  RUN_TEST(test_seq_harm_adv);
  RUN_TEST(test_seq_loop);
  RUN_TEST(test_seq_loop_adv);
  RUN_TEST(test_seq_loop_adv2);
  RUN_TEST(test_seq_transp);
  RUN_TEST(test_multiple_seqs_ok);
  RUN_TEST(test_sequenced_mod);
  RUN_TEST(test_sequenced_mod2);
  UNITY_END();
  return 0;
}

} // namespace TestSequence
