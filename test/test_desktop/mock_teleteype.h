#ifndef _MOCK_TELETYPE_H__
#define _MOCK_TELETYPE_H__

#include <functional>
#include <vector>

#include <espresso/parser.h>
#include <scene.h>
#include <voice.h>
#include <types.h>

class Voice;

class MockTeletype {
public:
  MockTeletype() : voices{
    {voiceSettings, TT_SCRIPT_1}, {voiceSettings, TT_SCRIPT_2}, {voiceSettings, TT_SCRIPT_3}, {voiceSettings, TT_SCRIPT_4},
    {voiceSettings, TT_SCRIPT_5}, {voiceSettings, TT_SCRIPT_6}, {voiceSettings, TT_SCRIPT_7}, {voiceSettings, TT_SCRIPT_8}
  }, scene{voices} {};

  Models::VoiceSettings voiceSettings;
  Voice voices[VOICE_COUNT];
  Scene scene;

  std::vector<script_number_t> scriptEvents;
  std::function<void(script_number_t)> cb;

  process_result_t run_script(Scene &state, script_number_t script_no) {
    if (script_no != TEMP_SCRIPT) {
      scriptEvents.push_back(script_no);
    }
    if (cb) cb(script_no);
    return {0};
  }
};

#endif
