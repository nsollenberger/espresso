#include <unity.h>
#include <vector>

#include <espresso/command.h>
#include <sequence.h>
#include <table.h>
#include <types.h>
#include <voice.h>

#include "teletype_stubs.h"
#include "test_helpers.h"
#include "mock_teleteype.h"

namespace TestVoice {

Espresso::Token generate_seq_data_basic(Sequence::NoteName note, int duration,
                                    Sequence::DurationUnit unit) {
  Espresso::Token data = {.tag = Espresso::ALDA_NOTE, .value = 0, .data = {0, 0, 0, 0}};
  Sequence::packNoteName(data.data[0], note);
  Sequence::packNoteDuration(data.value, duration, unit);
  return data;
}

void resetGateState() {
  tr_state[0] = 0;
  tr_state[1] = 0;
  tr_state[2] = 0;
  tr_state[3] = 0;
}

void resetCVState() {
  cv_state[0] = 0;
  cv_state[1] = 0;
  cv_state[2] = 0;
  cv_state[3] = 0;
}

struct VoiceFixture {
  MockTeletype *tt;
  Voice *v1;
  Sequence *seq1;
} fixture;

#define RUN_TEST_WITH_SETUP(fn) runWithSetup(fn, #fn, __LINE__)

void runWithSetup(void test(void), const char* name, int line) {
  resetGateState();
  resetCVState();
  midiEvents.clear();
  cvEvents.clear();
  MockTeletype tt;
  Voice v1(tt.voiceSettings, TT_SCRIPT_1);

  fixture.tt = &tt;
  fixture.v1 = &v1;
  fixture.seq1 = &v1.seq;

  UnityDefaultTestRun(test, name, line);
}

void test_voice_note_on_off_empty_seq() {
  auto &tt = *fixture.tt;
  auto &v1 = *fixture.v1;

  tt.voiceSettings.configs[TT_SCRIPT_1].midi_out_ch = 10;
  tt.voiceSettings.configs[TT_SCRIPT_1].gate_out = 1;

  v1.notesOn(0);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  TEST_ASSERT_EQUAL(tr_state[0], 0);
  TEST_ASSERT_EQUAL(tr_state[1], 0);
  TEST_ASSERT_EQUAL(tr_state[2], 0);
  TEST_ASSERT_EQUAL(tr_state[3], 0);

  v1.notesOn(1);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  TEST_ASSERT_EQUAL(tr_state[0], 0);
  TEST_ASSERT_EQUAL(tr_state[1], 0);
  TEST_ASSERT_EQUAL(tr_state[2], 0);
  TEST_ASSERT_EQUAL(tr_state[3], 0);

  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  TEST_ASSERT_EQUAL(tr_state[0], 0);
  TEST_ASSERT_EQUAL(tr_state[1], 0);
  TEST_ASSERT_EQUAL(tr_state[2], 0);
  TEST_ASSERT_EQUAL(tr_state[3], 0);

  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  TEST_ASSERT_EQUAL(tr_state[0], 0);
  TEST_ASSERT_EQUAL(tr_state[1], 0);
  TEST_ASSERT_EQUAL(tr_state[2], 0);
  TEST_ASSERT_EQUAL(tr_state[3], 0);
}

void test_voice_note_on_off_basic_looping() {
  auto &tt = *fixture.tt;
  auto &v1 = *fixture.v1;
  auto &seq1 = *fixture.seq1;

  tt.voiceSettings.configs[TT_SCRIPT_1].midi_out_ch = 10;

  tt.cb = [&](script_number_t n) {
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel));
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteC, 16, Sequence::DurRel));
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteD, 12, Sequence::DurRel));
  };
  tt.cb(TEMP_SCRIPT);

  v1.notesOn(0);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  TEST_ASSERT_EQUAL(midiEvents[0].ch, 10);
  midiEvents.clear();

  // Voice does not prevent duplicate NoteOns
  v1.notesOn(0);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  TEST_ASSERT_EQUAL(midiEvents[0].ch, 10);
  midiEvents.clear();

  // Voice does not care if notes are skipped
  // however, Voice will not allow more than 2 "groups" of notes
  // to be playing at once
  v1.notesOn(3);
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  TEST_ASSERT_NOTE_ON(1, Sequence::NoteD);
  TEST_ASSERT_EQUAL(midiEvents[0].ch, 10);
  TEST_ASSERT_EQUAL(midiEvents[1].ch, 10);
  midiEvents.clear();

  // Oldest notes will be turned off first
  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  TEST_ASSERT_EQUAL(midiEvents[0].ch, 10);
  midiEvents.clear();

  // nextSequence refreshes the sequence
  tt.run_script(tt.scene, v1.nextSequence());
  v1.notesOn(0);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  TEST_ASSERT_EQUAL(midiEvents[0].ch, 10);
  midiEvents.clear();

  // After looping, previous NoteOns are still tracked
  // and get turned off appropriately
  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteD);
  TEST_ASSERT_EQUAL(midiEvents[0].ch, 10);
  midiEvents.clear();
}

void test_voice_note_on_off_basic_looping_off() {
  auto &tt = *fixture.tt;
  auto &v1 = *fixture.v1;
  auto &seq1 = *fixture.seq1;

  v1.loop = false;
  tt.voiceSettings.configs[TT_SCRIPT_1].midi_out_ch = 10;

  tt.cb = [&](script_number_t n) {
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel));
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteC, 16, Sequence::DurRel));
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteD, 12, Sequence::DurRel));
  };
  tt.cb(TEMP_SCRIPT);

  v1.notesOn(3);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteD);
  midiEvents.clear();

  tt.run_script(tt.scene, v1.nextSequence());
  v1.notesOn(0);
  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteD);
  midiEvents.clear();

  tt.run_script(tt.scene, v1.nextSequence());
  v1.notesOn(0);
  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  tt.run_script(tt.scene, v1.nextSequence());
  v1.notesOn(0);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
}

void test_voice_note_on_off_basic_looping_gate() {
  auto &tt = *fixture.tt;
  auto &v1 = *fixture.v1;
  auto &seq1 = *fixture.seq1;

  tt.cb = [&](script_number_t n) {
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel));
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteC, 16, Sequence::DurRel));
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteD, 12, Sequence::DurRel));
  };
  tt.cb(TEMP_SCRIPT);

  tt.voiceSettings.configs[TT_SCRIPT_1].gate_out = 0;

  v1.notesOn(0);
  TEST_ASSERT_EQUAL(tr_state[0], 0);
  TEST_ASSERT_EQUAL(tr_state[1], 0);
  TEST_ASSERT_EQUAL(tr_state[2], 0);
  TEST_ASSERT_EQUAL(tr_state[3], 0);

  tt.voiceSettings.configs[TT_SCRIPT_1].gate_out = 1;

  v1.notesOn(0);
  TEST_ASSERT_EQUAL(tr_state[0], 1);
  TEST_ASSERT_EQUAL(tr_state[1], 0);
  TEST_ASSERT_EQUAL(tr_state[2], 0);
  TEST_ASSERT_EQUAL(tr_state[3], 0);

  v1.notesOff();
  TEST_ASSERT_EQUAL(tr_state[0], 0);
  TEST_ASSERT_EQUAL(tr_state[1], 0);
  TEST_ASSERT_EQUAL(tr_state[2], 0);
  TEST_ASSERT_EQUAL(tr_state[3], 0);

  tt.run_script(tt.scene, v1.nextSequence());
  v1.notesOn(0);
  TEST_ASSERT_EQUAL(tr_state[0], 1);
  TEST_ASSERT_EQUAL(tr_state[1], 0);
  TEST_ASSERT_EQUAL(tr_state[2], 0);
  TEST_ASSERT_EQUAL(tr_state[3], 0);

  // Gate only cares about on or off, not which note is playing
  v1.notesOff();
  TEST_ASSERT_EQUAL(tr_state[0], 0);
  TEST_ASSERT_EQUAL(tr_state[1], 0);
  TEST_ASSERT_EQUAL(tr_state[2], 0);
  TEST_ASSERT_EQUAL(tr_state[3], 0);

  v1.notesOn(2);
  TEST_ASSERT_EQUAL(tr_state[0], 1);
  TEST_ASSERT_EQUAL(tr_state[1], 0);
  TEST_ASSERT_EQUAL(tr_state[2], 0);
  TEST_ASSERT_EQUAL(tr_state[3], 0);

  // Gate should not touch other TR outputs
  tr_state[1] = 1;
  tr_state[2] = 1;
  tr_state[3] = 1;

  v1.notesOff();
  TEST_ASSERT_EQUAL(tr_state[0], 0);
  TEST_ASSERT_EQUAL(tr_state[1], 1);
  TEST_ASSERT_EQUAL(tr_state[2], 1);
  TEST_ASSERT_EQUAL(tr_state[3], 1);
}

void test_voice_note_off_legato_gate() {
  auto &tt = *fixture.tt;
  auto &v1 = *fixture.v1;
  auto &seq1 = *fixture.seq1;

  tt.voiceSettings.configs[TT_SCRIPT_1].gate_out = 1;

  seq1.unpackData(generate_seq_data_basic(Sequence::NoteA, 8, Sequence::DurRel));
  auto data1 = generate_seq_data_basic(Sequence::NoteB, 4, Sequence::DurRel);
  Sequence::packNoteLegato(data1.value, true);
  seq1.unpackData(data1);
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteC, 16, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteD, 12, Sequence::DurRel));

  v1.notesOn(0);
  TEST_ASSERT_EQUAL(tr_state[0], 1);

  v1.notesOff();
  TEST_ASSERT_EQUAL(tr_state[0], 0);

  v1.notesOn(1);
  TEST_ASSERT_EQUAL(tr_state[0], 1);

  // Legato - next note is started before current note is released
  v1.notesOn(2);
  TEST_ASSERT_EQUAL(tr_state[0], 1);
  v1.notesOff();
  TEST_ASSERT_EQUAL(tr_state[0], 1);

  // Non-legato, should be released normally
  v1.notesOff();
  TEST_ASSERT_EQUAL(tr_state[0], 0);

  v1.notesOn(3);
  TEST_ASSERT_EQUAL(tr_state[0], 1);

  v1.notesOff();
  TEST_ASSERT_EQUAL(tr_state[0], 0);
}

void test_voice_note_on_off_basic_looping_cv() {
  auto &tt = *fixture.tt;
  auto &v1 = *fixture.v1;
  auto &seq1 = *fixture.seq1;

  tt.cb = [&](script_number_t n) {
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel));
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteC, 16, Sequence::DurRel));
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteD, 12, Sequence::DurRel));
  };
  tt.cb(TEMP_SCRIPT);

  tt.voiceSettings.configs[TT_SCRIPT_1].cv_out = 0;

  v1.notesOn(0);
  TEST_ASSERT_EQUAL(cv_state[0], 0);
  TEST_ASSERT_EQUAL(cv_state[1], 0);
  TEST_ASSERT_EQUAL(cv_state[2], 0);
  TEST_ASSERT_EQUAL(cv_state[3], 0);

  tt.voiceSettings.configs[TT_SCRIPT_1].cv_out = 1;

  v1.notesOn(0);
  TEST_ASSERT_EQUAL(cv_state[0], table_n[VOICE_OCT_OFFSET + Sequence::NoteA]);
  TEST_ASSERT_EQUAL(cv_state[1], 0);
  TEST_ASSERT_EQUAL(cv_state[2], 0);
  TEST_ASSERT_EQUAL(cv_state[3], 0);

  // NoteOff should not impact CV output
  v1.notesOff();
  TEST_ASSERT_EQUAL(cv_state[0], table_n[VOICE_OCT_OFFSET + Sequence::NoteA]);
  TEST_ASSERT_EQUAL(cv_state[1], 0);
  TEST_ASSERT_EQUAL(cv_state[2], 0);
  TEST_ASSERT_EQUAL(cv_state[3], 0);

  v1.notesOn(1);
  TEST_ASSERT_EQUAL(cv_state[0], table_n[VOICE_OCT_OFFSET + Sequence::NoteB]);
  TEST_ASSERT_EQUAL(cv_state[1], 0);
  TEST_ASSERT_EQUAL(cv_state[2], 0);
  TEST_ASSERT_EQUAL(cv_state[3], 0);

  tt.run_script(tt.scene, v1.nextSequence());
  v1.notesOn(0);
  TEST_ASSERT_EQUAL(cv_state[0], table_n[VOICE_OCT_OFFSET + Sequence::NoteA]);
  TEST_ASSERT_EQUAL(cv_state[1], 0);
  TEST_ASSERT_EQUAL(cv_state[2], 0);
  TEST_ASSERT_EQUAL(cv_state[3], 0);

  v1.notesOn(2);
  TEST_ASSERT_EQUAL(cv_state[0], table_n[VOICE_OCT_OFFSET + Sequence::NoteC]);
  TEST_ASSERT_EQUAL(cv_state[1], 0);
  TEST_ASSERT_EQUAL(cv_state[2], 0);
  TEST_ASSERT_EQUAL(cv_state[3], 0);

  // Should not touch other CV outputs
  cv_state[1] = 20;
  cv_state[2] = 30;
  cv_state[3] = 40;

  v1.notesOn(3);
  TEST_ASSERT_EQUAL(cv_state[0], table_n[VOICE_OCT_OFFSET + Sequence::NoteD]);
  TEST_ASSERT_EQUAL(cv_state[1], 20);
  TEST_ASSERT_EQUAL(cv_state[2], 30);
  TEST_ASSERT_EQUAL(cv_state[3], 40);
}

void test_voice_note_off_last_note_looping() {
  auto &tt = *fixture.tt;
  auto &v1 = *fixture.v1;
  auto &seq1 = *fixture.seq1;

  tt.voiceSettings.configs[TT_SCRIPT_1].midi_out_ch = 10;

  // Should be ignored for empty seq
  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  // Does nothing if no notes are on, silly
  tt.cb = [&](script_number_t n) {
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel));
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteC, 16, Sequence::DurRel));
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteD, 12, Sequence::DurRel));
  };
  tt.cb(TEMP_SCRIPT);
  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  v1.notesOn(3);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteD);
  midiEvents.clear();

  tt.run_script(tt.scene, v1.nextSequence());
  v1.notesOn(0);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  midiEvents.clear();

  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteD);
  midiEvents.clear();

  v1.notesOn(1);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB);
  midiEvents.clear();

  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  midiEvents.clear();
}

void test_voice_note_off_last_note_looping_off() {
  auto &tt = *fixture.tt;
  auto &v1 = *fixture.v1;
  auto &seq1 = *fixture.seq1;

  tt.voiceSettings.configs[TT_SCRIPT_1].midi_out_ch = 10;
  v1.loop = false;

  seq1.unpackData(generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteC, 16, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteD, 12, Sequence::DurRel));

  v1.notesOn(3);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteD);
  midiEvents.clear();

  tt.run_script(tt.scene, v1.nextSequence());
  v1.notesOn(0);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  // Can still send last note from previous sequence, even
  // when we have reached the end of a sequence and are not looping
  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteD);
  midiEvents.clear();
}

void test_voice_note_on_octave_shift_looping() {
  auto &tt = *fixture.tt;
  auto &v1 = *fixture.v1;
  auto &seq1 = *fixture.seq1;

  tt.cb = [&](script_number_t n) {
    auto data1 = generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel);
    Sequence::packNoteOctave(data1.data[0], -1);
    seq1.unpackData(data1);

    seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 4, Sequence::DurRel));

    auto data2 = generate_seq_data_basic(Sequence::NoteC, 4, Sequence::DurRel);
    Sequence::packNoteOctave(data2.data[0], 2);
    seq1.unpackData(data2);

    auto data3 = generate_seq_data_basic(Sequence::NoteD, 4, Sequence::DurRel);
    Sequence::packNoteOctave(data3.data[0], 1);
    seq1.unpackData(data3);
  };
  tt.cb(TEMP_SCRIPT);

  tt.voiceSettings.configs[TT_SCRIPT_1].midi_out_ch = 10;

  v1.notesOn(0);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA - 12);
  midiEvents.clear();

  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA - 12);
  midiEvents.clear();

  v1.notesOn(1);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB - 12);
  midiEvents.clear();

  // Simulate Legato "on-then-off" order
  v1.notesOn(2);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteC + 12);
  midiEvents.clear();

  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB - 12);
  midiEvents.clear();

  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteC + 12);
  midiEvents.clear();

  v1.notesOn(3);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteD + 24);
  midiEvents.clear();

  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteD + 24);
  midiEvents.clear();

  // Loop! octave offset should reset
  tt.run_script(tt.scene, v1.nextSequence());
  v1.notesOn(0);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA - 12);
  TEST_ASSERT_EQUAL(midiEvents[0].ch, 10);
  midiEvents.clear();

  v1.notesOn(1);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB - 12);
  // ...
}

void test_voice_note_on_octave_shift_looping2() {
  auto &tt = *fixture.tt;
  auto &v1 = *fixture.v1;
  auto &seq1 = *fixture.seq1;

  tt.cb = [&](script_number_t n) {
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel));
    auto data2 = generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel);
    Sequence::packNoteOctave(data2.data[0], 1);
    seq1.unpackData(data2);
  };
  tt.cb(TEMP_SCRIPT);

  tt.voiceSettings.configs[TT_SCRIPT_1].midi_out_ch = 1;
  tt.voiceSettings.configs[TT_SCRIPT_1].cv_out = 1;

  v1.notesOn(0);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  midiEvents.clear();

  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  midiEvents.clear();

  v1.notesOn(1);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB + 12);
  midiEvents.clear();

  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB + 12);
  midiEvents.clear();

  // Loop! octave offset should reset
  tt.run_script(tt.scene, v1.nextSequence());
  v1.notesOn(0);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  midiEvents.clear();

  v1.notesOn(1);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB + 12);
  // ...
}

void test_voice_note_on_octave_shift_seq_change() {
  auto &tt = *fixture.tt;
  auto &v1 = *fixture.v1;
  auto &seq1 = *fixture.seq1;

  tt.cb = [&](script_number_t n) {
    auto data1 = generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel);
    Sequence::packNoteOctave(data1.data[0], 1);
    seq1.unpackData(data1);
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 4, Sequence::DurRel));
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteC, 4, Sequence::DurRel));
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteD, 4, Sequence::DurRel));
  };
  tt.cb(TEMP_SCRIPT);

  tt.voiceSettings.configs[TT_SCRIPT_1].midi_out_ch = 10;

  v1.notesOn(0);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA + 12);
  midiEvents.clear();

  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA + 12);
  midiEvents.clear();

  v1.notesOn(1);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB + 12);
  midiEvents.clear();

  seq1.resetData();
  tt.cb = [&](script_number_t n) {
    auto data2 = generate_seq_data_basic(Sequence::NoteAb, 4, Sequence::DurRel);
    Sequence::packNoteOctave(data2.data[0], -2);
    seq1.unpackData(data2);
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteBb, 4, Sequence::DurRel));
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteDb, 4, Sequence::DurRel));
    auto data3 = generate_seq_data_basic(Sequence::NoteEb, 4, Sequence::DurRel);
    Sequence::packNoteOctave(data3.data[0], 1);
    seq1.unpackData(data3);
  };
  tt.cb(TEMP_SCRIPT);

  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB + 12);
  midiEvents.clear();

  // Next note: picks up in new sequence (using correct octave)
  v1.notesOn(2);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteDb - 24);
  midiEvents.clear();

  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteDb - 24);
  midiEvents.clear();

  v1.notesOn(3);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteEb - 12);
  midiEvents.clear();

  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteEb - 12);
  midiEvents.clear();


  // Loop! octave offset should reset
  tt.run_script(tt.scene, v1.nextSequence());
  v1.notesOn(0);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteAb - 24);
  TEST_ASSERT_EQUAL(midiEvents[0].ch, 10);
  midiEvents.clear();

  v1.notesOn(1);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteBb - 24);
  // ...
}

void test_voice_note_on_octave_shift_seq_change_adv() {
  auto &tt = *fixture.tt;
  auto &v1 = *fixture.v1;
  auto &seq1 = *fixture.seq1;

  tt.cb = [&](script_number_t n) {
    auto data1 = generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel);
    Sequence::packNoteOctave(data1.data[0], 1);
    seq1.unpackData(data1);
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 4, Sequence::DurRel));
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteC, 4, Sequence::DurRel));
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteD, 4, Sequence::DurRel));
  };
  tt.cb(TEMP_SCRIPT);

  tt.voiceSettings.configs[TT_SCRIPT_1].midi_out_ch = 10;

  v1.notesOn(0);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA + 12);
  midiEvents.clear();

  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA + 12);
  midiEvents.clear();

  v1.notesOn(1);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB + 12);
  midiEvents.clear();

  seq1.resetData();
  tt.cb = [&](script_number_t n) {
    // Chord: multiple offsets should accumulate
    auto data2 = generate_seq_data_basic(Sequence::NoteAb, 4, Sequence::DurRel);
    Sequence::packNoteOctave(data2.data[0], 1);
    Sequence::packNoteName(data2.data[1], Sequence::NoteF);
    Sequence::packNoteOctave(data2.data[1], -1);
    seq1.unpackData(data2);
    auto data3 = generate_seq_data_basic(Sequence::NoteBb, 4, Sequence::DurRel);
    Sequence::packNoteOctave(data3.data[0], -1);
    seq1.unpackData(data3);
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteDb, 4, Sequence::DurRel));
    auto data4 = generate_seq_data_basic(Sequence::NoteEb, 4, Sequence::DurRel);
    Sequence::packNoteOctave(data4.data[0], 1);
    seq1.unpackData(data4);
  };
  tt.cb(TEMP_SCRIPT);

  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB + 12);
  midiEvents.clear();

  // Next note: picks up in new sequence (using correct octave)
  v1.notesOn(2);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteDb - 12);
  midiEvents.clear();

  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteDb - 12);
  midiEvents.clear();

  v1.notesOn(3);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteEb);
  midiEvents.clear();

  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteEb);
  midiEvents.clear();


  // Loop! octave offset should reset
  tt.run_script(tt.scene, v1.nextSequence());
  v1.notesOn(0);
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteAb + 12);
  TEST_ASSERT_NOTE_ON(1, Sequence::NoteF);
  midiEvents.clear();

  v1.notesOn(1);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteBb - 12);
  // ...
}

void test_voice_note_on_octave_shift_pause_resume() {
  auto &tt = *fixture.tt;
  auto &v1 = *fixture.v1;
  auto &seq1 = *fixture.seq1;

  tt.cb = [&](script_number_t n) {
    seq1.unpackData(generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
    auto data1 = generate_seq_data_basic(Sequence::NoteB, 4, Sequence::DurRel);
    Sequence::packNoteOctave(data1.data[0], 1);
    seq1.unpackData(data1);
    auto data2 = generate_seq_data_basic(Sequence::NoteC, 4, Sequence::DurRel);
    Sequence::packNoteOctave(data2.data[0], 1);
    seq1.unpackData(data2);
  };
  tt.cb(TEMP_SCRIPT);

  tt.voiceSettings.configs[TT_SCRIPT_1].midi_out_ch = 10;

  v1.notesOn(0);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  midiEvents.clear();

  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  midiEvents.clear();

  v1.notesOn(1);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB + 12);
  midiEvents.clear();

  // pause (V.PAUSE or M.ACT 0)
  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB + 12);
  midiEvents.clear();

  // resume (V.PLAY or M.ACT 1)
  v1.notesOn(1);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB + 12);
  midiEvents.clear();

  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB + 12);
  midiEvents.clear();

  v1.notesOn(2);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteC + 24);
  midiEvents.clear();

  v1.notesOff();
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteC + 24);
  midiEvents.clear();

  // Loop! octave offset should reset
  tt.run_script(tt.scene, v1.nextSequence());
  v1.notesOn(0);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  midiEvents.clear();

  v1.notesOn(1);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB + 12);
  // ...
}

void test_voice_cb_script() {
  auto &tt = *fixture.tt;
  auto &v1 = *fixture.v1;
  auto &seq1 = *fixture.seq1;

  tt.cb = [&](script_number_t n) {
    if (n == TT_SCRIPT_1) {
      seq1.unpackData(generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
      seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel));
      seq1.unpackData(generate_seq_data_basic(Sequence::NoteC, 16, Sequence::DurRel));
      seq1.unpackData(generate_seq_data_basic(Sequence::NoteD, 12, Sequence::DurRel));
    }
  };
  tt.cb(TT_SCRIPT_1);

  tt.voiceSettings.configs[TT_SCRIPT_1].midi_out_ch = 10;
  tt.scriptEvents.clear();

  // If script is set, it is called at end of each sequence
  tt.run_script(tt.scene, v1.nextSequence());
  TEST_ASSERT_EQUAL(tt.scriptEvents.size(), 1);
  TEST_ASSERT_EQUAL(tt.scriptEvents[0], TT_SCRIPT_1);
  tt.scriptEvents.clear();
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  v1.notesOn(0);
  TEST_ASSERT_EQUAL(tt.scriptEvents.size(), 0);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  midiEvents.clear();
}

void test_voice_slew() {
  auto &tt = *fixture.tt;
  auto &v1 = *fixture.v1;
  auto &seq1 = *fixture.seq1;

  seq1.unpackData(generate_seq_data_basic(Sequence::NoteC, 8, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteF, 0, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteE, 0, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteEb, 0, Sequence::DurRel));

  seq1.applyMod(Sequence::ModSlewDown, 99);

  tt.voiceSettings.configs[TT_SCRIPT_1].cv_out = 1;

  v1.notesOn(0);
  v1.notesOff();
  TEST_ASSERT_EQUAL(cvEvents.size(), 1);
  TEST_ASSERT_CV_SET_VALUE(0, table_n[VOICE_OCT_OFFSET + Sequence::NoteC], false);
  cvEvents.clear();

  v1.notesOn(1);
  v1.notesOff();
  TEST_ASSERT_EQUAL(cvEvents.size(), 1);
  TEST_ASSERT_CV_SET_VALUE(0, table_n[VOICE_OCT_OFFSET + Sequence::NoteF], false);
  cvEvents.clear();

  v1.notesOn(2);
  v1.notesOff();
  TEST_ASSERT_EQUAL(cvEvents.size(), 2);
  TEST_ASSERT_CV_SET_SLEW(0, 99);
  TEST_ASSERT_CV_SET_VALUE(1, table_n[VOICE_OCT_OFFSET + Sequence::NoteE], true);
  cvEvents.clear();

  v1.notesOn(3);
  v1.notesOff();
  TEST_ASSERT_EQUAL(cvEvents.size(), 2);
  TEST_ASSERT_CV_SET_SLEW(0, 99);
  TEST_ASSERT_CV_SET_VALUE(1, table_n[VOICE_OCT_OFFSET + Sequence::NoteEb], true);
  cvEvents.clear();

  seq1.applyMod(Sequence::ModSlewUp, 130);

  v1.notesOn(1);
  v1.notesOff();
  TEST_ASSERT_EQUAL(cvEvents.size(), 2);
  TEST_ASSERT_CV_SET_SLEW(0, 130);
  TEST_ASSERT_CV_SET_VALUE(1, table_n[VOICE_OCT_OFFSET + Sequence::NoteF], true);
  cvEvents.clear();

  v1.notesOn(3);
  v1.notesOff();
  TEST_ASSERT_EQUAL(cvEvents.size(), 2);
  TEST_ASSERT_CV_SET_SLEW(0, 99);
  TEST_ASSERT_CV_SET_VALUE(1, table_n[VOICE_OCT_OFFSET + Sequence::NoteEb], true);
  cvEvents.clear();

  seq1.applyMod(Sequence::ModSlewDown, 0);

  v1.notesOn(0);
  v1.notesOff();
  TEST_ASSERT_EQUAL(cvEvents.size(), 1);
  TEST_ASSERT_CV_SET_VALUE(0, table_n[VOICE_OCT_OFFSET + Sequence::NoteC], false);
  cvEvents.clear();
}

int main() {
  UNITY_BEGIN();
  RUN_TEST_WITH_SETUP(test_voice_note_on_off_empty_seq);
  RUN_TEST_WITH_SETUP(test_voice_note_on_off_basic_looping);
  RUN_TEST_WITH_SETUP(test_voice_note_on_off_basic_looping_off);
  RUN_TEST_WITH_SETUP(test_voice_note_on_off_basic_looping_gate);
  RUN_TEST_WITH_SETUP(test_voice_note_off_legato_gate);
  RUN_TEST_WITH_SETUP(test_voice_note_on_off_basic_looping_cv);
  RUN_TEST_WITH_SETUP(test_voice_note_off_last_note_looping);
  RUN_TEST_WITH_SETUP(test_voice_note_off_last_note_looping_off);
  RUN_TEST_WITH_SETUP(test_voice_note_on_octave_shift_looping);
  RUN_TEST_WITH_SETUP(test_voice_note_on_octave_shift_looping2);
  RUN_TEST_WITH_SETUP(test_voice_note_on_octave_shift_seq_change);
  RUN_TEST_WITH_SETUP(test_voice_note_on_octave_shift_seq_change_adv);
  RUN_TEST_WITH_SETUP(test_voice_note_on_octave_shift_pause_resume);
  RUN_TEST_WITH_SETUP(test_voice_cb_script);
  RUN_TEST_WITH_SETUP(test_voice_slew);
  UNITY_END();
  return 0;
}

} // namespace TestVoice
