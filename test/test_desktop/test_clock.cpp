#include <cstdint>
#include <functional>
#include <unity.h>

#include <clock.h>
#include <espresso/command.h>
#include <models/settings.h>
#include <models/voice_settings.h>
#include <sequence.h>
#include <types.h>
#include <voice.h>

#include "teletype_stubs.h"
#include "test_helpers.h"
#include "mock_teleteype.h"

namespace TestVoice {
Espresso::Token generate_seq_data_basic(Sequence::NoteName note, int duration,
                                    Sequence::DurationUnit unit);
}

namespace TestClock {

using TestVoice::generate_seq_data_basic;

class MockMidiService {
public:
  void sendClock() {
    midiEvents.push_back(
        {.type = eMidiTypeClock, .ch = 0, .data1 = 0, .data2 = 0});
  }
};

struct TestClockFixture {
  MockTeletype *tt;
  Scene *scene;
  Clock<MockTeletype, MockMidiService> *clock;
  Models::Settings *settings;
  Models::VoiceSettings *voiceSettings;
  Voice *v1;
  Voice *v2;
  Voice *v3;
  Sequence *seq1;
  Sequence *seq2;
  Sequence *seq3;
} fixture;

#define RUN_TEST_WITH_SETUP(fn, ...) runWithSetup(fn, #fn, __LINE__, ##__VA_ARGS__)

void runWithSetup(void test(void), const char *name, const int line, int voice_count = 1) {
  MockTeletype tt;
  Models::Settings settings;
  Models::VoiceSettings voiceSettings;
  MockMidiService midiService;

  Clock<MockTeletype, MockMidiService> clock(tt, tt.scene, midiService, settings);
  settings.isRunning = true;

  fixture.tt = &tt;
  fixture.scene = &tt.scene;
  fixture.clock = &clock;
  fixture.settings = &settings;
  fixture.voiceSettings = &voiceSettings;

  Voice v1(voiceSettings, TT_SCRIPT_1);
  clock.setVoice(v1, 0);
  voiceSettings.configs[TT_SCRIPT_1].midi_out_ch = 1;

  fixture.v1 = &v1;
  fixture.seq1 = &v1.seq;

  midiEvents.clear();

  if (voice_count < 2) {
    fixture.v2 = nullptr;
    fixture.seq2 = nullptr;
    UnityDefaultTestRun(test, name, line);
    return;
  }

  Voice v2(voiceSettings, TT_SCRIPT_2);
  clock.setVoice(v2, 1);
  voiceSettings.configs[TT_SCRIPT_2].midi_out_ch = 2;

  fixture.v2 = &v2;
  fixture.seq2 = &v2.seq;

  if (voice_count < 3) {
    fixture.v3 = nullptr;
    fixture.seq3 = nullptr;
    UnityDefaultTestRun(test, name, line);
    return;
  }

  Voice v3(voiceSettings, TT_SCRIPT_3);
  clock.setVoice(v3, 2);
  voiceSettings.configs[TT_SCRIPT_3].midi_out_ch = 3;

  fixture.v3 = &v3;
  fixture.seq3 = &v3.seq;

  UnityDefaultTestRun(test, name, line);
}

void test_clock_empty_seq_schedule_parsing() {
  TEST_ASSERT_EQUAL(fixture.clock->seq_schedules[0].voice, fixture.v1);
  TEST_ASSERT_EQUAL(fixture.clock->seq_schedules[0].event_count, 1); // Fall back to quarterNoteRest, to automatically check for new content each beat
  TEST_ASSERT_EQUAL(fixture.clock->seq_schedules[0].events[0].duration_ms, 500);
  TEST_ASSERT_EQUAL(fixture.clock->seq_schedules[0].events[0].duration_ticks, 24);
}

void test_clock_seq_schedule_parsing_basic_rel() {
  auto &seq1 = *fixture.seq1;
  auto &clock = *fixture.clock;

  // 0 = default to quarter note (4)
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteC, 16, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteD, 12, Sequence::DurRel));
  clock.setVoice(*fixture.v1, 0);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].voice, fixture.v1);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].event_count, 4);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[0].duration_ms, 500);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[0].duration_ticks, 24);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[1].duration_ms, 250);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[1].duration_ticks, 12);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[2].duration_ms, 125);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[2].duration_ticks, 6);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[3].duration_ms, 166);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[3].duration_ticks, 8);
}

void test_clock_seq_schedule_parsing_advanced_rel() {
  auto &seq1 = *fixture.seq1;
  auto &clock = *fixture.clock;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteC, 8, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteD, 0, Sequence::DurRel));
  clock.setVoice(*fixture.v1, 0);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].voice, fixture.v1);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].event_count, 4);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[0].duration_ms, 500);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[0].duration_ticks, 24);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[1].duration_ms, 500);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[1].duration_ticks, 24);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[2].duration_ms, 250);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[2].duration_ticks, 12);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[3].duration_ms, 250);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[3].duration_ticks, 12);
}

void test_clock_seq_schedule_parsing_basic_abs() {
  auto &seq1 = *fixture.seq1;
  auto &clock = *fixture.clock;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 2, Sequence::DurSec));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteB, 100, Sequence::DurMs));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteC, 500, Sequence::DurMs));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteD, 60, Sequence::DurSec));
  clock.setVoice(*fixture.v1, 0);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].voice, fixture.v1);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].event_count, 4);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[0].duration_ms, 2000);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[0].duration_ticks, 0);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[1].duration_ms, 100);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[1].duration_ticks, 0);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[2].duration_ms, 500);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[2].duration_ticks, 0);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[3].duration_ms, 60000);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[3].duration_ticks, 0);
}

void test_clock_seq_schedule_parsing_mixed() {
  auto &seq1 = *fixture.seq1;
  auto &clock = *fixture.clock;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
  seq1.unpackData(generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurMs));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteC, 16, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteD, 12, Sequence::DurSec));
  clock.setVoice(*fixture.v1, 0);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].voice, fixture.v1);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].event_count, 4);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[0].duration_ms, 500);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[0].duration_ticks, 24);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[1].duration_ms, 8);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[1].duration_ticks, 0);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[2].duration_ms, 125);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[2].duration_ticks, 6);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[3].duration_ms, 12000);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[3].duration_ticks, 0);
}

void test_clock_seq_schedule_parsing_basic_dots() {
  auto &seq1 = *fixture.seq1;
  auto &clock = *fixture.clock;

  auto data1 = generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel);
  Sequence::packNoteDots(data1.value, 1);
  auto data2 = generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel);
  Sequence::packNoteDots(data2.value, 2);
  auto data3 = generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel);
  Sequence::packNoteDots(data3.value, 3);

  seq1.unpackData(data1);
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
  seq1.unpackData(data2);
  seq1.unpackData(data3);
  clock.setVoice(*fixture.v1, 0);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].voice, fixture.v1);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].event_count, 4);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[0].duration_ms,
                    750); // 1/4 + 1/8
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[0].duration_ticks, 36);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[1].duration_ms, 500);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[1].duration_ticks, 24);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[2].duration_ms,
                    875); // 1/4 + 1/8 + 1/16
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[2].duration_ticks, 42);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[3].duration_ms,
                    937.5); // 1/4 + 1/8 + 1/16 + 1/32
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[3].duration_ticks, 45);
}

void test_clock_seq_schedule_parsing_basic_length() {
  auto &seq1 = *fixture.seq1;
  auto &clock = *fixture.clock;

  fixture.voiceSettings->configs[TT_SCRIPT_1].note_length = 80;

  auto data1 = generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel);
  Sequence::packNoteLegato(data1.value, true);
  auto data2 = generate_seq_data_basic(Sequence::NoteA, 8, Sequence::DurRel);
  Sequence::packNoteLegato(data2.value, true);
  auto data3 = generate_seq_data_basic(Sequence::NoteA, 16, Sequence::DurRel);
  Sequence::packNoteLegato(data3.value, true);

  seq1.unpackData(data1);
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteB, 4, Sequence::DurRel));
  seq1.unpackData(data2);
  seq1.unpackData(data3);
  clock.setVoice(*fixture.v1, 0);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].voice, fixture.v1);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].event_count, 4);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[0].duration_ms, 500);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[0].release_ms, 500);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[0].duration_ticks, 24);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[0].release_ticks, 24);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[1].duration_ms, 500);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[1].release_ms, 400);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[1].duration_ticks, 24);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[1].release_ticks, 19);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[2].duration_ms, 250);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[2].release_ms, 250);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[2].duration_ticks, 12);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[2].release_ticks, 12);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[3].duration_ms, 125);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[3].release_ms, 125);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[3].duration_ticks, 6);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[3].release_ticks, 6);
}

void test_clock_seq_schedule_parsing_basic_repeats() {
  auto &seq1 = *fixture.seq1;
  auto &clock = *fixture.clock;

  auto data1 = generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel);
  Sequence::packNoteRepeats(data1.value, 2);
  auto data2 = generate_seq_data_basic(Sequence::NoteC, 8, Sequence::DurRel);
  Sequence::packNoteRepeats(data2.value, 1);
  auto data3 = generate_seq_data_basic(Sequence::NoteD, 16, Sequence::DurRel);
  Sequence::packNoteRepeats(data3.value, 8);

  seq1.unpackData(data1);
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel));
  seq1.unpackData(data2);
  seq1.unpackData(data3);
  clock.setVoice(*fixture.v1, 0);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].voice, fixture.v1);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].event_count, 12);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[0].duration_ms, 500);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[0].duration_ticks, 24);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[1].duration_ms, 500);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[1].duration_ticks, 24);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[2].duration_ms, 250);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[2].duration_ticks, 12);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[3].duration_ms, 250);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[3].duration_ticks, 12);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[4].duration_ms, 125);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[4].duration_ticks, 6);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[5].duration_ms, 125);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[5].duration_ticks, 6);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[6].duration_ms, 125);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[6].duration_ticks, 6);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[7].duration_ms, 125);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[8].duration_ms, 125);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[9].duration_ms, 125);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[10].duration_ms, 125);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[11].duration_ms, 125);
}

void test_clock_seq_schedule_parsing_fast() {
  auto &seq1 = *fixture.seq1;
  auto &clock = *fixture.clock;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteC, 80, Sequence::DurMs));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteD, 2, Sequence::DurSec));
  seq1.applyMod(Sequence::ModFast, 2);
  clock.setVoice(*fixture.v1, 0);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].voice, fixture.v1);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].event_count, 4);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[0].duration_ms, 250);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[0].duration_ticks, 12);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[1].duration_ms, 125);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[1].duration_ticks, 6);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[2].duration_ms, 40);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[2].duration_ticks, 0);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[3].duration_ms, 1000);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[3].duration_ticks, 0);
}

void test_clock_seq_schedule_parsing_slow() {
  auto &seq1 = *fixture.seq1;
  auto &clock = *fixture.clock;

  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteC, 80, Sequence::DurMs));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteD, 2, Sequence::DurSec));
  seq1.applyMod(Sequence::ModSlow, 3);
  clock.setVoice(*fixture.v1, 0);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].voice, fixture.v1);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].event_count, 4);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[0].duration_ms, 1500);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[0].duration_ticks, 72);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[1].duration_ms, 750);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[1].duration_ticks, 36);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[2].duration_ms, 240);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[2].duration_ticks, 0);

  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[3].duration_ms, 6000);
  TEST_ASSERT_EQUAL(clock.seq_schedules[0].events[3].duration_ticks, 0);
}

void test_clock_tick_empty_schedule() {
  auto &seq1 = *fixture.seq1;
  auto &clock = *fixture.clock;

  unsigned long time = 0;
  for (unsigned long time = 0; time < 30000; time += 10) {
    TEST_ASSERT_EQUAL(clock.tick(time), false);
  }
}

void test_clock_tick_basic() {
  auto &seq1 = *fixture.seq1;
  auto &clock = *fixture.clock;

  auto data1 = generate_seq_data_basic(Sequence::NoteC, 8, Sequence::DurRel);
  Sequence::packNoteDots(data1.value, 1);
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteA, 2, Sequence::DurRel));
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteB, 4, Sequence::DurRel));
  seq1.unpackData(data1);
  seq1.unpackData(
      generate_seq_data_basic(Sequence::NoteD, 16, Sequence::DurRel));
  clock.setVoice(*fixture.v1, 0);

  unsigned long start_time = 1;

  // NoteOn (half)
  TEST_ASSERT_EQUAL(clock.tick(start_time), true);
  // same time should not duplicate side-effects
  TEST_ASSERT_EQUAL(clock.tick(start_time), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1), false);

  // NoteOff
  TEST_ASSERT_EQUAL(clock.tick(start_time + 799), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 800), true);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 801), false);

  // NoteOn (quarter)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1000), true);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1001), false);

  // NoteOff
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1399), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1400), true);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1401), false);

  // NoteOn (eighth, dotted)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1500), true);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1501), false);

  // NoteOff
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1799), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1800), true);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1801), false);

  // NoteOn (sixteenth)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1874), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1875), true);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1876), false);

  // NoteOff
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1974), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1975), true);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1976), false);
}

void test_clock_tick_looping() {
  auto &seq1 = *fixture.seq1;
  auto &clock = *fixture.clock;
  auto &tt = *fixture.tt;


  tt.cb = [&](script_number_t n) {
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteB, 4, Sequence::DurRel));
  };
  tt.cb(TEMP_SCRIPT);
  clock.setVoice(*fixture.v1, 0);

  unsigned long start_time = 1;

  // NoteOn
  TEST_ASSERT_EQUAL(clock.tick(start_time), true);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1), false);

  // NoteOff
  TEST_ASSERT_EQUAL(clock.tick(start_time + 399), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 400), true);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 401), false);

  // NoteOn
  TEST_ASSERT_EQUAL(clock.tick(start_time + 499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 500), true);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 501), false);

  // NoteOff
  TEST_ASSERT_EQUAL(clock.tick(start_time + 899), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 900), true);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 901), false);

  // NoteOn
  TEST_ASSERT_EQUAL(clock.tick(start_time + 999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1000), true);

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1001), false);

  // NoteOff
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1399), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1400), true);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1401), false);

  // NoteOn
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1500), true);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1501), false);

  // NoteOff
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1899), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1900), true);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1901), false);

  // NoteOn
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2000), true);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2001), false);

  // NoteOff
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2399), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2400), true);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2401), false);

  // NoteOn
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2500), true);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2501), false);

  // NoteOff
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2899), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2900), true);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2901), false);

  // NoteOn
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 3000), true);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 3001), false);

  // NoteOff
  TEST_ASSERT_EQUAL(clock.tick(start_time + 3399), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 3400), true);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 3401), false);

  // ...
}

void test_clock_tick_looping_off() {
  auto &seq1 = *fixture.seq1;
  auto &clock = *fixture.clock;
  auto &tt = *fixture.tt;

  fixture.v1->loop = false;

  tt.cb = [&](script_number_t n) {
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteB, 4, Sequence::DurRel));
  };
  tt.cb(TEMP_SCRIPT);
  clock.setVoice(*fixture.v1, 0);

  unsigned long start_time = 1;

  // NoteOn
  TEST_ASSERT_EQUAL(clock.tick(start_time), true);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1), false);

  // NoteOff
  TEST_ASSERT_EQUAL(clock.tick(start_time + 399), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 400), true);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 401), false);

  // NoteOn
  TEST_ASSERT_EQUAL(clock.tick(start_time + 499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 500), true);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 501), false);

  // NoteOff
  TEST_ASSERT_EQUAL(clock.tick(start_time + 899), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 900), true);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 901), false);

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1000), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1500), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2000), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2500), false);
}

void test_clock_tick_legato() {
  auto &seq1 = *fixture.seq1;
  auto &clock = *fixture.clock;
  auto &tt = *fixture.tt;

  tt.cb = [&](script_number_t n) {
    auto data1 = generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel);
    Sequence::packNoteLegato(data1.value, true);
    seq1.unpackData(data1);

    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteB, 4, Sequence::DurRel));

    auto data2 = generate_seq_data_basic(Sequence::NoteC, 4, Sequence::DurRel);
    Sequence::packNoteLegato(data2.value, true);

    seq1.unpackData(data2);
  };
  tt.cb(TEMP_SCRIPT);
  clock.setVoice(*fixture.v1, 0);

  unsigned long start_time = 1;

  // NoteOn
  TEST_ASSERT_EQUAL(clock.tick(start_time), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 401), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 500), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  // Legato should send next NoteOn _before_ prior NoteOff
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB);
  TEST_ASSERT_NOTE_OFF(1, Sequence::NoteA);
  midiEvents.clear();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 501), false);

  TEST_ASSERT_EQUAL(clock.tick(start_time + 899), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 900), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteC);
  midiEvents.clear();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1001), false);

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1401), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1500), true);
  // When wrapping the end of a sequence, NoteOn should
  // still overlap with prior NoteOff (for legato notes)
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  TEST_ASSERT_NOTE_OFF(1, Sequence::NoteC);
  midiEvents.clear();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1501), false);

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1901), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1999), false);
  // NoteOff, NoteOn...
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2000), true);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2001), false);
}

void test_clock_tick_legato_looping_off() {
  auto &seq1 = *fixture.seq1;
  auto &clock = *fixture.clock;
  auto &tt = *fixture.tt;

  fixture.v1->loop = false;

  tt.cb = [&](script_number_t n) {
    auto data1 = generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel);
    Sequence::packNoteLegato(data1.value, true);
    seq1.unpackData(data1);

    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteB, 4, Sequence::DurRel));

    auto data2 = generate_seq_data_basic(Sequence::NoteC, 4, Sequence::DurRel);
    Sequence::packNoteLegato(data2.value, true);
    seq1.unpackData(data2);
  };
  tt.cb(TEMP_SCRIPT);
  clock.setVoice(*fixture.v1, 0);

  unsigned long start_time = 1;

  // NoteOn
  TEST_ASSERT_EQUAL(clock.tick(start_time), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 401), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 500), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  // Legato should send next NoteOn _before_ prior NoteOff
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB);
  TEST_ASSERT_NOTE_OFF(1, Sequence::NoteA);
  midiEvents.clear();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 501), false);

  TEST_ASSERT_EQUAL(clock.tick(start_time + 899), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 900), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteC);
  midiEvents.clear();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1001), false);

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1401), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1500), true);
  // When reaching the end of a sequence with looping off,
  // deferred NoteOff (for legato notes) should still occur
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteC);
  midiEvents.clear();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1501), false);

  TEST_ASSERT_EQUAL(clock.tick(start_time + 2000), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2500), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 3000), false);
}

void test_clock_tick_repeats() {
  auto &seq1 = *fixture.seq1;
  auto &clock = *fixture.clock;
  auto &tt = *fixture.tt;

  tt.cb = [&](script_number_t n) {
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteA, 2, Sequence::DurRel));
    auto data1 = generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel);
    Sequence::packNoteRepeats(data1.value, 4);
    seq1.unpackData(data1);
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteC, 2, Sequence::DurRel));
  };
  tt.cb(TEMP_SCRIPT);
  clock.setVoice(*fixture.v1, 0);

  unsigned long start_time = 1;

  // NoteOn
  TEST_ASSERT_EQUAL(clock.tick(start_time), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  midiEvents.clear();

  // NoteOff
  TEST_ASSERT_EQUAL(clock.tick(start_time + 799), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 800), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1199), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1200), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1249), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1250), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1449), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1450), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1500), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1699), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1700), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1749), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1750), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1949), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1950), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteC);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 2799), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2800), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteC);
  midiEvents.clear();

  // loop!
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 3000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 3799), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 3800), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 3999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 4000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB);
  //...
}

void test_clock_tick_repeats_octave() {
  auto &seq1 = *fixture.seq1;
  auto &clock = *fixture.clock;
  auto &tt = *fixture.tt;

  tt.cb = [&](script_number_t n) {
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteA, 2, Sequence::DurRel));
    auto data1 = generate_seq_data_basic(Sequence::NoteB, 8, Sequence::DurRel);
    Sequence::packNoteRepeats(data1.value, 4);
    Sequence::packNoteOctave(data1.data[0], 1);
    seq1.unpackData(data1);
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteC, 2, Sequence::DurRel));
  };
  tt.cb(TEMP_SCRIPT);
  clock.setVoice(*fixture.v1, 0);

  unsigned long start_time = 1;

  // NoteOn
  TEST_ASSERT_EQUAL(clock.tick(start_time), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  midiEvents.clear();

  // NoteOff
  TEST_ASSERT_EQUAL(clock.tick(start_time + 799), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 800), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB + 12);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1199), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1200), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB + 12);
  midiEvents.clear();

  // Octave shift should only be applied once
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1249), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1250), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB + 12);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1449), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1450), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB + 12);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1500), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB + 12);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1699), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1700), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB + 12);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1749), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1750), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB + 12);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1949), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1950), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB + 12);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteC + 12);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 2799), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2800), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteC + 12);
  midiEvents.clear();

  // loop!
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 3000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 3799), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 3800), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 3999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 4000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB + 12);
  //...
}

void test_clock_tick_multi() {
  auto &seq1 = *fixture.seq1;
  auto &seq2 = *fixture.seq2;
  auto &clock = *fixture.clock;
  auto &tt = *fixture.tt;

  tt.cb = [&](script_number_t n) {
    if (n == TT_SCRIPT_1) {
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteA, 2, Sequence::DurRel));
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteB, 2, Sequence::DurRel));
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteC, 4, Sequence::DurRel));
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteD, 4, Sequence::DurRel));
    }
    if (n == TT_SCRIPT_2) {
      auto data1 = generate_seq_data_basic(Sequence::NoteE, 2, Sequence::DurRel);
      Sequence::packNoteDots(data1.value, 1);
      seq2.unpackData(data1);
      auto data2 = generate_seq_data_basic(Sequence::NoteF, 2, Sequence::DurRel);
      Sequence::packNoteDots(data2.value, 1);
      seq2.unpackData(data2);
    }
  };
  tt.cb(TT_SCRIPT_1);
  tt.cb(TT_SCRIPT_2);
  clock.reset();

  unsigned long start_time = 1;

  // NoteOn (seq1 + seq2)
  TEST_ASSERT_EQUAL(clock.tick(start_time), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  TEST_ASSERT_NOTE_ON(1, Sequence::NoteE);
  midiEvents.clear();

  // same time should not duplicate side-effects
  TEST_ASSERT_EQUAL(clock.tick(start_time), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1), false);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  // NoteOff (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 799), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 800), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  midiEvents.clear();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 801), false);

  // NoteOn (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB);
  midiEvents.clear();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1001), false);

  // NoteOff (seq2)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1199), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1200), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteE);
  midiEvents.clear();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1201), false);

  // NoteOn (seq2)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1500), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteF);
  midiEvents.clear();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1501), false);

  // NoteOff (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1799), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1800), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB);
  midiEvents.clear();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1801), false);

  // NoteOn (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteC);
  midiEvents.clear();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2001), false);

  // NoteOff (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2399), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2400), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteC);
  midiEvents.clear();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2401), false);

  // NoteOn (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2500), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteD);
  midiEvents.clear();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2501), false);

  // NoteOff (seq2)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2699), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2700), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteF);
  midiEvents.clear();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2701), false);

  // NoteOff (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2899), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2900), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteD);
  midiEvents.clear();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2901), false);
}

void test_clock_tick_multi_looping() {
  auto &seq1 = *fixture.seq1;
  auto &seq2 = *fixture.seq2;
  auto &clock = *fixture.clock;
  auto &tt = *fixture.tt;

  tt.cb = [&](script_number_t n) {
    if (n == TT_SCRIPT_1) {
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteA, 2, Sequence::DurRel));
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteB, 2, Sequence::DurRel));
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteC, 4, Sequence::DurRel));
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteD, 4, Sequence::DurRel));
    }
    if (n == TT_SCRIPT_2) {
      auto data1 = generate_seq_data_basic(Sequence::NoteE, 2, Sequence::DurRel);
      Sequence::packNoteDots(data1.value, 1);
      seq2.unpackData(data1);
      auto data2 = generate_seq_data_basic(Sequence::NoteF, 2, Sequence::DurRel);
      Sequence::packNoteDots(data2.value, 1);
      seq2.unpackData(data2);
    }
  };
  tt.cb(TT_SCRIPT_1);
  tt.cb(TT_SCRIPT_2);
  clock.reset();

  unsigned long start_time = 1;

  // NoteOn (seq1 + seq2)
  TEST_ASSERT_EQUAL(clock.tick(start_time), true);
  // NoteOff (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 800), true);
  // NoteOn (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1000), true);
  // NoteOff (seq2)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1200), true);
  // NoteOn (seq2)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1500), true);
  // NoteOff (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1800), true);
  // NoteOn (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2000), true);
  // NoteOff (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2400), true);
  // NoteOn (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2500), true);
  // NoteOff (seq2)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2700), true);
  // NoteOff (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2900), true);

  // Loop!
  midiEvents.clear();

  // NoteOn (seq1 + seq2)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 3000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  TEST_ASSERT_NOTE_ON(1, Sequence::NoteE);
  midiEvents.clear();

  // NoteOff (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 3799), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 3800), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  midiEvents.clear();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 3801), false);

  // NoteOn (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 3999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 4000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB);
  midiEvents.clear();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 4001), false);

  // NoteOff (seq2)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 4199), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 4200), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteE);
  midiEvents.clear();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 4201), false);

  // ...
}

void test_clock_tick_multi_looping_uneven() {
  auto &seq1 = *fixture.seq1;
  auto &seq2 = *fixture.seq2;
  auto &clock = *fixture.clock;
  auto &tt = *fixture.tt;

  tt.cb = [&](script_number_t n) {
    if (n == TT_SCRIPT_1) {
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteA, 2, Sequence::DurRel));
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteB, 2, Sequence::DurRel));
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteC, 2, Sequence::DurRel));
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteD, 2, Sequence::DurRel));
    }
    if (n == TT_SCRIPT_2) {
      auto data1 = generate_seq_data_basic(Sequence::NoteE, 2, Sequence::DurRel);
      Sequence::packNoteDots(data1.value, 1);
      seq2.unpackData(data1);
      auto data2 = generate_seq_data_basic(Sequence::NoteF, 2, Sequence::DurRel);
      Sequence::packNoteDots(data2.value, 1);
      seq2.unpackData(data2);
    }
  };
  tt.cb(TT_SCRIPT_1);
  tt.cb(TT_SCRIPT_2);
  clock.reset();

  unsigned long start_time = 1;

  // NoteOn (seq1 + seq2)
  TEST_ASSERT_EQUAL(clock.tick(start_time), true);
  // NoteOff (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 800), true);
  // NoteOn (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1000), true);
  // NoteOff (seq2)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1200), true);
  // NoteOn (seq2)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1500), true);
  // NoteOff (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1800), true);
  // NoteOn (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2000), true);
  // NoteOff (seq2)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2700), true);
  // NoteOff (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2800), true);

  // Loop seq2!
  midiEvents.clear();

  // NoteOn (seq1 + seq2)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 3000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteD);
  TEST_ASSERT_NOTE_ON(1, Sequence::NoteE);
  midiEvents.clear();

  // NoteOff (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 3799), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 3800), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteD);
  midiEvents.clear();

  // Loop seq1!

  // NoteOn (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 3999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 4000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  midiEvents.clear();

  // NoteOff (seq2)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 4199), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 4200), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteE);
  midiEvents.clear();

  // NoteOn (seq2)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 4499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 4500), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteF);
  midiEvents.clear();

  // NoteOff (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 4799), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 4800), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  midiEvents.clear();
}

void test_clock_tick_multi_legato() {
  auto &seq1 = *fixture.seq1;
  auto &seq2 = *fixture.seq2;
  auto &clock = *fixture.clock;
  auto &tt = *fixture.tt;

  tt.cb = [&](script_number_t n) {
    if (n == TT_SCRIPT_1) {
      auto data1 = generate_seq_data_basic(Sequence::NoteA, 2, Sequence::DurRel);
      Sequence::packNoteLegato(data1.value, true);
      seq1.unpackData(data1);
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteB, 2, Sequence::DurRel));
    }
    if (n == TT_SCRIPT_2) {
      auto data2 = generate_seq_data_basic(Sequence::NoteC, 2, Sequence::DurRel);
      Sequence::packNoteLegato(data2.value, true);
      seq2.unpackData(data2);
      auto data3 = generate_seq_data_basic(Sequence::NoteD, 2, Sequence::DurRel);
      Sequence::packNoteLegato(data3.value, true);
      seq2.unpackData(data3);
    }
  };
  tt.cb(TT_SCRIPT_1);
  tt.cb(TT_SCRIPT_2);
  clock.reset();

  unsigned long start_time = 1;

  // NoteOn (seq1 + seq2)
  TEST_ASSERT_EQUAL(clock.tick(start_time), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  TEST_ASSERT_NOTE_ON(1, Sequence::NoteC);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 800), false);
  // NoteOn, deferred NoteOff (seq1 + seq2)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 4);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB);
  TEST_ASSERT_NOTE_OFF(1, Sequence::NoteA);
  TEST_ASSERT_NOTE_ON(2, Sequence::NoteD);
  TEST_ASSERT_NOTE_OFF(3, Sequence::NoteC);
  midiEvents.clear();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1001), false);

  // NoteOff (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1799), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1800), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB);
  midiEvents.clear();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1801), false);

  // Loop!

  // NoteOn  (seq1), NoteOn + deferred NoteOff (seq2)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 3);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  TEST_ASSERT_NOTE_ON(1, Sequence::NoteC);
  TEST_ASSERT_NOTE_OFF(2, Sequence::NoteD);
  midiEvents.clear();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2001), false);
}

void test_clock_tick_tempo_change() {
  auto &seq1 = *fixture.seq1;
  auto &clock = *fixture.clock;
  auto &tt = *fixture.tt;
  auto &settings = *fixture.settings;

  tt.cb = [&](script_number_t n) {
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteB, 4, Sequence::DurRel));
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteC, 4, Sequence::DurRel));
  };
  tt.cb(TEMP_SCRIPT);
  clock.setVoice(*fixture.v1, 0);

  unsigned long start_time = 1;

  // NoteOn
  TEST_ASSERT_EQUAL(clock.tick(start_time), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  midiEvents.clear();

  // NoteOff
  TEST_ASSERT_EQUAL(clock.tick(start_time + 399), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 400), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  midiEvents.clear();

  // NoteOn
  TEST_ASSERT_EQUAL(clock.tick(start_time + 499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 500), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB);
  midiEvents.clear();

  settings.clockTempo = 60;
  TEST_ASSERT_EQUAL(clock.tick(start_time + 750), false);
  // We were 250ms in to beat 2
  // at 120BPM, that's 1/2 of a beat (1/8th note)

  // beat 2 should have released 150ms later, + ended 250ms later
  // after changing tempo to 60BPM (half-time)
  // it should now release 300ms later, + end 500ms later

  // NoteOff
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1049), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1050), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB);
  midiEvents.clear();

  // NoteOn
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1149), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1250), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteC);
  midiEvents.clear();

  // NoteOff
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2049), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2050), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteC);
  midiEvents.clear();

  // loop...
  // NoteOn
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2249), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2250), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  // ...
}

void test_clock_tick_length_change() {
  auto &seq1 = *fixture.seq1;
  auto &clock = *fixture.clock;
  auto &tt = *fixture.tt;

  tt.cb = [&](script_number_t n) {
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteB, 4, Sequence::DurRel));
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteC, 4, Sequence::DurRel));
  };
  tt.cb(TEMP_SCRIPT);
  clock.setVoice(*fixture.v1, 0);

  unsigned long start_time = 1;

  // NoteOn
  TEST_ASSERT_EQUAL(clock.tick(start_time), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  midiEvents.clear();

  // NoteOff
  TEST_ASSERT_EQUAL(clock.tick(start_time + 399), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 400), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  midiEvents.clear();

  // NoteOn
  TEST_ASSERT_EQUAL(clock.tick(start_time + 499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 500), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 750), false);
  fixture.voiceSettings->configs[TT_SCRIPT_1].note_length = 25;
  clock.refreshVoice(0);
  // quarter-note = 500ms
  // 25% note length = release at 125ms (already passed)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 751), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB);
  midiEvents.clear();

  // NoteOn
  TEST_ASSERT_EQUAL(clock.tick(start_time + 999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteC);
  midiEvents.clear();

  // NoteOff
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1124), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1125), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteC);
  midiEvents.clear();

  // loop!
  // NoteOn
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1500), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  midiEvents.clear();

  // NoteOff
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1624), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1625), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  // ...
}

void test_clock_tick_seq_change_basic() {
  auto &seq1 = *fixture.seq1;
  auto &clock = *fixture.clock;
  auto &tt = *fixture.tt;

  tt.cb = [&](script_number_t n) {
    auto data1 = generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel);
    Sequence::packNoteOctave(data1.data[0], 1);
    seq1.unpackData(data1);
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteB, 4, Sequence::DurRel));
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteC, 4, Sequence::DurRel));
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteD, 4, Sequence::DurRel));
  };
  tt.cb(TEMP_SCRIPT);
  clock.setVoice(*fixture.v1, 0);

  unsigned long start_time = 1;

  // NoteOn
  TEST_ASSERT_EQUAL(clock.tick(start_time), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA + 12);
  midiEvents.clear();

  // NoteOff
  TEST_ASSERT_EQUAL(clock.tick(start_time + 399), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 400), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA + 12);
  midiEvents.clear();

  // NoteOn
  TEST_ASSERT_EQUAL(clock.tick(start_time + 499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 500), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB + 12);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 600), false);

  // Changing a sequence immediately effects any voices playing that sequence
  tt.cb = [&](script_number_t n) {
    seq1.resetData();
    auto data2 = generate_seq_data_basic(Sequence::NoteAb, 4, Sequence::DurRel);
    Sequence::packNoteOctave(data2.data[0], -2);
    seq1.unpackData(data2);
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteBb, 8, Sequence::DurRel));
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteDb, 8, Sequence::DurRel));
    auto data3 = generate_seq_data_basic(Sequence::NoteEb, 4, Sequence::DurRel);
    Sequence::packNoteOctave(data3.data[0], 1);
    seq1.unpackData(data3);
  };
  tt.run_script(*fixture.scene, TEMP_SCRIPT);
  clock.refreshSequence(seq1);

  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB + 12);
  midiEvents.clear();

  // Changing sequence waits until next full note to begin playing new sequence

  // TEST_ASSERT_EQUAL(clock.tick(start_time + 601), true);
  // TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  // TEST_ASSERT_NOTE_ON(0, Sequence::NoteBb -24);
  // midiEvents.clear();

  // TEST_ASSERT_EQUAL(clock.tick(start_time + 699), false);
  // TEST_ASSERT_EQUAL(clock.tick(start_time + 700), true);
  // TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  // TEST_ASSERT_NOTE_OFF(0, Sequence::NoteBb - 24);
  // midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 749), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 750), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteDb - 24);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 949), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 950), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteDb - 24);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteEb - 12);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1399), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1400), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteEb - 12);
  midiEvents.clear();

  // loop!
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1500), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteAb - 24);
  //...
}

void test_clock_tick_seq_change_adv() {
  auto &seq1 = *fixture.seq1;
  auto &clock = *fixture.clock;
  auto &tt = *fixture.tt;

  tt.cb = [&](script_number_t n) {
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel));
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel));
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel));
  };
  tt.cb(TEMP_SCRIPT);
  clock.setVoice(*fixture.v1, 0);

  unsigned long start_time = 1;

  // NoteOn
  TEST_ASSERT_EQUAL(clock.tick(start_time), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  midiEvents.clear();

  // NoteOff
  TEST_ASSERT_EQUAL(clock.tick(start_time + 399), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 400), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  midiEvents.clear();

  // NoteOn
  TEST_ASSERT_EQUAL(clock.tick(start_time + 499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 500), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 690), false);

  // Changing a sequence immediately effects any voices playing that sequence
  tt.cb = [&](script_number_t n) {
    seq1.resetData();
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteAb, 0, Sequence::DurRel));
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteBb, 8, Sequence::DurRel));
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteDb, 0, Sequence::DurRel));
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteEb, 0, Sequence::DurRel));
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteF, 0, Sequence::DurRel));
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteGb, 0, Sequence::DurRel));
  };
  tt.run_script(*fixture.scene, TEMP_SCRIPT);
  clock.refreshSequence(seq1);

  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 749), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 750), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteDb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 949), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 950), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteDb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteEb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1199), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1200), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteEb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1249), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1250), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteF);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1449), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1450), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteF);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1500), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteGb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1699), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1700), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteGb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1749), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1750), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteAb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 2149), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2150), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteAb);
  //...
}

void test_clock_chime_multi() {
  auto &seq1 = *fixture.seq1;
  auto &seq2 = *fixture.seq2;
  auto &clock = *fixture.clock;
  auto &tt = *fixture.tt;

  fixture.settings->clockTempo = 60;
  fixture.settings->loopLength = 4;

  tt.cb = [&](script_number_t n) {
    if (n == TT_SCRIPT_1) {
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteB, 4, Sequence::DurRel));
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteC, 4, Sequence::DurRel));
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteD, 4, Sequence::DurRel));
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteEb, 4, Sequence::DurRel));
    }
    if (n == TT_SCRIPT_2) {
      auto data1 = generate_seq_data_basic(Sequence::NoteE, 4, Sequence::DurRel);
      Sequence::packNoteDots(data1.value, 1);
      seq2.unpackData(data1);
      auto data2 = generate_seq_data_basic(Sequence::NoteF, 4, Sequence::DurRel);
      Sequence::packNoteDots(data2.value, 1);
      seq2.unpackData(data2);
    }
  };
  tt.cb(TT_SCRIPT_1);
  tt.cb(TT_SCRIPT_2);
  clock.reset();

  unsigned long start_time = 1;

  // NoteOn (seq1 + seq2)
  TEST_ASSERT_EQUAL(clock.tick(start_time), true);
  // NoteOff (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 800), true);
  // NoteOn (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1000), true);
  // NoteOff (seq2)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1200), true);
  // NoteOn (seq2)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1500), true);
  // NoteOff (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1800), true);
  // NoteOn (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2000), true);
  // NoteOff (seq2)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2700), true);
  // NoteOff (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2800), true);

  // Loop seq2!
  midiEvents.clear();

  // NoteOn (seq1 + seq2)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 3000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteD);
  TEST_ASSERT_NOTE_ON(1, Sequence::NoteE);
  midiEvents.clear();

  // NoteOff (seq1)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 3799), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 3800), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteD);
  midiEvents.clear();

  // Chime! (reset all)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 3999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 4000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 3);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteE);
  TEST_ASSERT_NOTE_ON(1, Sequence::NoteA);
  TEST_ASSERT_NOTE_ON(2, Sequence::NoteE);
  midiEvents.clear();

  // // NoteOff (seq2)
  // TEST_ASSERT_EQUAL(clock.tick(start_time + 4199), false);
  // TEST_ASSERT_EQUAL(clock.tick(start_time + 4200), true);
  // TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  // TEST_ASSERT_NOTE_OFF(0, Sequence::NoteE);
  // midiEvents.clear();

  // // NoteOn (seq2)
  // TEST_ASSERT_EQUAL(clock.tick(start_time + 4499), false);
  // TEST_ASSERT_EQUAL(clock.tick(start_time + 4500), true);
  // TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  // TEST_ASSERT_NOTE_ON(0, Sequence::NoteF);
  // midiEvents.clear();

  // // NoteOff (seq1)
  // TEST_ASSERT_EQUAL(clock.tick(start_time + 4799), false);
  // TEST_ASSERT_EQUAL(clock.tick(start_time + 4800), true);
  // TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  // TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  // midiEvents.clear();
}

void test_clock_sync_out() {
  auto &clock = *fixture.clock;

  fixture.settings->clockTempo = 60;

  unsigned long time = 1;

  do {
    clock.tick(time);
    time++;
  } while (time < 1000);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  clock.setMidiSync(Clock<MockTeletype, MockMidiService>::SyncOut);
  clock.tick(time);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_EQUAL(midiEvents[0].type, eMidiTypeClock);
  midiEvents.clear();

  // Calling tick twice in the same millisecond should not duplicate side-effect
  clock.tick(time);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  // 24ppq at 60BPM = 41.667ms between MIDI clocks
  clock.tick(time + 41);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  clock.tick(time + 42);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_EQUAL(midiEvents[0].type, eMidiTypeClock);
  midiEvents.clear();
  clock.tick(time + 43);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  clock.tick(time + 81);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  clock.tick(time + 84);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_EQUAL(midiEvents[0].type, eMidiTypeClock);
  midiEvents.clear();
  clock.tick(time + 85);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  clock.tick(time + 125);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  clock.tick(time + 126);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_EQUAL(midiEvents[0].type, eMidiTypeClock);
  midiEvents.clear();
  clock.tick(time + 127);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
}

void test_clock_sync_in_basic() {
  auto &seq1 = *fixture.seq1;
  auto &clock = *fixture.clock;
  auto &tt = *fixture.tt;

  tt.cb = [&](script_number_t n) {
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteB, 4, Sequence::DurRel));
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteC, 8, Sequence::DurRel));
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteD, 12, Sequence::DurRel));
  };
  tt.cb(TEMP_SCRIPT);
  clock.setVoice(*fixture.v1, 0);
  clock.setMidiSync(Clock<MockTeletype, MockMidiService>::SyncIn);

  unsigned long time = 1;

  for (; time < 500; time += 5) {
    clock.tick(time);
  }
  // Nothing happens if there's no MIDI clock in
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  // clockIn automatically triggers running state, in SyncIn mode
  // (tho we should also receive a MIDI SeqStart event as well)
  clock.midiClockTick();
  TEST_ASSERT_EQUAL(clock.tick(++time), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  midiEvents.clear();

  // Relative-length notes are locked to MIDI clock in, not time (or local BPM
  // setting)
  for (; time < 500; time += 5) {
    clock.tick(time);
  }
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  for (int t = 2; t < 19; t++) {
    clock.midiClockTick();
    clock.tick(++time);
  }
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  clock.midiClockTick(); // t = 19
  TEST_ASSERT_EQUAL(clock.tick(++time), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  midiEvents.clear();

  for (int t = 20; t < 24; t++) {
    clock.midiClockTick();
    clock.tick(++time);
  }
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  clock.midiClockTick();
  clock.tick(++time);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB);
  midiEvents.clear();

  for (int t = 25; t < 43; t++) {
    clock.midiClockTick();
    clock.tick(++time);
  }
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  clock.midiClockTick();
  clock.tick(++time);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB);
  midiEvents.clear();

  for (int t = 44; t < 48; t++) {
    clock.midiClockTick();
    clock.tick(++time);
  }
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  clock.midiClockTick();
  clock.tick(++time);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteC);
  midiEvents.clear();

  for (int t = 49; t < 57; t++) {
    clock.midiClockTick();
    clock.tick(++time);
  }
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  clock.midiClockTick();
  clock.tick(++time);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteC);
  midiEvents.clear();

  for (int t = 58; t < 60; t++) {
    clock.midiClockTick();
    clock.tick(++time);
  }
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  clock.midiClockTick();
  clock.tick(++time);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteD);
  midiEvents.clear();

  for (int t = 61; t < 66; t++) {
    clock.midiClockTick();
    clock.tick(++time);
  }
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  clock.midiClockTick();
  clock.tick(++time);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteD);
  midiEvents.clear();

  // Loop!
  for (int t = 67; t < 68; t++) {
    clock.midiClockTick();
    clock.tick(++time);
  }
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  clock.midiClockTick();
  clock.tick(++time);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  midiEvents.clear();

  for (int t = 69; t < 87; t++) {
    clock.midiClockTick();
    clock.tick(++time);
  }
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  clock.midiClockTick();
  clock.tick(++time);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);

  // ...
}

void test_clock_sync_in_absolute() {
  auto &seq1 = *fixture.seq1;
  auto &clock = *fixture.clock;
  auto &tt = *fixture.tt;

  clock.setMidiSync(Clock<MockTeletype, MockMidiService>::SyncIn);

  tt.cb = [&](script_number_t n) {
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteA, 100, Sequence::DurMs));
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteB, 200, Sequence::DurMs));
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteC, 2, Sequence::DurSec));
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteD, 40, Sequence::DurMs));
  };
  tt.cb(TEMP_SCRIPT);
  clock.setVoice(*fixture.v1, 0);

  unsigned long time = 1;

  for (; time < 500; time += 5) {
    clock.tick(time);
  }
  // Nothing happens if there's no MIDI clock in, or
  // if we have not received a MIDI seq start event
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  clock.midiSeqStart();
  clock.tick(++time);

  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  midiEvents.clear();

  clock.tick(time + 79);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  TEST_ASSERT_EQUAL(clock.tick(time + 80), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  midiEvents.clear();
  clock.tick(time + 81);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  // MIDI clock ticks mean nothing to absolute-duration notes
  for (int t = 0; t < 1000; t++)
    clock.midiClockTick();
  clock.tick(time + 99);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  clock.tick(time + 100);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB);
  midiEvents.clear();
  clock.tick(time + 101);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  clock.tick(time + 259);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  clock.tick(time + 260);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB);
  midiEvents.clear();
  clock.tick(time + 261);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  clock.tick(time + 299);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  clock.tick(time + 300);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteC);
  midiEvents.clear();
  clock.tick(time + 301);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  clock.tick(time + 1899);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  clock.tick(time + 1900);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteC);
  midiEvents.clear();
  clock.tick(time + 1901);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  clock.tick(time + 2299);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  clock.tick(time + 2300);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteD);
  midiEvents.clear();
  clock.tick(time + 2301);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  clock.tick(time + 2331);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  clock.tick(time + 2332);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteD);
  midiEvents.clear();
  clock.tick(time + 2333);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  // Loop!

  clock.tick(time + 2339);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  clock.tick(time + 2340);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  midiEvents.clear();
  clock.tick(time + 2341);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  clock.tick(time + 2419);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  clock.tick(time + 2420);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  midiEvents.clear();
  clock.tick(time + 2421);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  // ...
}

void test_clock_sync_in_mixed() {
  auto &seq1 = *fixture.seq1;
  auto &clock = *fixture.clock;
  auto &tt = *fixture.tt;

  clock.setMidiSync(Clock<MockTeletype, MockMidiService>::SyncIn);

  tt.cb = [&](script_number_t n) {
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteB, 200, Sequence::DurMs));
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteC, 8, Sequence::DurRel));
    seq1.unpackData(
        generate_seq_data_basic(Sequence::NoteD, 2, Sequence::DurSec));
  };
  tt.cb(TEMP_SCRIPT);
  clock.setVoice(*fixture.v1, 0);

  unsigned long time = 1;

  clock.midiClockTick();
  TEST_ASSERT_EQUAL(clock.tick(time), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  midiEvents.clear();

  // absolute time does not matter to relative-duration note..
  TEST_ASSERT_EQUAL(clock.tick(time + 100), false);
  TEST_ASSERT_EQUAL(clock.tick(time + 200), false);
  TEST_ASSERT_EQUAL(clock.tick(time + 300), false);
  TEST_ASSERT_EQUAL(clock.tick(time + 400), false);
  TEST_ASSERT_EQUAL(clock.tick(time + 500), false);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  for (int t = 1; t < 19; t++)
    clock.midiClockTick();

  TEST_ASSERT_EQUAL(clock.tick(time + 600), false);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  clock.midiClockTick();
  TEST_ASSERT_EQUAL(clock.tick(time + 601), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(time + 700), false);
  TEST_ASSERT_EQUAL(clock.tick(time + 800), false);
  TEST_ASSERT_EQUAL(clock.tick(time + 900), false);
  TEST_ASSERT_EQUAL(clock.tick(time + 1000), false);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  for (int t = 20; t < 24; t++)
    clock.midiClockTick();

  TEST_ASSERT_EQUAL(clock.tick(time + 1099), false);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  // This MIDI clock tick reaches the end of NoteA (quarter note == 24 pulses)
  // and begins absolute-duration NoteB
  clock.midiClockTick();
  TEST_ASSERT_EQUAL(clock.tick(time + 1100), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB);
  midiEvents.clear();

  // MIDI clock ticks do not effect absolute-duration notes
  for (int t = 25; t < 101; t++)
    clock.midiClockTick();
  TEST_ASSERT_EQUAL(clock.tick(time + 1259), false);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  // This absolute time value reaches "release" time for NoteB (80% length of
  // 200ms duration)
  TEST_ASSERT_EQUAL(clock.tick(time + 1260), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB);
  midiEvents.clear();
  clock.tick(time + 1261);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  clock.tick(time + 1299);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  // This absolute time value reaches the end of NoteB
  // and begins relative-duration NoteC
  TEST_ASSERT_EQUAL(clock.tick(time + 1300), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteC);
  midiEvents.clear();

  for (int t = 101; t < 109; t++)
    clock.midiClockTick();

  TEST_ASSERT_EQUAL(clock.tick(time + 1500), false);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  clock.midiClockTick();
  TEST_ASSERT_EQUAL(clock.tick(time + 1500), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteC);
  midiEvents.clear();

  for (int t = 110; t < 112; t++)
    clock.midiClockTick();

  TEST_ASSERT_EQUAL(clock.tick(time + 1599), false);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  // This MIDI clock tick reaches the end of NoteC,
  // and begins absolute-duration NoteD
  clock.midiClockTick();
  TEST_ASSERT_EQUAL(clock.tick(time + 1600), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteD);
  midiEvents.clear();

  // This absolute time reaches the "release" of NoteD
  TEST_ASSERT_EQUAL(clock.tick(time + 3199), false);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  TEST_ASSERT_EQUAL(clock.tick(time + 3200), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteD);
  midiEvents.clear();
  clock.tick(time + 3201);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  for (int t = 113; t < 200; t++)
    clock.midiClockTick();
  TEST_ASSERT_EQUAL(clock.tick(time + 3599), false);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);
  // Loop!

  // This absolute time reaches the end of NoteD,
  // and loops back to NoteA
  TEST_ASSERT_EQUAL(clock.tick(time + 3600), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  midiEvents.clear();

  for (int t = 200; t < 218; t++)
    clock.midiClockTick();

  TEST_ASSERT_EQUAL(clock.tick(time + 3900), false);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  clock.midiClockTick();
  TEST_ASSERT_EQUAL(clock.tick(time + 3901), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);

  // ...
}

// All voices
void test_clock_pause_run_many() {
  auto &seq1 = *fixture.seq1;
  auto &seq2 = *fixture.seq2;
  auto &seq3 = *fixture.seq3;
  auto &clock = *fixture.clock;
  auto &tt = *fixture.tt;
  auto &settings = *fixture.settings;

  settings.clockTempo = 60;

  tt.cb = [&](script_number_t n) {
    if (n == TT_SCRIPT_1) {
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteB, 4, Sequence::DurRel));
    }
    if (n == TT_SCRIPT_2) {
      seq2.unpackData(
          generate_seq_data_basic(Sequence::NoteAb, 8, Sequence::DurRel));
      seq2.unpackData(
          generate_seq_data_basic(Sequence::NoteBb, 8, Sequence::DurRel));
    }
    if (n == TT_SCRIPT_3) {
      seq3.unpackData(
          generate_seq_data_basic(Sequence::NoteRest, 300, Sequence::DurMs));
      seq3.unpackData(
          generate_seq_data_basic(Sequence::NoteC, 2, Sequence::DurSec));
    }
  };
  tt.cb(TT_SCRIPT_1);
  tt.cb(TT_SCRIPT_2);
  tt.cb(TT_SCRIPT_3);
  clock.reset();

  unsigned long start_time = 1;

  TEST_ASSERT_EQUAL(clock.tick(start_time), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  TEST_ASSERT_NOTE_ON(1, Sequence::NoteAb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 299), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 300), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteC);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 399), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 400), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteAb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 500), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteBb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 599), false);

  settings.isRunning = false;
  TEST_ASSERT_EQUAL(clock.tick(start_time + 600), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 3);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  TEST_ASSERT_NOTE_OFF(1, Sequence::NoteBb);
  TEST_ASSERT_NOTE_OFF(2, Sequence::NoteC);
  midiEvents.clear();

  for (int i = 601; i < 1000; i += 5) {
    TEST_ASSERT_EQUAL(clock.tick(start_time + i), 0);
  }
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  settings.isRunning = true;
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 3);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  TEST_ASSERT_NOTE_ON(1, Sequence::NoteBb);
  TEST_ASSERT_NOTE_ON(2, Sequence::NoteC);
  midiEvents.clear();

  // Paused at 600:
  // seq1 had 200ms until NoteOff
  // seq2 had 300ms until NoteOff
  // seq3 had 1300ms until NoteOff
  // Resumed at 1000
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1199), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1200), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1299), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1300), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteBb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1399), false);
  // Loop seq2!
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1400), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB);
  TEST_ASSERT_NOTE_ON(1, Sequence::NoteAb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1799), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1800), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteAb);
  midiEvents.clear();

  settings.isRunning = false;
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1850), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB);
  TEST_ASSERT_NOTE_OFF(1, Sequence::NoteC);
  midiEvents.clear();

  // Paused at 1850:
  // seq1 had 350ms until NoteOff
  // seq2 had 50ms until next note (already released)
  // seq3 had 450ms until NoteOff
  // Resumed at 2000
  settings.isRunning = true;
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB);
  TEST_ASSERT_NOTE_ON(1, Sequence::NoteC);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 2049), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2050), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteBb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 2349), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2350), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 2449), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2450), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteBb);
  TEST_ASSERT_NOTE_OFF(1, Sequence::NoteC);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 2549), false);
  // Loop seq1 + seq2!
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2550), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  TEST_ASSERT_NOTE_ON(1, Sequence::NoteAb);
  midiEvents.clear();

  // Loop seq3! (well, it's a 300ms rest first...)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2850), false);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  TEST_ASSERT_EQUAL(clock.tick(start_time + 2949), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2950), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteAb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 3049), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 3050), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteBb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 3149), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 3150), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteC);

  // ...
}

void test_clock_reset_run_many() {
  auto &seq1 = *fixture.seq1;
  auto &seq2 = *fixture.seq2;
  auto &seq3 = *fixture.seq3;
  auto &clock = *fixture.clock;
  auto &tt = *fixture.tt;

  fixture.settings->clockTempo = 60;

  tt.cb = [&](script_number_t n) {
    if (n == TT_SCRIPT_1) {
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteB, 4, Sequence::DurRel));
    }
    if (n == TT_SCRIPT_2) {
      seq2.unpackData(
          generate_seq_data_basic(Sequence::NoteAb, 8, Sequence::DurRel));
      seq2.unpackData(
          generate_seq_data_basic(Sequence::NoteBb, 8, Sequence::DurRel));
    }
    if (n == TT_SCRIPT_3) {
      seq3.unpackData(
          generate_seq_data_basic(Sequence::NoteRest, 300, Sequence::DurMs));
      seq3.unpackData(
          generate_seq_data_basic(Sequence::NoteC, 2, Sequence::DurSec));
    }
  };
  tt.cb(TT_SCRIPT_1);
  tt.cb(TT_SCRIPT_2);
  tt.cb(TT_SCRIPT_3);
  clock.reset();

  unsigned long start_time = 1;

  TEST_ASSERT_EQUAL(clock.tick(start_time), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  TEST_ASSERT_NOTE_ON(1, Sequence::NoteAb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 299), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 300), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteC);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 399), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 400), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteAb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 500), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteBb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 599), false);

  // Immediately turns any playing notes off
  clock.reset();
  TEST_ASSERT_EQUAL(midiEvents.size(), 3);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  TEST_ASSERT_NOTE_OFF(1, Sequence::NoteBb);
  TEST_ASSERT_NOTE_OFF(2, Sequence::NoteC);
  midiEvents.clear();

  // Goes back to beginning of sequence
  TEST_ASSERT_EQUAL(clock.tick(start_time + 600), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  TEST_ASSERT_NOTE_ON(1, Sequence::NoteAb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 899), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 900), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteC);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteAb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1099), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1100), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteBb);
}

// Individual voices
void test_voice_pause_run_many() {
  auto &seq1 = *fixture.seq1;
  auto &seq2 = *fixture.seq2;
  auto &seq3 = *fixture.seq3;
  auto &clock = *fixture.clock;
  auto &tt = *fixture.tt;
  auto &v1 = *fixture.v1;
  auto &v3 = *fixture.v3;

  fixture.settings->clockTempo = 60;

  tt.cb = [&](script_number_t n) {
    if (n == TT_SCRIPT_1) {
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteB, 4, Sequence::DurRel));
    }
    if (n == TT_SCRIPT_2) {
      seq2.unpackData(
          generate_seq_data_basic(Sequence::NoteAb, 8, Sequence::DurRel));
      seq2.unpackData(
          generate_seq_data_basic(Sequence::NoteBb, 8, Sequence::DurRel));
    }
    if (n == TT_SCRIPT_3) {
      seq3.unpackData(
          generate_seq_data_basic(Sequence::NoteRest, 300, Sequence::DurMs));
      seq3.unpackData(
          generate_seq_data_basic(Sequence::NoteC, 2, Sequence::DurSec));
    }
  };
  tt.cb(TT_SCRIPT_1);
  tt.cb(TT_SCRIPT_2);
  tt.cb(TT_SCRIPT_3);
  clock.reset();

  unsigned long start_time = 1;

  TEST_ASSERT_EQUAL(clock.tick(start_time), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  TEST_ASSERT_NOTE_ON(1, Sequence::NoteAb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 299), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 300), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteC);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 399), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 400), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteAb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 500), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteBb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 599), false);
  v1.pause();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 600), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 899), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 900), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteBb);
  midiEvents.clear();

  // Loop seq2!

  TEST_ASSERT_EQUAL(clock.tick(start_time + 999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteAb);
  midiEvents.clear();

  // Paused at 600:
  // seq1 had 200ms until NoteOff, 400ms until next note
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1099), false);
  v1.play();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1100), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1199), false);
  v3.pause();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1200), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteC);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1299), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1300), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1399), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1400), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteAb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1500), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB);
  TEST_ASSERT_NOTE_ON(1, Sequence::NoteBb);
  midiEvents.clear();

  // Paused at 1200:
  // seq3 had 700ms until NoteOff, 1100ms until loop
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1599), false);
  v3.play();
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1600), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteC);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1899), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1900), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteBb);
  midiEvents.clear();

  // Loop seq2 again!
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteAb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 2299), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2300), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB);
  TEST_ASSERT_NOTE_OFF(1, Sequence::NoteC);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 2399), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2400), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteAb);
  midiEvents.clear();

  // Loop seq1!
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2500), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  TEST_ASSERT_NOTE_ON(1, Sequence::NoteBb);
  midiEvents.clear();

  // Loop seq3! (but first step is a rest)
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2700), false);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  // ...
}

void test_voice_reset_run_many() {
  auto &seq1 = *fixture.seq1;
  auto &seq2 = *fixture.seq2;
  auto &seq3 = *fixture.seq3;
  auto &clock = *fixture.clock;
  auto &tt = *fixture.tt;

  fixture.settings->clockTempo = 60;

  tt.cb = [&](script_number_t n) {
    if (n == TT_SCRIPT_1) {
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteA, 4, Sequence::DurRel));
      seq1.unpackData(
          generate_seq_data_basic(Sequence::NoteB, 4, Sequence::DurRel));
    }
    if (n == TT_SCRIPT_2) {
      seq2.unpackData(
          generate_seq_data_basic(Sequence::NoteAb, 8, Sequence::DurRel));
      seq2.unpackData(
          generate_seq_data_basic(Sequence::NoteBb, 8, Sequence::DurRel));
    }
    if (n == TT_SCRIPT_3) {
      seq3.unpackData(
          generate_seq_data_basic(Sequence::NoteRest, 300, Sequence::DurMs));
      seq3.unpackData(
          generate_seq_data_basic(Sequence::NoteC, 2, Sequence::DurSec));
    }
  };
  tt.cb(TT_SCRIPT_1);
  tt.cb(TT_SCRIPT_2);
  tt.cb(TT_SCRIPT_3);
  clock.reset();

  unsigned long start_time = 1;

  TEST_ASSERT_EQUAL(clock.tick(start_time), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  TEST_ASSERT_NOTE_ON(1, Sequence::NoteAb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 299), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 300), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteC);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 399), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 400), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteAb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 500), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteBb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 599), false);
  clock.resetVoice(0);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  midiEvents.clear();

  // Seq1 immediately restarts from beginning
  TEST_ASSERT_EQUAL(clock.tick(start_time + 600), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 899), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 900), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteBb);
  midiEvents.clear();

  // Loop seq2!
  TEST_ASSERT_EQUAL(clock.tick(start_time + 999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteAb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1199), false);
  clock.resetVoice(2);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteC);
  midiEvents.clear();

  // seq3's first step is a rest
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1200), false);
  TEST_ASSERT_EQUAL(midiEvents.size(), 0);

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1399), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1400), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteA);
  TEST_ASSERT_NOTE_OFF(1, Sequence::NoteAb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1500), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteBb);
  TEST_ASSERT_NOTE_ON(1, Sequence::NoteC);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1599), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1600), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteB);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 1899), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1900), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteBb);
  midiEvents.clear();

  // Loop seq2 again!
  TEST_ASSERT_EQUAL(clock.tick(start_time + 1999), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2000), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteAb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 2399), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2400), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 2);
  TEST_ASSERT_NOTE_OFF(0, Sequence::NoteB);
  TEST_ASSERT_NOTE_OFF(1, Sequence::NoteAb);
  midiEvents.clear();

  TEST_ASSERT_EQUAL(clock.tick(start_time + 2499), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2500), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteBb);
  midiEvents.clear();

  // Loop seq1!
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2599), false);
  TEST_ASSERT_EQUAL(clock.tick(start_time + 2600), true);
  TEST_ASSERT_EQUAL(midiEvents.size(), 1);
  TEST_ASSERT_NOTE_ON(0, Sequence::NoteA);
  midiEvents.clear();

  // ...
}

int main() {
  UNITY_BEGIN();
  RUN_TEST_WITH_SETUP(test_clock_empty_seq_schedule_parsing);
  RUN_TEST_WITH_SETUP(test_clock_seq_schedule_parsing_basic_rel);
  RUN_TEST_WITH_SETUP(test_clock_seq_schedule_parsing_advanced_rel);
  RUN_TEST_WITH_SETUP(test_clock_seq_schedule_parsing_basic_abs);
  RUN_TEST_WITH_SETUP(test_clock_seq_schedule_parsing_mixed);
  RUN_TEST_WITH_SETUP(test_clock_seq_schedule_parsing_basic_dots);
  RUN_TEST_WITH_SETUP(test_clock_seq_schedule_parsing_basic_length);
  RUN_TEST_WITH_SETUP(test_clock_seq_schedule_parsing_basic_repeats);
  RUN_TEST_WITH_SETUP(test_clock_seq_schedule_parsing_fast);
  RUN_TEST_WITH_SETUP(test_clock_seq_schedule_parsing_slow);
  RUN_TEST_WITH_SETUP(test_clock_tick_empty_schedule);
  RUN_TEST_WITH_SETUP(test_clock_tick_basic);
  RUN_TEST_WITH_SETUP(test_clock_tick_looping);
  RUN_TEST_WITH_SETUP(test_clock_tick_looping_off);
  RUN_TEST_WITH_SETUP(test_clock_tick_legato);
  RUN_TEST_WITH_SETUP(test_clock_tick_legato_looping_off);
  RUN_TEST_WITH_SETUP(test_clock_tick_repeats);
  RUN_TEST_WITH_SETUP(test_clock_tick_repeats_octave);
  RUN_TEST_WITH_SETUP(test_clock_tick_multi, 2);
  RUN_TEST_WITH_SETUP(test_clock_tick_multi_looping, 2);
  RUN_TEST_WITH_SETUP(test_clock_tick_multi_looping_uneven, 2);
  RUN_TEST_WITH_SETUP(test_clock_tick_multi_legato, 2);
  RUN_TEST_WITH_SETUP(test_clock_tick_tempo_change);
  RUN_TEST_WITH_SETUP(test_clock_tick_length_change);
  RUN_TEST_WITH_SETUP(test_clock_tick_seq_change_basic);
  RUN_TEST_WITH_SETUP(test_clock_tick_seq_change_adv);
  RUN_TEST_WITH_SETUP(test_clock_chime_multi, 2);
  RUN_TEST_WITH_SETUP(test_clock_sync_out);
  RUN_TEST_WITH_SETUP(test_clock_sync_in_basic);
  RUN_TEST_WITH_SETUP(test_clock_sync_in_absolute);
  RUN_TEST_WITH_SETUP(test_clock_sync_in_mixed);
  RUN_TEST_WITH_SETUP(test_clock_pause_run_many, 3);
  RUN_TEST_WITH_SETUP(test_clock_reset_run_many, 3);
  RUN_TEST_WITH_SETUP(test_voice_pause_run_many, 3);
  RUN_TEST_WITH_SETUP(test_voice_reset_run_many, 3);
  UNITY_END();
  return 0;
}

} // namespace TestClock
