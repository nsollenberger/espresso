#include <stdint.h>
#include <tc.h>
#include <timers.h>
#include <unity.h>

namespace TestTimers {

void test_timers_tick() {
  Timers timers;
  timers.initAndRun();
  uint64_t ticks = timers.getTicks();
  TEST_ASSERT_EQUAL(ticks, 0);

  Tc::tick();
  ticks = timers.getTicks();
  TEST_ASSERT_EQUAL(ticks, 1);

  Tc::tick();
  ticks = timers.getTicks();
  TEST_ASSERT_EQUAL(ticks, 2);

  Tc::tick();
  ticks = timers.getTicks();
  TEST_ASSERT_EQUAL(ticks, 3);
}

static uint32_t flag_1{0};
static bool flag1_set(void *value) {
  flag_1 = *static_cast<uint32_t *>(value);
  return false;
}
static bool flag1_bitset(void *bit) {
  flag_1 |= 1 << *static_cast<uint32_t *>(bit);
  return false;
}

bool empty_fn(void *data) { return false; }

void test_timers_add() {
  Timers timers;
  timers.initAndRun();
  Timers::softTimer_t timer1;
  uint32_t testArg = 42;

  bool result = timers.add(timer1, 10, empty_fn, &testArg);

  TEST_ASSERT_EQUAL(result, true);
  TEST_ASSERT_EQUAL(timer1.ticks, 10);
  TEST_ASSERT_EQUAL(timer1.ticksRemain, 10);
  TEST_ASSERT_EQUAL(timer1.callback, &empty_fn);
  TEST_ASSERT_EQUAL(timer1.userArg, &testArg);
}

void test_timers_callbacks() {
  Timers timers;
  timers.initAndRun();
  Timers::softTimer_t timer1;
  Timers::softTimer_t timer2;
  Timers::softTimer_t timer3;
  Timers::softTimer_t timer4;

  uint32_t val1{1};
  uint32_t val2{2};
  uint32_t val3{0xffff};
  uint32_t val4{0};

  timers.add(timer1, 2, flag1_bitset, &val1);
  timers.add(timer2, 3, flag1_bitset, &val2);
  timers.add(timer3, 4, flag1_set, &val3);
  timers.add(timer4, 5, flag1_set, &val4);

  // Tick 1: none
  Tc::tick();
  TEST_ASSERT_EQUAL(flag_1, 0);

  // Tick 2: timer1 called
  Tc::tick();
  TEST_ASSERT_EQUAL(flag_1, 0x02);

  // Tick 3: timer2 called
  Tc::tick();
  TEST_ASSERT_EQUAL(flag_1, 0x06);

  // Tick 4: timer1 + timer3 called
  Tc::tick();
  TEST_ASSERT_EQUAL(flag_1, 0xffff);

  // Tick 5: timer4 called
  Tc::tick();
  TEST_ASSERT_EQUAL(flag_1, 0);
}

static uint32_t flag_2{0};
static bool flag2_incr(void *_) {
  flag_2++;
  return false;
}

void test_timers_update_remove() {
  Timers timers;
  timers.initAndRun();
  Timers::softTimer_t timer1;

  timers.add(timer1, 1, flag2_incr, nullptr);

  Tc::tick();
  TEST_ASSERT_EQUAL(flag_2, 1);

  Tc::tick();
  TEST_ASSERT_EQUAL(flag_2, 2);

  // Update does not reset ticksRemaining
  timers.update(timer1, 2);
  Tc::tick();
  TEST_ASSERT_EQUAL(flag_2, 3);

  // Now we see new duration taking effect
  Tc::tick();
  TEST_ASSERT_EQUAL(flag_2, 3);

  Tc::tick();
  TEST_ASSERT_EQUAL(flag_2, 4);

  Tc::tick();
  TEST_ASSERT_EQUAL(flag_2, 4);
}

static uint32_t flag_3{0};
static bool flag3_incr(void *_) {
  flag_3++;
  return false;
}

void test_timers_reset() {
  Timers timers;
  timers.initAndRun();
  Timers::softTimer_t timer1;

  timers.add(timer1, 3, flag3_incr, nullptr);

  Tc::tick();
  Tc::tick();
  TEST_ASSERT_EQUAL(flag_3, 0);

  Tc::tick();
  TEST_ASSERT_EQUAL(flag_3, 1);

  Tc::tick();
  Tc::tick();
  TEST_ASSERT_EQUAL(flag_3, 1);

  timers.reset(timer1);
  Tc::tick();
  Tc::tick();
  TEST_ASSERT_EQUAL(flag_3, 1);

  Tc::tick();
  TEST_ASSERT_EQUAL(flag_3, 2);
}

int main() {
  UNITY_BEGIN();
  RUN_TEST(test_timers_tick);
  RUN_TEST(test_timers_add);
  RUN_TEST(test_timers_callbacks);
  RUN_TEST(test_timers_update_remove);
  RUN_TEST(test_timers_reset);
  UNITY_END();
  return 0;
}

} // namespace TestTimers