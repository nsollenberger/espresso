#ifndef _TEST_HELPERS_H__
#define _TEST_HELPERS_H__

#define VOICE_OCT_OFFSET 23

#define TEST_ASSERT_NOTE_ON(index, note)                                       \
  TEST_ASSERT_EQUAL(eMidiTypeNote, midiEvents[index].type);                    \
  TEST_ASSERT_EQUAL(100, midiEvents[index].data2);                             \
  TEST_ASSERT_EQUAL(VOICE_OCT_OFFSET + note, midiEvents[index].data1);

#define TEST_ASSERT_NOTE_OFF(index, note)                                      \
  TEST_ASSERT_EQUAL(eMidiTypeNote, midiEvents[index].type);                    \
  TEST_ASSERT_EQUAL(0, midiEvents[index].data2);                               \
  TEST_ASSERT_EQUAL(VOICE_OCT_OFFSET + note, midiEvents[index].data1);

#define TEST_ASSERT_CV_SET_SLEW(index, value)                                  \
  TEST_ASSERT_EQUAL(eCvSetSlew, cvEvents[index].type);                         \
  TEST_ASSERT_EQUAL(value, cvEvents[index].data1);

#define TEST_ASSERT_CV_SET_VALUE(index, value, slew)                           \
  TEST_ASSERT_EQUAL(eCvSetValue, cvEvents[index].type);                        \
  TEST_ASSERT_EQUAL(value, cvEvents[index].data1);                             \
  TEST_ASSERT_EQUAL(slew, cvEvents[index].data2);


#endif
