#include <stdint.h>
#include <unity.h>

#include <ops/op.h>
#include <ops/op_enum.h>
#include <espresso/command.h>
#include <espresso/command_state.h>
#include <espresso/exec_state.h>
#include <espresso/parser.h>
#include <random.h>
#include <sequence.h>
#include <scene.h>
#include <voice.h>

#include "mock_teleteype.h"
#include "teletype_stubs.h"

namespace TestVoice {
Espresso::Token generate_seq_data_basic(Sequence::NoteName note, int duration,
                                    Sequence::DurationUnit unit);
}

namespace TestTeletypeOps {

using Espresso::CommandState;
using Espresso::ExecState;
using TestVoice::generate_seq_data_basic;

void test_get_set_variables() {
  tele_op_t op_B = *tele_ops[E_OP_B];

  MockTeletype tt;
  Scene &scene = tt.scene;
  ExecState es;

  es.push();

  // run_script()
  es.setScript(TEMP_SCRIPT);
  es.setLine(0);

  // B
  {
    // process_command()
    CommandState cs;

    op_B.get(op_B.data, &scene, &es, &cs);
    TEST_ASSERT_EQUAL(cs.size(), 1);
    TEST_ASSERT_EQUAL(cs.pop(), 2);
  }

  // B 105
  {
    // process_command()
    CommandState cs;
    cs.push(105);

    op_B.set(op_B.data, &scene, &es, &cs);
    TEST_ASSERT_EQUAL(cs.size(), 0);
  }

  // B
  {
    // process_command()
    CommandState cs;

    op_B.get(op_B.data, &scene, &es, &cs);
    TEST_ASSERT_EQUAL(cs.size(), 1);
    TEST_ASSERT_EQUAL(cs.pop(), 105);
  }
}

void test_get_set_rand_seed() {
  tele_op_t op_RAND_SEED = *tele_ops[E_OP_RAND_SEED];

  Port::rand_val = 18474;
  MockTeletype tt;
  Scene &scene = tt.scene;
  ExecState es;

  es.push();

  // run_script()
  es.setScript(TEMP_SCRIPT);
  es.setLine(0);

  // RAND.SEED
  {
    // process_command()
    CommandState cs;

    op_RAND_SEED.get(op_RAND_SEED.data, &scene, &es, &cs);
    TEST_ASSERT_EQUAL(cs.size(), 1);
    // mocked value
    TEST_ASSERT_EQUAL(cs.pop(), 18474);
  }

  // RAND.SEED 123
  {
    // process_command()
    CommandState cs;
    cs.push(123);

    op_RAND_SEED.set(op_RAND_SEED.data, &scene, &es, &cs);
    TEST_ASSERT_EQUAL(cs.size(), 0);
  }

  // RAND.SEED
  {
    // process_command()
    CommandState cs;

    op_RAND_SEED.get(op_RAND_SEED.data, &scene, &es, &cs);
    TEST_ASSERT_EQUAL(cs.size(), 1);
    TEST_ASSERT_EQUAL(cs.pop(), 123);
  }
}

void test_mod_opt_params() {
  auto mod_IF = *tele_mods[E_MOD_IF];
  TEST_ASSERT_EQUAL(mod_IF.params, 1);
  TEST_ASSERT_EQUAL(mod_IF.opt_params, 0);

  auto mod_N = *tele_mods[E_MOD_SEQ];
  TEST_ASSERT_EQUAL(mod_N.params, 0);
  TEST_ASSERT_EQUAL(mod_N.opt_params, 1);

  // ----------

  MockTeletype tt;
  Scene &scene = tt.scene;

  ExecState es;
  Espresso::Command post_command = {};
  post_command.comment = false;
  post_command.length = 1;
  post_command.data[0] = generate_seq_data_basic(Sequence::NoteAb, 4, Sequence::DurRel);

  es.push();

  // run_script()
  es.setScript(TT_SCRIPT_3);
  es.setLine(0);

  TEST_ASSERT_EQUAL(es.current()->script_number, TT_SCRIPT_3);

  // N: A-
  {
    TEST_ASSERT_EQUAL(scene.voices[TT_SCRIPT_3].seq.length(), 0);

    // process_command()
    CommandState cs;

    mod_N.func(&scene, &es, &cs, &post_command);
    TEST_ASSERT_EQUAL(cs.size(), 0);

    // assert sequence 1 has been updated
    TEST_ASSERT_EQUAL(scene.voices[TT_SCRIPT_3].seq.length(), 1);
  }
}

int main() {
  UNITY_BEGIN();
  RUN_TEST(test_get_set_variables);
  RUN_TEST(test_get_set_rand_seed);
  RUN_TEST(test_mod_opt_params);
  UNITY_END();
  return 0;
}

} // namespace TestTeletypeOps
