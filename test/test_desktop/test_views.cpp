#include <unity.h>

#include <hid_keys.h>
#include <line_editor.h>
#include <models/edit_mode.h>
#include <models/help_mode.h>
#include <models/voice_settings.h>
#include <region.h>
#include <screen.h>
#include <views/editor.h>
#include <views/help.h>
#include <views/voice_settings_1.h>

#include "ascii_buffer.h"
#include "mock_teleteype.h"
#include "models/settings.h"
#include "views/settings_1.h"

namespace TestViews {

void test_settings_default() {
  screen_buffer_t screen_buf = {{0, 0},  {0, 8},  {0, 16}, {0, 24},
                              {0, 32}, {0, 40}, {0, 48}, {0, 56}};
  Screen screen(screen_buf);

  Models::Settings model;
  Views::Settings_1 view(model);

  model.activate();
  view.render(screen);

  model.clockTempo = 120.83;
  model.dirty = true;
  view.render(screen);

  AsciiFileSnapshot snapshot(screen_buf);
  TEST_ASSERT_EQUAL(snapshot.save("settings_1.txt"), true);
}

void test_settings_scroll() {
  screen_buffer_t screen_buf = {{0, 0},  {0, 8},  {0, 16}, {0, 24},
                              {0, 32}, {0, 40}, {0, 48}, {0, 56}};
  Screen screen(screen_buf);

  Models::Settings model;
  Views::Settings_1 view(model);

  model.activate();
  view.render(screen);

  model.row_index = 0;
  model.col_index = 1;
  model.dirty = true;
  view.render(screen);

  AsciiFileSnapshot snapshot(screen_buf);
  TEST_ASSERT_EQUAL(snapshot.save("settings_2.txt"), true);
}


void test_editor_default() {
  screen_buffer_t screen_buf = {{0, 0},  {0, 8},  {0, 16}, {0, 24},
                              {0, 32}, {0, 40}, {0, 48}, {0, 56}};
  Screen screen(screen_buf);

  MockTeletype tt;
  LineEditor le;
  Models::EditMode model(tt.scene, le);
  Views::Editor view(model);

  model.activate();
  view.render(screen);

  AsciiFileSnapshot snapshot(screen_buf);
  TEST_ASSERT_EQUAL(snapshot.match("editor.txt"), true);
}

void test_voice_settings_1_default() {
  screen_buffer_t screen_buf = {{0, 0},  {0, 8},  {0, 16}, {0, 24},
                              {0, 32}, {0, 40}, {0, 48}, {0, 56}};
  Screen screen(screen_buf);

  Models::VoiceSettings model;
  Views::VoiceSettings_1 view(model);

  model.activate();
  view.render(screen);

  AsciiFileSnapshot snapshot(screen_buf);
  TEST_ASSERT_EQUAL(snapshot.match("voice_settings_1.txt"), true);
}

void test_voice_settings_1_scroll() {
  screen_buffer_t screen_buf = {{0, 0},  {0, 8},  {0, 16}, {0, 24},
                              {0, 32}, {0, 40}, {0, 48}, {0, 56}};
  Screen screen(screen_buf);

  Models::VoiceSettings model;
  Views::VoiceSettings_1 view(model);

  model.activate();
  view.render(screen);

  model.selected_index = 4;
  model.back_to_top = false;
  model.dirty = true;
  view.render(screen);

  model.selected_index = 5;
  model.dirty = true;
  view.render(screen);

  AsciiFileSnapshot snapshot(screen_buf);
  TEST_ASSERT_EQUAL(snapshot.match("voice_settings_2.txt"), true);
}

void test_voice_settings_1_scroll_up() {
  screen_buffer_t screen_buf = {{0, 0},  {0, 8},  {0, 16}, {0, 24},
                              {0, 32}, {0, 40}, {0, 48}, {0, 56}};
  Screen screen(screen_buf);

  Models::VoiceSettings model;
  Views::VoiceSettings_1 view(model);

  model.activate();
  view.render(screen);

  model.selected_index = 5;
  model.back_to_top = false;
  model.dirty = true;
  // Scroll offset is re-calculated during render
  view.render(screen);

  model.selected_index = 3;
  model.dirty = true;
  view.render(screen);

  AsciiFileSnapshot snapshot(screen_buf);
  TEST_ASSERT_EQUAL(snapshot.match("voice_settings_3.txt"), true);
}

void test_voice_settings_1_is_editing() {
  screen_buffer_t screen_buf = {{0, 0},  {0, 8},  {0, 16}, {0, 24},
                              {0, 32}, {0, 40}, {0, 48}, {0, 56}};
  Screen screen(screen_buf);

  Models::VoiceSettings model;
  Views::VoiceSettings_1 view(model);

  model.activate();
  view.render(screen);

  model.is_editing = true;
  model.configs[0].cv_out = 3;
  model.configs[0].gate_out = 0;
  model.selected_index = 1;
  model.dirty = true;
  view.render(screen);

  AsciiFileSnapshot snapshot(screen_buf);
  TEST_ASSERT_EQUAL(snapshot.match("voice_settings_4.txt"), true);
}

void test_help_mode_default() {
  screen_buffer_t screen_buf = {{0, 0},  {0, 8},  {0, 16}, {0, 24},
                              {0, 32}, {0, 40}, {0, 48}, {0, 56}};
  Screen screen(screen_buf);

  LineEditor le;
  Models::HelpMode model(le);
  Views::Help view(model);

  model.activate();
  view.render(screen);

  AsciiFileSnapshot snapshot(screen_buf);
  TEST_ASSERT_EQUAL(snapshot.match("help.txt"), true);
}

void test_help_mode_scroll() {
  screen_buffer_t screen_buf = {{0, 0},  {0, 8},  {0, 16}, {0, 24},
                              {0, 32}, {0, 40}, {0, 48}, {0, 56}};
  Screen screen(screen_buf);

  LineEditor le;
  Models::HelpMode model(le);
  Views::Help view(model);

  model.activate();
  view.render(screen);

  model.offset = 3;
  model.dirty = true;

  view.render(screen);

  AsciiFileSnapshot snapshot(screen_buf);
  TEST_ASSERT_EQUAL(snapshot.match("help_2.txt"), true);
}

void test_help_mode_search() {
  screen_buffer_t screen_buf = {{0, 0},  {0, 8},  {0, 16}, {0, 24},
                              {0, 32}, {0, 40}, {0, 48}, {0, 56}};
  Screen screen(screen_buf);

  LineEditor le;
  Models::HelpMode model(le);
  Views::Help view(model);

  model.activate();
  view.render(screen);

  model.processKeypress(HID_F, HID_MODIFIER_LEFT_CTRL, false);
  model.processKeypress(HID_C, HID_MODIFIER_NONE, false);
  model.processKeypress(HID_A, HID_MODIFIER_NONE, false);
  model.processKeypress(HID_T, HID_MODIFIER_NONE, false);
  model.processKeypress(HID_ENTER, HID_MODIFIER_NONE, false);

  view.render(screen);

  AsciiFileSnapshot snapshot(screen_buf);
  TEST_ASSERT_EQUAL(snapshot.match("help_3.txt"), true);
}

int main() {
  UNITY_BEGIN();
  RUN_TEST(test_settings_default);
  RUN_TEST(test_settings_scroll);
  RUN_TEST(test_editor_default);
  RUN_TEST(test_voice_settings_1_default);
  RUN_TEST(test_voice_settings_1_scroll);
  RUN_TEST(test_voice_settings_1_scroll_up);
  RUN_TEST(test_voice_settings_1_is_editing);
  RUN_TEST(test_help_mode_default);
  RUN_TEST(test_help_mode_scroll);
  RUN_TEST(test_help_mode_search);
  UNITY_END();
  return 0;
}

} // namespace TestViews
