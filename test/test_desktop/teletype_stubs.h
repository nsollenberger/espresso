#ifndef _TELETYPE_STUBS_H__
#define _TELETYPE_STUBS_H__

#include <vector>

enum midi_event_type {
  eMidiTypeNote,
  eMidiTypeCC,
  eMidiTypeClock
};

struct midi_event {
  midi_event_type type;
  uint8_t ch;
  uint8_t data1;
  uint8_t data2;
};

extern std::vector<midi_event> midiEvents;

extern bool tr_state[4];
extern uint16_t cv_state[4];

enum cv_event_type {
  eCvSetValue,
  eCvSetSlew
};

struct cv_event {
  cv_event_type type;
  uint8_t ch;
  int16_t data1;
  bool data2;
};

extern std::vector<cv_event> cvEvents;

#endif