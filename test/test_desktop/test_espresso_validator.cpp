#include <stdint.h>
#include <unity.h>

#include <ops/op_enum.h>
#include <espresso/command.h>
#include <espresso/validator.h>
#include <scene.h>
#include <sequence.h>
#include <voice.h>

#include "mock_teleteype.h"

using Espresso::Command;
using Espresso::validate;

namespace TestVoice {
Espresso::Token generate_seq_data_basic(Sequence::NoteName note, int duration,
                                    Sequence::DurationUnit unit);
}

namespace TestEspressoValidator {

using TestVoice::generate_seq_data_basic;

void test_returns_ok_on_valid_op() {
  Command command = { 3, -1, false, {
              {.tag = Espresso::OP, .value = E_OP_ADD},
              {.tag = Espresso::NUMBER, .value = 2},
              {.tag = Espresso::NUMBER, .value = 0},
  }};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = validate(&command, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
}

void test_returns_ok_on_valid_mod_sep_op() {
  Command command = { 6, 2, false, {
              {.tag = Espresso::MOD, .value = E_MOD_IF},
              {.tag = Espresso::NUMBER, .value = 2},
              {.tag = Espresso::PRE_SEP, .value = 0},
              {.tag = Espresso::OP, .value = E_OP_CV},
              {.tag = Espresso::NUMBER, .value = 4},
              {.tag = Espresso::NUMBER, .value = 1},
  }};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = validate(&command, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
}

void test_returns_ok_on_valid_op_sub_op() {
  Command command = { 4, -1, false, {
              {.tag = Espresso::OP, .value = E_OP_X},
              {.tag = Espresso::OP, .value = E_OP_IN},
              {.tag = Espresso::SUB_SEP, .value = 0},
              {.tag = Espresso::OP, .value = E_OP_SEED},
  }};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = validate(&command, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
}

void test_returns_ok_on_valid_op_rnd() {
  Command command = { 3, -1, false, {
              {.tag = Espresso::OP, .value = E_OP_Z},
              {.tag = Espresso::OP, .value = E_OP_RND},
              {.tag = Espresso::NUMBER, .value = 10},
  }};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = validate(&command, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
}

void test_validates_seq_mod_alda_basic() {
  Command command = { 7, 2, false, {
              {.tag = Espresso::MOD, .value = E_MOD_SEQ},
              {.tag = Espresso::NUMBER, .value = 3},
              {.tag = Espresso::PRE_SEP, .value = 0},
              generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel),
              generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel),
              generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel),
              generate_seq_data_basic(Sequence::NoteD, 0, Sequence::DurRel),
  }};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = validate(&command, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
}

void test_validates_seq_mod_alda_sharp_flat() {
  Command command = { 7, 2, false, {
              {.tag = Espresso::MOD, .value = E_MOD_SEQ},
              {.tag = Espresso::NUMBER, .value = 3},
              {.tag = Espresso::PRE_SEP, .value = 0},
              // sharps are actually encoded as flats
              generate_seq_data_basic(Sequence::NoteBb, 0, Sequence::DurRel),
              generate_seq_data_basic(Sequence::NoteBb, 0, Sequence::DurRel),
              generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel),
              generate_seq_data_basic(Sequence::NoteEb, 0, Sequence::DurRel),
  }};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = validate(&command, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
}

void test_validates_seq_mod_alda_chords() {
  auto data1 = generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel);
  Sequence::packNoteName(data1.data[1], Sequence::NoteBb);

  auto data2 = generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel);
  Sequence::packNoteName(data2.data[1], Sequence::NoteE);
  Sequence::packNoteName(data2.data[2], Sequence::NoteG);

  auto data3 = generate_seq_data_basic(Sequence::NoteD, 0, Sequence::DurRel);
  Sequence::packNoteName(data3.data[1], Sequence::NoteEb);
  Sequence::packNoteName(data3.data[2], Sequence::NoteF);
  Sequence::packNoteName(data3.data[3], Sequence::NoteAb);

  Command command = { 7, 2, false, {
              {.tag = Espresso::MOD, .value = E_MOD_SEQ},
              {.tag = Espresso::NUMBER, .value = 3},
              {.tag = Espresso::PRE_SEP, .value = 0},
              data1,
              generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel),
              data2,
              data3,
  }};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = validate(&command, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
}

void test_validates_seq_mod_alda_octave_up_down() {
  auto data1 = generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel);
  Sequence::packNoteOctave(data1.data[0], 1);

  auto data2 = generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel);
  Sequence::packNoteOctave(data2.data[0], 2);

  auto data3 = generate_seq_data_basic(Sequence::NoteD, 0, Sequence::DurRel);
  Sequence::packNoteOctave(data3.data[0], -1);

  Command command = { 7, 2, false, {
              {.tag = Espresso::MOD, .value = E_MOD_SEQ},
              {.tag = Espresso::NUMBER, .value = 3},
              {.tag = Espresso::PRE_SEP, .value = 0},
              data1,
              generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel),
              data2,
              data3,
  }};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = validate(&command, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
}

void test_validates_seq_mod_alda_duration_rel() {
  auto data1 = generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel);
  Sequence::packNoteDuration(data1.value, 4, Sequence::DurRel);

  auto data2 = generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel);
  Sequence::packNoteDuration(data2.value, 8, Sequence::DurRel);

  auto data3 = generate_seq_data_basic(Sequence::NoteD, 0, Sequence::DurRel);
  Sequence::packNoteDuration(data3.value, 2, Sequence::DurRel);

  Command command = { 7, 2, false, {
              {.tag = Espresso::MOD, .value = E_MOD_SEQ},
              {.tag = Espresso::NUMBER, .value = 3},
              {.tag = Espresso::PRE_SEP, .value = 0},
              data1,
              generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel),
              data2,
              data3,
  }};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = validate(&command, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
}

void test_validates_seq_mod_alda_duration_abs() {
  auto data1 = generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel);
  Sequence::packNoteDuration(data1.value, 800, Sequence::DurMs);

  auto data2 = generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel);
  Sequence::packNoteDuration(data2.value, 50, Sequence::DurMs);

  auto data3 = generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel);
  Sequence::packNoteDuration(data3.value, 1, Sequence::DurSec);

  auto data4 = generate_seq_data_basic(Sequence::NoteD, 0, Sequence::DurRel);
  Sequence::packNoteDuration(data4.value, 255, Sequence::DurSec);

  Command command = { 7, 2, false, {
              {.tag = Espresso::MOD, .value = E_MOD_SEQ},
              {.tag = Espresso::NUMBER, .value = 3},
              {.tag = Espresso::PRE_SEP, .value = 0},
              data1,
              data2,
              data3,
              data4,
  }};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = validate(&command, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
}

void test_validates_seq_mod_alda_duration_dots() {
  auto data1 = generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel);
  Sequence::packNoteDots(data1.value, 1);

  auto data2 = generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel);
  Sequence::packNoteDots(data2.value, 2);

  auto data3 = generate_seq_data_basic(Sequence::NoteD, 0, Sequence::DurRel);
  Sequence::packNoteDots(data3.value, 3);

  Command command = { 7, 2, false, {
              {.tag = Espresso::MOD, .value = E_MOD_SEQ},
              {.tag = Espresso::NUMBER, .value = 3},
              {.tag = Espresso::PRE_SEP, .value = 0},
              data1,
              generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel),
              data2,
              data3,
  }};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = validate(&command, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
}

void test_validates_seq_mod_alda_rests() {
  auto data2 = generate_seq_data_basic(Sequence::NoteRest, 0, Sequence::DurRel);
  Sequence::packNoteDots(data2.value, 2);

  Command command = { 7, 2, false, {
              {.tag = Espresso::MOD, .value = E_MOD_SEQ},
              {.tag = Espresso::NUMBER, .value = 3},
              {.tag = Espresso::PRE_SEP, .value = 0},
              generate_seq_data_basic(Sequence::NoteRest, 0, Sequence::DurRel),
              generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel),
              data2,
              generate_seq_data_basic(Sequence::NoteRest, 8, Sequence::DurRel)
  }};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = validate(&command, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
}

void test_validates_seq_mod_alda_repeats() {
  auto data1 = generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel);
  Sequence::packNoteRepeats(data1.value, 2);

  auto data2 = generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel);
  Sequence::packNoteName(data2.data[1], Sequence::NoteDb);
  Sequence::packNoteRepeats(data2.value, 3);

  auto data3 = generate_seq_data_basic(Sequence::NoteD, 0, Sequence::DurRel);
  Sequence::packNoteRepeats(data3.value, 8);

  Command command = { 7, 2, false, {
              {.tag = Espresso::MOD, .value = E_MOD_SEQ},
              {.tag = Espresso::NUMBER, .value = 3},
              {.tag = Espresso::PRE_SEP, .value = 0},
              data1,
              generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel),
              data2,
              data3,
  }};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = validate(&command, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
}

void test_validates_seq_mod_alda_legato() {
  auto data1 = generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel);
  Sequence::packNoteLegato(data1.value, true);

  auto data2 = generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel);
  Sequence::packNoteName(data2.data[1], Sequence::NoteD);
  Sequence::packNoteLegato(data2.value, true);

  auto data3 = generate_seq_data_basic(Sequence::NoteDb, 8, Sequence::DurRel);
  Sequence::packNoteLegato(data3.value, true);

  Command command = { 7, 2, false, {
              {.tag = Espresso::MOD, .value = E_MOD_SEQ},
              {.tag = Espresso::NUMBER, .value = 3},
              {.tag = Espresso::PRE_SEP, .value = 0},
              data1,
              generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel),
              data2,
              data3,
  }};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = validate(&command, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
}

void test_validates_seq_mod_alda_max_length() {
  Command command = { 15, 2, false, {
              {.tag = Espresso::MOD, .value = E_MOD_SEQ},
              {.tag = Espresso::NUMBER, .value = 3},
              {.tag = Espresso::PRE_SEP, .value = 0},
              generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel),
              generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel),
              generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel),
              generate_seq_data_basic(Sequence::NoteD, 0, Sequence::DurRel),
              generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel),
              generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel),
              generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel),
              generate_seq_data_basic(Sequence::NoteD, 0, Sequence::DurRel),
              generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel),
              generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel),
              generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel),
              generate_seq_data_basic(Sequence::NoteD, 0, Sequence::DurRel),
  }};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = validate(&command, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
}

void test_validates_patterned_mod() {
  Command command = { 5, -1, false, {
              {.tag = Espresso::MOD, .value = E_MOD_SEQ_STUT},
              {.tag = Espresso::PARENS, .value = 3},
              {.tag = Espresso::NUMBER, .value = 2},
              {.tag = Espresso::NUMBER, .value = 0},
              {.tag = Espresso::NUMBER, .value = 1},
  }};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = validate(&command, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
}

void test_validates_patterned_mod_adv() {
  Command command = { 13, 2, false, {
              {.tag = Espresso::MOD, .value = E_MOD_IF},
              {.tag = Espresso::OP, .value = E_OP_X},
              {.tag = Espresso::PRE_SEP, .value = 0},
              {.tag = Espresso::MOD, .value = E_MOD_SEQ},
              {.tag = Espresso::PARENS, .value = 2},
              {.tag = Espresso::ALDA_NOTE, .value = 32},
              {.tag = Espresso::ALDA_NOTE, .value = 32},
              {.tag = Espresso::SUB_SEP, .value = 0},
              {.tag = Espresso::MOD, .value = E_MOD_SEQ_QNT},
              {.tag = Espresso::PARENS, .value = 3},
              {.tag = Espresso::NUMBER, .value = 2},
              {.tag = Espresso::NUMBER, .value = 7},
              {.tag = Espresso::NUMBER, .value = 0},
  }};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = validate(&command, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
}

void test_validates_series_op() {
  Command command = { 5, -1, false, {
              {.tag = Espresso::OP, .value = E_OP_SEQ_STUT},
              {.tag = Espresso::BRACKETS, .value = 3},
              {.tag = Espresso::NUMBER, .value = 2},
              {.tag = Espresso::NUMBER, .value = 0},
              {.tag = Espresso::NUMBER, .value = 1},
  }};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = validate(&command, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
}

void test_validates_series_mod() {
  Command command = { 8, 6, false, {
              {.tag = Espresso::MOD, .value = E_MOD_IF},
              {.tag = Espresso::BRACKETS, .value = 4},
              {.tag = Espresso::NUMBER, .value = 0},
              {.tag = Espresso::NUMBER, .value = 0},
              {.tag = Espresso::NUMBER, .value = 1},
              {.tag = Espresso::NUMBER, .value = 0},
              {.tag = Espresso::PRE_SEP, .value = 0},
              {.tag = Espresso::OP, .value = E_OP_X},
  }};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = validate(&command, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
}

// validator is responsible for making sure sequence of tokens is sensible

void test_returns_err_on_excess_args() {
  Command command = { 4, -1, false, {
              {.tag = Espresso::OP, .value = E_OP_ADD},
              {.tag = Espresso::NUMBER, .value = 3},
              {.tag = Espresso::NUMBER, .value = 1},
              {.tag = Espresso::NUMBER, .value = 1},
  }};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = validate(&command, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_EXTRA_PARAMS);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
}

void test_returns_err_on_missing_args() {
  Command command = { 1, -1, false, {
              {.tag = Espresso::OP, .value = E_OP_SYM_PLUS},
  }};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = validate(&command, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_NEED_PARAMS);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "+");
}

void test_returns_err_on_invalid_mod_sequence() {
  Command command = { 4, 2, false, {
              {.tag = Espresso::MOD, .value = E_MOD_IF},
              {.tag = Espresso::MOD, .value = E_MOD_IF},
              {.tag = Espresso::PRE_SEP, .value = 0},
              {.tag = Espresso::OP, .value = E_OP_A},
  }};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = validate(&command, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_NO_MOD_HERE);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "IF");
}

int main() {
  UNITY_BEGIN();
  RUN_TEST(test_returns_ok_on_valid_op);
  RUN_TEST(test_returns_ok_on_valid_mod_sep_op);
  RUN_TEST(test_returns_ok_on_valid_op_sub_op);
  RUN_TEST(test_returns_ok_on_valid_op_rnd);

  RUN_TEST(test_validates_seq_mod_alda_basic);
  RUN_TEST(test_validates_seq_mod_alda_sharp_flat);
  RUN_TEST(test_validates_seq_mod_alda_chords);
  RUN_TEST(test_validates_seq_mod_alda_octave_up_down);
  RUN_TEST(test_validates_seq_mod_alda_duration_rel);
  RUN_TEST(test_validates_seq_mod_alda_duration_abs);
  RUN_TEST(test_validates_seq_mod_alda_duration_dots);
  RUN_TEST(test_validates_seq_mod_alda_rests);
  RUN_TEST(test_validates_seq_mod_alda_repeats);
  RUN_TEST(test_validates_seq_mod_alda_legato);
  RUN_TEST(test_validates_seq_mod_alda_max_length);
  RUN_TEST(test_validates_patterned_mod);
  RUN_TEST(test_validates_patterned_mod_adv);
  RUN_TEST(test_validates_series_op);
  RUN_TEST(test_validates_series_mod);

  RUN_TEST(test_returns_err_on_excess_args);
  RUN_TEST(test_returns_err_on_missing_args);
  RUN_TEST(test_returns_err_on_invalid_mod_sequence);
  
  UNITY_END();
  return 0;
}

} // namespace TestEspressoValidator
