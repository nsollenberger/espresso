#include <ops/op.h>
#include <espresso/parser.h>
#include <espresso/command.h>
#include <scene.h>
#include <sequence.h>
#include <stdint.h>
#include <unity.h>

using Espresso::parser;
using Espresso::Command;

namespace TestEspressoParser {

void test_returns_ok_on_empty() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 0);
}

void test_returns_ok_on_valid_op() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("+ 2 0", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 3);

  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::OP);
  auto op1 = tele_ops[commandOut.data[0].value];
  TEST_ASSERT_EQUAL(op1->name, "+");

  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[1].value, 2);

  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[2].value, 0);
}

void test_returns_ok_on_valid_mod_sep_op() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("IF 2: CV 4 1", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 6);
  TEST_ASSERT_EQUAL(commandOut.separator, 2);

  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::MOD);
  auto mod1 = tele_mods[commandOut.data[0].value];
  TEST_ASSERT_EQUAL(mod1->name, "IF");

  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[1].value, 2);

  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::PRE_SEP);

  TEST_ASSERT_EQUAL(commandOut.data[3].tag, Espresso::OP);
  auto op1 = tele_ops[commandOut.data[3].value];
  TEST_ASSERT_EQUAL(op1->name, "CV");

  TEST_ASSERT_EQUAL(commandOut.data[4].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[4].value, 4);

  TEST_ASSERT_EQUAL(commandOut.data[5].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[5].value, 1);
}

void test_returns_ok_on_valid_op_sub_op() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("X 8; M.RESET", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 4);
  TEST_ASSERT_EQUAL(commandOut.separator, -1);

  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::OP);
  auto op1 = tele_ops[commandOut.data[0].value];
  TEST_ASSERT_EQUAL(op1->name, "X");

  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[1].value, 8);

  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::SUB_SEP);

  TEST_ASSERT_EQUAL(commandOut.data[3].tag, Espresso::OP);
  auto op3 = tele_ops[commandOut.data[3].value];
  TEST_ASSERT_EQUAL(op3->name, "M.RESET");
}

void test_returns_ok_on_valid_op_rnd() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("Z RND 10", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 3);

  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::OP);
  auto op1 = tele_ops[commandOut.data[0].value];
  TEST_ASSERT_EQUAL(op1->name, "Z");

  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::OP);
  auto op2 = tele_ops[commandOut.data[1].value];
  TEST_ASSERT_EQUAL(op2->name, "RND");

  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[2].value, 10);
}

void test_parses_lshift_x3() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("<<< 100 1", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);

  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");

  TEST_ASSERT_EQUAL(commandOut.comment, false);

  TEST_ASSERT_EQUAL(commandOut.length, 3);

  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::OP);
  auto op1 = tele_ops[commandOut.data[0].value];
  TEST_ASSERT_EQUAL(op1->name, "<<<");

  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[1].value, 100);

  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[2].value, 1);
}

void test_parses_seq_mod_alda_basic() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("N: A B C D", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 6);
  TEST_ASSERT_EQUAL(commandOut.separator, 1);

  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::MOD);
  auto mod1 = tele_mods[commandOut.data[0].value];
  TEST_ASSERT_EQUAL(mod1->name, "N");

  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::PRE_SEP);

  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[3].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[4].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[5].tag, Espresso::ALDA_NOTE);

  Sequence seq;
  seq.unpackData(commandOut.data[2]);
  seq.unpackData(commandOut.data[3]);
  seq.unpackData(commandOut.data[4]);
  seq.unpackData(commandOut.data[5]);
  
  TEST_ASSERT_EQUAL(seq.steps[0].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[0].notes[0], Sequence::NoteA);

  TEST_ASSERT_EQUAL(seq.steps[1].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[1].notes[0], Sequence::NoteB);

  TEST_ASSERT_EQUAL(seq.steps[2].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[2].notes[0], Sequence::NoteC);

  TEST_ASSERT_EQUAL(seq.steps[3].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[3].notes[0], Sequence::NoteD);
}

void test_parses_seq_mod_alda_parens() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("N(A B C D)", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 6);
  TEST_ASSERT_EQUAL(commandOut.separator, -1);

  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::MOD);
  auto mod1 = tele_mods[commandOut.data[0].value];
  TEST_ASSERT_EQUAL(mod1->name, "N");

  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::PARENS);
  TEST_ASSERT_EQUAL(commandOut.data[1].value, 4);

  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[3].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[4].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[5].tag, Espresso::ALDA_NOTE);

  Sequence seq;
  seq.unpackData(commandOut.data[2]);
  seq.unpackData(commandOut.data[3]);
  seq.unpackData(commandOut.data[4]);
  seq.unpackData(commandOut.data[5]);
  
  TEST_ASSERT_EQUAL(seq.steps[0].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[0].notes[0], Sequence::NoteA);

  TEST_ASSERT_EQUAL(seq.steps[1].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[1].notes[0], Sequence::NoteB);

  TEST_ASSERT_EQUAL(seq.steps[2].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[2].notes[0], Sequence::NoteC);

  TEST_ASSERT_EQUAL(seq.steps[3].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[3].notes[0], Sequence::NoteD);
}

void test_parses_seq_mod_alda_parens_adv() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("IF X: N(A B C); X 6", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 11);
  TEST_ASSERT_EQUAL(commandOut.separator, 2);

  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::MOD);
  auto mod1 = tele_mods[commandOut.data[0].value];
  TEST_ASSERT_EQUAL(mod1->name, "IF");

  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::OP);
  auto op1 = tele_ops[commandOut.data[1].value];
  TEST_ASSERT_EQUAL(op1->name, "X");

  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::PRE_SEP);

  TEST_ASSERT_EQUAL(commandOut.data[3].tag, Espresso::MOD);
  auto mod2 = tele_mods[commandOut.data[3].value];
  TEST_ASSERT_EQUAL(mod2->name, "N");

  TEST_ASSERT_EQUAL(commandOut.data[4].tag, Espresso::PARENS);
  TEST_ASSERT_EQUAL(commandOut.data[4].value, 3);

  TEST_ASSERT_EQUAL(commandOut.data[5].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[6].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[7].tag, Espresso::ALDA_NOTE);

  TEST_ASSERT_EQUAL(commandOut.data[8].tag, Espresso::SUB_SEP);

  TEST_ASSERT_EQUAL(commandOut.data[9].tag, Espresso::OP);
  auto op2 = tele_ops[commandOut.data[9].value];
  TEST_ASSERT_EQUAL(op2->name, "X");

  TEST_ASSERT_EQUAL(commandOut.data[10].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[10].value, 6);

  Sequence seq;
  seq.unpackData(commandOut.data[5]);
  seq.unpackData(commandOut.data[6]);
  seq.unpackData(commandOut.data[7]);

  TEST_ASSERT_EQUAL(seq.length(), 3);  
  TEST_ASSERT_EQUAL(seq.steps[0].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[0].notes[0], Sequence::NoteA);

  TEST_ASSERT_EQUAL(seq.steps[1].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[1].notes[0], Sequence::NoteB);

  TEST_ASSERT_EQUAL(seq.steps[2].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[2].notes[0], Sequence::NoteC);
}

void test_parses_seq_mod_alda_parens_adv2() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("N(A B- C); N 2(E F)", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 11);
  TEST_ASSERT_EQUAL(commandOut.separator, -1);

  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::MOD);
  auto mod1 = tele_mods[commandOut.data[0].value];
  TEST_ASSERT_EQUAL(mod1->name, "N");

  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::PARENS);
  TEST_ASSERT_EQUAL(commandOut.data[1].value, 3);

  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[3].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[4].tag, Espresso::ALDA_NOTE);

  TEST_ASSERT_EQUAL(commandOut.data[5].tag, Espresso::SUB_SEP);

  TEST_ASSERT_EQUAL(commandOut.data[6].tag, Espresso::MOD);
  auto mod2 = tele_mods[commandOut.data[6].value];
  TEST_ASSERT_EQUAL(mod2->name, "N");

  TEST_ASSERT_EQUAL(commandOut.data[7].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[7].value, 2);

  TEST_ASSERT_EQUAL(commandOut.data[8].tag, Espresso::PARENS);
  TEST_ASSERT_EQUAL(commandOut.data[8].value, 2);

  TEST_ASSERT_EQUAL(commandOut.data[9].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[10].tag, Espresso::ALDA_NOTE);

  Sequence seq1;
  seq1.unpackData(commandOut.data[2]);
  seq1.unpackData(commandOut.data[3]);
  seq1.unpackData(commandOut.data[4]);

  TEST_ASSERT_EQUAL(seq1.length(), 3);  
  TEST_ASSERT_EQUAL(seq1.steps[0].note_count, 1);
  TEST_ASSERT_EQUAL(seq1.steps[0].notes[0], Sequence::NoteA);

  TEST_ASSERT_EQUAL(seq1.steps[1].note_count, 1);
  TEST_ASSERT_EQUAL(seq1.steps[1].notes[0], Sequence::NoteBb);

  TEST_ASSERT_EQUAL(seq1.steps[2].note_count, 1);
  TEST_ASSERT_EQUAL(seq1.steps[2].notes[0], Sequence::NoteC);

  Sequence seq2;
  seq2.unpackData(commandOut.data[9]);
  seq2.unpackData(commandOut.data[10]);

  TEST_ASSERT_EQUAL(seq2.length(), 2);  
  TEST_ASSERT_EQUAL(seq2.steps[0].note_count, 1);
  TEST_ASSERT_EQUAL(seq2.steps[0].notes[0], Sequence::NoteE);

  TEST_ASSERT_EQUAL(seq2.steps[1].note_count, 1);
  TEST_ASSERT_EQUAL(seq2.steps[1].notes[0], Sequence::NoteF);
}

void test_parses_seq_mod_alda_sharp_flat() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("N 3: A+ B- C- D+", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 7);
  TEST_ASSERT_EQUAL(commandOut.separator, 2);

  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::MOD);
  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::PRE_SEP);
  TEST_ASSERT_EQUAL(commandOut.data[3].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[4].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[5].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[6].tag, Espresso::ALDA_NOTE);

  Sequence seq;
  seq.unpackData(commandOut.data[3]);
  TEST_ASSERT_EQUAL(seq.steps[0].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[0].notes[0], Sequence::NoteBb);

  seq.unpackData(commandOut.data[4]);
  TEST_ASSERT_EQUAL(seq.steps[1].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[1].notes[0], Sequence::NoteBb);

  seq.unpackData(commandOut.data[5]);
  TEST_ASSERT_EQUAL(seq.steps[2].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[2].notes[0], Sequence::NoteB);

  seq.unpackData(commandOut.data[6]);
  TEST_ASSERT_EQUAL(seq.steps[3].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[3].notes[0], Sequence::NoteEb);
}

void test_parses_seq_mod_alda_chords() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("N 3: A/A+ B C/E/G D/E-/F/G+", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 7);
  TEST_ASSERT_EQUAL(commandOut.separator, 2);

  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::MOD);
  auto mod1 = tele_mods[commandOut.data[0].value];
  TEST_ASSERT_EQUAL(mod1->name, "N");

  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[1].value, 3);

  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::PRE_SEP);

  TEST_ASSERT_EQUAL(commandOut.data[3].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[4].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[5].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[6].tag, Espresso::ALDA_NOTE);

  Sequence seq;
  seq.unpackData(commandOut.data[3]);
  TEST_ASSERT_EQUAL(seq.steps[0].note_count, 2);
  TEST_ASSERT_EQUAL(seq.steps[0].notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq.steps[0].notes[1], Sequence::NoteBb);

  seq.unpackData(commandOut.data[4]);
  TEST_ASSERT_EQUAL(seq.steps[1].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[1].notes[0], Sequence::NoteB);

  seq.unpackData(commandOut.data[5]);
  TEST_ASSERT_EQUAL(seq.steps[2].note_count, 3);
  TEST_ASSERT_EQUAL(seq.steps[2].notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq.steps[2].notes[1], Sequence::NoteE);
  TEST_ASSERT_EQUAL(seq.steps[2].notes[2], Sequence::NoteG);

  seq.unpackData(commandOut.data[6]);
  TEST_ASSERT_EQUAL(seq.steps[3].note_count, 4);
  TEST_ASSERT_EQUAL(seq.steps[3].notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq.steps[3].notes[1], Sequence::NoteEb);
  TEST_ASSERT_EQUAL(seq.steps[3].notes[2], Sequence::NoteF);
  TEST_ASSERT_EQUAL(seq.steps[3].notes[3], Sequence::NoteAb);
}

void test_parses_seq_mod_alda_chords_optional_sep() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("N 3: AA BBB CEGB- D", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 7);
  TEST_ASSERT_EQUAL(commandOut.separator, 2);

  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::MOD);
  auto mod1 = tele_mods[commandOut.data[0].value];
  TEST_ASSERT_EQUAL(mod1->name, "N");

  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[1].value, 3);

  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::PRE_SEP);

  TEST_ASSERT_EQUAL(commandOut.data[3].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[4].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[5].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[6].tag, Espresso::ALDA_NOTE);

  Sequence seq;
  seq.unpackData(commandOut.data[3]);
  TEST_ASSERT_EQUAL(seq.steps[0].note_count, 2);
  TEST_ASSERT_EQUAL(seq.steps[0].notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq.steps[0].notes[1], Sequence::NoteA);

  seq.unpackData(commandOut.data[4]);
  TEST_ASSERT_EQUAL(seq.steps[1].note_count, 3);
  TEST_ASSERT_EQUAL(seq.steps[1].notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq.steps[1].notes[1], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq.steps[1].notes[2], Sequence::NoteB);

  seq.unpackData(commandOut.data[5]);
  TEST_ASSERT_EQUAL(seq.steps[2].note_count, 4);
  TEST_ASSERT_EQUAL(seq.steps[2].notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq.steps[2].notes[1], Sequence::NoteE);
  TEST_ASSERT_EQUAL(seq.steps[2].notes[2], Sequence::NoteG);
  TEST_ASSERT_EQUAL(seq.steps[2].notes[3], Sequence::NoteBb);

  seq.unpackData(commandOut.data[6]);
  TEST_ASSERT_EQUAL(seq.steps[3].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[3].notes[0], Sequence::NoteD);
}

void test_parses_seq_mod_alda_octave_up_down() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("N 3: >A B >>C <D", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 7);
  TEST_ASSERT_EQUAL(commandOut.separator, 2);

  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::MOD);
  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::PRE_SEP);
  TEST_ASSERT_EQUAL(commandOut.data[3].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[4].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[5].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[6].tag, Espresso::ALDA_NOTE);

  Sequence seq;
  seq.unpackData(commandOut.data[3]);
  TEST_ASSERT_EQUAL(seq.steps[0].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[0].notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq.steps[0].octave_offset[0], 1);

  seq.unpackData(commandOut.data[4]);
  TEST_ASSERT_EQUAL(seq.steps[1].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[1].notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq.steps[1].octave_offset[0], 0);

  seq.unpackData(commandOut.data[5]);
  TEST_ASSERT_EQUAL(seq.steps[2].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[2].notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq.steps[2].octave_offset[0], 2);

  seq.unpackData(commandOut.data[6]);
  TEST_ASSERT_EQUAL(seq.steps[3].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[3].notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq.steps[3].octave_offset[0], -1);
}

void test_parses_seq_mod_alda_octave_up_down2() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("N 3: A >B", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 5);
  TEST_ASSERT_EQUAL(commandOut.separator, 2);

  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::MOD);
  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::PRE_SEP);
  TEST_ASSERT_EQUAL(commandOut.data[3].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[4].tag, Espresso::ALDA_NOTE);

  Sequence seq;
  seq.unpackData(commandOut.data[3]);
  TEST_ASSERT_EQUAL(seq.steps[0].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[0].notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq.steps[0].octave_offset[0], 0);

  seq.unpackData(commandOut.data[4]);
  TEST_ASSERT_EQUAL(seq.steps[1].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[1].notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq.steps[1].octave_offset[0], 1);
}

void test_parses_seq_mod_alda_duration_rel() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("N 3: A4 B8 C2 D", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 7);
  TEST_ASSERT_EQUAL(commandOut.separator, 2);

  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::MOD);
  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::PRE_SEP);
  TEST_ASSERT_EQUAL(commandOut.data[3].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[4].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[5].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[6].tag, Espresso::ALDA_NOTE);

  Sequence seq;
  seq.unpackData(commandOut.data[3]);
  TEST_ASSERT_EQUAL(seq.steps[0].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[0].notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq.steps[0].duration, 4);
  TEST_ASSERT_EQUAL(seq.steps[0].duration_unit, Sequence::DurRel);

  seq.unpackData(commandOut.data[4]);
  TEST_ASSERT_EQUAL(seq.steps[1].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[1].notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq.steps[1].duration, 8);
  TEST_ASSERT_EQUAL(seq.steps[1].duration_unit, Sequence::DurRel);

  seq.unpackData(commandOut.data[5]);
  TEST_ASSERT_EQUAL(seq.steps[2].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[2].notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq.steps[2].duration, 2);
  TEST_ASSERT_EQUAL(seq.steps[2].duration_unit, Sequence::DurRel);

  seq.unpackData(commandOut.data[6]);
  TEST_ASSERT_EQUAL(seq.steps[3].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[3].notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq.steps[3].duration, 0);
  TEST_ASSERT_EQUAL(seq.steps[3].duration_unit, Sequence::DurRel);
}

void test_parses_seq_mod_alda_duration_abs() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("N 3: A800MS B50MS C1S D255S", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 7);
  TEST_ASSERT_EQUAL(commandOut.separator, 2);

  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::MOD);
  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::PRE_SEP);
  TEST_ASSERT_EQUAL(commandOut.data[3].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[4].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[5].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[6].tag, Espresso::ALDA_NOTE);

  Sequence seq;
  seq.unpackData(commandOut.data[3]);
  TEST_ASSERT_EQUAL(seq.steps[0].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[0].notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq.steps[0].duration, 800);
  TEST_ASSERT_EQUAL(seq.steps[0].duration_unit, Sequence::DurMs);

  seq.unpackData(commandOut.data[4]);
  TEST_ASSERT_EQUAL(seq.steps[1].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[1].notes[0], Sequence::NoteB);
  // Note: MS looses bottom 2 bits of precision, in order to fit 1000MS in 1byte
  TEST_ASSERT_EQUAL(seq.steps[1].duration, 48);
  TEST_ASSERT_EQUAL(seq.steps[1].duration_unit, Sequence::DurMs);

  seq.unpackData(commandOut.data[5]);
  TEST_ASSERT_EQUAL(seq.steps[2].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[2].notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq.steps[2].duration, 1);
  TEST_ASSERT_EQUAL(seq.steps[2].duration_unit, Sequence::DurSec);

  seq.unpackData(commandOut.data[6]);
  TEST_ASSERT_EQUAL(seq.steps[3].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[3].notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq.steps[3].duration, 255);
  TEST_ASSERT_EQUAL(seq.steps[3].duration_unit, Sequence::DurSec);
}

void test_parses_seq_mod_alda_duration_dots() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("N 3: A4. B8 C1.. D...", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 7);
  TEST_ASSERT_EQUAL(commandOut.separator, 2);

  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::MOD);
  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::PRE_SEP);
  TEST_ASSERT_EQUAL(commandOut.data[3].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[4].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[5].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[6].tag, Espresso::ALDA_NOTE);

  Sequence seq;
  seq.unpackData(commandOut.data[3]);
  TEST_ASSERT_EQUAL(seq.steps[0].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[0].notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq.steps[0].dots, 1);
  TEST_ASSERT_EQUAL(seq.steps[0].duration, 4);
  TEST_ASSERT_EQUAL(seq.steps[0].duration_unit, Sequence::DurRel);


  seq.unpackData(commandOut.data[4]);
  TEST_ASSERT_EQUAL(seq.steps[1].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[1].notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq.steps[1].dots, 0);
  TEST_ASSERT_EQUAL(seq.steps[1].duration, 8);
  TEST_ASSERT_EQUAL(seq.steps[1].duration_unit, Sequence::DurRel);

  seq.unpackData(commandOut.data[5]);
  TEST_ASSERT_EQUAL(seq.steps[2].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[2].notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq.steps[2].dots, 2);
  TEST_ASSERT_EQUAL(seq.steps[2].duration, 1);
  TEST_ASSERT_EQUAL(seq.steps[2].duration_unit, Sequence::DurRel);

  seq.unpackData(commandOut.data[6]);
  TEST_ASSERT_EQUAL(seq.steps[3].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[3].notes[0], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq.steps[3].dots, 3);
  TEST_ASSERT_EQUAL(seq.steps[3].duration, 0);
  TEST_ASSERT_EQUAL(seq.steps[3].duration_unit, Sequence::DurRel);
}

void test_parses_seq_mod_alda_rests() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("N 3: R8 A4 R C", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 7);
  TEST_ASSERT_EQUAL(commandOut.separator, 2);

  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::MOD);
  auto mod1 = tele_mods[commandOut.data[0].value];
  TEST_ASSERT_EQUAL(mod1->name, "N");

  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[1].value, 3);

  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::PRE_SEP);

  TEST_ASSERT_EQUAL(commandOut.data[3].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[4].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[5].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[6].tag, Espresso::ALDA_NOTE);

  Sequence seq;
  seq.unpackData(commandOut.data[3]);
  seq.unpackData(commandOut.data[4]);
  seq.unpackData(commandOut.data[5]);
  seq.unpackData(commandOut.data[6]);

  TEST_ASSERT_EQUAL(seq.length(), 4);
  // Note: rests do not count as notes
  auto note = seq.getNotesAtStep(0);
  TEST_ASSERT_EQUAL(note->note_count, 0);
  TEST_ASSERT_EQUAL(note->notes[0], Sequence::NoteRest);
  TEST_ASSERT_EQUAL(note->duration, 8);
  TEST_ASSERT_EQUAL(note->duration_unit, Sequence::DurRel);
  TEST_ASSERT_EQUAL(note->repeats, 1);

  note = seq.getNotesAtStep(1);
  TEST_ASSERT_EQUAL(note->note_count, 1);
  TEST_ASSERT_EQUAL(note->notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(note->duration, 4);
  TEST_ASSERT_EQUAL(note->duration_unit, Sequence::DurRel);

  // Note: mixing rests with other notes will fail softly
  note = seq.getNotesAtStep(2);
  TEST_ASSERT_EQUAL(note->note_count, 0);
  TEST_ASSERT_EQUAL(note->notes[0], Sequence::NoteRest);
  TEST_ASSERT_EQUAL(note->duration, 4);
  TEST_ASSERT_EQUAL(note->duration_unit, Sequence::DurRel);

  note = seq.getNotesAtStep(3);
  TEST_ASSERT_EQUAL(note->note_count, 1);
  TEST_ASSERT_EQUAL(note->notes[0], Sequence::NoteC);
}

void test_parses_seq_mod_alda_repeats() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("N 3: A*2 B CD-*3 D*9", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 7);
  TEST_ASSERT_EQUAL(commandOut.separator, 2);

  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::MOD);
  auto mod1 = tele_mods[commandOut.data[0].value];
  TEST_ASSERT_EQUAL(mod1->name, "N");

  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[1].value, 3);

  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::PRE_SEP);

  TEST_ASSERT_EQUAL(commandOut.data[3].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[4].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[5].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[6].tag, Espresso::ALDA_NOTE);

  Sequence seq;
  seq.unpackData(commandOut.data[3]);
  TEST_ASSERT_EQUAL(seq.steps[0].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[0].notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq.steps[0].repeats, 2);

  seq.unpackData(commandOut.data[4]);
  TEST_ASSERT_EQUAL(seq.steps[1].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[1].notes[0], Sequence::NoteB);
  // 1 repeats = play once
  TEST_ASSERT_EQUAL(seq.steps[1].repeats, 1);

  seq.unpackData(commandOut.data[5]);
  TEST_ASSERT_EQUAL(seq.steps[2].note_count, 2);
  TEST_ASSERT_EQUAL(seq.steps[2].notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq.steps[2].notes[1], Sequence::NoteDb);
  TEST_ASSERT_EQUAL(seq.steps[2].repeats, 3);

  seq.unpackData(commandOut.data[6]);
  TEST_ASSERT_EQUAL(seq.steps[3].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[3].notes[0], Sequence::NoteD);
  // Note: max repeats = 8
  TEST_ASSERT_EQUAL(seq.steps[3].repeats, 8);
}

void test_parses_seq_mod_alda_legato() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("N 3: A~ B CD~ D-~", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 7);
  TEST_ASSERT_EQUAL(commandOut.separator, 2);

  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::MOD);
  auto mod1 = tele_mods[commandOut.data[0].value];
  TEST_ASSERT_EQUAL(mod1->name, "N");

  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[1].value, 3);

  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::PRE_SEP);

  TEST_ASSERT_EQUAL(commandOut.data[3].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[4].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[5].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[6].tag, Espresso::ALDA_NOTE);

  Sequence seq;
  seq.unpackData(commandOut.data[3]);
  TEST_ASSERT_EQUAL(seq.steps[0].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[0].notes[0], Sequence::NoteA);
  TEST_ASSERT_EQUAL(seq.steps[0].legato, true);

  seq.unpackData(commandOut.data[4]);
  TEST_ASSERT_EQUAL(seq.steps[1].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[1].notes[0], Sequence::NoteB);
  TEST_ASSERT_EQUAL(seq.steps[1].legato, false);

  seq.unpackData(commandOut.data[5]);
  TEST_ASSERT_EQUAL(seq.steps[2].note_count, 2);
  TEST_ASSERT_EQUAL(seq.steps[2].notes[0], Sequence::NoteC);
  TEST_ASSERT_EQUAL(seq.steps[2].notes[1], Sequence::NoteD);
  TEST_ASSERT_EQUAL(seq.steps[2].legato, true);

  seq.unpackData(commandOut.data[6]);
  TEST_ASSERT_EQUAL(seq.steps[3].note_count, 1);
  TEST_ASSERT_EQUAL(seq.steps[3].notes[0], Sequence::NoteDb);
  TEST_ASSERT_EQUAL(seq.steps[3].legato, true);
}

void test_parses_seq_mod_alda_max_length() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("N 3: A B C D A B C D A B C D", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 15);
  TEST_ASSERT_EQUAL(commandOut.separator, 2);

  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::MOD);
  auto mod1 = tele_mods[commandOut.data[0].value];
  TEST_ASSERT_EQUAL(mod1->name, "N");

  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[1].value, 3);

  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::PRE_SEP);

  TEST_ASSERT_EQUAL(commandOut.data[3].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[4].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[5].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[6].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[7].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[8].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[9].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[10].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[11].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[12].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[13].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[14].tag, Espresso::ALDA_NOTE);
}

void test_parses_patterned_mod() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("STUT(2 0 1)", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 5);

  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::MOD);
  auto mod1 = tele_mods[commandOut.data[0].value];
  TEST_ASSERT_EQUAL(mod1->name, "STUT");

  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::PARENS);
  TEST_ASSERT_EQUAL(commandOut.data[1].value, 3);

  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[2].value, 2);

  TEST_ASSERT_EQUAL(commandOut.data[3].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[3].value, 0);

  TEST_ASSERT_EQUAL(commandOut.data[4].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[4].value, 1);
}

void test_parses_patterned_mod_adv() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("IF X: N(C D); QNT(2 7 0)", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 13);

  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::MOD);
  auto mod1 = tele_mods[commandOut.data[0].value];
  TEST_ASSERT_EQUAL(mod1->name, "IF");

  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::OP);
  auto op1 = tele_ops[commandOut.data[1].value];
  TEST_ASSERT_EQUAL(op1->name, "X");

  TEST_ASSERT_EQUAL(commandOut.separator, 2);
  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::PRE_SEP);

  TEST_ASSERT_EQUAL(commandOut.data[3].tag, Espresso::MOD);
  auto mod2 = tele_mods[commandOut.data[3].value];
  TEST_ASSERT_EQUAL(mod2->name, "N");

  TEST_ASSERT_EQUAL(commandOut.data[4].tag, Espresso::PARENS);
  TEST_ASSERT_EQUAL(commandOut.data[4].value, 2);

  TEST_ASSERT_EQUAL(commandOut.data[5].tag, Espresso::ALDA_NOTE);
  TEST_ASSERT_EQUAL(commandOut.data[6].tag, Espresso::ALDA_NOTE);

  TEST_ASSERT_EQUAL(commandOut.data[7].tag, Espresso::SUB_SEP);

  TEST_ASSERT_EQUAL(commandOut.data[8].tag, Espresso::MOD);
  auto mod3 = tele_mods[commandOut.data[8].value];
  TEST_ASSERT_EQUAL(mod3->name, "QNT");

  TEST_ASSERT_EQUAL(commandOut.data[9].tag, Espresso::PARENS);
  TEST_ASSERT_EQUAL(commandOut.data[9].value, 3);

  TEST_ASSERT_EQUAL(commandOut.data[10].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[10].value, 2);

  TEST_ASSERT_EQUAL(commandOut.data[11].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[11].value, 7);

  TEST_ASSERT_EQUAL(commandOut.data[12].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[12].value, 0);
}

void test_parses_series_op() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("STUT [2 0 1]", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 5);

  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::OP);
  auto op1 = tele_ops[commandOut.data[0].value];
  TEST_ASSERT_EQUAL(op1->name, "STUT");

  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::BRACKETS);
  TEST_ASSERT_EQUAL(commandOut.data[1].value, 3);

  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[2].value, 2);

  TEST_ASSERT_EQUAL(commandOut.data[3].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[3].value, 0);

  TEST_ASSERT_EQUAL(commandOut.data[4].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[4].value, 1);
}

void test_parses_series_mod() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("IF [0 0 1 0]: X", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 8);

  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::MOD);
  auto mod1 = tele_mods[commandOut.data[0].value];
  TEST_ASSERT_EQUAL(mod1->name, "IF");

  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::BRACKETS);
  TEST_ASSERT_EQUAL(commandOut.data[1].value, 4);

  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[2].value, 0);

  TEST_ASSERT_EQUAL(commandOut.data[3].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[3].value, 0);

  TEST_ASSERT_EQUAL(commandOut.data[4].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[4].value, 1);

  TEST_ASSERT_EQUAL(commandOut.data[5].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[5].value, 0);

  TEST_ASSERT_EQUAL(commandOut.data[6].tag, Espresso::PRE_SEP);

  TEST_ASSERT_EQUAL(commandOut.data[7].tag, Espresso::OP);
  auto op2 = tele_ops[commandOut.data[7].value];
  TEST_ASSERT_EQUAL(op2->name, "X");
}

void test_returns_ok_on_mod_sep_missing_space() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("IF 1:X", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 4);
  TEST_ASSERT_EQUAL(commandOut.separator, 2);
  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::MOD);
  auto mod1 = tele_mods[commandOut.data[0].value];
  TEST_ASSERT_EQUAL(mod1->name, "IF");
  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[1].value, 1);

  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::PRE_SEP);

  TEST_ASSERT_EQUAL(commandOut.data[3].tag, Espresso::OP);
  auto op1 = tele_ops[commandOut.data[3].value];
  TEST_ASSERT_EQUAL(op1->name, "X");
}

// scanner returns err on invalid tokens (including white-space)

void test_returns_err_on_invalid_op() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("TTR 3 1", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_PARSE);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "TTR");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 0);
}

void test_returns_err_on_invalid_mod() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("IIF 1: X", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_PARSE);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "IIF");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 0);
}

void test_returns_err_on_invalid_alda_seq() {
  {
    Command commandOut = {};
    char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

    auto result = parser("N 3: X", commandOut, errorMsgOut);

    TEST_ASSERT_EQUAL(result, Espresso::E_PARSE);
    TEST_ASSERT_EQUAL_STRING(errorMsgOut, "X");
    TEST_ASSERT_EQUAL(commandOut.comment, false);
    TEST_ASSERT_EQUAL(commandOut.length, 3);
  }

  {
    Command commandOut = {};
    char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

    auto result = parser("N 3: ~A.6", commandOut, errorMsgOut);

    TEST_ASSERT_EQUAL(result, Espresso::E_PARSE);
    TEST_ASSERT_EQUAL_STRING(errorMsgOut, "~A.6");
    TEST_ASSERT_EQUAL(commandOut.comment, false);
    TEST_ASSERT_EQUAL(commandOut.length, 3);
  }

  {
    Command commandOut = {};
    char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

    auto result = parser("N 3: C++", commandOut, errorMsgOut);

    TEST_ASSERT_EQUAL(result, Espresso::E_PARSE);
    TEST_ASSERT_EQUAL_STRING(errorMsgOut, "C++");
    TEST_ASSERT_EQUAL(commandOut.comment, false);
    TEST_ASSERT_EQUAL(commandOut.length, 3);
  }

  {
    Command commandOut = {};
    char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

    auto result = parser("N 3: E1.4S", commandOut, errorMsgOut);

    TEST_ASSERT_EQUAL(result, Espresso::E_PARSE);
    TEST_ASSERT_EQUAL_STRING(errorMsgOut, "E1.4S");
    TEST_ASSERT_EQUAL(commandOut.comment, false);
    TEST_ASSERT_EQUAL(commandOut.length, 3);
  }

  {
    Command commandOut = {};
    char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

    auto result = parser("N 3: /C/E/G", commandOut, errorMsgOut);

    TEST_ASSERT_EQUAL(result, Espresso::E_PARSE);
    TEST_ASSERT_EQUAL_STRING(errorMsgOut, "/C/E/G");
    TEST_ASSERT_EQUAL(commandOut.comment, false);
    TEST_ASSERT_EQUAL(commandOut.length, 3);
  }

  {
    Command commandOut = {};
    char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

    auto result = parser("N 3: C**8", commandOut, errorMsgOut);

    TEST_ASSERT_EQUAL(result, Espresso::E_PARSE);
    TEST_ASSERT_EQUAL_STRING(errorMsgOut, "C**8");
    TEST_ASSERT_EQUAL(commandOut.comment, false);
    TEST_ASSERT_EQUAL(commandOut.length, 3);
  }

  {
    Command commandOut = {};
    char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

    auto result = parser("N 3: RC", commandOut, errorMsgOut);

    TEST_ASSERT_EQUAL(result, Espresso::E_PARSE);
    TEST_ASSERT_EQUAL_STRING(errorMsgOut, "RC");
    TEST_ASSERT_EQUAL(commandOut.comment, false);
    TEST_ASSERT_EQUAL(commandOut.length, 3);
  }

  {
    Command commandOut = {};
    char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

    auto result = parser("N 3: >R", commandOut, errorMsgOut);

    TEST_ASSERT_EQUAL(result, Espresso::E_PARSE);
    TEST_ASSERT_EQUAL_STRING(errorMsgOut, ">R");
    TEST_ASSERT_EQUAL(commandOut.comment, false);
    TEST_ASSERT_EQUAL(commandOut.length, 3);
  }
}

void test_returns_err_on_invalid_mod_sequence() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("IF IF: A", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_PARSE);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "IF:");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 1);
}

// scanner is only responsible for parsing tokens

void test_returns_ok_on_excess_args() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("+ 3 1 1", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 4);
  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::OP);
  TEST_ASSERT_EQUAL(commandOut.data[1].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[2].tag, Espresso::NUMBER);
  TEST_ASSERT_EQUAL(commandOut.data[3].tag, Espresso::NUMBER);
}

void test_returns_ok_on_missing_args() {
  Command commandOut = {};
  char errorMsgOut[PARSE_ERROR_STR_LENGTH] = {};

  auto result = parser("+", commandOut, errorMsgOut);

  TEST_ASSERT_EQUAL(result, Espresso::E_OK);
  TEST_ASSERT_EQUAL_STRING(errorMsgOut, "");
  TEST_ASSERT_EQUAL(commandOut.comment, false);
  TEST_ASSERT_EQUAL(commandOut.length, 1);
  TEST_ASSERT_EQUAL(commandOut.data[0].tag, Espresso::OP);
}

int main() {
  UNITY_BEGIN();
  RUN_TEST(test_returns_ok_on_empty);
  RUN_TEST(test_returns_ok_on_valid_op);
  RUN_TEST(test_returns_ok_on_valid_mod_sep_op);
  RUN_TEST(test_returns_ok_on_valid_op_sub_op);
  RUN_TEST(test_returns_ok_on_valid_op_rnd);
  RUN_TEST(test_parses_lshift_x3);
  RUN_TEST(test_parses_seq_mod_alda_basic);
  RUN_TEST(test_parses_seq_mod_alda_parens);
  RUN_TEST(test_parses_seq_mod_alda_parens_adv);
  RUN_TEST(test_parses_seq_mod_alda_parens_adv2);
  RUN_TEST(test_parses_seq_mod_alda_sharp_flat);
  RUN_TEST(test_parses_seq_mod_alda_chords);
  RUN_TEST(test_parses_seq_mod_alda_chords_optional_sep);
  RUN_TEST(test_parses_seq_mod_alda_octave_up_down);
  RUN_TEST(test_parses_seq_mod_alda_duration_rel);
  RUN_TEST(test_parses_seq_mod_alda_duration_abs);
  RUN_TEST(test_parses_seq_mod_alda_duration_dots);
  RUN_TEST(test_parses_seq_mod_alda_rests);
  RUN_TEST(test_parses_seq_mod_alda_repeats);
  RUN_TEST(test_parses_seq_mod_alda_legato);
  RUN_TEST(test_parses_seq_mod_alda_max_length);
  RUN_TEST(test_parses_patterned_mod);
  RUN_TEST(test_parses_patterned_mod_adv);
  RUN_TEST(test_parses_series_op);
  // RUN_TEST(test_parses_series_mod);
  RUN_TEST(test_returns_ok_on_mod_sep_missing_space);

  RUN_TEST(test_returns_err_on_invalid_op);
  RUN_TEST(test_returns_err_on_invalid_mod);
  RUN_TEST(test_returns_err_on_invalid_alda_seq);
  RUN_TEST(test_returns_err_on_invalid_mod_sequence);

  RUN_TEST(test_returns_ok_on_excess_args);
  RUN_TEST(test_returns_ok_on_missing_args);
  UNITY_END();
  return 0;
}

} // namespace TestEspressoParser
