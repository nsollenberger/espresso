#include <font.h>
#include <region.h>
#include "ascii_buffer.h"
#include <unity.h>

namespace TestFont {

void test_font_string() {
  // extra width = required padding
  Region screenBuf(0, 0, 15, 8);
  AsciiBuffer asciiBuf(screenBuf);

  Font::string_region_clip(screenBuf, "abc", 0, 0, eColorDark2, eColorBlank);

  TEST_ASSERT_EQUAL_STRING(asciiBuf.stringVal(),
                           ":                              :\n"
                           ":        XX                    :\n"
                           ":  XX    XXXX      XX          :\n"
                           ":    XX  XX  XX  XX            :\n"
                           ":  XXXX  XX  XX  XX            :\n"
                           ":XX  XX  XX  XX  XX            :\n"
                           ":  XXXX  XXXX      XX          :\n"
                           ":                              :\n");
}

void test_font_string_clipped() {
  Region screenBuf(0, 0, 15, 8);
  AsciiBuffer asciiBuf(screenBuf);

  // screenBuf is not wide enough for extra chars
  Font::string_region_clip(screenBuf, "DEFG", 0, 0, eColorDark2, eColorBlank);

  TEST_ASSERT_EQUAL_STRING(asciiBuf.stringVal(),
                           ":                              :\n"
                           ":XXXX    XXXXXX  XXXXXX        :\n"
                           ":XX  XX  XX      XX            :\n"
                           ":XX  XX  XXXX    XXXX          :\n"
                           ":XX  XX  XX      XX            :\n"
                           ":XX  XX  XX      XX            :\n"
                           ":XXXX    XXXXXX  XX            :\n"
                           ":                              :\n");
}

void test_font_string_with_offset() {
  Region screenBuf(0, 0, 18, 8);
  AsciiBuffer asciiBuf(screenBuf);

  Font::string_region_clip(screenBuf, "3X*", 1, 2, eColorDark2, eColorBlank);

  TEST_ASSERT_EQUAL_STRING(asciiBuf.stringVal(),
                           ":                                    :\n"
                           ":                                    :\n"
                           ":                                    :\n"
                           ":  XXXXXX  XX  XX                    :\n"
                           ":      XX  XX  XX  XX  XX            :\n"
                           ":    XX      XX      XX              :\n"
                           ":      XX    XX    XXXXXX            :\n"
                           ":      XX  XX  XX    XX              :\n");
}

void test_font_string_with_offset_negative() {
  Region screenBuf(0, 0, 18, 8);
  AsciiBuffer asciiBuf(screenBuf);

  Font::string_region_clip(screenBuf, "3X*", 0, -2, eColorDark2, eColorBlank);

  TEST_ASSERT_EQUAL_STRING(asciiBuf.stringVal(),
                           ":    XX  XX  XX  XX  XX              :\n"
                          ":  XX      XX      XX                :\n"
                          ":    XX    XX    XXXXXX              :\n"
                          ":    XX  XX  XX    XX                :\n"
                          ":XXXX    XX  XX  XX  XX              :\n"
                          ":                                    :\n"
                          ":                                    :\n"
                          ":                                    :\n");
}

void test_font_string_right_with_offset() {
  Region screenBuf(0, 0, 36, 8);
  AsciiBuffer asciiBuf(screenBuf);

  // places text to the *left* of the x-offset
  Font::string_region_clip_right(screenBuf, "3X*", 34, 0, eColorDark2, eColorBlank);

  TEST_ASSERT_EQUAL_STRING(asciiBuf.stringVal(),
                           // clang-format off
":                                                                        :\n"
":                                            XXXXXX  XX  XX              :\n"
":                                                XX  XX  XX  XX  XX      :\n"
":                                              XX      XX      XX        :\n"
":                                                XX    XX    XXXXXX      :\n"
":                                                XX  XX  XX    XX        :\n"
":                                            XXXX    XX  XX  XX  XX      :\n"
":                                                                        :\n"
                           // clang-format on
  );
}

void test_font_string_special_chars() {
  Region screenBuf(0, 0, 54, 8);
  AsciiBuffer asciiBuf(screenBuf);

  Font::string_region_clip(screenBuf, "!@#$%^&*()", 0, 0, eColorDark2, eColorBlank);

  // strange carrot
  TEST_ASSERT_EQUAL_STRING(
      asciiBuf.stringVal(),
      // clang-format off
":                                                                                                            :\n"
":XX    XXXXXX      XX  XX        XX                    XX        XX                  XX  XX                  :\n"
":XX  XX      XX  XXXXXXXXXX    XXXXXXXX              XXXXXX    XX  XX      XX  XX  XX      XX                :\n"
":XX  XX    XXXX    XX  XX    XX  XX      XX    XX  XX  XX  XX    XX          XX    XX      XX                :\n"
":XX  XX  XX  XX    XX  XX      XXXXXX        XX        XX      XX  XX  XX  XXXXXX  XX      XX                :\n"
":    XX    XXXX  XXXXXXXXXX      XX  XX    XX          XX      XX    XX      XX    XX      XX                :\n"
":XX  XX            XX  XX    XXXXXXXX    XX    XX      XX        XXXX  XX  XX  XX    XX  XX                  :\n"
":      XXXXXX                    XX                                                                          :\n"
      // clang-format on
  );

  screenBuf.fill(eColorBlank);
  Font::string_region_clip(screenBuf, "`-=[]\\;',./+", 0, 0, eColorDark2, eColorBlank);

  // huh, that's not a back-tick
  TEST_ASSERT_EQUAL_STRING(
      asciiBuf.stringVal(),
      // clang-format off
":                                                                                                            :\n"
":    XXXX                XXXX  XXXX  XX            XX                XX                                      :\n"
":  XX                    XX      XX  XX        XX  XX                XX                                      :\n"
":XXXXXX          XXXXXX  XX      XX    XX                          XX      XX                                :\n"
":  XX      XXXX          XX      XX    XX                          XX    XXXXXX                              :\n"
":  XX            XXXXXX  XX      XX      XX    XX        XX      XX        XX                                :\n"
":XXXXXXXX                XXXX  XXXX      XX    XX        XX  XX  XX                                          :\n"
":                                            XX        XX                                                    :\n"
      // clang-format on
  );

  screenBuf.fill(eColorBlank);
  Font::string_region_clip(screenBuf, "~{}|:\"<>?_", 0, 0, eColorDark2, eColorBlank);

  TEST_ASSERT_EQUAL_STRING(
      asciiBuf.stringVal(),
      // clang-format off
":                                                                                                            :\n"
":  XX  XX    XXXX  XXXX    XX      XX  XX                  XXXX                                              :\n"
":XX  XX      XX      XX    XX      XX  XX      XX  XX          XX                                            :\n"
":          XX          XX  XX  XX            XX      XX      XX                                              :\n"
":            XX      XX    XX              XX          XX  XX                                                :\n"
":            XX      XX    XX                XX      XX                                                      :\n"
":            XXXX  XXXX    XX  XX              XX  XX      XX                                                :\n"
":                                                                  XXXXXX                                    :\n"
      // clang-format on
  );
}

void test_font_string_unprintable_chars() {
  Region screenBuf(0, 0, 15, 8);
  AsciiBuffer asciiBuf(screenBuf);

  char badData[4] = {0x01, 0x1f, static_cast<char>(0xff), 0};

  screenBuf.fill(eColorBlank);
  Font::string_region_clip(screenBuf, badData, 0, 0, eColorDark2, eColorBlank);

  TEST_ASSERT_EQUAL_STRING(asciiBuf.stringVal(),
                           ":                              :\n"
                           ":XXXX    XXXX    XXXX          :\n"
                           ":    XX      XX      XX        :\n"
                           ":  XX      XX      XX          :\n"
                           ":XX      XX      XX            :\n"
                           ":                              :\n"
                           ":XX      XX      XX            :\n"
                           ":                              :\n");
}

void test_font_string_tab_clipped() {
  Region screenBuf(0, 0, 96, 8);
  AsciiBuffer asciiBuf(screenBuf);

  // this string is too long to fit, *should* be clipped
  Font::string_region_clip(screenBuf, "SCENE.P\tSET SCENE, EXCL PATTERN", 0, 0,
                           eColorDark2, eColorBlank);

  TEST_ASSERT_EQUAL_STRING(
      asciiBuf.stringVal(),
      // clang-format off
":                                                                                                                                                                                                :\n"
":  XXXX    XXXX  XXXXXX  XX    XX  XXXXXX      XXXX                                                XXXX  XXXXXX  XXXXXX            XXXX    XXXX  XXXXXX  XX    XX  XXXXXX                        :\n"
":XX      XX      XX      XXXX  XX  XX          XX  XX                                            XX      XX        XX            XX      XX      XX      XXXX  XX  XX                            :\n"
":  XX    XX      XXXX    XX  XXXX  XXXX        XX  XX                                              XX    XXXX      XX              XX    XX      XXXX    XX  XXXX  XXXX                          :\n"
":    XX  XX      XX      XX    XX  XX          XXXX                                                  XX  XX        XX                XX  XX      XX      XX    XX  XX                            :\n"
":    XX  XX      XX      XX    XX  XX          XX                                                    XX  XX        XX                XX  XX      XX      XX    XX  XX        XX                  :\n"
":XXXX      XXXX  XXXXXX  XX    XX  XXXXXX  XX  XX                                                XXXX    XXXXXX    XX            XXXX      XXXX  XXXXXX  XX    XX  XXXXXX    XX                  :\n"
":                                                                                                                                                                          XX                    :\n"
      // clang-format on
  );
}

void test_font_string_tab_prevents_overlap() {
  Region screenBuf(0, 0, 96, 8);
  AsciiBuffer asciiBuf(screenBuf);

  // the "pre-tab" text here would potentially overlap with the "post-tab" text,
  // and should also be clipped
  Font::string_region_clip(screenBuf, "SCENE.P, MORE TEXT\t... ABCDEFGHIJK", 0,
                           0, eColorDark2, eColorBlank);

  TEST_ASSERT_EQUAL_STRING(
      asciiBuf.stringVal(),
      // clang-format off
":                                                                                                                                                                                                :\n"
":  XXXX    XXXX  XXXXXX  XX    XX  XXXXXX      XXXX                  XX      XX    XX                                  XX    XXXX      XXXX  XXXX    XXXXXX  XXXXXX    XXXX  XX  XX              :\n"
":XX      XX      XX      XXXX  XX  XX          XX  XX                XXXX  XXXX  XX  XX                              XX  XX  XX  XX  XX      XX  XX  XX      XX      XX      XX  XX              :\n"
":  XX    XX      XXXX    XX  XXXX  XXXX        XX  XX                XX  XX  XX  XX  XX                              XX  XX  XXXX    XX      XX  XX  XXXX    XXXX    XX      XXXXXX              :\n"
":    XX  XX      XX      XX    XX  XX          XXXX                  XX      XX  XX  XX                              XXXXXX  XX  XX  XX      XX  XX  XX      XX      XX  XX  XX  XX              :\n"
":    XX  XX      XX      XX    XX  XX          XX        XX          XX      XX  XX  XX                              XX  XX  XX  XX  XX      XX  XX  XX      XX      XX  XX  XX  XX              :\n"
":XXXX      XXXX  XXXXXX  XX    XX  XXXXXX  XX  XX        XX          XX      XX    XX            XX  XX  XX          XX  XX  XXXX      XXXX  XXXX    XXXXXX  XX        XXXX  XX  XX              :\n"
":                                                      XX                                                                                                                                        :\n"
      // clang-format on
  );
}

void test_font_string_highlight() {
  Region screenBuf(0, 0, 15, 8);
  AsciiBuffer asciiBuf(screenBuf);

  Font::string_region_clip_hid(screenBuf, "abc", 0, 0, eColorDark1, eColorBlank, 1, eColorLight1);

  TEST_ASSERT_EQUAL_STRING(asciiBuf.stringVal(),
                           ":        . . . .               :\n"
                           ":        Xx. . .               :\n"
                           ":  Xx    XxXx. .   Xx          :\n"
                           ":    Xx  Xx. Xx. Xx            :\n"
                           ":  XxXx  Xx. Xx. Xx            :\n"
                           ":Xx  Xx  Xx. Xx. Xx            :\n"
                           ":  XxXx  XxXx. .   Xx          :\n"
                           ":        . . . .               :\n");
}


void test_font_string_large() {
  // extra width = required padding
  Region screenBuf(0, 0, 15, 8);
  AsciiBuffer asciiBuf(screenBuf);

  Font::string_region_clip(screenBuf, "abc", 0, 0, eColorDark2, eColorBlank, Font::Size16);

  TEST_ASSERT_EQUAL_STRING(asciiBuf.stringVal(),
                           ":                              :\n"
                           ":                              :\n"
                           ":              XXXX            :\n"
                           ":              XXXX            :\n"
                           ":    XXXX      XXXXXXXX        :\n"
                           ":    XXXX      XXXXXXXX        :\n"
                           ":        XXXX  XXXX    XXXX    :\n"
                           ":        XXXX  XXXX    XXXX    :\n");

  screenBuf.fill(eColorBlank);
  Font::string_region_clip(screenBuf, "abc", 0, -4, eColorDark2, eColorBlank, Font::Size16);

  TEST_ASSERT_EQUAL_STRING(asciiBuf.stringVal(),
                           ":    XXXXXXXX  XXXX    XXXX    :\n"
                           ":    XXXXXXXX  XXXX    XXXX    :\n"
                           ":XXXX    XXXX  XXXX    XXXX    :\n"
                           ":XXXX    XXXX  XXXX    XXXX    :\n"
                           ":    XXXXXXXX  XXXXXXXX        :\n"
                           ":    XXXXXXXX  XXXXXXXX        :\n"
                           ":                              :\n"
                           ":                              :\n");
}



int main() {
  UNITY_BEGIN();
  RUN_TEST(test_font_string);
  RUN_TEST(test_font_string_clipped);
  RUN_TEST(test_font_string_with_offset);
  RUN_TEST(test_font_string_with_offset_negative);
  RUN_TEST(test_font_string_right_with_offset);
  RUN_TEST(test_font_string_special_chars);
  RUN_TEST(test_font_string_unprintable_chars);
  RUN_TEST(test_font_string_tab_clipped);
  RUN_TEST(test_font_string_tab_prevents_overlap);
  RUN_TEST(test_font_string_highlight);
  RUN_TEST(test_font_string_large);
  UNITY_END();
  return 0;
}

} // namespace TestFont
