#include "teletype_stubs.h"
#include "types.h"
#include <teletype_io.h>

class Scene; // Forward declarations
class Sequence;

using Espresso::Command;
using Espresso::ExecState;

void tele_run_script(Scene *ss, ExecState *es,
                                     script_number_t script_no) {}

process_result_t tele_process_command(Scene *ss, ExecState *es,
                                      const Command *cmd) {
  process_result_t result = {
      .has_value = false,
      .value = 0,
  };
  return result;
}

uint32_t tele_get_ticks(void) { return 123; }

void tele_metro_updated(void) {}

void tele_metro_reset(void) {}

bool tr_state[4] = {0};

void tele_tr(uint8_t i, int16_t v) { tr_state[i] = v; }

uint16_t cv_state[4] = {0};

std::vector<cv_event> cvEvents;

void tele_cv(uint8_t i, int16_t v, uint8_t s) {
  cv_state[i] = v;
  cv_event e = { eCvSetValue, i, v, !!s };
  cvEvents.push_back(e);
}

void tele_cv_slew(uint8_t i, int16_t v) {
  cv_event e = { eCvSetSlew, i, v };
  cvEvents.push_back(e);
}

void tele_env_config(uint8_t i, uint16_t attack, uint16_t decay, uint16_t sustain, uint16_t release) {}
void tele_env(uint8_t i, bool high) {}

void tele_update_adc(bool force) {}

void tele_has_stack(bool has_stack) {}

void tele_cv_off(uint8_t i, int16_t v) {}
void tele_scene(uint8_t sceneIndex, bool loadPattern) {}

void tele_vars_updated(void) {}

void tele_kill(void) {}
bool tele_get_input_state(uint8_t) { return 0xf; }

void tele_save_calibration(void) {}

void device_flip(void) {}

std::vector<midi_event> midiEvents;

void send_midi_note(uint8_t ch, uint8_t note, uint8_t vel) {
  midiEvents.push_back(
      {.type = eMidiTypeNote, .ch = ch, .data1 = note, .data2 = vel});
}

void send_midi_cc(uint8_t ch, uint8_t cc_num, uint8_t cc_val) {
  midiEvents.push_back(
      {.type = eMidiTypeCC, .ch = ch, .data1 = cc_num, .data2 = cc_val});
}

void clock_reset_voice(int i) {}
void clock_refresh_voice(int i) {}
void clock_refresh_seq(Sequence &seq) {}
