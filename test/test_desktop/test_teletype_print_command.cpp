#include <espresso/command.h>
#include <ops/op_enum.h>
#include <sequence.h>
#include <unity.h>

using Espresso::Command;
using Espresso::Token;

namespace TestVoice {
Token generate_seq_data_basic(Sequence::NoteName note, int duration,
                                    Sequence::DurationUnit unit);
}

namespace TestTeletypePrintCommand {

using TestVoice::generate_seq_data_basic;

void test_print_command_basic() {
  Command command = { 3, -1, false, {
    {.tag = Espresso::OP, .value = E_OP_SYM_PLUS },
    {.tag = Espresso::NUMBER, .value = 2},
    {.tag = Espresso::NUMBER, .value = 0},
  }};

  char out[32];
  command.print(out);

  TEST_ASSERT_EQUAL_STRING(out, "+ 2 0");
}

void test_print_command_seq_alda_basic() {
  Command command = { 7, 2, false, {
    {.tag = Espresso::MOD, .value = E_MOD_SEQ},
    {.tag = Espresso::NUMBER, .value = 3},
    {.tag = Espresso::PRE_SEP, .value = 0},
    generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel),
    generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel),
    generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel),
    generate_seq_data_basic(Sequence::NoteD, 0, Sequence::DurRel),
  }};

  char out[32];
  command.print(out);

  TEST_ASSERT_EQUAL_STRING(out, "N 3: A B C D");
}

void test_print_command_seq_alda_parens() {
  Command command = { 5, -1, false,
          {
              {.tag = Espresso::MOD, .value = E_MOD_SEQ},
              {.tag = Espresso::PARENS, .value = 3},
              generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel),
              generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel),
              generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel),
          },
  };

  char out[32];
  command.print(out);

  TEST_ASSERT_EQUAL_STRING(out, "N(A B C)");
}

void test_print_command_seq_alda_parens_adv() {
  Command command = { 12, 2, false,
          {
              {.tag = Espresso::MOD, .value = E_MOD_IF},
              {.tag = Espresso::OP, .value = E_OP_X},
              {.tag = Espresso::PRE_SEP, .value = 0},
              {.tag = Espresso::MOD, .value = E_MOD_SEQ},
              {.tag = Espresso::NUMBER, .value = 5},
              {.tag = Espresso::PARENS, .value = 3},
              generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel),
              generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel),
              generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel),
              {.tag = Espresso::SUB_SEP, .value = 0},
              {.tag = Espresso::OP, .value = E_OP_X},
              {.tag = Espresso::NUMBER, .value = 7},
          },
  };

  char out[32];
  command.print(out);

  TEST_ASSERT_EQUAL_STRING(out, "IF X: N 5(A B C); X 7");
}

void test_print_command_seq_alda_sharp_flat() {
  Command command = { 7, 2, false,
          {
              {.tag = Espresso::MOD, .value = E_MOD_SEQ},
              {.tag = Espresso::NUMBER, .value = 3},
              {.tag = Espresso::PRE_SEP, .value = 0},
              // sharps are actually encoded as flats
              generate_seq_data_basic(Sequence::NoteBb, 0, Sequence::DurRel),
              generate_seq_data_basic(Sequence::NoteBb, 0, Sequence::DurRel),
              generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel),
              generate_seq_data_basic(Sequence::NoteEb, 0, Sequence::DurRel),
          },
  };

  char out[32];
  command.print(out);

  TEST_ASSERT_EQUAL_STRING(out, "N 3: B- B- B E-");
}

void test_print_command_seq_alda_chords() {
  auto data1 = generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel);
  Sequence::packNoteName(data1.data[1], Sequence::NoteBb);

  auto data2 = generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel);
  Sequence::packNoteName(data2.data[1], Sequence::NoteE);
  Sequence::packNoteName(data2.data[2], Sequence::NoteG);

  auto data3 = generate_seq_data_basic(Sequence::NoteD, 0, Sequence::DurRel);
  Sequence::packNoteName(data3.data[1], Sequence::NoteEb);
  Sequence::packNoteName(data3.data[2], Sequence::NoteF);
  Sequence::packNoteName(data3.data[3], Sequence::NoteAb);

  Command command = { 7, 2, false,
          {
              {.tag = Espresso::MOD, .value = E_MOD_SEQ},
              {.tag = Espresso::NUMBER, .value = 3},
              {.tag = Espresso::PRE_SEP, .value = 0},
              data1,
              generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel),
              data2,
              data3,
          },
  };

  char out[32];
  command.print(out);

  TEST_ASSERT_EQUAL_STRING(out, "N 3: AB- B CEG DE-FA-");
}

void test_print_command_seq_alda_octave_up_down() {
  auto data1 = generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel);
  Sequence::packNoteOctave(data1.data[0], 1);

  auto data2 = generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel);
  Sequence::packNoteOctave(data2.data[0], 2);

  auto data3 = generate_seq_data_basic(Sequence::NoteD, 0, Sequence::DurRel);
  Sequence::packNoteOctave(data3.data[0], -1);

  Command command = { 7, 2, false,
          {
              {.tag = Espresso::MOD, .value = E_MOD_SEQ},
              {.tag = Espresso::NUMBER, .value = 3},
              {.tag = Espresso::PRE_SEP, .value = 0},
              data1,
              generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel),
              data2,
              data3,
          },
  };

  char out[32];
  command.print(out);

  TEST_ASSERT_EQUAL_STRING(out, "N 3: >A B >>C <D");
}

void test_print_command_seq_alda_duration_rel() {
  auto data1 = generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel);
  Sequence::packNoteDuration(data1.value, 4, Sequence::DurRel);

  auto data2 = generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel);
  Sequence::packNoteDuration(data2.value, 8, Sequence::DurRel);

  auto data3 = generate_seq_data_basic(Sequence::NoteD, 0, Sequence::DurRel);
  Sequence::packNoteDuration(data3.value, 2, Sequence::DurRel);

  Command command = { 7, 2, false, {
    {.tag = Espresso::MOD, .value = E_MOD_SEQ},
    {.tag = Espresso::NUMBER, .value = 3},
    {.tag = Espresso::PRE_SEP, .value = 0},
    data1,
    generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel),
    data2,
    data3,
  }};

  char out[32];
  command.print(out);

  TEST_ASSERT_EQUAL_STRING(out, "N 3: A4 B C8 D2");
}

void test_print_command_seq_alda_duration_abs() {
  auto data1 = generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel);
  Sequence::packNoteDuration(data1.value, 800, Sequence::DurMs);

  auto data2 = generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel);
  Sequence::packNoteDuration(data2.value, 48, Sequence::DurMs);

  auto data3 = generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel);
  Sequence::packNoteDuration(data3.value, 1, Sequence::DurSec);

  auto data4 = generate_seq_data_basic(Sequence::NoteD, 0, Sequence::DurRel);
  Sequence::packNoteDuration(data4.value, 255, Sequence::DurSec);

  Command command = { 7, 2, false, {
    {.tag = Espresso::MOD, .value = E_MOD_SEQ},
    {.tag = Espresso::NUMBER, .value = 3},
    {.tag = Espresso::PRE_SEP, .value = 0},
    data1,
    data2,
    data3,
    data4,
  }};

  char out[32];
  command.print(out);

  TEST_ASSERT_EQUAL_STRING(out, "N 3: A800MS B48MS C1S D255S");
}

void test_print_command_seq_alda_duration_dots() {
  auto data1 = generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel);
  Sequence::packNoteDots(data1.value, 1);

  auto data2 = generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel);
  Sequence::packNoteDots(data2.value, 2);

  auto data3 = generate_seq_data_basic(Sequence::NoteD, 0, Sequence::DurRel);
  Sequence::packNoteDots(data3.value, 3);

  Command command = { 7, 2, false, {
    {.tag = Espresso::MOD, .value = E_MOD_SEQ},
    {.tag = Espresso::NUMBER, .value = 3},
    {.tag = Espresso::PRE_SEP, .value = 0},
    data1,
    generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel),
    data2,
    data3,
  }};

  char out[32];
  command.print(out);

  TEST_ASSERT_EQUAL_STRING(out, "N 3: A. B C.. D...");
}

void test_print_command_seq_alda_rests() {
  auto data2 = generate_seq_data_basic(Sequence::NoteRest, 0, Sequence::DurRel);
  Sequence::packNoteDots(data2.value, 2);

  Command command = { 7, 2, false, {
    {.tag = Espresso::MOD, .value = E_MOD_SEQ},
    {.tag = Espresso::NUMBER, .value = 3},
    {.tag = Espresso::PRE_SEP, .value = 0},
    generate_seq_data_basic(Sequence::NoteRest, 0, Sequence::DurRel),
    generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel),
    data2,
    generate_seq_data_basic(Sequence::NoteRest, 8, Sequence::DurRel)
  }};

  char out[32];
  command.print(out);

  TEST_ASSERT_EQUAL_STRING(out, "N 3: R A R.. R8");
}

void test_print_command_seq_alda_rests2() {
  Command command = { 3, -1, false, {
    {.tag = Espresso::MOD, .value = E_MOD_SEQ},
    {.tag = Espresso::PARENS, .value = 1},
    generate_seq_data_basic(Sequence::NoteRest, 0, Sequence::DurRel),
  }};

  char out[32];
  command.print(out);

  TEST_ASSERT_EQUAL_STRING(out, "N(R)");
}

void test_print_command_seq_alda_repeats() {
  auto data1 = generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel);
  Sequence::packNoteRepeats(data1.value, 2);

  auto data2 = generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel);
  Sequence::packNoteName(data2.data[1], Sequence::NoteDb);
  Sequence::packNoteRepeats(data2.value, 3);

  auto data3 = generate_seq_data_basic(Sequence::NoteD, 0, Sequence::DurRel);
  Sequence::packNoteRepeats(data3.value, 8);

  Command command = { 7, 2, false, {
    {.tag = Espresso::MOD, .value = E_MOD_SEQ},
    {.tag = Espresso::NUMBER, .value = 3},
    {.tag = Espresso::PRE_SEP, .value = 0},
    data1,
    generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel),
    data2,
    data3,
  }};

  char out[32];
  command.print(out);

  TEST_ASSERT_EQUAL_STRING(out, "N 3: A*2 B CD-*3 D*8");
}

void test_print_command_seq_alda_legato() {
  auto data1 = generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel);
  Sequence::packNoteLegato(data1.value, true);

  auto data2 = generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel);
  Sequence::packNoteName(data2.data[1], Sequence::NoteD);
  Sequence::packNoteLegato(data2.value, true);

  auto data3 = generate_seq_data_basic(Sequence::NoteDb, 8, Sequence::DurRel);
  Sequence::packNoteLegato(data3.value, true);

  Command command = { 7, 2, false, {
    {.tag = Espresso::MOD, .value = E_MOD_SEQ},
    {.tag = Espresso::NUMBER, .value = 3},
    {.tag = Espresso::PRE_SEP, .value = 0},
    data1,
    generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel),
    data2,
    data3,
  }};

  char out[32];
  command.print(out);

  TEST_ASSERT_EQUAL_STRING(out, "N 3: A~ B CD~ D-8~");
}

void test_print_command_seq_alda_max_length() {
  Command command = { 15, 2, false, {
    {.tag = Espresso::MOD, .value = E_MOD_SEQ},
    {.tag = Espresso::NUMBER, .value = 3},
    {.tag = Espresso::PRE_SEP, .value = 0},
    generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel),
    generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel),
    generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel),
    generate_seq_data_basic(Sequence::NoteD, 0, Sequence::DurRel),
    generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel),
    generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel),
    generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel),
    generate_seq_data_basic(Sequence::NoteD, 0, Sequence::DurRel),
    generate_seq_data_basic(Sequence::NoteA, 0, Sequence::DurRel),
    generate_seq_data_basic(Sequence::NoteB, 0, Sequence::DurRel),
    generate_seq_data_basic(Sequence::NoteC, 0, Sequence::DurRel),
    generate_seq_data_basic(Sequence::NoteD, 0, Sequence::DurRel),
  }};

  char out[32];
  command.print(out);

  TEST_ASSERT_EQUAL_STRING(out, "N 3: A B C D A B C D A B C D");
}

void test_print_command_series_op() {
  Command command = { 5, -1, false, {
    {.tag = Espresso::OP, .value = E_OP_SEQ_STUT},
    {.tag = Espresso::BRACKETS, .value = 3},
    {.tag = Espresso::NUMBER, .value = 2},
    {.tag = Espresso::NUMBER, .value = 0},
    {.tag = Espresso::NUMBER, .value = 1},
  }};

  char out[32];
  command.print(out);

  TEST_ASSERT_EQUAL_STRING(out, "STUT [2 0 1]");
}

void test_print_command_series_mod() {
  Command command = { 8, 6, false, {
    {.tag = Espresso::MOD, .value = E_MOD_IF},
    {.tag = Espresso::BRACKETS, .value = 4},
    {.tag = Espresso::NUMBER, .value = 0},
    {.tag = Espresso::NUMBER, .value = 0},
    {.tag = Espresso::NUMBER, .value = 1},
    {.tag = Espresso::NUMBER, .value = 0},
    {.tag = Espresso::PRE_SEP, .value = 0},
    {.tag = Espresso::OP, .value = E_OP_X},
  }};

  char out[32];
  command.print(out);

  TEST_ASSERT_EQUAL_STRING(out, "IF [0 0 1 0]: X");
}

int main() {
  UNITY_BEGIN();
  RUN_TEST(test_print_command_basic);
  RUN_TEST(test_print_command_seq_alda_basic);
  RUN_TEST(test_print_command_seq_alda_parens);
  RUN_TEST(test_print_command_seq_alda_parens_adv);
  RUN_TEST(test_print_command_seq_alda_sharp_flat);
  RUN_TEST(test_print_command_seq_alda_chords);
  RUN_TEST(test_print_command_seq_alda_octave_up_down);
  RUN_TEST(test_print_command_seq_alda_duration_rel);
  RUN_TEST(test_print_command_seq_alda_duration_abs);
  RUN_TEST(test_print_command_seq_alda_duration_dots);
  RUN_TEST(test_print_command_seq_alda_rests);
  RUN_TEST(test_print_command_seq_alda_rests2);
  RUN_TEST(test_print_command_seq_alda_repeats);
  RUN_TEST(test_print_command_seq_alda_legato);
  RUN_TEST(test_print_command_seq_alda_max_length);
  RUN_TEST(test_print_command_series_op);
  RUN_TEST(test_print_command_series_mod);
  UNITY_END();
  return 0;
}

} // namespace TestTeletypePrintCommand
