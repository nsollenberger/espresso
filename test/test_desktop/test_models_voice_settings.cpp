#include <unity.h>

#include <hid_keys.h>
#include <models/mode_manager.h>
#include <models/voice_settings.h>

#include "mock_teleteype.h"

using Models::VoiceSettings;

namespace TestModelsVoiceSettings {

void test_defaults() {
  VoiceSettings model;

  TEST_ASSERT_EQUAL(model.script, 0);
  TEST_ASSERT_EQUAL(model.selected_index, 0);
  TEST_ASSERT_EQUAL(model.back_to_top, true);
  TEST_ASSERT_EQUAL(model.is_editing, false);
}

void test_change_selection() {
  VoiceSettings model;

  model.processKeypress(HID_DOWN, HID_MODIFIER_NONE, false);
  TEST_ASSERT_EQUAL(model.selected_index, 1);

  model.processKeypress(HID_DOWN, HID_MODIFIER_NONE, false);
  TEST_ASSERT_EQUAL(model.selected_index, 2);

  model.processKeypress(HID_DOWN, HID_MODIFIER_NONE, false);
  model.processKeypress(HID_DOWN, HID_MODIFIER_NONE, false);
  model.processKeypress(HID_DOWN, HID_MODIFIER_NONE, false);
  TEST_ASSERT_EQUAL(model.selected_index, 5);
  model.processKeypress(HID_DOWN, HID_MODIFIER_NONE, false);
  model.processKeypress(HID_DOWN, HID_MODIFIER_NONE, false);
  TEST_ASSERT_EQUAL(model.selected_index, 5);

  model.processKeypress(HID_UP, HID_MODIFIER_NONE, false);
  model.processKeypress(HID_UP, HID_MODIFIER_NONE, false);
  TEST_ASSERT_EQUAL(model.selected_index, 3);

  model.processKeypress(HID_UP, HID_MODIFIER_NONE, false);
  model.processKeypress(HID_UP, HID_MODIFIER_NONE, false);
  TEST_ASSERT_EQUAL(model.selected_index, 1);

  model.processKeypress(HID_UP, HID_MODIFIER_NONE, false);
  model.processKeypress(HID_UP, HID_MODIFIER_NONE, false);
  TEST_ASSERT_EQUAL(model.selected_index, 0);
}

void test_back_to_top() {
  VoiceSettings model;

  TEST_ASSERT_EQUAL(model.back_to_top, true);

  model.processKeypress(HID_DOWN, HID_MODIFIER_NONE, false);
  TEST_ASSERT_EQUAL(model.back_to_top, false);

  model.processKeypress(HID_DOWN, HID_MODIFIER_NONE, false);
  model.processKeypress(HID_DOWN, HID_MODIFIER_NONE, false);
  model.processKeypress(HID_DOWN, HID_MODIFIER_NONE, false);
  model.processKeypress(HID_DOWN, HID_MODIFIER_NONE, false);
  model.processKeypress(HID_DOWN, HID_MODIFIER_NONE, false);
  TEST_ASSERT_EQUAL(model.selected_index, 5);
  TEST_ASSERT_EQUAL(model.back_to_top, false);

  model.processKeypress(HID_UP, HID_MODIFIER_NONE, false);
  TEST_ASSERT_EQUAL(model.back_to_top, false);
  model.processKeypress(HID_UP, HID_MODIFIER_NONE, false);
  model.processKeypress(HID_UP, HID_MODIFIER_NONE, false);
  model.processKeypress(HID_UP, HID_MODIFIER_NONE, false);
  model.processKeypress(HID_UP, HID_MODIFIER_NONE, false);
  TEST_ASSERT_EQUAL(model.selected_index, 0);
  TEST_ASSERT_EQUAL(model.back_to_top, false);

  model.processKeypress(HID_UP, HID_MODIFIER_NONE, false);
  TEST_ASSERT_EQUAL(model.selected_index, 0);
  TEST_ASSERT_EQUAL(model.back_to_top, true);

  model.processKeypress(HID_DOWN, HID_MODIFIER_NONE, false);
  TEST_ASSERT_EQUAL(model.selected_index, 1);
  TEST_ASSERT_EQUAL(model.back_to_top, false);

  model.processKeypress(HID_UP, HID_MODIFIER_NONE, false);
  TEST_ASSERT_EQUAL(model.selected_index, 0);
  TEST_ASSERT_EQUAL(model.back_to_top, false);

  model.processKeypress(HID_UP, HID_MODIFIER_NONE, false);
  TEST_ASSERT_EQUAL(model.selected_index, 0);
  TEST_ASSERT_EQUAL(model.back_to_top, true);
}

void test_edit_value_arrows() {
  VoiceSettings model;

  TEST_ASSERT_EQUAL(model.is_editing, false);
  TEST_ASSERT_EQUAL(model.configs[0].cv_out, 1);

  model.processKeypress(HID_ENTER, HID_MODIFIER_NONE, false);
  TEST_ASSERT_EQUAL(model.is_editing, true);
  TEST_ASSERT_EQUAL(model.configs[0].cv_out, 1);

  model.processKeypress(HID_UP, HID_MODIFIER_NONE, false);
  TEST_ASSERT_EQUAL(model.is_editing, true);
  TEST_ASSERT_EQUAL(model.selected_index, 0);
  TEST_ASSERT_EQUAL(model.configs[0].cv_out, 2);

  model.processKeypress(HID_UP, HID_MODIFIER_NONE, false);
  model.processKeypress(HID_UP, HID_MODIFIER_NONE, false);
  TEST_ASSERT_EQUAL(model.configs[0].cv_out, 4);

  model.processKeypress(HID_DOWN, HID_MODIFIER_NONE, false);
  TEST_ASSERT_EQUAL(model.configs[0].cv_out, 3);

  model.processKeypress(HID_ENTER, HID_MODIFIER_NONE, false);
  TEST_ASSERT_EQUAL(model.is_editing, false);
  TEST_ASSERT_EQUAL(model.configs[0].cv_out, 3);

  model.processKeypress(HID_DOWN, HID_MODIFIER_NONE, false);
  TEST_ASSERT_EQUAL(model.is_editing, false);
  TEST_ASSERT_EQUAL(model.selected_index, 1);
  TEST_ASSERT_EQUAL(model.configs[0].cv_out, 3);
}

int main() {
  UNITY_BEGIN();
  RUN_TEST(test_defaults);
  RUN_TEST(test_change_selection);
  RUN_TEST(test_back_to_top);
  RUN_TEST(test_edit_value_arrows);
  UNITY_END();
  return 0;
}

}
