#ifndef TIMER_CALLBACKS_H__
#define TIMER_CALLBACKS_H__

#include <timers.h>

extern Timers::softTimer_t sleepTimer;
extern Timers::softTimer_t refreshTimer;
extern Timers::softTimer_t keyTimer;
extern Timers::softTimer_t cvTimer;
extern Timers::softTimer_t adcTimer;
extern Timers::softTimer_t clockTimer;

bool cvTimer_callback(void *o);
bool sleepTimer_callback(void *o);
bool refreshTimer_callback(void *o);
bool keyTimer_callback(void *o);
bool adcTimer_callback(void *o);
bool clockTimer_callback(void *o);

#endif
