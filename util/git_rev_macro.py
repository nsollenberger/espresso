import subprocess

revision = (
    subprocess.check_output(["git", "describe", "--always", "--dirty"])
    .strip()
    .upper()
    .decode("utf-8")
)
print("-DGIT_REV='\"%s\"'" % revision)