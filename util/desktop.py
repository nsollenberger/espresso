Import("env")

env.Append(CXXFLAGS=["-std=c++11"])

# Used by RtMidi (same for __MACOSX_CORE__)
if "-DSIMULATOR" in env["BUILD_FLAGS"]:
	env.Append(LINKFLAGS=[
		"-framework", "CoreMIDI",
		"-framework", "CoreAudio",
		"-framework", "CoreFoundation"
	])