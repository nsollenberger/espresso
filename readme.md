# Espresso

Revision 3, targeting Teensy4.

## What is it?

A work in progress :)

## Unit Tests

Run tests with:

    pio test -e native

Debug tests (on macOS) with:

    pio debug -e native
    lldb .pio/build/native/program

## Simulator

Run simulator on desktop/native with:

    pio run -e sim
    ./.pio/build/sim/program

(Does this still work? Need to port views to native...)

## clangd

Useful for code hints/linting. Generate compile_commands.json with:

    pio run -e teensy -t compiledb

## Issues

Dependencies of local libs are not automatically downloaded:

https://github.com/platformio/platformio-core/issues/2910

Workaround: manually download/install these dependencies using `pio lib install <URL>`,
and they will be discoverable by the local lib.


- Does not re-render view after SCENE OP changes scene

Isochronic tones:

N: C1S
Fast 10

(How can we adjust tone frequency smoothly? "Fast" accepts integer only...)

Seed Beads (in delay mode) with quarter or eigth notes, Time at 0, Density at 50%, +-5V to Density CV

- [x] Allow empty lines
- [X] MOD(ulo) MOD! like EV(ery). MOD 3 8: play first 3 of 8. MOD 8 3: play last 3 of 8

- [~] Voice Config view

- [~] Series-patterned mods, using brackets: OFFS [4 7 2].
  One value is used each loop, automatically advancing each time thru


- [] SWING n modifier: shift n eighth-notes from second half of sequence to first half of sequence (stolen/distributed in a round-robbin fashion, where there is more than 1 note in a "half")
- [] TUR(ing) modifier: given some seed value, an iteration index, and a mutation probability, mutate the sequence



